#ifndef QCUSTOMDRAGDROPTREEWIDGET_H
#define QCUSTOMDRAGDROPTREEWIDGET_H

#include <QAbstractItemModel>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QTreeWidget>

class QCustomDragDropTreeWidget : public QTreeWidget
{
    Q_OBJECT

public:
    explicit QCustomDragDropTreeWidget(QWidget *parent = 0);

signals:
//    void dropEventSignal(QTreeWidgetItem*);
//    void dragEnterEventSignal(QTreeWidgetItem*);
//    void dragMoveEventSignal(QTreeWidgetItem*);
//    void dragLeaveEventSignal(QTreeWidgetItem*);

protected:
//    void dropEvent(QDropEvent *event);
//    void dragEnterEvent(QDragEnterEvent *event);
//    void dragMoveEvent(QDragMoveEvent *event);
//    void dragLeaveEvent(QDragLeaveEvent *event);

private:
    QWidget* _parent;
};

#endif // QCUSTOMDRAGDROPTREEWIDGET_H
