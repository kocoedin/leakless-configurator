#ifndef WRITINGTHREAD_H
#define WRITINGTHREAD_H

#include <QWidget>
#include <QThread>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QCheckBox>
#include <QtGui>
#include <QLineEdit>
#include <QSpinBox>
#include <QList>
#include <QDoubleSpinBox>
#include <QFileSystemModel>
#include "plottingwidget.h"
#include "dialogwait.h"
#include "detector.h"

class WritingThread : public QThread
{
    Q_OBJECT
    void run();
public:
    void SetMemory (quint32 MemorySize, QList<quint32> MemoryArray);

public slots:
    void StopRequest ();
    void RecieveData (QString Data);
    void TimerTimeout ();


signals:
    void SendData(QString Data);
    void resultReady();
    void progressChanged(int progress);

private:
    QList<quint32> m_memoryArray;
    quint32 m_memorySize;
    quint32 m_currentAddress;
    quint32 m_currentMemory;
    bool m_timeoutFlag;
    bool m_waitingForReply;
    bool m_stopRequest;
};

#endif // WRITINGTHREAD_H
