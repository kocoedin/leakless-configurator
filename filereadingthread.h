#ifndef FILEREADINGTHREAD_H
#define FILEREADINGTHREAD_H

#include <QWidget>
#include <QThread>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QCheckBox>
#include <QtGui>
#include <QLineEdit>
#include <QSpinBox>
#include <QList>
#include <QDoubleSpinBox>
#include <QFileSystemModel>
#include "plottingwidget.h"
#include "dialogwait.h"
#include "detector.h"


class FileReadingThread : public QThread
{
    Q_OBJECT
    void run();
public:
    void SetData (QString filePath,quint32 MemoryStart,quint32 MemorySize);

public slots:
    void StopRequest ();

signals:
    void resultReady(QList<quint32> AddressArray,QList<quint32> DataArray);
    void progressChanged(int progress);

private:
    QString m_filePath;
    quint32 m_currentAddress;
    quint32 m_memoryStart;
    quint32 m_memorySize;
    bool m_stopRequest;
};

#endif // FILEREADINGTHREAD_H
