#include "treefolderview.h"
#include "ui_treefolderview.h"

#include <QDebug>

TreeFolderView::TreeFolderView(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::TreeFolderView)
{
    ui->setupUi(this);


    QSettings Setting("SMTech","LeakLessConfigurator");
    Setting.beginGroup("General");
    initialPath = Setting.value("ProjectPath").toString();
    Setting.endGroup();

    if(initialPath==""){
        initialPath = "C:/QtLeakLess";
    }

    //itemCopy = new QTreeWidgetItem();
    //itemPaste = new QTreeWidgetItem();
    itemCopy = NULL;

    ui->lblRoot->setText("Root");
    ui->treeWidget->setHeaderLabel("LeakLess Data Manager");
    ui->txtFileInfo->setReadOnly(true);

    ui->treeWidget->setSelectionMode(QAbstractItemView::SingleSelection);
    ui->treeWidget->setDragDropMode(QAbstractItemView::InternalMove);
    ui->treeWidget->setDefaultDropAction(Qt::MoveAction); // Qt::CoppyAction
    ui->treeWidget->setDropIndicatorShown(true);

    ui->treeWidget->setAcceptDrops(true);
    ui->treeWidget->setDragEnabled(true);
    //setAcceptDrops(true);

    QTreeWidgetItem *itm = ui->treeWidget->invisibleRootItem();
    itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);

    connect(ui->treeWidget, SIGNAL(dropEventSignal(QTreeWidgetItem*)),      this, SLOT(slotOnDropEvent(QTreeWidgetItem*)));
    connect(ui->treeWidget, SIGNAL(dragMoveEventSignal(QTreeWidgetItem*)),  this, SLOT(slotOnDragMoveEvent(QTreeWidgetItem*)));
    connect(ui->treeWidget, SIGNAL(dragEnterEventSignal(QTreeWidgetItem*)), this, SLOT(slotOnDragEnterEvent(QTreeWidgetItem*)));
    connect(ui->treeWidget, SIGNAL(dragLeaveEventSignal(QTreeWidgetItem*)), this, SLOT(slotOnDragLeaveEvent(QTreeWidgetItem*)));

    treeInitialSetup();
}

TreeFolderView::~TreeFolderView()
{
    delete ui;
}

void TreeFolderView::treeInitialSetup()
{
    // initialize the treefolderview based on an INI file

    ui->treeWidget->clear();
    ui->editLinePath->setText(initialPath);

    QDir dirProj(initialPath);
    dirProj.setFilter(QDir::Dirs);
    dirProj.setSorting(QDir::Size);
    QStringList filtersProj;
    filtersProj << "Proj_*";
    dirProj.setNameFilters(filtersProj);
    QFileInfoList listProj = dirProj.entryInfoList();
    for (int i = 0; i < listProj.size(); ++i) {
        QFileInfo fileInfoProj = listProj.at(i);
        QString nameProj = fileInfoProj.fileName();
        nameProj.replace(0, 5, "");

        QString fullPath = initialPath + "/" + fileInfoProj.fileName();
        AddProject(nameProj,fullPath);
    }

    ui->treeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
}

void TreeFolderView::treeRefresh(){
    treeInitialSetup();
}


void TreeFolderView::AddProject(QString name, QString pathToItem){
    QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidget,ItemProject);
    QIcon icon(":/icons/win8/PNG/System/edit_property/edit_property-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    itm->setExpanded(true);

    pathToItem.append("/");
    QDir dir(pathToItem);
    dir.setFilter(QDir::Dirs);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "Loc_*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        QString fileName = fileInfo.fileName();
        fileName.replace(0, 4, "");

        QString fullPath = pathToItem + "/" + fileInfo.fileName();
        AddLocation(itm,fileName,fullPath);
    }
}

void TreeFolderView::AddLocation(QTreeWidgetItem *project, QString name, QString pathToItem){
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemLocation);
    QIcon icon(":/icons/win8/PNG/Maps_and_Geolocation/point_objects/point_objects-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable);
    project->addChild(itm);
    itm->setExpanded(true);

    pathToItem.append("/");
    QDir dir(pathToItem);
    qDebug() << pathToItem;
    dir.setFilter(QDir::Dirs);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "Dev_*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        QString fileName = fileInfo.fileName();
        fileName.replace(0, 4, "");

        QString fullPath = pathToItem + "/" + fileInfo.fileName();
        AddDevice(itm,fileName,fullPath);
    }
}

void TreeFolderView::AddDevice(QTreeWidgetItem *location, QString name, QString pathToItem){
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemDevice);
    QIcon icon(":/icons/win8/PNG/Industry/electronics/electronics-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    location->addChild(itm);
    itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDropEnabled);
    itm->setExpanded(true);

    pathToItem.append("/");
    QDir dir(pathToItem);
    qDebug() << pathToItem;
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "*.log*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        AddMeasurementFile(itm,fileInfo.fileName());
    }
}

void TreeFolderView::AddMeasurementFile(QTreeWidgetItem *device, QString name)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemMeasurement);
    itm->setText(0,name);
    itm->setSizeHint(0,QSize(20,20));
    itm->setFlags(Qt::ItemIsEnabled | Qt::ItemIsSelectable | Qt::ItemIsDragEnabled);
    device->addChild(itm);


}


void TreeFolderView::on_btnChoose_clicked()
{
    QFileDialog dialog(this);
    dialog.setFileMode(QFileDialog::DirectoryOnly);
    QString fileName = QFileDialog::getExistingDirectory(this, tr("Open Directory"),"/home",
                        QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks);
    qDebug() << fileName;

    if(fileName.isNull()){
        return;
        qDebug() << "NULL";
    }else if(QString::compare(fileName,ui->editLinePath->text())){
        ui->editLinePath->setText(fileName);
        initialPath = fileName;

        QSettings Setting("SMTech","LeakLessConfigurator");
        Setting.beginGroup("General");
        Setting.setValue("ProjectPath",initialPath);
        Setting.endGroup();

        treeInitialSetup();
        qDebug() << fileName;

    }else qDebug() << "EQUAL";

}

void TreeFolderView::on_treeWidget_customContextMenuRequested(const QPoint &pos)
{
    QPoint globalPos = ui->treeWidget->mapToGlobal(pos);
    QTreeWidgetItem *item = ui->treeWidget->itemAt(pos);
    if (item)
    {
        showContextMenu(item, globalPos);
    }
    else
    {
        QMenu menu(tr("Context menu"), this);

        // New project
        QAction actionAddNewProject("New project",this);
        connect(&actionAddNewProject, SIGNAL(triggered()), this, SLOT(addNewProject()));
        menu.addAction(&actionAddNewProject);
        menu.exec(globalPos);
    }

    qDebug() << "IN on_treeWidget_customContextMenuRequested";
}

void TreeFolderView::showContextMenu(QTreeWidgetItem *item, const QPoint& globalPos) {
    QMenu menu(tr("Context menu"), this);
    switch (item->type()) {
    case -1:
    {
        //menu.addAction("This is a type -1");

        // New project
        QAction actionAddNewProject("New project",this);
        connect(&actionAddNewProject, SIGNAL(triggered()), this, SLOT(addNewProject()));
        menu.addAction(&actionAddNewProject);

        menu.exec(globalPos);

    }
    case ItemProject:
    {
        //menu.addAction("This is a type 1");

        // Add new location
        QAction actionAddNewLocation("Add location",this);
        menu.addAction(&actionAddNewLocation);
        connect(&actionAddNewLocation, SIGNAL(triggered()), this, SLOT(addNewLocation()));

        // Edit project
        QAction actionEditProject("Edit project", this);
        connect(&actionEditProject, SIGNAL(triggered()), this, SLOT(editProject()));
        menu.addAction(&actionEditProject);

        // Delete project
        QAction actionDeleteProject("Delete project", this);
        connect(&actionDeleteProject, SIGNAL(triggered()), this, SLOT(deleteProject()));
        menu.addAction(&actionDeleteProject);

        menu.exec(globalPos);

        break;
    }
    case ItemLocation:
    {
        //menu.addAction("This is a type 2");

        // Add new device
        QAction actionAddNewDevice("Add device", this);
        connect(&actionAddNewDevice, SIGNAL(triggered()), this, SLOT(addNewDevice()));
        menu.addAction(&actionAddNewDevice);

        // Edit location
        QAction actionEditLocation("Edit location", this);
        connect(&actionEditLocation, SIGNAL(triggered()), this, SLOT(editLocation()));
        menu.addAction(&actionEditLocation);

        // Delete location
        QAction actionDeleteLocation("Delete location", this);
        connect(&actionDeleteLocation, SIGNAL(triggered()), this, SLOT(deleteLocation()));
        menu.addAction(&actionDeleteLocation);

        menu.exec(globalPos);

        break;
    }
    case ItemDevice:
    {
        //menu.addAction("This is a type 3");

        // Edit device
        QAction actionEditDevice("Edit device", this);
        connect(&actionEditDevice, SIGNAL(triggered()), this, SLOT(editDevice()));
        menu.addAction(&actionEditDevice);

        // Delete location
        QAction actionDeleteDevice("Delete device", this);
        connect(&actionDeleteDevice, SIGNAL(triggered()), this, SLOT(deleteDevice()));
        menu.addAction(&actionDeleteDevice);

        if (itemCopy!=NULL)
        {
            // Paste measurement log
            QAction actionPasteLog("Paste measurement log", this);
            connect(&actionPasteLog, SIGNAL(triggered()), this, SLOT(pasteLog()));
            menu.addAction(&actionPasteLog);
            menu.exec(globalPos);
        }else{
            menu.exec(globalPos);
        }

        break;
    }
    case ItemMeasurement:
    {
        //menu.addAction("This is a type 3");

        // Plot data action
        QAction actionPlotData("Plot data", this);
        connect(&actionPlotData, SIGNAL(triggered()), this, SLOT(plotData()));
        menu.addAction(&actionPlotData);

        // Copy measurement log
        QAction actionCopyLog("Copy measurement log", this);
        connect(&actionCopyLog, SIGNAL(triggered()), this, SLOT(copyLog()));
        menu.addAction(&actionCopyLog);

        // Delete measurement log
        QAction actionDeleteLog("Delete measurement log", this);
        connect(&actionDeleteLog, SIGNAL(triggered()), this, SLOT(deleteLog()));
        menu.addAction(&actionDeleteLog);

        menu.exec(globalPos);

        break;
    }
    }
}

void TreeFolderView::addNewProject(){
    qDebug() << "addNewProject";
    // open a new project dialog
    // editLine za name
    // editText za comment
    // propose to save or discard
    // make ini file if saved
    QTreeWidget *_treeWidget = ui->treeWidget;

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemProject;
    newPlainDialog->actionType = ItemNew;
    newPlainDialog->setWindowTitle("New project");
    newPlainDialog->_lblName = "Project name";
    newPlainDialog->_lblComment = "Comment";
    QString path;
    path = initialPath;
    newPlainDialog->fullPath = path;
    newPlainDialog->dialogTreeWidget = _treeWidget;
    newPlainDialog->refreshDialog();

    newPlainDialog->show();
    connect(newPlainDialog,SIGNAL(dialogClosedOk()),this,SLOT(dialogClosedRefresh()));
}

void TreeFolderView::addNewLocation(){
    qDebug() << "addNewLocation";
    // open a new location dialog
    // editLine za name
    // editText za comment
    // propose to save or discard
    // make ini file if saved

    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemLocation;
    newPlainDialog->actionType = ItemNew;
    newPlainDialog->setWindowTitle("New location");
    newPlainDialog->_lblName = "Location name";
    newPlainDialog->_lblComment = "Comment";
    QString path;
    qDebug() << "toSetPath";
    path = initialPath + "/Proj_" + item->text(0) + "/";
    newPlainDialog->fullPath = path;
    qDebug() << "toRefreshDialog";
    newPlainDialog->item = item;
    newPlainDialog->refreshDialog();

    qDebug() << "toShowDialog";
    newPlainDialog->show();
    connect(newPlainDialog,SIGNAL(dialogClosedOk()),this,SLOT(dialogClosedRefresh()));
}

void TreeFolderView::editProject(){
    // check for ini file
    // if no available ini files, make new
    // open a new project dialog
    // propose to save or discard
    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemProject;
    newPlainDialog->actionType = ItemEdit;
    newPlainDialog->setWindowTitle("Edit Project");
    newPlainDialog->_lblName = "Project name";
    newPlainDialog->_lblComment = "Comment";
    qDebug() << "toRefreshDialog";
    QString path;
    qDebug() << "toSetPath";
    path = initialPath + "/Proj_" + item->text(0) + "/";
    newPlainDialog->fullPath = path;
    newPlainDialog->item = item;
    //refreshDialog comes after setting up path
    newPlainDialog->refreshDialog();

    qDebug() << "toShowDialog";
    newPlainDialog->show();
    //connect(newPlainDialog,SIGNAL(dialogClosedOk()),this,SLOT(dialogClosedRefresh()));

}

void TreeFolderView::deleteProject(){
    // are you sure you want to delete current project and all files within it?
    // propose YES or NO
    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Project delete");
    msgBox.setText("Are you sure that you want to delete the project and all the containing material?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    if(msgBox.exec() == QMessageBox::Yes){
      // do something
        QString path = initialPath + "/Proj_" + item->text(0);
        QDir FileDir(path);
        FileDir.removeRecursively();
        delete item;
    }else {
      // do something else
    }
}

void TreeFolderView::addNewDevice(){
    qDebug() << "addNewDevice";

    // open a new device dialog
    // editLine za name
    // editText za comment
    // propose to save or discard
    // make ini file if saved

    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemDevice;
    newPlainDialog->actionType = ItemNew;
    newPlainDialog->setWindowTitle("New device");
    newPlainDialog->_lblName = "Device name";
    newPlainDialog->_lblComment = "Comment";
    qDebug() << "toRefreshDialog";
    QString path;
    qDebug() << "toSetPath";
    path = initialPath + "/Proj_" + item->parent()->text(0) + "/Loc_" + item->text(0) + "/";
    newPlainDialog->fullPath = path;
    newPlainDialog->item = item;
    newPlainDialog->refreshDialog();

    qDebug() << "toShowDialog";
    newPlainDialog->show();
    connect(newPlainDialog,SIGNAL(dialogClosedOk()),this,SLOT(dialogClosedRefresh()));
}

void TreeFolderView::editLocation(){
    // check for ini file
    // if no available ini files, make new
    // open a new location dialog
    // propose to save or discard

    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemLocation;
    newPlainDialog->actionType = ItemEdit;
    newPlainDialog->setWindowTitle("Edit Location");
    newPlainDialog->_lblName = "Location name";
    newPlainDialog->_lblComment = "Comment";
    qDebug() << "toRefreshDialog";
    QString path;
    qDebug() << "toSetPath";
    path = initialPath + "/Proj_" + item->parent()->text(0) + "/Loc_" + item->text(0) + "/";
    newPlainDialog->fullPath = path;
    newPlainDialog->item = item;
    //refreshDialog comes after setting up path
    newPlainDialog->refreshDialog();

    qDebug() << "toShowDialog";
    newPlainDialog->show();
    //connect(newPlainDialog,SIGNAL(dialogClosedOk()),this,SLOT(dialogClosedRefresh()));
}

void TreeFolderView::deleteLocation(){
    // are you sure you want to delete current location and all files within it?
    // propose YES or NO
    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Location delete");
    msgBox.setText("Are you sure that you want to delete the location and all the containing material?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    if(msgBox.exec() == QMessageBox::Yes){
      // do something
        QString path = initialPath + "/Proj_" + item->parent()->text(0) + "/Loc_" + item->text(0);
        QDir FileDir(path);
        FileDir.removeRecursively();
        delete item;
    }else{
      // do something else
    }

}

void TreeFolderView::editDevice(){
    // check for ini file
    // if no available ini files, make new
    // open a new device dialog
    // propose to save or discard

    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    newPlainDialog = new NewPlainDialog();
    newPlainDialog->folderType = ItemDevice;
    newPlainDialog->actionType = ItemEdit;
    newPlainDialog->setWindowTitle("Edit Device");
    newPlainDialog->_lblName = "Device name";
    newPlainDialog->_lblComment = "Comment";
    QString path;
    path = initialPath +
            "/Proj_" + item->parent()->parent()->text(0) +
            "/Loc_" + item->parent()->text(0) +
            "/Dev_" + item->text(0) + "/";
    newPlainDialog->fullPath = path;
    newPlainDialog->item = item;
    //refreshDialog comes after setting up path
    newPlainDialog->refreshDialog();

    qDebug() << "toShowDialog";
    newPlainDialog->show();
}

void TreeFolderView::deleteDevice(){
    // are you sure you want to delete current device and all files within it?
    // propose YES or NO
    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Device delete");
    msgBox.setText("Are you sure that you want to delete the device and all the containing material?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    if(msgBox.exec() == QMessageBox::Yes){
      // do something
        QString path = initialPath +
                "/Proj_" + item->parent()->parent()->text(0) +
                "/Loc_" + item->parent()->text(0) +
                "/Dev_" + item->text(0);
        QDir FileDir(path);
        FileDir.removeRecursively();
        delete item;
    }else{
      // do something else
    }
}

void TreeFolderView::plotData(){
    QTreeWidgetItem *item = ui->treeWidget->currentItem();
    QString path;
    path = initialPath +
            "/Proj_" +  item->parent()->parent()->parent()->text(0) +
            "/Loc_" +   item->parent()->parent()->text(0) +
            "/Dev_" +   item->parent()->text(0) +
            "/" +       item->text(0);

    emit dataToPlot(path,item->text(0));
}

void TreeFolderView::deleteLog(){
    // are you sure you want to delete current location and all files within it?
    // propose YES or NO
    QTreeWidgetItem *item = ui->treeWidget->currentItem();

    QMessageBox msgBox;
    msgBox.setWindowTitle("Measurement log delete");
    msgBox.setText("Are you sure that you want to delete the log file and all the containing measurements?");
    msgBox.setStandardButtons(QMessageBox::Yes);
    msgBox.addButton(QMessageBox::No);
    msgBox.setDefaultButton(QMessageBox::No);
    if(msgBox.exec() == QMessageBox::Yes){
        // do something
        QString path = initialPath +
                "/Proj_" +  item->parent()->parent()->parent()->text(0) +
                "/Loc_" +   item->parent()->parent()->text(0) +
                "/Dev_" +   item->parent()->text(0) +
                "/" +       item->text(0);
        QFile logFile(path);
        logFile.remove();

        QString pathIni = path.replace(path.length()-4,4,".ini");
        QFile iniFile(pathIni);
        iniFile.remove();
        delete item;
    }else{
        // do something else
    }
}

void TreeFolderView::copyLog(){
    itemCopy = ui->treeWidget->currentItem();
}

void TreeFolderView::pasteLog(){
    itemPaste = ui->treeWidget->currentItem();

    QString pathCopyLocation;
    pathCopyLocation = initialPath +
            "/Proj_" + itemCopy->parent()->parent()->parent()->text(0) +
            "/Loc_" + itemCopy->parent()->parent()->text(0) +
            "/Dev_" + itemCopy->parent()->text(0) +
            "/" + itemCopy->text(0);
    QString pathPasteFile;
    pathPasteFile = initialPath +
            "/Proj_" + itemPaste->parent()->parent()->text(0) +
            "/Loc_" + itemPaste->parent()->text(0) +
            "/Dev_" + itemPaste->text(0) +
            "/" + itemCopy->text(0); // only a file name stays the same

    // COPYING LOG FILE
    QFile fromFile(pathCopyLocation);
    QFile toFile(pathPasteFile);
    qint64 nCopySize = fromFile.size();
    if (nCopySize>1000){
        if (toFile.exists()) toFile.remove();
        Dialogwait *progressDialog;
        progressDialog = new Dialogwait(this);
        progressDialog->show();

        fromFile.open(QIODevice::ReadOnly);
        toFile.open(QIODevice::WriteOnly|QIODevice::Truncate);
        progressDialog->setMin(0);
        progressDialog->setMax(nCopySize);
        for (qint64 i = 0; i < nCopySize; i+=512) {
            toFile.write(fromFile.read(i)); // write a byte
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
            fromFile.seek(i);  // move to next byte to read
            toFile.seek(i); // move to next byte to write
            progressDialog->setValue((int)(i));
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        }
        fromFile.close();
        toFile.close();
        progressDialog->setText("Copying data...");
        progressDialog->startWait();
        QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        progressDialog->stopWait();
    }else{
        toFile.copy(pathCopyLocation,pathPasteFile);
    }

    // COPYING INI FILE
    QString pathCopyLocationIni = pathCopyLocation.replace(pathCopyLocation.length()-4,4,".ini");
    QString pathPasteFileIni = pathPasteFile.replace(pathPasteFile.length()-4,4,".ini");
    toFile.copy(pathCopyLocationIni,pathPasteFileIni);

    treeRefresh();
}


void TreeFolderView::dialogClosedRefresh(){
    qDebug() << "dialogClosedRefresh";
    timer=new QTimer();
    timer->setInterval(1000);
    timer->start();
    timer->connect(timer, SIGNAL(timeout()), this, SLOT(timer_timeout()));
}

void TreeFolderView::timer_timeout(){
    qDebug() << "timer_timeout";
    timer->stop();
    treeInitialSetup();
}

void TreeFolderView::on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous)
{
    if (current == NULL) return;
    if (current->type()!=ItemMeasurement) return;

    QString path;
    path = initialPath +
            "/Proj_" +  current->parent()->parent()->parent()->text(0) +
            "/Loc_" +   current->parent()->parent()->text(0) +
            "/Dev_" +   current->parent()->text(0) +
            "/" +       current->text(0);
    QString iniFIle = path.replace(path.length()-4,4,".ini");
    QSettings preferences(iniFIle,QSettings::IniFormat);

    QString SerialNumber=preferences.value("SerialNumber").toString();
    QString Identifier=preferences.value("Identifier").toString();
    QString SavingTimeDate=preferences.value("SavingTimeDate").toString();
    QString StartTimeDate=preferences.value("StartTimeDate").toString();
    QString EndTimeDate=preferences.value("EndTimeDate").toString();

    ui->txtFileInfo->setText(
            "SerialNumber: " + SerialNumber + "\n" +
            "Identifier: " + Identifier + "\n" +
            "SavingTimeDate: " + SavingTimeDate + "\n" +
            "StartTimeDate: " + StartTimeDate + "\n" +
            "EndTimeDate: " + EndTimeDate + "\n");
}

void TreeFolderView::on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column)
{
    // check for item level

    if (item == NULL) return;
    if (item->type()!=ItemMeasurement) return;

    QString path;
    path = initialPath +
            "/Proj_" +  item->parent()->parent()->parent()->text(0) +
            "/Loc_" +   item->parent()->parent()->text(0) +
            "/Dev_" +   item->parent()->text(0) +
            "/" +       item->text(0);

    emit dataToPlot(path,item->text(0));

    // if Project - info dialog
    // if Location - info dialog
    // if Device - open in right area

}

void TreeFolderView::slotOnDropEvent(QTreeWidgetItem* itm){

}

void TreeFolderView::slotOnDragMoveEvent(QTreeWidgetItem* itm){

}

void TreeFolderView::slotOnDragEnterEvent(QTreeWidgetItem* itm){

}

void TreeFolderView::slotOnDragLeaveEvent(QTreeWidgetItem* itm){

}
