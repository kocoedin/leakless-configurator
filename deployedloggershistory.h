#ifndef DEPLOYEDLOGGERSHISTORY_H
#define DEPLOYEDLOGGERSHISTORY_H

#include <QWidget>
#include <QWizard>
#include <QMessageBox>
#include <QFile>
#include <QCamera>
#include <QDateTime>
#include <QCameraControl>
#include <QCameraInfo>
#include <QCameraViewfinder>
#include <QListWidget>
#include <QCameraImageCapture>
#include <QMenu>
#include <QSettings>
#include <QStandardPaths>
#include <QTimer>
#include <QDir>
#include <QTreeWidget>
#include <QTimeLine>

namespace Ui {
class DeployedLoggersHistory;
}

class DeployedLoggersHistory : public QWidget
{
    Q_OBJECT

public:
    explicit DeployedLoggersHistory(QWidget *parent = 0);
    ~DeployedLoggersHistory();
//signals:

public slots:
    void refreshLoggersHistoryList();

private slots:
    void on_trwLoggersHistory_itemClicked(QTreeWidgetItem *item, int column);
    void on_trwLoggersHistory_customContextMenuRequested(const QPoint &pos);

    void on_rbxFilter_toggled(bool checked);

private:
    void fillFilterComboBoxes();

    Ui::DeployedLoggersHistory *ui;
};

#endif // DEPLOYEDLOGGERSHISTORY_H
