/********************************************************************************
** Form generated from reading UI file 'loggerconfpdlh2_BT_simple.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFPDLH2_BT_SIMPLE_H
#define UI_LOGGERCONFPDLH2_BT_SIMPLE_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfPDLH2_BT_simple
{
public:
    QVBoxLayout *verticalLayout_5;
    QToolBox *toolBox;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *InfoTreeWidget;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *Sensor_Checkbox;
    QCheckBox *Battery_Checkbox;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_4;
    QVBoxLayout *verticalLayout_3;
    QCustomPlot *realtimeWidget;
    QSlider *horizontalSlider;
    QSlider *verticalSlider;

    void setupUi(QWidget *LoggerConfPDLH2_BT_simple)
    {
        if (LoggerConfPDLH2_BT_simple->objectName().isEmpty())
            LoggerConfPDLH2_BT_simple->setObjectName(QStringLiteral("LoggerConfPDLH2_BT_simple"));
        LoggerConfPDLH2_BT_simple->resize(908, 725);
        verticalLayout_5 = new QVBoxLayout(LoggerConfPDLH2_BT_simple);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        toolBox = new QToolBox(LoggerConfPDLH2_BT_simple);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        QFont font;
        font.setPointSize(12);
        toolBox->setFont(font);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 890, 641));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        InfoTreeWidget = new QTreeWidget(page);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font1);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/barcode/barcode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/worldwide_location/worldwide_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/define_location/define_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Gardening/water_hose/water_hose-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/empty_flag/empty_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/filled_flag/filled_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/map_marker/map_marker-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/bluetooth/bluetooth-256 - Copy.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Forum/online/online-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/electrical_sensor-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/lock/lock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/win8/PNG/Photo_Video/start/start-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/win8/PNG/Objects/tear_off_calendar/tear_off_calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/calendar/calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/win8/PNG/Ecommerce/alarm_clock/alarm_clock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/win8/PNG/Microsoft/system_report/system_report-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/win8/PNG/Battery/full_battery/full_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon25;
        icon25.addFile(QStringLiteral(":/icons/win8/PNG/Battery/50_percent/50_percent-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon26;
        icon26.addFile(QStringLiteral(":/icons/win8/PNG/Battery/almost_empty/almost_empty-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font);
        __qtreewidgetitem1->setIcon(0, icon);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem5->setFont(0, font);
        __qtreewidgetitem5->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem6->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem7->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem8->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem9->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem10->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem11->setFont(0, font);
        __qtreewidgetitem11->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(__qtreewidgetitem11);
        __qtreewidgetitem12->setIcon(0, icon11);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem13->setFont(0, font);
        __qtreewidgetitem13->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem13);
        __qtreewidgetitem14->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem13);
        __qtreewidgetitem15->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(__qtreewidgetitem13);
        __qtreewidgetitem16->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem13);
        __qtreewidgetitem17->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(__qtreewidgetitem13);
        __qtreewidgetitem18->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem19 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem19->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem20 = new QTreeWidgetItem(__qtreewidgetitem19);
        __qtreewidgetitem20->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem21 = new QTreeWidgetItem(__qtreewidgetitem19);
        __qtreewidgetitem21->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem22 = new QTreeWidgetItem(__qtreewidgetitem19);
        __qtreewidgetitem22->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem23 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem23->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem24 = new QTreeWidgetItem(__qtreewidgetitem23);
        __qtreewidgetitem24->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem25 = new QTreeWidgetItem(__qtreewidgetitem23);
        __qtreewidgetitem25->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem26 = new QTreeWidgetItem(__qtreewidgetitem23);
        __qtreewidgetitem26->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem27 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem27->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem28 = new QTreeWidgetItem(__qtreewidgetitem27);
        __qtreewidgetitem28->setIcon(0, icon20);
        QTreeWidgetItem *__qtreewidgetitem29 = new QTreeWidgetItem(__qtreewidgetitem27);
        __qtreewidgetitem29->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem30 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem30->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem31 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem31->setIcon(0, icon23);
        new QTreeWidgetItem(__qtreewidgetitem29);
        new QTreeWidgetItem(__qtreewidgetitem29);
        new QTreeWidgetItem(__qtreewidgetitem29);
        QTreeWidgetItem *__qtreewidgetitem32 = new QTreeWidgetItem(__qtreewidgetitem27);
        __qtreewidgetitem32->setIcon(0, icon24);
        new QTreeWidgetItem(__qtreewidgetitem32);
        new QTreeWidgetItem(__qtreewidgetitem32);
        new QTreeWidgetItem(__qtreewidgetitem32);
        QTreeWidgetItem *__qtreewidgetitem33 = new QTreeWidgetItem(__qtreewidgetitem27);
        __qtreewidgetitem33->setIcon(0, icon25);
        new QTreeWidgetItem(__qtreewidgetitem33);
        new QTreeWidgetItem(__qtreewidgetitem33);
        new QTreeWidgetItem(__qtreewidgetitem33);
        QTreeWidgetItem *__qtreewidgetitem34 = new QTreeWidgetItem(__qtreewidgetitem27);
        __qtreewidgetitem34->setIcon(0, icon26);
        new QTreeWidgetItem(__qtreewidgetitem34);
        new QTreeWidgetItem(__qtreewidgetitem34);
        new QTreeWidgetItem(__qtreewidgetitem34);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        verticalLayout_2->addWidget(InfoTreeWidget);

        QIcon icon27;
        icon27.addFile(QStringLiteral(":/icons/win8/PNG/System/settings/settings-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page, icon27, QStringLiteral("Configuration"));
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        page_2->setGeometry(QRect(0, 0, 890, 641));
        verticalLayout_4 = new QVBoxLayout(page_2);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        groupBox_3 = new QGroupBox(page_2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_3);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        Realtime_START = new QPushButton(groupBox_3);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
        Realtime_START->setIcon(icon13);
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_3);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
        QIcon icon28;
        icon28.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon28);
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout_3->addWidget(Realtime_STOP);

        Sensor_Checkbox = new QCheckBox(groupBox_3);
        Sensor_Checkbox->setObjectName(QStringLiteral("Sensor_Checkbox"));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        Sensor_Checkbox->setFont(font2);

        horizontalLayout_3->addWidget(Sensor_Checkbox);

        Battery_Checkbox = new QCheckBox(groupBox_3);
        Battery_Checkbox->setObjectName(QStringLiteral("Battery_Checkbox"));
        Battery_Checkbox->setFont(font2);

        horizontalLayout_3->addWidget(Battery_Checkbox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_4);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font3;
        font3.setPointSize(14);
        label_5->setFont(font3);

        horizontalLayout_3->addWidget(label_5);


        verticalLayout_4->addWidget(groupBox_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        realtimeWidget = new QCustomPlot(page_2);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy1);

        verticalLayout_3->addWidget(realtimeWidget);

        horizontalSlider = new QSlider(page_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(2);
        horizontalSlider->setMaximum(100);
        horizontalSlider->setPageStep(2);
        horizontalSlider->setValue(10);
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_3->addWidget(horizontalSlider);


        horizontalLayout_4->addLayout(verticalLayout_3);

        verticalSlider = new QSlider(page_2);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setMinimum(1);
        verticalSlider->setMaximum(30);
        verticalSlider->setPageStep(2);
        verticalSlider->setValue(5);
        verticalSlider->setOrientation(Qt::Vertical);

        horizontalLayout_4->addWidget(verticalSlider);


        verticalLayout_4->addLayout(horizontalLayout_4);

        QIcon icon29;
        icon29.addFile(QStringLiteral(":/icons/win8/PNG/Charts/scatter_plot/scatter_plot-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_2, icon29, QStringLiteral("Realtime monitor"));

        verticalLayout_5->addWidget(toolBox);


        retranslateUi(LoggerConfPDLH2_BT_simple);

        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LoggerConfPDLH2_BT_simple);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfPDLH2_BT_simple)
    {
        LoggerConfPDLH2_BT_simple->setWindowTitle(QApplication::translate("LoggerConfPDLH2_BT_simple", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "IDN STRING 0 10 RW", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "FIRM STRING 0 10 R", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "HIDE", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "ID", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Location information", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem5->child(0);
        ___qtreewidgetitem6->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "MELOC STRING 0 22 RW", 0));
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Location", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem5->child(1);
        ___qtreewidgetitem7->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "METYPE STRING 0 22 RW", 0));
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Measurement type", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem5->child(2);
        ___qtreewidgetitem8->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "MELAT STRING 0 22 RW", 0));
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Latitude", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem5->child(3);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "MELNG STRING 0 22 RW", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Longitude", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem5->child(4);
        ___qtreewidgetitem10->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "MEHGHT STRING 0 22 RW", 0));
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Height", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Bluetooth", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem11->child(0);
        ___qtreewidgetitem12->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BTACT TF RW", 0));
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Active", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Operation", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem13->child(0);
        ___qtreewidgetitem14->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "OPS ENUM Standby Continuous Interval GPRS RW", 0));
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Operation mode", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem13->child(1);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "LOM TF RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Lock operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = ___qtreewidgetitem13->child(2);
        ___qtreewidgetitem16->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "TS TF RW HIDE", 0));
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Delayed start", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem13->child(3);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "TIME_START DATETIME RW HIDE", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Start time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem13->child(4);
        ___qtreewidgetitem18->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "TIME_END DATETIME RW HIDE", 0));
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "End time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem19 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem19->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Continuous sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem19->child(0);
        ___qtreewidgetitem20->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "CSI UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem20->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem19->child(1);
        ___qtreewidgetitem21->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "LSNC STRING 0 22 RW", 0));
        ___qtreewidgetitem21->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem19->child(2);
        ___qtreewidgetitem22->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "CSS INT32 1 10000000 RW", 0));
        ___qtreewidgetitem22->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Samples per file", 0));
        QTreeWidgetItem *___qtreewidgetitem23 = InfoTreeWidget->topLevelItem(5);
        ___qtreewidgetitem23->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Interval sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem23->child(0);
        ___qtreewidgetitem24->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "ISI UINT32 2 1000000 RW", 0));
        ___qtreewidgetitem24->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem23->child(1);
        ___qtreewidgetitem25->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "LSNI STRING 0 22 RW", 0));
        ___qtreewidgetitem25->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem23->child(2);
        ___qtreewidgetitem26->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "LSF ENUM FILE_PER_MINUTE FILE_PER_HOUR FILE_PER_DAY FILE_PER_MONTH FILE_PER_YEAR RW", 0));
        ___qtreewidgetitem26->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Log save option", 0));
        QTreeWidgetItem *___qtreewidgetitem27 = InfoTreeWidget->topLevelItem(6);
        ___qtreewidgetitem27->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "GSM operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem27->child(0);
        ___qtreewidgetitem28->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "GSMSWT UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem28->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sensor wait time", 0));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem27->child(1);
        ___qtreewidgetitem29->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Alarm settings", 0));
        QTreeWidgetItem *___qtreewidgetitem30 = ___qtreewidgetitem29->child(0);
        ___qtreewidgetitem30->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "ATT ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW", 0));
        ___qtreewidgetitem30->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Alarm trigger type", 0));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem29->child(1);
        ___qtreewidgetitem31->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "ARO ENUM TCP SMS EMAIL RW", 0));
        ___qtreewidgetitem31->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Alarm report type", 0));
        QTreeWidgetItem *___qtreewidgetitem32 = ___qtreewidgetitem29->child(2);
        ___qtreewidgetitem32->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "AMAXV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem32->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Maximum value", 0));
        QTreeWidgetItem *___qtreewidgetitem33 = ___qtreewidgetitem29->child(3);
        ___qtreewidgetitem33->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "ADELV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem33->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem34 = ___qtreewidgetitem29->child(4);
        ___qtreewidgetitem34->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "AMINV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem34->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Minimum value", 0));
        QTreeWidgetItem *___qtreewidgetitem35 = ___qtreewidgetitem27->child(2);
        ___qtreewidgetitem35->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery full setting", 0));
        QTreeWidgetItem *___qtreewidgetitem36 = ___qtreewidgetitem35->child(0);
        ___qtreewidgetitem36->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BFV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem36->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem37 = ___qtreewidgetitem35->child(1);
        ___qtreewidgetitem37->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BFRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem37->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem38 = ___qtreewidgetitem35->child(2);
        ___qtreewidgetitem38->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BFSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem38->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem39 = ___qtreewidgetitem27->child(3);
        ___qtreewidgetitem39->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery half setting", 0));
        QTreeWidgetItem *___qtreewidgetitem40 = ___qtreewidgetitem39->child(0);
        ___qtreewidgetitem40->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BHV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem40->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem41 = ___qtreewidgetitem39->child(1);
        ___qtreewidgetitem41->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BHRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem41->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem42 = ___qtreewidgetitem39->child(2);
        ___qtreewidgetitem42->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BHSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem42->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem43 = ___qtreewidgetitem27->child(4);
        ___qtreewidgetitem43->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery empty setting", 0));
        QTreeWidgetItem *___qtreewidgetitem44 = ___qtreewidgetitem43->child(0);
        ___qtreewidgetitem44->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BEV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem44->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem45 = ___qtreewidgetitem43->child(1);
        ___qtreewidgetitem45->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BERI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem45->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem46 = ___qtreewidgetitem43->child(2);
        ___qtreewidgetitem46->setText(1, QApplication::translate("LoggerConfPDLH2_BT_simple", "BESI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem46->setText(0, QApplication::translate("LoggerConfPDLH2_BT_simple", "Sampling interval (seconds)", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("LoggerConfPDLH2_BT_simple", "Configuration", 0));
        groupBox_3->setTitle(QApplication::translate("LoggerConfPDLH2_BT_simple", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfPDLH2_BT_simple", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfPDLH2_BT_simple", "STOP", 0));
        Sensor_Checkbox->setText(QApplication::translate("LoggerConfPDLH2_BT_simple", "Sensor", 0));
        Battery_Checkbox->setText(QApplication::translate("LoggerConfPDLH2_BT_simple", "Battery", 0));
        label_5->setText(QApplication::translate("LoggerConfPDLH2_BT_simple", "Export:", 0));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("LoggerConfPDLH2_BT_simple", "Realtime monitor", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfPDLH2_BT_simple: public Ui_LoggerConfPDLH2_BT_simple {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFPDLH2_BT_SIMPLE_H
