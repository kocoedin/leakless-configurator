/********************************************************************************
** Form generated from reading UI file 'deployedloggershistory.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEPLOYEDLOGGERSHISTORY_H
#define UI_DEPLOYEDLOGGERSHISTORY_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DeployedLoggersHistory
{
public:
    QVBoxLayout *verticalLayout_6;
    QHBoxLayout *horizontalLayout_4;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_5;
    QRadioButton *rbxNoFilter;
    QRadioButton *rbxFilter;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer;
    QVBoxLayout *verticalLayout_4;
    QCheckBox *cbxDate;
    QHBoxLayout *horizontalLayout_2;
    QDateTimeEdit *dteDeployTime;
    QDateTimeEdit *dteRemoveTime;
    QCheckBox *cbxLocation;
    QComboBox *cmbxLocation;
    QCheckBox *cbxType;
    QComboBox *cmbxType;
    QSpacerItem *verticalSpacer;
    QTreeWidget *trwLoggersHistory;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QTextBrowser *txbLoggersHistory;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QListWidget *lswLoggersHistory;

    void setupUi(QWidget *DeployedLoggersHistory)
    {
        if (DeployedLoggersHistory->objectName().isEmpty())
            DeployedLoggersHistory->setObjectName(QStringLiteral("DeployedLoggersHistory"));
        DeployedLoggersHistory->resize(872, 602);
        verticalLayout_6 = new QVBoxLayout(DeployedLoggersHistory);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        groupBox_3 = new QGroupBox(DeployedLoggersHistory);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_5 = new QVBoxLayout(groupBox_3);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        rbxNoFilter = new QRadioButton(groupBox_3);
        rbxNoFilter->setObjectName(QStringLiteral("rbxNoFilter"));
        rbxNoFilter->setChecked(true);

        verticalLayout_5->addWidget(rbxNoFilter);

        rbxFilter = new QRadioButton(groupBox_3);
        rbxFilter->setObjectName(QStringLiteral("rbxFilter"));

        verticalLayout_5->addWidget(rbxFilter);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer = new QSpacerItem(10, 20, QSizePolicy::Fixed, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        cbxDate = new QCheckBox(groupBox_3);
        cbxDate->setObjectName(QStringLiteral("cbxDate"));
        cbxDate->setEnabled(false);

        verticalLayout_4->addWidget(cbxDate);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        dteDeployTime = new QDateTimeEdit(groupBox_3);
        dteDeployTime->setObjectName(QStringLiteral("dteDeployTime"));
        dteDeployTime->setEnabled(false);
        dteDeployTime->setCalendarPopup(true);

        horizontalLayout_2->addWidget(dteDeployTime);

        dteRemoveTime = new QDateTimeEdit(groupBox_3);
        dteRemoveTime->setObjectName(QStringLiteral("dteRemoveTime"));
        dteRemoveTime->setEnabled(false);
        dteRemoveTime->setCalendarPopup(true);

        horizontalLayout_2->addWidget(dteRemoveTime);


        verticalLayout_4->addLayout(horizontalLayout_2);

        cbxLocation = new QCheckBox(groupBox_3);
        cbxLocation->setObjectName(QStringLiteral("cbxLocation"));
        cbxLocation->setEnabled(false);

        verticalLayout_4->addWidget(cbxLocation);

        cmbxLocation = new QComboBox(groupBox_3);
        cmbxLocation->setObjectName(QStringLiteral("cmbxLocation"));
        cmbxLocation->setEnabled(false);
        cmbxLocation->setEditable(true);

        verticalLayout_4->addWidget(cmbxLocation);

        cbxType = new QCheckBox(groupBox_3);
        cbxType->setObjectName(QStringLiteral("cbxType"));
        cbxType->setEnabled(false);

        verticalLayout_4->addWidget(cbxType);

        cmbxType = new QComboBox(groupBox_3);
        cmbxType->setObjectName(QStringLiteral("cmbxType"));
        cmbxType->setEnabled(false);
        cmbxType->setEditable(true);

        verticalLayout_4->addWidget(cmbxType);


        horizontalLayout_3->addLayout(verticalLayout_4);


        verticalLayout_5->addLayout(horizontalLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer);


        horizontalLayout_4->addWidget(groupBox_3);

        trwLoggersHistory = new QTreeWidget(DeployedLoggersHistory);
        trwLoggersHistory->setObjectName(QStringLiteral("trwLoggersHistory"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(trwLoggersHistory->sizePolicy().hasHeightForWidth());
        trwLoggersHistory->setSizePolicy(sizePolicy);
        QFont font;
        font.setPointSize(12);
        trwLoggersHistory->setFont(font);
        trwLoggersHistory->setContextMenuPolicy(Qt::CustomContextMenu);
        trwLoggersHistory->setAlternatingRowColors(true);

        horizontalLayout_4->addWidget(trwLoggersHistory);


        verticalLayout_6->addLayout(horizontalLayout_4);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(DeployedLoggersHistory);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        txbLoggersHistory = new QTextBrowser(DeployedLoggersHistory);
        txbLoggersHistory->setObjectName(QStringLiteral("txbLoggersHistory"));

        verticalLayout_2->addWidget(txbLoggersHistory);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(DeployedLoggersHistory);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        lswLoggersHistory = new QListWidget(DeployedLoggersHistory);
        lswLoggersHistory->setObjectName(QStringLiteral("lswLoggersHistory"));
        lswLoggersHistory->setViewMode(QListView::IconMode);

        verticalLayout->addWidget(lswLoggersHistory);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);


        verticalLayout_6->addLayout(verticalLayout_3);


        retranslateUi(DeployedLoggersHistory);

        QMetaObject::connectSlotsByName(DeployedLoggersHistory);
    } // setupUi

    void retranslateUi(QWidget *DeployedLoggersHistory)
    {
        DeployedLoggersHistory->setWindowTitle(QApplication::translate("DeployedLoggersHistory", "Form", 0));
        groupBox_3->setTitle(QApplication::translate("DeployedLoggersHistory", "Filters", 0));
        rbxNoFilter->setText(QApplication::translate("DeployedLoggersHistory", "No Filter", 0));
        rbxFilter->setText(QApplication::translate("DeployedLoggersHistory", "Filter", 0));
        cbxDate->setText(QApplication::translate("DeployedLoggersHistory", "By Date", 0));
        cbxLocation->setText(QApplication::translate("DeployedLoggersHistory", "By Location", 0));
        cbxType->setText(QApplication::translate("DeployedLoggersHistory", "By Type", 0));
        QTreeWidgetItem *___qtreewidgetitem = trwLoggersHistory->headerItem();
        ___qtreewidgetitem->setText(5, QApplication::translate("DeployedLoggersHistory", "File path", 0));
        ___qtreewidgetitem->setText(4, QApplication::translate("DeployedLoggersHistory", "Remove time", 0));
        ___qtreewidgetitem->setText(3, QApplication::translate("DeployedLoggersHistory", "Deploy time", 0));
        ___qtreewidgetitem->setText(2, QApplication::translate("DeployedLoggersHistory", "Logger ID", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("DeployedLoggersHistory", "Longitude", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("DeployedLoggersHistory", "Latitude", 0));
        label_2->setText(QApplication::translate("DeployedLoggersHistory", "Information", 0));
        label->setText(QApplication::translate("DeployedLoggersHistory", "Images", 0));
    } // retranslateUi

};

namespace Ui {
    class DeployedLoggersHistory: public Ui_DeployedLoggersHistory {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEPLOYEDLOGGERSHISTORY_H
