#include "dialogwait.h"
#include "ui_dialogwait.h"

Dialogwait::Dialogwait(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialogwait)
{
    waitTimer = new QTimer();
    ui->setupUi(this);
    this->show();
    counter=0;
}

Dialogwait::~Dialogwait()
{
    delete ui;
}

void Dialogwait::startWait(){
    ui->progressBar->setMaximum(100);
    ui->progressBar->setMinimum(0);
    ui->progressBar->setValue(100);
    counter=0;
    waitTimer->start(10);
}

void Dialogwait::stopWait(){
    waitTimer->stop();
    ui->progressBar->setValue(100);
    this->close();
}

void Dialogwait::setValue(int value){
    ui->progressBar->setValue(value);
}
void Dialogwait::setMax(int value){
    ui->progressBar->setMaximum(value);
}
void Dialogwait::setMin(int value){
    ui->progressBar->setMinimum(value);
}
void Dialogwait::timeout(){
    counter++;
    if (counter>100) counter=0;
    ui->progressBar->setValue(counter);
}
void Dialogwait::setText(QString text){

    ui->label->setText(text);
}


