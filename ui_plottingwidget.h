/********************************************************************************
** Form generated from reading UI file 'plottingwidget.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_PLOTTINGWIDGET_H
#define UI_PLOTTINGWIDGET_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_plottingWidget
{
public:
    QHBoxLayout *horizontalLayout;
    QCustomPlot *customPlot;
    QVBoxLayout *verticalLayout;
    QPushButton *pngSave;
    QPushButton *jpegSave;
    QPushButton *pdfSave;
    QPushButton *csvSave;
    QPushButton *txtSave;
    QSpacerItem *verticalSpacer;

    void setupUi(QWidget *plottingWidget)
    {
        if (plottingWidget->objectName().isEmpty())
            plottingWidget->setObjectName(QStringLiteral("plottingWidget"));
        plottingWidget->resize(811, 348);
        horizontalLayout = new QHBoxLayout(plottingWidget);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        customPlot = new QCustomPlot(plottingWidget);
        customPlot->setObjectName(QStringLiteral("customPlot"));
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(customPlot->sizePolicy().hasHeightForWidth());
        customPlot->setSizePolicy(sizePolicy);

        horizontalLayout->addWidget(customPlot);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
        pngSave = new QPushButton(plottingWidget);
        pngSave->setObjectName(QStringLiteral("pngSave"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/png/png-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        pngSave->setIcon(icon);
        pngSave->setIconSize(QSize(32, 32));

        verticalLayout->addWidget(pngSave);

        jpegSave = new QPushButton(plottingWidget);
        jpegSave->setObjectName(QStringLiteral("jpegSave"));
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/jpg/jpg-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        jpegSave->setIcon(icon1);
        jpegSave->setIconSize(QSize(32, 32));

        verticalLayout->addWidget(jpegSave);

        pdfSave = new QPushButton(plottingWidget);
        pdfSave->setObjectName(QStringLiteral("pdfSave"));
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/pdf/pdf-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        pdfSave->setIcon(icon2);
        pdfSave->setIconSize(QSize(32, 32));

        verticalLayout->addWidget(pdfSave);

        csvSave = new QPushButton(plottingWidget);
        csvSave->setObjectName(QStringLiteral("csvSave"));
        csvSave->setEnabled(false);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/csv/csv-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        csvSave->setIcon(icon3);
        csvSave->setIconSize(QSize(32, 32));

        verticalLayout->addWidget(csvSave);

        txtSave = new QPushButton(plottingWidget);
        txtSave->setObjectName(QStringLiteral("txtSave"));
        txtSave->setEnabled(false);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/txt/txt-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        txtSave->setIcon(icon4);
        txtSave->setIconSize(QSize(32, 32));

        verticalLayout->addWidget(txtSave);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer);


        horizontalLayout->addLayout(verticalLayout);


        retranslateUi(plottingWidget);

        QMetaObject::connectSlotsByName(plottingWidget);
    } // setupUi

    void retranslateUi(QWidget *plottingWidget)
    {
        plottingWidget->setWindowTitle(QApplication::translate("plottingWidget", "Form", 0));
        pngSave->setText(QString());
        jpegSave->setText(QString());
        pdfSave->setText(QString());
        csvSave->setText(QString());
        txtSave->setText(QString());
    } // retranslateUi

};

namespace Ui {
    class plottingWidget: public Ui_plottingWidget {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_PLOTTINGWIDGET_H
