#include "datavisualizer.h"
#include "ui_datavisualizer.h"

DataVisualizer::DataVisualizer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataVisualizer)
{
    ui->setupUi(this);
    PlotColors<<Qt::red<<Qt::blue<<Qt::cyan<<Qt::black<<Qt::green;
}

DataVisualizer::~DataVisualizer()
{
    delete ui;
}

void DataVisualizer::plotData(QString Path,QString Name){
    int i=0;
    QVector<QString>                Names;
    QVector<QString>                Units;
    QDate                           Date;
    QTime                           Time;
    QDateTime                       TimeStamp;
    QVector<double>                 TimeStamps;
    QVector<QVector<double> >       Values;

    int dataToPlotCount=0;
    Names.clear();
    Units.clear();
    QFile file(Path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) return;

    QTextStream in(&file);

    /* Citam header file */
    QString line = in.readLine();
    QStringList Data=line.split(";");

    if(Data.length()<2){
        QMessageBox::warning(this, tr("Error"),tr("Error parsing the log file"),QMessageBox::Ok);
        return;
    }

    for(i=2;i<Data.count();i++){
        QStringList Split;
        Split=Data.at(i).split(" ");
        if(Split.count()==2){
            Names.append(Split.at(0));
            Units.append(Split.at(1));
            dataToPlotCount++;
        }
    }
    Values.resize(dataToPlotCount);
    Dialogwait *progressDialog;
    progressDialog = new Dialogwait(this);
    progressDialog->show();
    progressDialog->setMin(0);
    progressDialog->setMax(1);
    progressDialog->setValue(1);
    progressDialog->setText("Plotting data...");
    progressDialog->startWait();

    plottingWidget *plotting=new plottingWidget(this);
    ui->plottingTab->addTab(plotting,Name);

    /* Ovdje odredujem sta uopce trebam plottati */
    while (!in.atEnd()) {
        line = in.readLine();
        Data=line.split(";");
        Data.removeAll("");
        if (Data.count()<2+dataToPlotCount) continue;

        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
        if (Data.at(0).contains('.'))
            Time=QTime::fromString(Data.at(0),"HH:mm:ss.zzz");
        else
            Time=QTime::fromString(Data.at(0),"HH:mm:ss");
        Date=QDate::fromString(Data.at(1),"dd/MM/yy");
        Date=Date.addYears(100);
        TimeStamp=QDateTime(Date,Time);
        if (TimeStamp.isValid()) TimeStamps.append(TimeStamp.toMSecsSinceEpoch()/1000.0);
        else continue;
        for (int j=0; j<dataToPlotCount;j++){
            Values[j].append(Data.at(2+j).toDouble());
        }
    }
    file.close();

    QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
    for (int i=0;i<dataToPlotCount;i++){
        plotting->addGraph(TimeStamps,Values[i],Names[i],Units[i],PlotColors.at(i));
    }
    progressDialog->stopWait();

}

