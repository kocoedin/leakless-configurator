/********************************************************************************
** Form generated from reading UI file 'loggerconfpdlh2_USB.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFPDLH2_USB_H
#define UI_LOGGERCONFPDLH2_USB_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfPDLH2_USB
{
public:
    QVBoxLayout *verticalLayout_5;
    QTabWidget *moduleOptions;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *InfoTreeWidget;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QTreeView *dirTree;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_3;
    QListView *fileTree;
    QTabWidget *plottingTab;
    QWidget *tab;
    QVBoxLayout *verticalLayout_15;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *Sensor_Checkbox;
    QCheckBox *Battery_Checkbox;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_8;
    QCustomPlot *realtimeWidget;
    QSlider *horizontalSlider;
    QSlider *verticalSlider;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout;
    QToolBox *toolBox;
    QWidget *page;
    QGridLayout *gridLayout;
    QPushButton *btnReadSens;
    QPushButton *btnReadBatt;
    QPushButton *btnSDDetect;
    QPushButton *btnExtLedOn;
    QPushButton *btnChargerStat;
    QPushButton *btnOpaPowOn;
    QPushButton *btnIntLedOff;
    QPushButton *btnExtLedOff;
    QPushButton *BtnMemPowOff;
    QPushButton *btnSensPowOff;
    QPushButton *btnOpaPowOff;
    QPushButton *btnIntLedOn;
    QPushButton *btnChargerOn;
    QPushButton *btnSensPowOn;
    QPushButton *btnMemPowOn;
    QPushButton *btnChargerOff;
    QWidget *page_5;
    QVBoxLayout *verticalLayout_9;
    QSplitter *splitter_3;
    QPushButton *btnModemPowOn;
    QPushButton *BtnModemPowOff;
    QPushButton *btnSimStatus;
    QSpacerItem *verticalSpacer_8;
    QSplitter *splitter_4;
    QLineEdit *tbxModemSimPin;
    QPushButton *btnSimPIN;
    QSpacerItem *verticalSpacer_2;
    QSplitter *splitter_6;
    QPushButton *btnModemLogin;
    QPushButton *btnModemLogout;
    QSpacerItem *verticalSpacer_9;
    QSplitter *splitter_5;
    QLineEdit *tbxModemCommand;
    QPushButton *btnModemSend;
    QSpacerItem *verticalSpacer_3;
    QWidget *page_2;
    QVBoxLayout *verticalLayout_10;
    QGridLayout *gridLayout_2;
    QPushButton *btnTCPAttach;
    QPushButton *btnTCPDeattach;
    QPushButton *btnTCPConnect;
    QPushButton *btnTCPDisconnect;
    QSpacerItem *verticalSpacer_5;
    QPushButton *btnTCPStatus;
    QSpacerItem *verticalSpacer_10;
    QSplitter *splitter_9;
    QLineEdit *tbxTCPSend;
    QPushButton *btnTCPSend;
    QSpacerItem *verticalSpacer_4;
    QWidget *page_6;
    QVBoxLayout *verticalLayout_13;
    QPushButton *btnMAILActivate;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout_11;
    QLabel *label_6;
    QLineEdit *tbxMAILSubject;
    QVBoxLayout *verticalLayout_12;
    QLabel *label_7;
    QLineEdit *tbxMAILText;
    QPushButton *btnMAILSend;
    QSpacerItem *verticalSpacer_6;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_14;
    QGridLayout *gridLayout_4;
    QPushButton *btnBTOn;
    QPushButton *btnBTDisconnect;
    QPushButton *btnBTUnpair;
    QPushButton *btnBToff;
    QPushButton *btnBTStatus;
    QSpacerItem *verticalSpacer_7;
    QWidget *page_4;
    QVBoxLayout *verticalLayout_6;
    QGridLayout *gridLayout_3;
    QPushButton *btnSendCommand;
    QPushButton *btnCustomTimeSet;
    QDateTimeEdit *dateTimeEdit;
    QLineEdit *customCommand;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_11;
    QCheckBox *autoclearCheckBox;
    QSpacerItem *horizontalSpacer;
    QPushButton *clearTerminalBrowser;
    QTextEdit *terminalBrowser;

    void setupUi(QWidget *LoggerConfPDLH2_USB)
    {
        if (LoggerConfPDLH2_USB->objectName().isEmpty())
            LoggerConfPDLH2_USB->setObjectName(QStringLiteral("LoggerConfPDLH2_USB"));
        LoggerConfPDLH2_USB->resize(662, 585);
        verticalLayout_5 = new QVBoxLayout(LoggerConfPDLH2_USB);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        moduleOptions = new QTabWidget(LoggerConfPDLH2_USB);
        moduleOptions->setObjectName(QStringLiteral("moduleOptions"));
        moduleOptions->setEnabled(true);
        QFont font;
        font.setPointSize(9);
        moduleOptions->setFont(font);
        moduleOptions->setAutoFillBackground(false);
        moduleOptions->setIconSize(QSize(16, 16));
        moduleOptions->setElideMode(Qt::ElideNone);
        moduleOptions->setDocumentMode(false);
        moduleOptions->setTabsClosable(false);
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_2 = new QVBoxLayout(tab_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        InfoTreeWidget = new QTreeWidget(tab_2);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font1);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font2;
        font2.setPointSize(12);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/barcode/barcode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Microsoft/system_report/system_report-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/worldwide_location/worldwide_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/define_location/define_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Gardening/water_hose/water_hose-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/empty_flag/empty_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/filled_flag/filled_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/map_marker/map_marker-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/settings2/settings2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electricity/electricity-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Transport/car_battery/car_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/win8/PNG/Folders/documents/documents-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/win8/PNG/Objects/timer/timer-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/document/document-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/radio_tower-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/server/server-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/win8/PNG/Folders/internet/internet-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/win8/PNG/Users/user/user-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/win8/PNG/Registration/password2/password2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/card_inserting/card_inserting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon25;
        icon25.addFile(QStringLiteral(":/icons/win8/PNG/Cell_Phones/sms/sms-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon26;
        icon26.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/bluetooth/bluetooth-256 - Copy.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon27;
        icon27.addFile(QStringLiteral(":/icons/win8/PNG/Forum/online/online-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon28;
        icon28.addFile(QStringLiteral(":/icons/win8/PNG/Buzz/gmail_login/gmail_login-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon29;
        icon29.addFile(QStringLiteral(":/icons/win8/PNG/Registration/email/email-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon30;
        icon30.addFile(QStringLiteral(":/icons/electrical_sensor-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon31;
        icon31.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon32;
        icon32.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/lock/lock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon33;
        icon33.addFile(QStringLiteral(":/icons/win8/PNG/Industry/display/display-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon34;
        icon34.addFile(QStringLiteral(":/icons/win8/PNG/Photo_Video/start/start-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon35;
        icon35.addFile(QStringLiteral(":/icons/win8/PNG/Objects/tear_off_calendar/tear_off_calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon36;
        icon36.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon37;
        icon37.addFile(QStringLiteral(":/icons/win8/PNG/Battery/almost_empty/almost_empty-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon38;
        icon38.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/calendar/calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon39;
        icon39.addFile(QStringLiteral(":/icons/win8/PNG/Ecommerce/alarm_clock/alarm_clock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon40;
        icon40.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon41;
        icon41.addFile(QStringLiteral(":/icons/win8/PNG/Battery/full_battery/full_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon42;
        icon42.addFile(QStringLiteral(":/icons/win8/PNG/Battery/50_percent/50_percent-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font2);
        __qtreewidgetitem1->setIcon(0, icon);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem5->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem6->setFont(0, font2);
        __qtreewidgetitem6->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem7->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem8->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem9->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem10->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem11->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem12->setFont(0, font2);
        __qtreewidgetitem12->setIcon(0, icon11);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem13->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem14->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem15->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem16->setFont(0, font2);
        __qtreewidgetitem16->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem17->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem18->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem19 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem19->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem20 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem20->setFont(0, font2);
        __qtreewidgetitem20->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem21 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem21->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem22 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem22->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem23 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem23->setIcon(0, icon20);
        QTreeWidgetItem *__qtreewidgetitem24 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem24->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem25 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem25->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem26 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem26->setIcon(0, icon23);
        QTreeWidgetItem *__qtreewidgetitem27 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem27->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem28 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem28->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem29 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem29->setIcon(0, icon25);
        QTreeWidgetItem *__qtreewidgetitem30 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem30->setFont(0, font2);
        __qtreewidgetitem30->setIcon(0, icon26);
        QTreeWidgetItem *__qtreewidgetitem31 = new QTreeWidgetItem(__qtreewidgetitem30);
        __qtreewidgetitem31->setIcon(0, icon27);
        QTreeWidgetItem *__qtreewidgetitem32 = new QTreeWidgetItem(__qtreewidgetitem30);
        __qtreewidgetitem32->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem33 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem33->setFont(0, font2);
        __qtreewidgetitem33->setIcon(0, icon28);
        QTreeWidgetItem *__qtreewidgetitem34 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem34->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem35 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem35->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem36 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem36->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem37 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem37->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem38 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem38->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem39 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem39->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem40 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem40->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem41 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem41->setFont(0, font2);
        __qtreewidgetitem41->setIcon(0, icon30);
        QTreeWidgetItem *__qtreewidgetitem42 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem42->setIcon(0, icon31);
        QTreeWidgetItem *__qtreewidgetitem43 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem43->setIcon(0, icon32);
        QTreeWidgetItem *__qtreewidgetitem44 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem44->setIcon(0, icon33);
        QTreeWidgetItem *__qtreewidgetitem45 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem45->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem46 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem46->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem47 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem47->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem48 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem48->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem49 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem49->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem50 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem50->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem51 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem51->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem52 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem52->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem53 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem53->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem54 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem54->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem55 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem55->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem56 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem56->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem57 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem57->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem58 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem58->setIcon(0, icon38);
        QTreeWidgetItem *__qtreewidgetitem59 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem59->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem60 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem60->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem61 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem61->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem62 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem62->setIcon(0, icon39);
        QTreeWidgetItem *__qtreewidgetitem63 = new QTreeWidgetItem(__qtreewidgetitem62);
        __qtreewidgetitem63->setIcon(0, icon40);
        QTreeWidgetItem *__qtreewidgetitem64 = new QTreeWidgetItem(__qtreewidgetitem62);
        __qtreewidgetitem64->setIcon(0, icon4);
        new QTreeWidgetItem(__qtreewidgetitem62);
        new QTreeWidgetItem(__qtreewidgetitem62);
        new QTreeWidgetItem(__qtreewidgetitem62);
        QTreeWidgetItem *__qtreewidgetitem65 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem65->setIcon(0, icon41);
        new QTreeWidgetItem(__qtreewidgetitem65);
        new QTreeWidgetItem(__qtreewidgetitem65);
        new QTreeWidgetItem(__qtreewidgetitem65);
        QTreeWidgetItem *__qtreewidgetitem66 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem66->setIcon(0, icon42);
        new QTreeWidgetItem(__qtreewidgetitem66);
        new QTreeWidgetItem(__qtreewidgetitem66);
        new QTreeWidgetItem(__qtreewidgetitem66);
        QTreeWidgetItem *__qtreewidgetitem67 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem67->setIcon(0, icon37);
        new QTreeWidgetItem(__qtreewidgetitem67);
        new QTreeWidgetItem(__qtreewidgetitem67);
        new QTreeWidgetItem(__qtreewidgetitem67);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        verticalLayout_2->addWidget(InfoTreeWidget);

        moduleOptions->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_7 = new QVBoxLayout(tab_3);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(tab_3);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(30, 30));
        label->setFrameShape(QFrame::NoFrame);
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/folder/folder-256.png")));
        label->setScaledContents(true);

        horizontalLayout_3->addWidget(label);

        label_2 = new QLabel(tab_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_3);

        dirTree = new QTreeView(tab_3);
        dirTree->setObjectName(QStringLiteral("dirTree"));

        verticalLayout_3->addWidget(dirTree);


        horizontalLayout_5->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(30, 30));
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/document/document-256.png")));
        label_3->setScaledContents(true);

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(tab_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout_4);

        fileTree = new QListView(tab_3);
        fileTree->setObjectName(QStringLiteral("fileTree"));
        fileTree->setMinimumSize(QSize(0, 0));
        fileTree->setAlternatingRowColors(true);
        fileTree->setSelectionRectVisible(true);

        verticalLayout_4->addWidget(fileTree);


        horizontalLayout_5->addLayout(verticalLayout_4);


        verticalLayout_7->addLayout(horizontalLayout_5);

        plottingTab = new QTabWidget(tab_3);
        plottingTab->setObjectName(QStringLiteral("plottingTab"));
        QSizePolicy sizePolicy1(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(plottingTab->sizePolicy().hasHeightForWidth());
        plottingTab->setSizePolicy(sizePolicy1);
        plottingTab->setMinimumSize(QSize(0, 400));
        plottingTab->setTabPosition(QTabWidget::South);
        plottingTab->setTabShape(QTabWidget::Rounded);
        plottingTab->setElideMode(Qt::ElideNone);
        plottingTab->setDocumentMode(false);
        plottingTab->setTabsClosable(true);
        plottingTab->setMovable(true);

        verticalLayout_7->addWidget(plottingTab);

        moduleOptions->addTab(tab_3, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_15 = new QVBoxLayout(tab);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_6 = new QHBoxLayout(groupBox_3);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        Realtime_START = new QPushButton(groupBox_3);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
        Realtime_START->setIcon(icon31);
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_3);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
        QIcon icon43;
        icon43.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon43);
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_STOP);

        Sensor_Checkbox = new QCheckBox(groupBox_3);
        Sensor_Checkbox->setObjectName(QStringLiteral("Sensor_Checkbox"));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        Sensor_Checkbox->setFont(font3);

        horizontalLayout_6->addWidget(Sensor_Checkbox);

        Battery_Checkbox = new QCheckBox(groupBox_3);
        Battery_Checkbox->setObjectName(QStringLiteral("Battery_Checkbox"));
        Battery_Checkbox->setFont(font3);

        horizontalLayout_6->addWidget(Battery_Checkbox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font4;
        font4.setPointSize(14);
        label_5->setFont(font4);

        horizontalLayout_6->addWidget(label_5);


        verticalLayout_15->addWidget(groupBox_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        realtimeWidget = new QCustomPlot(tab);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy2);

        verticalLayout_8->addWidget(realtimeWidget);

        horizontalSlider = new QSlider(tab);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(2);
        horizontalSlider->setMaximum(100);
        horizontalSlider->setPageStep(2);
        horizontalSlider->setValue(10);
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_8->addWidget(horizontalSlider);


        horizontalLayout_7->addLayout(verticalLayout_8);

        verticalSlider = new QSlider(tab);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setMinimum(1);
        verticalSlider->setMaximum(30);
        verticalSlider->setPageStep(2);
        verticalSlider->setValue(5);
        verticalSlider->setOrientation(Qt::Vertical);

        horizontalLayout_7->addWidget(verticalSlider);


        verticalLayout_15->addLayout(horizontalLayout_7);

        moduleOptions->addTab(tab, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        horizontalLayout = new QHBoxLayout(tab_4);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        toolBox = new QToolBox(tab_4);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 307, 342));
        gridLayout = new QGridLayout(page);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        btnReadSens = new QPushButton(page);
        btnReadSens->setObjectName(QStringLiteral("btnReadSens"));
        QSizePolicy sizePolicy3(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(btnReadSens->sizePolicy().hasHeightForWidth());
        btnReadSens->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnReadSens, 7, 0, 1, 1);

        btnReadBatt = new QPushButton(page);
        btnReadBatt->setObjectName(QStringLiteral("btnReadBatt"));
        sizePolicy3.setHeightForWidth(btnReadBatt->sizePolicy().hasHeightForWidth());
        btnReadBatt->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnReadBatt, 6, 0, 1, 1);

        btnSDDetect = new QPushButton(page);
        btnSDDetect->setObjectName(QStringLiteral("btnSDDetect"));
        sizePolicy3.setHeightForWidth(btnSDDetect->sizePolicy().hasHeightForWidth());
        btnSDDetect->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnSDDetect, 7, 1, 1, 1);

        btnExtLedOn = new QPushButton(page);
        btnExtLedOn->setObjectName(QStringLiteral("btnExtLedOn"));
        sizePolicy3.setHeightForWidth(btnExtLedOn->sizePolicy().hasHeightForWidth());
        btnExtLedOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnExtLedOn, 4, 0, 1, 1);

        btnChargerStat = new QPushButton(page);
        btnChargerStat->setObjectName(QStringLiteral("btnChargerStat"));
        sizePolicy3.setHeightForWidth(btnChargerStat->sizePolicy().hasHeightForWidth());
        btnChargerStat->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnChargerStat, 6, 1, 1, 1);

        btnOpaPowOn = new QPushButton(page);
        btnOpaPowOn->setObjectName(QStringLiteral("btnOpaPowOn"));
        sizePolicy3.setHeightForWidth(btnOpaPowOn->sizePolicy().hasHeightForWidth());
        btnOpaPowOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnOpaPowOn, 2, 0, 1, 1);

        btnIntLedOff = new QPushButton(page);
        btnIntLedOff->setObjectName(QStringLiteral("btnIntLedOff"));
        sizePolicy3.setHeightForWidth(btnIntLedOff->sizePolicy().hasHeightForWidth());
        btnIntLedOff->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnIntLedOff, 3, 1, 1, 1);

        btnExtLedOff = new QPushButton(page);
        btnExtLedOff->setObjectName(QStringLiteral("btnExtLedOff"));
        sizePolicy3.setHeightForWidth(btnExtLedOff->sizePolicy().hasHeightForWidth());
        btnExtLedOff->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnExtLedOff, 4, 1, 1, 1);

        BtnMemPowOff = new QPushButton(page);
        BtnMemPowOff->setObjectName(QStringLiteral("BtnMemPowOff"));
        sizePolicy3.setHeightForWidth(BtnMemPowOff->sizePolicy().hasHeightForWidth());
        BtnMemPowOff->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(BtnMemPowOff, 0, 1, 1, 1);

        btnSensPowOff = new QPushButton(page);
        btnSensPowOff->setObjectName(QStringLiteral("btnSensPowOff"));
        sizePolicy3.setHeightForWidth(btnSensPowOff->sizePolicy().hasHeightForWidth());
        btnSensPowOff->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnSensPowOff, 1, 1, 1, 1);

        btnOpaPowOff = new QPushButton(page);
        btnOpaPowOff->setObjectName(QStringLiteral("btnOpaPowOff"));
        sizePolicy3.setHeightForWidth(btnOpaPowOff->sizePolicy().hasHeightForWidth());
        btnOpaPowOff->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnOpaPowOff, 2, 1, 1, 1);

        btnIntLedOn = new QPushButton(page);
        btnIntLedOn->setObjectName(QStringLiteral("btnIntLedOn"));
        sizePolicy3.setHeightForWidth(btnIntLedOn->sizePolicy().hasHeightForWidth());
        btnIntLedOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnIntLedOn, 3, 0, 1, 1);

        btnChargerOn = new QPushButton(page);
        btnChargerOn->setObjectName(QStringLiteral("btnChargerOn"));
        sizePolicy3.setHeightForWidth(btnChargerOn->sizePolicy().hasHeightForWidth());
        btnChargerOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnChargerOn, 5, 1, 1, 1);

        btnSensPowOn = new QPushButton(page);
        btnSensPowOn->setObjectName(QStringLiteral("btnSensPowOn"));
        sizePolicy3.setHeightForWidth(btnSensPowOn->sizePolicy().hasHeightForWidth());
        btnSensPowOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnSensPowOn, 1, 0, 1, 1);

        btnMemPowOn = new QPushButton(page);
        btnMemPowOn->setObjectName(QStringLiteral("btnMemPowOn"));
        sizePolicy3.setHeightForWidth(btnMemPowOn->sizePolicy().hasHeightForWidth());
        btnMemPowOn->setSizePolicy(sizePolicy3);

        gridLayout->addWidget(btnMemPowOn, 0, 0, 1, 1);

        btnChargerOff = new QPushButton(page);
        btnChargerOff->setObjectName(QStringLiteral("btnChargerOff"));
        sizePolicy3.setHeightForWidth(btnChargerOff->sizePolicy().hasHeightForWidth());
        btnChargerOff->setSizePolicy(sizePolicy3);
        btnChargerOff->setAutoFillBackground(false);

        gridLayout->addWidget(btnChargerOff, 5, 0, 1, 1);

        QIcon icon44;
        icon44.addFile(QStringLiteral(":/icons/win8/PNG/System/settings/settings-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page, icon44, QStringLiteral("General"));
        page_5 = new QWidget();
        page_5->setObjectName(QStringLiteral("page_5"));
        page_5->setGeometry(QRect(0, 0, 307, 342));
        verticalLayout_9 = new QVBoxLayout(page_5);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        splitter_3 = new QSplitter(page_5);
        splitter_3->setObjectName(QStringLiteral("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        btnModemPowOn = new QPushButton(splitter_3);
        btnModemPowOn->setObjectName(QStringLiteral("btnModemPowOn"));
        sizePolicy3.setHeightForWidth(btnModemPowOn->sizePolicy().hasHeightForWidth());
        btnModemPowOn->setSizePolicy(sizePolicy3);
        splitter_3->addWidget(btnModemPowOn);
        BtnModemPowOff = new QPushButton(splitter_3);
        BtnModemPowOff->setObjectName(QStringLiteral("BtnModemPowOff"));
        splitter_3->addWidget(BtnModemPowOff);
        btnSimStatus = new QPushButton(splitter_3);
        btnSimStatus->setObjectName(QStringLiteral("btnSimStatus"));
        splitter_3->addWidget(btnSimStatus);

        verticalLayout_9->addWidget(splitter_3);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_8);

        splitter_4 = new QSplitter(page_5);
        splitter_4->setObjectName(QStringLiteral("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        tbxModemSimPin = new QLineEdit(splitter_4);
        tbxModemSimPin->setObjectName(QStringLiteral("tbxModemSimPin"));
        splitter_4->addWidget(tbxModemSimPin);
        btnSimPIN = new QPushButton(splitter_4);
        btnSimPIN->setObjectName(QStringLiteral("btnSimPIN"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnSimPIN->sizePolicy().hasHeightForWidth());
        btnSimPIN->setSizePolicy(sizePolicy4);
        splitter_4->addWidget(btnSimPIN);

        verticalLayout_9->addWidget(splitter_4);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_2);

        splitter_6 = new QSplitter(page_5);
        splitter_6->setObjectName(QStringLiteral("splitter_6"));
        splitter_6->setOrientation(Qt::Horizontal);
        btnModemLogin = new QPushButton(splitter_6);
        btnModemLogin->setObjectName(QStringLiteral("btnModemLogin"));
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(btnModemLogin->sizePolicy().hasHeightForWidth());
        btnModemLogin->setSizePolicy(sizePolicy5);
        btnModemLogin->setCheckable(false);
        splitter_6->addWidget(btnModemLogin);
        btnModemLogout = new QPushButton(splitter_6);
        btnModemLogout->setObjectName(QStringLiteral("btnModemLogout"));
        splitter_6->addWidget(btnModemLogout);

        verticalLayout_9->addWidget(splitter_6);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_9);

        splitter_5 = new QSplitter(page_5);
        splitter_5->setObjectName(QStringLiteral("splitter_5"));
        splitter_5->setOrientation(Qt::Horizontal);
        tbxModemCommand = new QLineEdit(splitter_5);
        tbxModemCommand->setObjectName(QStringLiteral("tbxModemCommand"));
        splitter_5->addWidget(tbxModemCommand);
        btnModemSend = new QPushButton(splitter_5);
        btnModemSend->setObjectName(QStringLiteral("btnModemSend"));
        splitter_5->addWidget(btnModemSend);

        verticalLayout_9->addWidget(splitter_5);

        verticalSpacer_3 = new QSpacerItem(20, 63, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_3);

        QIcon icon45;
        icon45.addFile(QStringLiteral(":/icons/win8/PNG/Ethernet/rj45/rj45-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_5, icon45, QStringLiteral("Modem"));
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        page_2->setGeometry(QRect(0, 0, 307, 342));
        verticalLayout_10 = new QVBoxLayout(page_2);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        gridLayout_2 = new QGridLayout();
        gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
        gridLayout_2->setSizeConstraint(QLayout::SetMinAndMaxSize);
        btnTCPAttach = new QPushButton(page_2);
        btnTCPAttach->setObjectName(QStringLiteral("btnTCPAttach"));
        sizePolicy3.setHeightForWidth(btnTCPAttach->sizePolicy().hasHeightForWidth());
        btnTCPAttach->setSizePolicy(sizePolicy3);

        gridLayout_2->addWidget(btnTCPAttach, 0, 0, 1, 1);

        btnTCPDeattach = new QPushButton(page_2);
        btnTCPDeattach->setObjectName(QStringLiteral("btnTCPDeattach"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::Ignored);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(btnTCPDeattach->sizePolicy().hasHeightForWidth());
        btnTCPDeattach->setSizePolicy(sizePolicy6);

        gridLayout_2->addWidget(btnTCPDeattach, 0, 1, 1, 1);

        btnTCPConnect = new QPushButton(page_2);
        btnTCPConnect->setObjectName(QStringLiteral("btnTCPConnect"));
        sizePolicy3.setHeightForWidth(btnTCPConnect->sizePolicy().hasHeightForWidth());
        btnTCPConnect->setSizePolicy(sizePolicy3);

        gridLayout_2->addWidget(btnTCPConnect, 1, 0, 1, 1);

        btnTCPDisconnect = new QPushButton(page_2);
        btnTCPDisconnect->setObjectName(QStringLiteral("btnTCPDisconnect"));
        sizePolicy3.setHeightForWidth(btnTCPDisconnect->sizePolicy().hasHeightForWidth());
        btnTCPDisconnect->setSizePolicy(sizePolicy3);

        gridLayout_2->addWidget(btnTCPDisconnect, 1, 1, 1, 1);


        verticalLayout_10->addLayout(gridLayout_2);

        verticalSpacer_5 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_10->addItem(verticalSpacer_5);

        btnTCPStatus = new QPushButton(page_2);
        btnTCPStatus->setObjectName(QStringLiteral("btnTCPStatus"));
        sizePolicy3.setHeightForWidth(btnTCPStatus->sizePolicy().hasHeightForWidth());
        btnTCPStatus->setSizePolicy(sizePolicy3);

        verticalLayout_10->addWidget(btnTCPStatus);

        verticalSpacer_10 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_10);

        splitter_9 = new QSplitter(page_2);
        splitter_9->setObjectName(QStringLiteral("splitter_9"));
        splitter_9->setOrientation(Qt::Horizontal);
        tbxTCPSend = new QLineEdit(splitter_9);
        tbxTCPSend->setObjectName(QStringLiteral("tbxTCPSend"));
        splitter_9->addWidget(tbxTCPSend);
        btnTCPSend = new QPushButton(splitter_9);
        btnTCPSend->setObjectName(QStringLiteral("btnTCPSend"));
        splitter_9->addWidget(btnTCPSend);

        verticalLayout_10->addWidget(splitter_9);

        verticalSpacer_4 = new QSpacerItem(20, 135, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_10->addItem(verticalSpacer_4);

        QIcon icon46;
        icon46.addFile(QStringLiteral(":/icons/win8/PNG/Industry/radio_tower/radio_tower-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_2, icon46, QStringLiteral("GSM"));
        page_6 = new QWidget();
        page_6->setObjectName(QStringLiteral("page_6"));
        page_6->setGeometry(QRect(0, 0, 307, 342));
        verticalLayout_13 = new QVBoxLayout(page_6);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        btnMAILActivate = new QPushButton(page_6);
        btnMAILActivate->setObjectName(QStringLiteral("btnMAILActivate"));
        sizePolicy3.setHeightForWidth(btnMAILActivate->sizePolicy().hasHeightForWidth());
        btnMAILActivate->setSizePolicy(sizePolicy3);

        verticalLayout_13->addWidget(btnMAILActivate);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        horizontalLayout_2->setSizeConstraint(QLayout::SetMinimumSize);
        verticalLayout_11 = new QVBoxLayout();
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        label_6 = new QLabel(page_6);
        label_6->setObjectName(QStringLiteral("label_6"));

        verticalLayout_11->addWidget(label_6);

        tbxMAILSubject = new QLineEdit(page_6);
        tbxMAILSubject->setObjectName(QStringLiteral("tbxMAILSubject"));
        sizePolicy4.setHeightForWidth(tbxMAILSubject->sizePolicy().hasHeightForWidth());
        tbxMAILSubject->setSizePolicy(sizePolicy4);
        tbxMAILSubject->setMaxLength(10);

        verticalLayout_11->addWidget(tbxMAILSubject);


        horizontalLayout_2->addLayout(verticalLayout_11);

        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        label_7 = new QLabel(page_6);
        label_7->setObjectName(QStringLiteral("label_7"));

        verticalLayout_12->addWidget(label_7);

        tbxMAILText = new QLineEdit(page_6);
        tbxMAILText->setObjectName(QStringLiteral("tbxMAILText"));
        sizePolicy4.setHeightForWidth(tbxMAILText->sizePolicy().hasHeightForWidth());
        tbxMAILText->setSizePolicy(sizePolicy4);
        tbxMAILText->setMaxLength(20);

        verticalLayout_12->addWidget(tbxMAILText);


        horizontalLayout_2->addLayout(verticalLayout_12);


        verticalLayout_13->addLayout(horizontalLayout_2);

        btnMAILSend = new QPushButton(page_6);
        btnMAILSend->setObjectName(QStringLiteral("btnMAILSend"));
        sizePolicy3.setHeightForWidth(btnMAILSend->sizePolicy().hasHeightForWidth());
        btnMAILSend->setSizePolicy(sizePolicy3);

        verticalLayout_13->addWidget(btnMAILSend);

        verticalSpacer_6 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_13->addItem(verticalSpacer_6);

        toolBox->addItem(page_6, icon28, QStringLiteral("E-Mail"));
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        page_3->setGeometry(QRect(0, 0, 307, 342));
        verticalLayout_14 = new QVBoxLayout(page_3);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QStringLiteral("gridLayout_4"));
        btnBTOn = new QPushButton(page_3);
        btnBTOn->setObjectName(QStringLiteral("btnBTOn"));
        sizePolicy3.setHeightForWidth(btnBTOn->sizePolicy().hasHeightForWidth());
        btnBTOn->setSizePolicy(sizePolicy3);

        gridLayout_4->addWidget(btnBTOn, 0, 0, 1, 1);

        btnBTDisconnect = new QPushButton(page_3);
        btnBTDisconnect->setObjectName(QStringLiteral("btnBTDisconnect"));
        sizePolicy3.setHeightForWidth(btnBTDisconnect->sizePolicy().hasHeightForWidth());
        btnBTDisconnect->setSizePolicy(sizePolicy3);

        gridLayout_4->addWidget(btnBTDisconnect, 1, 1, 1, 1);

        btnBTUnpair = new QPushButton(page_3);
        btnBTUnpair->setObjectName(QStringLiteral("btnBTUnpair"));
        sizePolicy3.setHeightForWidth(btnBTUnpair->sizePolicy().hasHeightForWidth());
        btnBTUnpair->setSizePolicy(sizePolicy3);

        gridLayout_4->addWidget(btnBTUnpair, 1, 0, 1, 1);

        btnBToff = new QPushButton(page_3);
        btnBToff->setObjectName(QStringLiteral("btnBToff"));
        sizePolicy3.setHeightForWidth(btnBToff->sizePolicy().hasHeightForWidth());
        btnBToff->setSizePolicy(sizePolicy3);

        gridLayout_4->addWidget(btnBToff, 0, 1, 1, 1);


        verticalLayout_14->addLayout(gridLayout_4);

        btnBTStatus = new QPushButton(page_3);
        btnBTStatus->setObjectName(QStringLiteral("btnBTStatus"));
        sizePolicy3.setHeightForWidth(btnBTStatus->sizePolicy().hasHeightForWidth());
        btnBTStatus->setSizePolicy(sizePolicy3);

        verticalLayout_14->addWidget(btnBTStatus);

        verticalSpacer_7 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_14->addItem(verticalSpacer_7);

        QIcon icon47;
        icon47.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/bluetooth/bluetooth-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_3, icon47, QStringLiteral("Bluetooth"));
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        page_4->setGeometry(QRect(0, 0, 307, 342));
        verticalLayout_6 = new QVBoxLayout(page_4);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        btnSendCommand = new QPushButton(page_4);
        btnSendCommand->setObjectName(QStringLiteral("btnSendCommand"));
        sizePolicy3.setHeightForWidth(btnSendCommand->sizePolicy().hasHeightForWidth());
        btnSendCommand->setSizePolicy(sizePolicy3);

        gridLayout_3->addWidget(btnSendCommand, 0, 1, 1, 1);

        btnCustomTimeSet = new QPushButton(page_4);
        btnCustomTimeSet->setObjectName(QStringLiteral("btnCustomTimeSet"));
        sizePolicy3.setHeightForWidth(btnCustomTimeSet->sizePolicy().hasHeightForWidth());
        btnCustomTimeSet->setSizePolicy(sizePolicy3);

        gridLayout_3->addWidget(btnCustomTimeSet, 1, 1, 1, 1);

        dateTimeEdit = new QDateTimeEdit(page_4);
        dateTimeEdit->setObjectName(QStringLiteral("dateTimeEdit"));
        sizePolicy3.setHeightForWidth(dateTimeEdit->sizePolicy().hasHeightForWidth());
        dateTimeEdit->setSizePolicy(sizePolicy3);
        dateTimeEdit->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        dateTimeEdit->setProperty("showGroupSeparator", QVariant(false));
        dateTimeEdit->setCalendarPopup(true);

        gridLayout_3->addWidget(dateTimeEdit, 1, 0, 1, 1);

        customCommand = new QLineEdit(page_4);
        customCommand->setObjectName(QStringLiteral("customCommand"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(customCommand->sizePolicy().hasHeightForWidth());
        customCommand->setSizePolicy(sizePolicy7);

        gridLayout_3->addWidget(customCommand, 0, 0, 1, 1);


        verticalLayout_6->addLayout(gridLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer);

        QIcon icon48;
        icon48.addFile(QStringLiteral(":/icons/win8/PNG/System/help/help-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_4, icon48, QStringLiteral("Other"));

        horizontalLayout->addWidget(toolBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        autoclearCheckBox = new QCheckBox(tab_4);
        autoclearCheckBox->setObjectName(QStringLiteral("autoclearCheckBox"));
        autoclearCheckBox->setChecked(true);

        horizontalLayout_11->addWidget(autoclearCheckBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer);

        clearTerminalBrowser = new QPushButton(tab_4);
        clearTerminalBrowser->setObjectName(QStringLiteral("clearTerminalBrowser"));

        horizontalLayout_11->addWidget(clearTerminalBrowser);


        verticalLayout->addLayout(horizontalLayout_11);

        terminalBrowser = new QTextEdit(tab_4);
        terminalBrowser->setObjectName(QStringLiteral("terminalBrowser"));

        verticalLayout->addWidget(terminalBrowser);


        horizontalLayout->addLayout(verticalLayout);

        moduleOptions->addTab(tab_4, QString());

        verticalLayout_5->addWidget(moduleOptions);


        retranslateUi(LoggerConfPDLH2_USB);

        moduleOptions->setCurrentIndex(1);
        toolBox->setCurrentIndex(1);


        QMetaObject::connectSlotsByName(LoggerConfPDLH2_USB);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfPDLH2_USB)
    {
        LoggerConfPDLH2_USB->setWindowTitle(QApplication::translate("LoggerConfPDLH2_USB", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "IDN STRING 0 10 RW", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "FIRM STRING 0 10 R", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "HIDE", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "ID", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "SRI UINT16 100 10000 RW", 0));
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Status report interval", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Location information", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MELOC STRING 0 22 RW", 0));
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Location", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem6->child(1);
        ___qtreewidgetitem8->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "METYPE STRING 0 22 RW", 0));
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Measurement type", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem6->child(2);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MELAT STRING 0 22 RW", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Latitude", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem6->child(3);
        ___qtreewidgetitem10->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MELNG STRING 0 22 RW", 0));
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Longitude", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem6->child(4);
        ___qtreewidgetitem11->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MEHGHT STRING 0 22 RW", 0));
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Height", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Calibration", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem12->child(0);
        ___qtreewidgetitem13->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "SCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sensor calibration coefficient", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem12->child(1);
        ___qtreewidgetitem14->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "CCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Charging current calibration ", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem12->child(2);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BAC UINT32 0 100000 RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery capacity", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem16->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "HIDE", 0));
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Logging", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "TF ENUM hh:mm:ss:dd:mm:yyyy hh:mm:ss_dd/MM/yyyy hh:mm:ss_dd/MM/yy RW HIDE", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Time format", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem16->child(1);
        ___qtreewidgetitem18->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LF ENUM Format_1 Format_2 Format_3 RW HIDE", 0));
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Log format", 0));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem16->child(2);
        ___qtreewidgetitem19->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LSN STRING 0 10 RW HIDE", 0));
        ___qtreewidgetitem19->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Free string", 0));
        QTreeWidgetItem *___qtreewidgetitem20 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem20->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "GSM settings", 0));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem20->child(0);
        ___qtreewidgetitem21->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMIP STRING 0 22 RW", 0));
        ___qtreewidgetitem21->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Server IP", 0));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem20->child(1);
        ___qtreewidgetitem22->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMPRT STRING 0 10 RW", 0));
        ___qtreewidgetitem22->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Server port", 0));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem20->child(2);
        ___qtreewidgetitem23->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMAPN STRING 0 22 RW", 0));
        ___qtreewidgetitem23->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Network APN", 0));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem20->child(3);
        ___qtreewidgetitem24->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMUSR STRING 0 22 RW", 0));
        ___qtreewidgetitem24->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Network username", 0));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem20->child(4);
        ___qtreewidgetitem25->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMPWD STRING 0 22 RW", 0));
        ___qtreewidgetitem25->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Network password", 0));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem20->child(5);
        ___qtreewidgetitem26->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMPIN STRING 0 10 RW", 0));
        ___qtreewidgetitem26->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "SIM pin", 0));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem20->child(6);
        ___qtreewidgetitem27->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMRC UINT32 1 10 RW", 0));
        ___qtreewidgetitem27->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Reconnect count", 0));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem20->child(7);
        ___qtreewidgetitem28->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMWT UINT32 0 120 RW", 0));
        ___qtreewidgetitem28->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Reconnect wait", 0));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem20->child(8);
        ___qtreewidgetitem29->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMNUM STRING 0 22 RW", 0));
        ___qtreewidgetitem29->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "SMS number", 0));
        QTreeWidgetItem *___qtreewidgetitem30 = InfoTreeWidget->topLevelItem(5);
        ___qtreewidgetitem30->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Bluetooth", 0));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem30->child(0);
        ___qtreewidgetitem31->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BTACT TF RW", 0));
        ___qtreewidgetitem31->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Active", 0));
        QTreeWidgetItem *___qtreewidgetitem32 = ___qtreewidgetitem30->child(1);
        ___qtreewidgetitem32->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BTHOST STRING 0 22 RW", 0));
        ___qtreewidgetitem32->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Host ID", 0));
        QTreeWidgetItem *___qtreewidgetitem33 = InfoTreeWidget->topLevelItem(6);
        ___qtreewidgetitem33->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Mail", 0));
        QTreeWidgetItem *___qtreewidgetitem34 = ___qtreewidgetitem33->child(0);
        ___qtreewidgetitem34->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILSRV STRING 0 22 RW", 0));
        ___qtreewidgetitem34->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Mail server", 0));
        QTreeWidgetItem *___qtreewidgetitem35 = ___qtreewidgetitem33->child(1);
        ___qtreewidgetitem35->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILPRT STRING 0 10 RW", 0));
        ___qtreewidgetitem35->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Mail port", 0));
        QTreeWidgetItem *___qtreewidgetitem36 = ___qtreewidgetitem33->child(2);
        ___qtreewidgetitem36->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILUSR STRING 0 22 RW", 0));
        ___qtreewidgetitem36->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Mail username", 0));
        QTreeWidgetItem *___qtreewidgetitem37 = ___qtreewidgetitem33->child(3);
        ___qtreewidgetitem37->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILPWD STRING 0 22 RW", 0));
        ___qtreewidgetitem37->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Mail password", 0));
        QTreeWidgetItem *___qtreewidgetitem38 = ___qtreewidgetitem33->child(4);
        ___qtreewidgetitem38->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILFRM STRING 0 22 RW", 0));
        ___qtreewidgetitem38->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sender mail", 0));
        QTreeWidgetItem *___qtreewidgetitem39 = ___qtreewidgetitem33->child(5);
        ___qtreewidgetitem39->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILTO1 STRING 0 22 RW", 0));
        ___qtreewidgetitem39->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Receiver mail", 0));
        QTreeWidgetItem *___qtreewidgetitem40 = ___qtreewidgetitem33->child(6);
        ___qtreewidgetitem40->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "MAILTO2 STRING 0 22 RW", 0));
        ___qtreewidgetitem40->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Receiver mail", 0));
        QTreeWidgetItem *___qtreewidgetitem41 = InfoTreeWidget->topLevelItem(7);
        ___qtreewidgetitem41->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Operation", 0));
        QTreeWidgetItem *___qtreewidgetitem42 = ___qtreewidgetitem41->child(0);
        ___qtreewidgetitem42->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "OPS ENUM Standby Continuous Interval GPRS RW", 0));
        ___qtreewidgetitem42->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Operation mode", 0));
        QTreeWidgetItem *___qtreewidgetitem43 = ___qtreewidgetitem41->child(1);
        ___qtreewidgetitem43->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LOM TF RW", 0));
        ___qtreewidgetitem43->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Lock operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem44 = ___qtreewidgetitem41->child(2);
        ___qtreewidgetitem44->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "IT UINT32 1 1000000 RW HIDE", 0));
        ___qtreewidgetitem44->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "LCD indication timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem45 = ___qtreewidgetitem41->child(3);
        ___qtreewidgetitem45->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "TS TF RW HIDE", 0));
        ___qtreewidgetitem45->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Delayed start", 0));
        QTreeWidgetItem *___qtreewidgetitem46 = ___qtreewidgetitem41->child(4);
        ___qtreewidgetitem46->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "TIME_START DATETIME RW HIDE", 0));
        ___qtreewidgetitem46->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Start time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem47 = ___qtreewidgetitem41->child(5);
        ___qtreewidgetitem47->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "TIME_END DATETIME RW HIDE", 0));
        ___qtreewidgetitem47->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "End time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem48 = InfoTreeWidget->topLevelItem(8);
        ___qtreewidgetitem48->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "HIDE", 0));
        ___qtreewidgetitem48->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Standby mode", 0));
        QTreeWidgetItem *___qtreewidgetitem49 = InfoTreeWidget->topLevelItem(9);
        ___qtreewidgetitem49->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Continuous sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem50 = ___qtreewidgetitem49->child(0);
        ___qtreewidgetitem50->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "CSI UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem50->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem51 = ___qtreewidgetitem49->child(1);
        ___qtreewidgetitem51->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LSNC STRING 0 22 RW", 0));
        ___qtreewidgetitem51->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem52 = ___qtreewidgetitem49->child(2);
        ___qtreewidgetitem52->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "CSS INT32 1 10000000 RW", 0));
        ___qtreewidgetitem52->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Samples per file", 0));
        QTreeWidgetItem *___qtreewidgetitem53 = ___qtreewidgetitem49->child(3);
        ___qtreewidgetitem53->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "CSB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem53->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem54 = InfoTreeWidget->topLevelItem(10);
        ___qtreewidgetitem54->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Interval sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem55 = ___qtreewidgetitem54->child(0);
        ___qtreewidgetitem55->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ISI UINT32 2 1000000 RW", 0));
        ___qtreewidgetitem55->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem56 = ___qtreewidgetitem54->child(1);
        ___qtreewidgetitem56->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ISSW UINT32 0 10000 RW", 0));
        ___qtreewidgetitem56->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sensor wait time (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem57 = ___qtreewidgetitem54->child(2);
        ___qtreewidgetitem57->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LSNI STRING 0 22 RW", 0));
        ___qtreewidgetitem57->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem58 = ___qtreewidgetitem54->child(3);
        ___qtreewidgetitem58->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "LSF ENUM FILE_PER_MINUTE FILE_PER_HOUR FILE_PER_DAY FILE_PER_MONTH FILE_PER_YEAR RW", 0));
        ___qtreewidgetitem58->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Log save option", 0));
        QTreeWidgetItem *___qtreewidgetitem59 = ___qtreewidgetitem54->child(4);
        ___qtreewidgetitem59->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ISB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem59->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem60 = InfoTreeWidget->topLevelItem(11);
        ___qtreewidgetitem60->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "GSM operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem61 = ___qtreewidgetitem60->child(0);
        ___qtreewidgetitem61->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "GSMSWT UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem61->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sensor wait time", 0));
        QTreeWidgetItem *___qtreewidgetitem62 = ___qtreewidgetitem60->child(1);
        ___qtreewidgetitem62->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Alarm settings", 0));
        QTreeWidgetItem *___qtreewidgetitem63 = ___qtreewidgetitem62->child(0);
        ___qtreewidgetitem63->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ATT ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW", 0));
        ___qtreewidgetitem63->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Alarm trigger type", 0));
        QTreeWidgetItem *___qtreewidgetitem64 = ___qtreewidgetitem62->child(1);
        ___qtreewidgetitem64->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ARO ENUM TCP SMS EMAIL RW", 0));
        ___qtreewidgetitem64->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Alarm report type", 0));
        QTreeWidgetItem *___qtreewidgetitem65 = ___qtreewidgetitem62->child(2);
        ___qtreewidgetitem65->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "AMAXV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem65->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Maximum value", 0));
        QTreeWidgetItem *___qtreewidgetitem66 = ___qtreewidgetitem62->child(3);
        ___qtreewidgetitem66->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "ADELV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem66->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem67 = ___qtreewidgetitem62->child(4);
        ___qtreewidgetitem67->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "AMINV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem67->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Minimum value", 0));
        QTreeWidgetItem *___qtreewidgetitem68 = ___qtreewidgetitem60->child(2);
        ___qtreewidgetitem68->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery full setting", 0));
        QTreeWidgetItem *___qtreewidgetitem69 = ___qtreewidgetitem68->child(0);
        ___qtreewidgetitem69->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BFV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem69->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem70 = ___qtreewidgetitem68->child(1);
        ___qtreewidgetitem70->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BFRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem70->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem71 = ___qtreewidgetitem68->child(2);
        ___qtreewidgetitem71->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BFSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem71->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem72 = ___qtreewidgetitem60->child(3);
        ___qtreewidgetitem72->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery half setting", 0));
        QTreeWidgetItem *___qtreewidgetitem73 = ___qtreewidgetitem72->child(0);
        ___qtreewidgetitem73->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BHV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem73->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem74 = ___qtreewidgetitem72->child(1);
        ___qtreewidgetitem74->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BHRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem74->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem75 = ___qtreewidgetitem72->child(2);
        ___qtreewidgetitem75->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BHSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem75->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem76 = ___qtreewidgetitem60->child(4);
        ___qtreewidgetitem76->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery empty setting", 0));
        QTreeWidgetItem *___qtreewidgetitem77 = ___qtreewidgetitem76->child(0);
        ___qtreewidgetitem77->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BEV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem77->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem78 = ___qtreewidgetitem76->child(1);
        ___qtreewidgetitem78->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BERI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem78->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem79 = ___qtreewidgetitem76->child(2);
        ___qtreewidgetitem79->setText(1, QApplication::translate("LoggerConfPDLH2_USB", "BESI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem79->setText(0, QApplication::translate("LoggerConfPDLH2_USB", "Sampling interval (seconds)", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        moduleOptions->setTabText(moduleOptions->indexOf(tab_2), QApplication::translate("LoggerConfPDLH2_USB", "Configuration", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("LoggerConfPDLH2_USB", "Folder browser", 0));
        label_3->setText(QString());
        label_4->setText(QApplication::translate("LoggerConfPDLH2_USB", "Log browser", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_3), QApplication::translate("LoggerConfPDLH2_USB", "Data browser", 0));
        groupBox_3->setTitle(QApplication::translate("LoggerConfPDLH2_USB", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfPDLH2_USB", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfPDLH2_USB", "STOP", 0));
        Sensor_Checkbox->setText(QApplication::translate("LoggerConfPDLH2_USB", "Sensor", 0));
        Battery_Checkbox->setText(QApplication::translate("LoggerConfPDLH2_USB", "Battery", 0));
        label_5->setText(QApplication::translate("LoggerConfPDLH2_USB", "Export:", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab), QApplication::translate("LoggerConfPDLH2_USB", "Realtime monitoring", 0));
        btnReadSens->setText(QApplication::translate("LoggerConfPDLH2_USB", "READ SENS", 0));
        btnReadBatt->setText(QApplication::translate("LoggerConfPDLH2_USB", "READ BATT", 0));
        btnSDDetect->setText(QApplication::translate("LoggerConfPDLH2_USB", "SD DETECT", 0));
        btnExtLedOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "LCD LED ON", 0));
        btnChargerStat->setText(QApplication::translate("LoggerConfPDLH2_USB", "CHARGER STAT", 0));
        btnOpaPowOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "OPA POW ON", 0));
        btnIntLedOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "INT LED OFF", 0));
        btnExtLedOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "LCD LED OFF", 0));
        BtnMemPowOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "MEM POW OFF", 0));
        btnSensPowOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "SENS POW OFF", 0));
        btnOpaPowOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "OPA POW OFF", 0));
        btnIntLedOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "INT LED ON", 0));
        btnChargerOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "CHARGER OFF", 0));
        btnSensPowOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "SENS POW ON", 0));
        btnMemPowOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "MEM POW ON", 0));
        btnChargerOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "CHARGER ON", 0));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("LoggerConfPDLH2_USB", "General", 0));
        btnModemPowOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "POWER ON", 0));
        BtnModemPowOff->setText(QApplication::translate("LoggerConfPDLH2_USB", "POWER OFF", 0));
        btnSimStatus->setText(QApplication::translate("LoggerConfPDLH2_USB", "SIM STATUS", 0));
        btnSimPIN->setText(QApplication::translate("LoggerConfPDLH2_USB", "SET SIM PIN", 0));
        btnModemLogin->setText(QApplication::translate("LoggerConfPDLH2_USB", "LOGIN", 0));
        btnModemLogout->setText(QApplication::translate("LoggerConfPDLH2_USB", "LOGOUT", 0));
        btnModemSend->setText(QApplication::translate("LoggerConfPDLH2_USB", "SEND COMMAND", 0));
        toolBox->setItemText(toolBox->indexOf(page_5), QApplication::translate("LoggerConfPDLH2_USB", "Modem", 0));
        btnTCPAttach->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP ATTACH", 0));
        btnTCPDeattach->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP DEATTACH", 0));
        btnTCPConnect->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP CONNECT", 0));
        btnTCPDisconnect->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP DISCONNECT", 0));
        btnTCPStatus->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP STATUS", 0));
        tbxTCPSend->setText(QString());
        btnTCPSend->setText(QApplication::translate("LoggerConfPDLH2_USB", "TCP SEND", 0));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("LoggerConfPDLH2_USB", "GSM", 0));
        btnMAILActivate->setText(QApplication::translate("LoggerConfPDLH2_USB", "ACTIVATE MAIL SERVICE", 0));
        label_6->setText(QApplication::translate("LoggerConfPDLH2_USB", "Subject:", 0));
        tbxMAILSubject->setText(QString());
        label_7->setText(QApplication::translate("LoggerConfPDLH2_USB", "Text:", 0));
        tbxMAILText->setText(QString());
        btnMAILSend->setText(QApplication::translate("LoggerConfPDLH2_USB", "SEND", 0));
        toolBox->setItemText(toolBox->indexOf(page_6), QApplication::translate("LoggerConfPDLH2_USB", "E-Mail", 0));
        btnBTOn->setText(QApplication::translate("LoggerConfPDLH2_USB", "POWER ON", 0));
        btnBTDisconnect->setText(QApplication::translate("LoggerConfPDLH2_USB", "DISCONNECT ALL", 0));
        btnBTUnpair->setText(QApplication::translate("LoggerConfPDLH2_USB", "UNPAIR ALL", 0));
        btnBToff->setText(QApplication::translate("LoggerConfPDLH2_USB", "POWER OFF", 0));
        btnBTStatus->setText(QApplication::translate("LoggerConfPDLH2_USB", "STATUS", 0));
        toolBox->setItemText(toolBox->indexOf(page_3), QApplication::translate("LoggerConfPDLH2_USB", "Bluetooth", 0));
        btnSendCommand->setText(QApplication::translate("LoggerConfPDLH2_USB", "SEND COMMAND", 0));
        btnCustomTimeSet->setText(QApplication::translate("LoggerConfPDLH2_USB", "SET CUSTOM TIME", 0));
        toolBox->setItemText(toolBox->indexOf(page_4), QApplication::translate("LoggerConfPDLH2_USB", "Other", 0));
        autoclearCheckBox->setText(QApplication::translate("LoggerConfPDLH2_USB", "Auto clear", 0));
        clearTerminalBrowser->setText(QApplication::translate("LoggerConfPDLH2_USB", "Clear screen", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_4), QApplication::translate("LoggerConfPDLH2_USB", "Testing", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfPDLH2_USB: public Ui_LoggerConfPDLH2_USB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFPDLH2_USB_H
