#-------------------------------------------------
#
# Project created by QtCreator 2014-01-10T14:45:44
#
#-------------------------------------------------

QT       += core gui sql printsupport serialport network multimedia multimediawidgets

TARGET = Proba
TEMPLATE = app

SOURCES +=  main.cpp\
            mainwindow.cpp \
            qcustomplot.cpp \
            plottingwidget.cpp \
    detector.cpp \
    dialogwait.cpp \
    loggerinfo.cpp \
    deployedloggers.cpp \
    deployedloggershistory.cpp \
    readingthread.cpp \
    filereadingthread.cpp \
    writingthread.cpp \
    loggerconfpdlag_USB.cpp \
    treefolderview.cpp \
    newplaindialog.cpp \
    datatransfer.cpp \
    datavisualizer.cpp \
    qcustomdragdroptreewidget.cpp
HEADERS  += mainwindow.h \
    qcustomplot.h \
    plottingwidget.h \
    detector.h \
    dialogwait.h \
    loggerinfo.h \
    general.h \
    deployedloggers.h \
    deployedloggershistory.h \
    readingthread.h \
    filereadingthread.h \
    writingthread.h \
    loggerconfpdlag_USB.h \
    treefolderview.h \
    newplaindialog.h \
    datatransfer.h \
    datavisualizer.h \
    qcustomdragdroptreewidget.h
FORMS    += mainwindow.ui \
    plottingwidget.ui \
    dialogwait.ui \
    loggerinfo.ui \
    deployedloggers.ui \
    deployedloggershistory.ui \
    loggerconfpdlag_USB.ui \
    treefolderview.ui \
    newplaindialog.ui \
    datatransfer.ui \
    datavisualizer.ui

RESOURCES += \
    resources.qrc

RC_FILE = myapp.rc

OTHER_FILES +=

SUBDIRS += \
    configWizard.pro

DISTFILES +=
