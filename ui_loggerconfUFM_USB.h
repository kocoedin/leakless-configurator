/********************************************************************************
** Form generated from reading UI file 'loggerconfUFM_USB.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFUFM_USB_H
#define UI_LOGGERCONFUFM_USB_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QFrame>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTableWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfUFM_USB
{
public:
    QVBoxLayout *verticalLayout_5;
    QTabWidget *moduleOptions;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *InfoTreeWidget;
    QWidget *tab;
    QHBoxLayout *horizontalLayout_18;
    QVBoxLayout *verticalLayout_9;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnTDC1000_12_SaveTo;
    QLineEdit *tbxTDC1000_12_SaveToName;
    QHBoxLayout *horizontalLayout_8;
    QPushButton *btnTDC1000_12_LoadFrom;
    QComboBox *cbxTDC1000_12_LoadName;
    QPushButton *btnTDC1000_12_LoadFromRefresh;
    QTreeWidget *TDC1000_CH1_TreeWidget;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_14;
    QHBoxLayout *horizontalLayout_12;
    QPushButton *btnTDC7200_12_SaveTo_2;
    QLineEdit *tbxTDC7200_12_SaveToName_2;
    QHBoxLayout *horizontalLayout_15;
    QPushButton *btnTDC7200_12_LoadFrom_2;
    QComboBox *cbxTDC7200_12_LoadName_2;
    QPushButton *btnTDC7200_12_LoadFromRefresh_2;
    QTreeWidget *TDC7200_CH1_TreeWidget_2;
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox_3;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_9;
    QPushButton *btnTDC1000_34_SaveTo;
    QLineEdit *tbxTDC1000_34_SaveToName;
    QHBoxLayout *horizontalLayout_10;
    QPushButton *btnTDC1000_34_LoadFrom;
    QComboBox *cbxTDC1000_34_LoadName;
    QPushButton *btnTDC1000_34_LoadFromRefresh;
    QTreeWidget *TDC1000_CH2_TreeWidget;
    QGroupBox *groupBox_6;
    QVBoxLayout *verticalLayout_15;
    QHBoxLayout *horizontalLayout_16;
    QPushButton *btnTDC7200_34_SaveTo_2;
    QLineEdit *tbxTDC7200_34_SaveToName_2;
    QHBoxLayout *horizontalLayout_17;
    QPushButton *btnTDC7200_34_LoadFrom_2;
    QComboBox *cbxTDC7200_34_LoadName_2;
    QPushButton *btnTDC7200_34_LoadFromRefresh_2;
    QTreeWidget *TDC7200_CH2_TreeWidget_2;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox;
    QHBoxLayout *horizontalLayout_6;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnWriteFlash;
    QLabel *label;
    QLineEdit *tbxWriteSize;
    QSpacerItem *horizontalSpacer_4;
    QHBoxLayout *horizontalLayout_3;
    QPushButton *btnReadFlash;
    QLabel *label_2;
    QLineEdit *tbxReadSize;
    QSpacerItem *horizontalSpacer_5;
    QHBoxLayout *horizontalLayout_4;
    QPushButton *btnLoadHEX;
    QLabel *label_3;
    QLineEdit *txtHexPath;
    QSpacerItem *horizontalSpacer_6;
    QFrame *frame;
    QVBoxLayout *verticalLayout_7;
    QPushButton *btnEraseFlash;
    QTableWidget *tblMemory;
    QWidget *tab_5;
    QVBoxLayout *verticalLayout_13;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_14;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *Data1_Checkbox;
    QCheckBox *Data2_Checkbox;
    QCheckBox *Data3_Checkbox;
    QCheckBox *Data4_Checkbox;
    QCheckBox *Data5_Checkbox;
    QCheckBox *Data6_Checkbox;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_13;
    QVBoxLayout *verticalLayout_12;
    QCustomPlot *realtimeWidget;
    QSlider *horizontalSlider;
    QSlider *verticalSlider;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout;
    QToolBox *toolBox;
    QWidget *page_4;
    QVBoxLayout *verticalLayout_6;
    QGridLayout *gridLayout_3;
    QPushButton *btnSendCommand;
    QLineEdit *customCommand;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_11;
    QCheckBox *autoclearCheckBox;
    QSpacerItem *horizontalSpacer;
    QPushButton *clearTerminalBrowser;
    QTextEdit *terminalBrowser;
    QHBoxLayout *horizontalLayout_5;
    QProgressBar *progressBar;
    QPushButton *btnStop;

    void setupUi(QWidget *LoggerConfUFM_USB)
    {
        if (LoggerConfUFM_USB->objectName().isEmpty())
            LoggerConfUFM_USB->setObjectName(QStringLiteral("LoggerConfUFM_USB"));
        LoggerConfUFM_USB->resize(926, 682);
        verticalLayout_5 = new QVBoxLayout(LoggerConfUFM_USB);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        moduleOptions = new QTabWidget(LoggerConfUFM_USB);
        moduleOptions->setObjectName(QStringLiteral("moduleOptions"));
        moduleOptions->setEnabled(true);
        QFont font;
        font.setPointSize(9);
        moduleOptions->setFont(font);
        moduleOptions->setAutoFillBackground(false);
        moduleOptions->setIconSize(QSize(16, 16));
        moduleOptions->setElideMode(Qt::ElideNone);
        moduleOptions->setDocumentMode(false);
        moduleOptions->setTabsClosable(false);
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_2 = new QVBoxLayout(tab_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        InfoTreeWidget = new QTreeWidget(tab_2);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font1);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font2;
        font2.setPointSize(12);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/barcode/barcode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Microsoft/system_report/system_report-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/worldwide_location/worldwide_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/define_location/define_location-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Gardening/water_hose/water_hose-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/empty_flag/empty_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/filled_flag/filled_flag-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/Maps_and_Geolocation/map_marker/map_marker-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/settings2/settings2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electricity/electricity-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Transport/car_battery/car_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/win8/PNG/Folders/documents/documents-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/win8/PNG/Objects/timer/timer-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/document/document-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/radio_tower-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/server/server-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/win8/PNG/Folders/internet/internet-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/win8/PNG/Users/user/user-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/win8/PNG/Registration/password2/password2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/card_inserting/card_inserting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon25;
        icon25.addFile(QStringLiteral(":/icons/win8/PNG/Cell_Phones/sms/sms-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon26;
        icon26.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/bluetooth/bluetooth-256 - Copy.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon27;
        icon27.addFile(QStringLiteral(":/icons/win8/PNG/Forum/online/online-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon28;
        icon28.addFile(QStringLiteral(":/icons/win8/PNG/Buzz/gmail_login/gmail_login-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon29;
        icon29.addFile(QStringLiteral(":/icons/win8/PNG/Registration/email/email-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon30;
        icon30.addFile(QStringLiteral(":/icons/electrical_sensor-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon31;
        icon31.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon32;
        icon32.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/lock/lock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon33;
        icon33.addFile(QStringLiteral(":/icons/win8/PNG/Industry/display/display-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon34;
        icon34.addFile(QStringLiteral(":/icons/win8/PNG/Photo_Video/start/start-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon35;
        icon35.addFile(QStringLiteral(":/icons/win8/PNG/Objects/tear_off_calendar/tear_off_calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon36;
        icon36.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon37;
        icon37.addFile(QStringLiteral(":/icons/win8/PNG/Battery/almost_empty/almost_empty-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon38;
        icon38.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/calendar/calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon39;
        icon39.addFile(QStringLiteral(":/icons/win8/PNG/Ecommerce/alarm_clock/alarm_clock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon40;
        icon40.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon41;
        icon41.addFile(QStringLiteral(":/icons/win8/PNG/Battery/full_battery/full_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon42;
        icon42.addFile(QStringLiteral(":/icons/win8/PNG/Battery/50_percent/50_percent-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font2);
        __qtreewidgetitem1->setIcon(0, icon);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem5->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem6->setFont(0, font2);
        __qtreewidgetitem6->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem7->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem8->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem9->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem10->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem11->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem12->setFont(0, font2);
        __qtreewidgetitem12->setIcon(0, icon11);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem13->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem14->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem15->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem16->setFont(0, font2);
        __qtreewidgetitem16->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem17->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem18->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem19 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem19->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem20 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem20->setFont(0, font2);
        __qtreewidgetitem20->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem21 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem21->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem22 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem22->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem23 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem23->setIcon(0, icon20);
        QTreeWidgetItem *__qtreewidgetitem24 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem24->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem25 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem25->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem26 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem26->setIcon(0, icon23);
        QTreeWidgetItem *__qtreewidgetitem27 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem27->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem28 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem28->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem29 = new QTreeWidgetItem(__qtreewidgetitem20);
        __qtreewidgetitem29->setIcon(0, icon25);
        QTreeWidgetItem *__qtreewidgetitem30 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem30->setFont(0, font2);
        __qtreewidgetitem30->setIcon(0, icon26);
        QTreeWidgetItem *__qtreewidgetitem31 = new QTreeWidgetItem(__qtreewidgetitem30);
        __qtreewidgetitem31->setIcon(0, icon27);
        QTreeWidgetItem *__qtreewidgetitem32 = new QTreeWidgetItem(__qtreewidgetitem30);
        __qtreewidgetitem32->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem33 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem33->setFont(0, font2);
        __qtreewidgetitem33->setIcon(0, icon28);
        QTreeWidgetItem *__qtreewidgetitem34 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem34->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem35 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem35->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem36 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem36->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem37 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem37->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem38 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem38->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem39 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem39->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem40 = new QTreeWidgetItem(__qtreewidgetitem33);
        __qtreewidgetitem40->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem41 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem41->setFont(0, font2);
        __qtreewidgetitem41->setIcon(0, icon30);
        QTreeWidgetItem *__qtreewidgetitem42 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem42->setIcon(0, icon31);
        QTreeWidgetItem *__qtreewidgetitem43 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem43->setIcon(0, icon32);
        QTreeWidgetItem *__qtreewidgetitem44 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem44->setIcon(0, icon33);
        QTreeWidgetItem *__qtreewidgetitem45 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem45->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem46 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem46->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem47 = new QTreeWidgetItem(__qtreewidgetitem41);
        __qtreewidgetitem47->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem48 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem48->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem49 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem49->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem50 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem50->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem51 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem51->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem52 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem52->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem53 = new QTreeWidgetItem(__qtreewidgetitem49);
        __qtreewidgetitem53->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem54 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem54->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem55 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem55->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem56 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem56->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem57 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem57->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem58 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem58->setIcon(0, icon38);
        QTreeWidgetItem *__qtreewidgetitem59 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem59->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem60 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem60->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem61 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem61->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem62 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem62->setIcon(0, icon39);
        QTreeWidgetItem *__qtreewidgetitem63 = new QTreeWidgetItem(__qtreewidgetitem62);
        __qtreewidgetitem63->setIcon(0, icon40);
        QTreeWidgetItem *__qtreewidgetitem64 = new QTreeWidgetItem(__qtreewidgetitem62);
        __qtreewidgetitem64->setIcon(0, icon4);
        new QTreeWidgetItem(__qtreewidgetitem62);
        new QTreeWidgetItem(__qtreewidgetitem62);
        new QTreeWidgetItem(__qtreewidgetitem62);
        QTreeWidgetItem *__qtreewidgetitem65 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem65->setIcon(0, icon41);
        new QTreeWidgetItem(__qtreewidgetitem65);
        new QTreeWidgetItem(__qtreewidgetitem65);
        new QTreeWidgetItem(__qtreewidgetitem65);
        QTreeWidgetItem *__qtreewidgetitem66 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem66->setIcon(0, icon42);
        new QTreeWidgetItem(__qtreewidgetitem66);
        new QTreeWidgetItem(__qtreewidgetitem66);
        new QTreeWidgetItem(__qtreewidgetitem66);
        QTreeWidgetItem *__qtreewidgetitem67 = new QTreeWidgetItem(__qtreewidgetitem60);
        __qtreewidgetitem67->setIcon(0, icon37);
        new QTreeWidgetItem(__qtreewidgetitem67);
        new QTreeWidgetItem(__qtreewidgetitem67);
        new QTreeWidgetItem(__qtreewidgetitem67);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        verticalLayout_2->addWidget(InfoTreeWidget);

        moduleOptions->addTab(tab_2, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        horizontalLayout_18 = new QHBoxLayout(tab);
        horizontalLayout_18->setObjectName(QStringLiteral("horizontalLayout_18"));
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        groupBox_2 = new QGroupBox(tab);
        groupBox_2->setObjectName(QStringLiteral("groupBox_2"));
        verticalLayout_10 = new QVBoxLayout(groupBox_2);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        btnTDC1000_12_SaveTo = new QPushButton(groupBox_2);
        btnTDC1000_12_SaveTo->setObjectName(QStringLiteral("btnTDC1000_12_SaveTo"));

        horizontalLayout_7->addWidget(btnTDC1000_12_SaveTo);

        tbxTDC1000_12_SaveToName = new QLineEdit(groupBox_2);
        tbxTDC1000_12_SaveToName->setObjectName(QStringLiteral("tbxTDC1000_12_SaveToName"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tbxTDC1000_12_SaveToName->sizePolicy().hasHeightForWidth());
        tbxTDC1000_12_SaveToName->setSizePolicy(sizePolicy1);

        horizontalLayout_7->addWidget(tbxTDC1000_12_SaveToName);


        verticalLayout_10->addLayout(horizontalLayout_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        btnTDC1000_12_LoadFrom = new QPushButton(groupBox_2);
        btnTDC1000_12_LoadFrom->setObjectName(QStringLiteral("btnTDC1000_12_LoadFrom"));

        horizontalLayout_8->addWidget(btnTDC1000_12_LoadFrom);

        cbxTDC1000_12_LoadName = new QComboBox(groupBox_2);
        cbxTDC1000_12_LoadName->setObjectName(QStringLiteral("cbxTDC1000_12_LoadName"));

        horizontalLayout_8->addWidget(cbxTDC1000_12_LoadName);

        btnTDC1000_12_LoadFromRefresh = new QPushButton(groupBox_2);
        btnTDC1000_12_LoadFromRefresh->setObjectName(QStringLiteral("btnTDC1000_12_LoadFromRefresh"));
        QSizePolicy sizePolicy2(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(btnTDC1000_12_LoadFromRefresh->sizePolicy().hasHeightForWidth());
        btnTDC1000_12_LoadFromRefresh->setSizePolicy(sizePolicy2);
        QIcon icon43;
        icon43.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/refresh/refresh-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnTDC1000_12_LoadFromRefresh->setIcon(icon43);

        horizontalLayout_8->addWidget(btnTDC1000_12_LoadFromRefresh);


        verticalLayout_10->addLayout(horizontalLayout_8);


        verticalLayout_9->addWidget(groupBox_2);

        TDC1000_CH1_TreeWidget = new QTreeWidget(tab);
        QTreeWidgetItem *__qtreewidgetitem68 = new QTreeWidgetItem();
        __qtreewidgetitem68->setFont(1, font1);
        TDC1000_CH1_TreeWidget->setHeaderItem(__qtreewidgetitem68);
        QIcon icon44;
        icon44.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/temperature/temperature-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem69 = new QTreeWidgetItem(TDC1000_CH1_TreeWidget);
        __qtreewidgetitem69->setFont(0, font2);
        __qtreewidgetitem69->setIcon(0, icon16);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem69);
        QTreeWidgetItem *__qtreewidgetitem70 = new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem70);
        new QTreeWidgetItem(__qtreewidgetitem70);
        new QTreeWidgetItem(__qtreewidgetitem70);
        new QTreeWidgetItem(__qtreewidgetitem70);
        new QTreeWidgetItem(__qtreewidgetitem70);
        QTreeWidgetItem *__qtreewidgetitem71 = new QTreeWidgetItem(__qtreewidgetitem69);
        new QTreeWidgetItem(__qtreewidgetitem71);
        new QTreeWidgetItem(__qtreewidgetitem71);
        new QTreeWidgetItem(__qtreewidgetitem71);
        new QTreeWidgetItem(__qtreewidgetitem71);
        QTreeWidgetItem *__qtreewidgetitem72 = new QTreeWidgetItem(TDC1000_CH1_TreeWidget);
        __qtreewidgetitem72->setFont(1, font2);
        __qtreewidgetitem72->setFont(0, font2);
        __qtreewidgetitem72->setIcon(0, icon44);
        new QTreeWidgetItem(__qtreewidgetitem72);
        new QTreeWidgetItem(__qtreewidgetitem72);
        new QTreeWidgetItem(__qtreewidgetitem72);
        TDC1000_CH1_TreeWidget->setObjectName(QStringLiteral("TDC1000_CH1_TreeWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(TDC1000_CH1_TreeWidget->sizePolicy().hasHeightForWidth());
        TDC1000_CH1_TreeWidget->setSizePolicy(sizePolicy3);
        TDC1000_CH1_TreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        TDC1000_CH1_TreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        TDC1000_CH1_TreeWidget->setAlternatingRowColors(true);
        TDC1000_CH1_TreeWidget->setAutoExpandDelay(-1);
        TDC1000_CH1_TreeWidget->setUniformRowHeights(false);
        TDC1000_CH1_TreeWidget->setAnimated(true);
        TDC1000_CH1_TreeWidget->setWordWrap(true);
        TDC1000_CH1_TreeWidget->setHeaderHidden(true);
        TDC1000_CH1_TreeWidget->header()->setVisible(false);
        TDC1000_CH1_TreeWidget->header()->setCascadingSectionResizes(false);
        TDC1000_CH1_TreeWidget->header()->setHighlightSections(false);
        TDC1000_CH1_TreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        TDC1000_CH1_TreeWidget->header()->setStretchLastSection(true);

        verticalLayout_9->addWidget(TDC1000_CH1_TreeWidget);

        groupBox_5 = new QGroupBox(tab);
        groupBox_5->setObjectName(QStringLiteral("groupBox_5"));
        verticalLayout_14 = new QVBoxLayout(groupBox_5);
        verticalLayout_14->setObjectName(QStringLiteral("verticalLayout_14"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        btnTDC7200_12_SaveTo_2 = new QPushButton(groupBox_5);
        btnTDC7200_12_SaveTo_2->setObjectName(QStringLiteral("btnTDC7200_12_SaveTo_2"));

        horizontalLayout_12->addWidget(btnTDC7200_12_SaveTo_2);

        tbxTDC7200_12_SaveToName_2 = new QLineEdit(groupBox_5);
        tbxTDC7200_12_SaveToName_2->setObjectName(QStringLiteral("tbxTDC7200_12_SaveToName_2"));
        sizePolicy1.setHeightForWidth(tbxTDC7200_12_SaveToName_2->sizePolicy().hasHeightForWidth());
        tbxTDC7200_12_SaveToName_2->setSizePolicy(sizePolicy1);

        horizontalLayout_12->addWidget(tbxTDC7200_12_SaveToName_2);


        verticalLayout_14->addLayout(horizontalLayout_12);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QStringLiteral("horizontalLayout_15"));
        btnTDC7200_12_LoadFrom_2 = new QPushButton(groupBox_5);
        btnTDC7200_12_LoadFrom_2->setObjectName(QStringLiteral("btnTDC7200_12_LoadFrom_2"));

        horizontalLayout_15->addWidget(btnTDC7200_12_LoadFrom_2);

        cbxTDC7200_12_LoadName_2 = new QComboBox(groupBox_5);
        cbxTDC7200_12_LoadName_2->setObjectName(QStringLiteral("cbxTDC7200_12_LoadName_2"));

        horizontalLayout_15->addWidget(cbxTDC7200_12_LoadName_2);

        btnTDC7200_12_LoadFromRefresh_2 = new QPushButton(groupBox_5);
        btnTDC7200_12_LoadFromRefresh_2->setObjectName(QStringLiteral("btnTDC7200_12_LoadFromRefresh_2"));
        sizePolicy2.setHeightForWidth(btnTDC7200_12_LoadFromRefresh_2->sizePolicy().hasHeightForWidth());
        btnTDC7200_12_LoadFromRefresh_2->setSizePolicy(sizePolicy2);
        btnTDC7200_12_LoadFromRefresh_2->setIcon(icon43);

        horizontalLayout_15->addWidget(btnTDC7200_12_LoadFromRefresh_2);


        verticalLayout_14->addLayout(horizontalLayout_15);


        verticalLayout_9->addWidget(groupBox_5);

        TDC7200_CH1_TreeWidget_2 = new QTreeWidget(tab);
        QTreeWidgetItem *__qtreewidgetitem73 = new QTreeWidgetItem();
        __qtreewidgetitem73->setFont(1, font1);
        TDC7200_CH1_TreeWidget_2->setHeaderItem(__qtreewidgetitem73);
        QTreeWidgetItem *__qtreewidgetitem74 = new QTreeWidgetItem(TDC7200_CH1_TreeWidget_2);
        __qtreewidgetitem74->setFont(0, font2);
        __qtreewidgetitem74->setIcon(0, icon16);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        new QTreeWidgetItem(__qtreewidgetitem74);
        TDC7200_CH1_TreeWidget_2->setObjectName(QStringLiteral("TDC7200_CH1_TreeWidget_2"));
        sizePolicy3.setHeightForWidth(TDC7200_CH1_TreeWidget_2->sizePolicy().hasHeightForWidth());
        TDC7200_CH1_TreeWidget_2->setSizePolicy(sizePolicy3);
        TDC7200_CH1_TreeWidget_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        TDC7200_CH1_TreeWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
        TDC7200_CH1_TreeWidget_2->setAlternatingRowColors(true);
        TDC7200_CH1_TreeWidget_2->setAutoExpandDelay(-1);
        TDC7200_CH1_TreeWidget_2->setUniformRowHeights(false);
        TDC7200_CH1_TreeWidget_2->setAnimated(true);
        TDC7200_CH1_TreeWidget_2->setWordWrap(true);
        TDC7200_CH1_TreeWidget_2->setHeaderHidden(true);
        TDC7200_CH1_TreeWidget_2->header()->setVisible(false);
        TDC7200_CH1_TreeWidget_2->header()->setCascadingSectionResizes(false);
        TDC7200_CH1_TreeWidget_2->header()->setHighlightSections(false);
        TDC7200_CH1_TreeWidget_2->header()->setProperty("showSortIndicator", QVariant(false));
        TDC7200_CH1_TreeWidget_2->header()->setStretchLastSection(true);

        verticalLayout_9->addWidget(TDC7200_CH1_TreeWidget_2);


        horizontalLayout_18->addLayout(verticalLayout_9);

        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        verticalLayout_11 = new QVBoxLayout(groupBox_3);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        btnTDC1000_34_SaveTo = new QPushButton(groupBox_3);
        btnTDC1000_34_SaveTo->setObjectName(QStringLiteral("btnTDC1000_34_SaveTo"));

        horizontalLayout_9->addWidget(btnTDC1000_34_SaveTo);

        tbxTDC1000_34_SaveToName = new QLineEdit(groupBox_3);
        tbxTDC1000_34_SaveToName->setObjectName(QStringLiteral("tbxTDC1000_34_SaveToName"));
        sizePolicy1.setHeightForWidth(tbxTDC1000_34_SaveToName->sizePolicy().hasHeightForWidth());
        tbxTDC1000_34_SaveToName->setSizePolicy(sizePolicy1);

        horizontalLayout_9->addWidget(tbxTDC1000_34_SaveToName);


        verticalLayout_11->addLayout(horizontalLayout_9);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        btnTDC1000_34_LoadFrom = new QPushButton(groupBox_3);
        btnTDC1000_34_LoadFrom->setObjectName(QStringLiteral("btnTDC1000_34_LoadFrom"));

        horizontalLayout_10->addWidget(btnTDC1000_34_LoadFrom);

        cbxTDC1000_34_LoadName = new QComboBox(groupBox_3);
        cbxTDC1000_34_LoadName->setObjectName(QStringLiteral("cbxTDC1000_34_LoadName"));

        horizontalLayout_10->addWidget(cbxTDC1000_34_LoadName);

        btnTDC1000_34_LoadFromRefresh = new QPushButton(groupBox_3);
        btnTDC1000_34_LoadFromRefresh->setObjectName(QStringLiteral("btnTDC1000_34_LoadFromRefresh"));
        sizePolicy2.setHeightForWidth(btnTDC1000_34_LoadFromRefresh->sizePolicy().hasHeightForWidth());
        btnTDC1000_34_LoadFromRefresh->setSizePolicy(sizePolicy2);
        btnTDC1000_34_LoadFromRefresh->setIcon(icon43);

        horizontalLayout_10->addWidget(btnTDC1000_34_LoadFromRefresh);


        verticalLayout_11->addLayout(horizontalLayout_10);


        verticalLayout_8->addWidget(groupBox_3);

        TDC1000_CH2_TreeWidget = new QTreeWidget(tab);
        QTreeWidgetItem *__qtreewidgetitem75 = new QTreeWidgetItem();
        __qtreewidgetitem75->setFont(1, font1);
        TDC1000_CH2_TreeWidget->setHeaderItem(__qtreewidgetitem75);
        QTreeWidgetItem *__qtreewidgetitem76 = new QTreeWidgetItem(TDC1000_CH2_TreeWidget);
        __qtreewidgetitem76->setFont(0, font2);
        __qtreewidgetitem76->setIcon(0, icon16);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem76);
        QTreeWidgetItem *__qtreewidgetitem77 = new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem77);
        new QTreeWidgetItem(__qtreewidgetitem77);
        new QTreeWidgetItem(__qtreewidgetitem77);
        new QTreeWidgetItem(__qtreewidgetitem77);
        new QTreeWidgetItem(__qtreewidgetitem77);
        QTreeWidgetItem *__qtreewidgetitem78 = new QTreeWidgetItem(__qtreewidgetitem76);
        new QTreeWidgetItem(__qtreewidgetitem78);
        new QTreeWidgetItem(__qtreewidgetitem78);
        new QTreeWidgetItem(__qtreewidgetitem78);
        new QTreeWidgetItem(__qtreewidgetitem78);
        QTreeWidgetItem *__qtreewidgetitem79 = new QTreeWidgetItem(TDC1000_CH2_TreeWidget);
        __qtreewidgetitem79->setFont(1, font2);
        __qtreewidgetitem79->setFont(0, font2);
        __qtreewidgetitem79->setIcon(0, icon44);
        new QTreeWidgetItem(__qtreewidgetitem79);
        new QTreeWidgetItem(__qtreewidgetitem79);
        new QTreeWidgetItem(__qtreewidgetitem79);
        TDC1000_CH2_TreeWidget->setObjectName(QStringLiteral("TDC1000_CH2_TreeWidget"));
        sizePolicy3.setHeightForWidth(TDC1000_CH2_TreeWidget->sizePolicy().hasHeightForWidth());
        TDC1000_CH2_TreeWidget->setSizePolicy(sizePolicy3);
        TDC1000_CH2_TreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        TDC1000_CH2_TreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        TDC1000_CH2_TreeWidget->setAlternatingRowColors(true);
        TDC1000_CH2_TreeWidget->setAutoExpandDelay(-1);
        TDC1000_CH2_TreeWidget->setUniformRowHeights(false);
        TDC1000_CH2_TreeWidget->setAnimated(true);
        TDC1000_CH2_TreeWidget->setWordWrap(true);
        TDC1000_CH2_TreeWidget->setHeaderHidden(true);
        TDC1000_CH2_TreeWidget->header()->setVisible(false);
        TDC1000_CH2_TreeWidget->header()->setCascadingSectionResizes(false);
        TDC1000_CH2_TreeWidget->header()->setHighlightSections(false);
        TDC1000_CH2_TreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        TDC1000_CH2_TreeWidget->header()->setStretchLastSection(true);

        verticalLayout_8->addWidget(TDC1000_CH2_TreeWidget);

        groupBox_6 = new QGroupBox(tab);
        groupBox_6->setObjectName(QStringLiteral("groupBox_6"));
        verticalLayout_15 = new QVBoxLayout(groupBox_6);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setObjectName(QStringLiteral("horizontalLayout_16"));
        btnTDC7200_34_SaveTo_2 = new QPushButton(groupBox_6);
        btnTDC7200_34_SaveTo_2->setObjectName(QStringLiteral("btnTDC7200_34_SaveTo_2"));

        horizontalLayout_16->addWidget(btnTDC7200_34_SaveTo_2);

        tbxTDC7200_34_SaveToName_2 = new QLineEdit(groupBox_6);
        tbxTDC7200_34_SaveToName_2->setObjectName(QStringLiteral("tbxTDC7200_34_SaveToName_2"));
        sizePolicy1.setHeightForWidth(tbxTDC7200_34_SaveToName_2->sizePolicy().hasHeightForWidth());
        tbxTDC7200_34_SaveToName_2->setSizePolicy(sizePolicy1);

        horizontalLayout_16->addWidget(tbxTDC7200_34_SaveToName_2);


        verticalLayout_15->addLayout(horizontalLayout_16);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setObjectName(QStringLiteral("horizontalLayout_17"));
        btnTDC7200_34_LoadFrom_2 = new QPushButton(groupBox_6);
        btnTDC7200_34_LoadFrom_2->setObjectName(QStringLiteral("btnTDC7200_34_LoadFrom_2"));

        horizontalLayout_17->addWidget(btnTDC7200_34_LoadFrom_2);

        cbxTDC7200_34_LoadName_2 = new QComboBox(groupBox_6);
        cbxTDC7200_34_LoadName_2->setObjectName(QStringLiteral("cbxTDC7200_34_LoadName_2"));

        horizontalLayout_17->addWidget(cbxTDC7200_34_LoadName_2);

        btnTDC7200_34_LoadFromRefresh_2 = new QPushButton(groupBox_6);
        btnTDC7200_34_LoadFromRefresh_2->setObjectName(QStringLiteral("btnTDC7200_34_LoadFromRefresh_2"));
        sizePolicy2.setHeightForWidth(btnTDC7200_34_LoadFromRefresh_2->sizePolicy().hasHeightForWidth());
        btnTDC7200_34_LoadFromRefresh_2->setSizePolicy(sizePolicy2);
        btnTDC7200_34_LoadFromRefresh_2->setIcon(icon43);

        horizontalLayout_17->addWidget(btnTDC7200_34_LoadFromRefresh_2);


        verticalLayout_15->addLayout(horizontalLayout_17);


        verticalLayout_8->addWidget(groupBox_6);

        TDC7200_CH2_TreeWidget_2 = new QTreeWidget(tab);
        QTreeWidgetItem *__qtreewidgetitem80 = new QTreeWidgetItem();
        __qtreewidgetitem80->setFont(1, font1);
        TDC7200_CH2_TreeWidget_2->setHeaderItem(__qtreewidgetitem80);
        QTreeWidgetItem *__qtreewidgetitem81 = new QTreeWidgetItem(TDC7200_CH2_TreeWidget_2);
        __qtreewidgetitem81->setFont(0, font2);
        __qtreewidgetitem81->setIcon(0, icon16);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        new QTreeWidgetItem(__qtreewidgetitem81);
        TDC7200_CH2_TreeWidget_2->setObjectName(QStringLiteral("TDC7200_CH2_TreeWidget_2"));
        sizePolicy3.setHeightForWidth(TDC7200_CH2_TreeWidget_2->sizePolicy().hasHeightForWidth());
        TDC7200_CH2_TreeWidget_2->setSizePolicy(sizePolicy3);
        TDC7200_CH2_TreeWidget_2->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        TDC7200_CH2_TreeWidget_2->setEditTriggers(QAbstractItemView::NoEditTriggers);
        TDC7200_CH2_TreeWidget_2->setAlternatingRowColors(true);
        TDC7200_CH2_TreeWidget_2->setAutoExpandDelay(-1);
        TDC7200_CH2_TreeWidget_2->setUniformRowHeights(false);
        TDC7200_CH2_TreeWidget_2->setAnimated(true);
        TDC7200_CH2_TreeWidget_2->setWordWrap(true);
        TDC7200_CH2_TreeWidget_2->setHeaderHidden(true);
        TDC7200_CH2_TreeWidget_2->header()->setVisible(false);
        TDC7200_CH2_TreeWidget_2->header()->setCascadingSectionResizes(false);
        TDC7200_CH2_TreeWidget_2->header()->setHighlightSections(false);
        TDC7200_CH2_TreeWidget_2->header()->setProperty("showSortIndicator", QVariant(false));
        TDC7200_CH2_TreeWidget_2->header()->setStretchLastSection(true);

        verticalLayout_8->addWidget(TDC7200_CH2_TreeWidget_2);


        horizontalLayout_18->addLayout(verticalLayout_8);

        moduleOptions->addTab(tab, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_4 = new QVBoxLayout(tab_3);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        groupBox = new QGroupBox(tab_3);
        groupBox->setObjectName(QStringLiteral("groupBox"));
        horizontalLayout_6 = new QHBoxLayout(groupBox);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btnWriteFlash = new QPushButton(groupBox);
        btnWriteFlash->setObjectName(QStringLiteral("btnWriteFlash"));

        horizontalLayout_2->addWidget(btnWriteFlash);

        label = new QLabel(groupBox);
        label->setObjectName(QStringLiteral("label"));

        horizontalLayout_2->addWidget(label);

        tbxWriteSize = new QLineEdit(groupBox);
        tbxWriteSize->setObjectName(QStringLiteral("tbxWriteSize"));
        tbxWriteSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_2->addWidget(tbxWriteSize);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_4);


        verticalLayout_3->addLayout(horizontalLayout_2);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        btnReadFlash = new QPushButton(groupBox);
        btnReadFlash->setObjectName(QStringLiteral("btnReadFlash"));

        horizontalLayout_3->addWidget(btnReadFlash);

        label_2 = new QLabel(groupBox);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        tbxReadSize = new QLineEdit(groupBox);
        tbxReadSize->setObjectName(QStringLiteral("tbxReadSize"));
        tbxReadSize->setAlignment(Qt::AlignRight|Qt::AlignTrailing|Qt::AlignVCenter);

        horizontalLayout_3->addWidget(tbxReadSize);

        horizontalSpacer_5 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_5);


        verticalLayout_3->addLayout(horizontalLayout_3);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        btnLoadHEX = new QPushButton(groupBox);
        btnLoadHEX->setObjectName(QStringLiteral("btnLoadHEX"));

        horizontalLayout_4->addWidget(btnLoadHEX);

        label_3 = new QLabel(groupBox);
        label_3->setObjectName(QStringLiteral("label_3"));

        horizontalLayout_4->addWidget(label_3);

        txtHexPath = new QLineEdit(groupBox);
        txtHexPath->setObjectName(QStringLiteral("txtHexPath"));

        horizontalLayout_4->addWidget(txtHexPath);

        horizontalSpacer_6 = new QSpacerItem(80, 20, QSizePolicy::MinimumExpanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_6);


        verticalLayout_3->addLayout(horizontalLayout_4);


        horizontalLayout_6->addLayout(verticalLayout_3);

        frame = new QFrame(groupBox);
        frame->setObjectName(QStringLiteral("frame"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        verticalLayout_7 = new QVBoxLayout(frame);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        btnEraseFlash = new QPushButton(frame);
        btnEraseFlash->setObjectName(QStringLiteral("btnEraseFlash"));

        verticalLayout_7->addWidget(btnEraseFlash);


        horizontalLayout_6->addWidget(frame);


        verticalLayout_4->addWidget(groupBox);

        tblMemory = new QTableWidget(tab_3);
        tblMemory->setObjectName(QStringLiteral("tblMemory"));
        tblMemory->horizontalHeader()->setDefaultSectionSize(70);
        tblMemory->horizontalHeader()->setStretchLastSection(false);

        verticalLayout_4->addWidget(tblMemory);

        moduleOptions->addTab(tab_3, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        verticalLayout_13 = new QVBoxLayout(tab_5);
        verticalLayout_13->setObjectName(QStringLiteral("verticalLayout_13"));
        groupBox_4 = new QGroupBox(tab_5);
        groupBox_4->setObjectName(QStringLiteral("groupBox_4"));
        horizontalLayout_14 = new QHBoxLayout(groupBox_4);
        horizontalLayout_14->setObjectName(QStringLiteral("horizontalLayout_14"));
        Realtime_START = new QPushButton(groupBox_4);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
        Realtime_START->setIcon(icon31);
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout_14->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_4);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
        QIcon icon45;
        icon45.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon45);
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout_14->addWidget(Realtime_STOP);

        Data1_Checkbox = new QCheckBox(groupBox_4);
        Data1_Checkbox->setObjectName(QStringLiteral("Data1_Checkbox"));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        Data1_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data1_Checkbox);

        Data2_Checkbox = new QCheckBox(groupBox_4);
        Data2_Checkbox->setObjectName(QStringLiteral("Data2_Checkbox"));
        Data2_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data2_Checkbox);

        Data3_Checkbox = new QCheckBox(groupBox_4);
        Data3_Checkbox->setObjectName(QStringLiteral("Data3_Checkbox"));
        Data3_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data3_Checkbox);

        Data4_Checkbox = new QCheckBox(groupBox_4);
        Data4_Checkbox->setObjectName(QStringLiteral("Data4_Checkbox"));
        Data4_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data4_Checkbox);

        Data5_Checkbox = new QCheckBox(groupBox_4);
        Data5_Checkbox->setObjectName(QStringLiteral("Data5_Checkbox"));
        Data5_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data5_Checkbox);

        Data6_Checkbox = new QCheckBox(groupBox_4);
        Data6_Checkbox->setObjectName(QStringLiteral("Data6_Checkbox"));
        Data6_Checkbox->setFont(font3);

        horizontalLayout_14->addWidget(Data6_Checkbox);

        horizontalSpacer_7 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_14->addItem(horizontalSpacer_7);


        verticalLayout_13->addWidget(groupBox_4);

        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QStringLiteral("horizontalLayout_13"));
        verticalLayout_12 = new QVBoxLayout();
        verticalLayout_12->setObjectName(QStringLiteral("verticalLayout_12"));
        realtimeWidget = new QCustomPlot(tab_5);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        sizePolicy3.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy3);

        verticalLayout_12->addWidget(realtimeWidget);

        horizontalSlider = new QSlider(tab_5);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(2);
        horizontalSlider->setMaximum(100);
        horizontalSlider->setPageStep(2);
        horizontalSlider->setValue(10);
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_12->addWidget(horizontalSlider);


        horizontalLayout_13->addLayout(verticalLayout_12);

        verticalSlider = new QSlider(tab_5);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setMinimum(1);
        verticalSlider->setMaximum(30);
        verticalSlider->setPageStep(2);
        verticalSlider->setValue(5);
        verticalSlider->setOrientation(Qt::Vertical);

        horizontalLayout_13->addWidget(verticalSlider);


        verticalLayout_13->addLayout(horizontalLayout_13);

        moduleOptions->addTab(tab_5, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        horizontalLayout = new QHBoxLayout(tab_4);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        toolBox = new QToolBox(tab_4);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        page_4->setGeometry(QRect(0, 0, 439, 558));
        verticalLayout_6 = new QVBoxLayout(page_4);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        gridLayout_3->setSizeConstraint(QLayout::SetDefaultConstraint);
        btnSendCommand = new QPushButton(page_4);
        btnSendCommand->setObjectName(QStringLiteral("btnSendCommand"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnSendCommand->sizePolicy().hasHeightForWidth());
        btnSendCommand->setSizePolicy(sizePolicy4);

        gridLayout_3->addWidget(btnSendCommand, 0, 1, 1, 1);

        customCommand = new QLineEdit(page_4);
        customCommand->setObjectName(QStringLiteral("customCommand"));
        QSizePolicy sizePolicy5(QSizePolicy::Expanding, QSizePolicy::Minimum);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(customCommand->sizePolicy().hasHeightForWidth());
        customCommand->setSizePolicy(sizePolicy5);

        gridLayout_3->addWidget(customCommand, 0, 0, 1, 1);


        verticalLayout_6->addLayout(gridLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);

        verticalLayout_6->addItem(verticalSpacer);

        QIcon icon46;
        icon46.addFile(QStringLiteral(":/icons/win8/PNG/System/help/help-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_4, icon46, QStringLiteral("Other"));

        horizontalLayout->addWidget(toolBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        autoclearCheckBox = new QCheckBox(tab_4);
        autoclearCheckBox->setObjectName(QStringLiteral("autoclearCheckBox"));
        autoclearCheckBox->setChecked(true);

        horizontalLayout_11->addWidget(autoclearCheckBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer);

        clearTerminalBrowser = new QPushButton(tab_4);
        clearTerminalBrowser->setObjectName(QStringLiteral("clearTerminalBrowser"));

        horizontalLayout_11->addWidget(clearTerminalBrowser);


        verticalLayout->addLayout(horizontalLayout_11);

        terminalBrowser = new QTextEdit(tab_4);
        terminalBrowser->setObjectName(QStringLiteral("terminalBrowser"));

        verticalLayout->addWidget(terminalBrowser);


        horizontalLayout->addLayout(verticalLayout);

        moduleOptions->addTab(tab_4, QString());

        verticalLayout_5->addWidget(moduleOptions);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        progressBar = new QProgressBar(LoggerConfUFM_USB);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setValue(0);

        horizontalLayout_5->addWidget(progressBar);

        btnStop = new QPushButton(LoggerConfUFM_USB);
        btnStop->setObjectName(QStringLiteral("btnStop"));

        horizontalLayout_5->addWidget(btnStop);


        verticalLayout_5->addLayout(horizontalLayout_5);


        retranslateUi(LoggerConfUFM_USB);

        moduleOptions->setCurrentIndex(1);
        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LoggerConfUFM_USB);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfUFM_USB)
    {
        LoggerConfUFM_USB->setWindowTitle(QApplication::translate("LoggerConfUFM_USB", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfUFM_USB", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfUFM_USB", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfUFM_USB", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfUFM_USB", "IDN STRING 0 10 RW", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfUFM_USB", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfUFM_USB", "FIRM STRING 0 10 R", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfUFM_USB", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfUFM_USB", "HIDE", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfUFM_USB", "ID", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(1, QApplication::translate("LoggerConfUFM_USB", "SRI UINT16 100 10000 RW", 0));
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfUFM_USB", "Status report interval", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfUFM_USB", "Location information", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(1, QApplication::translate("LoggerConfUFM_USB", "MELOC STRING 0 22 RW", 0));
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfUFM_USB", "Location", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem6->child(1);
        ___qtreewidgetitem8->setText(1, QApplication::translate("LoggerConfUFM_USB", "METYPE STRING 0 22 RW", 0));
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfUFM_USB", "Measurement type", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem6->child(2);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfUFM_USB", "MELAT STRING 0 22 RW", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfUFM_USB", "Latitude", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem6->child(3);
        ___qtreewidgetitem10->setText(1, QApplication::translate("LoggerConfUFM_USB", "MELNG STRING 0 22 RW", 0));
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfUFM_USB", "Longitude", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem6->child(4);
        ___qtreewidgetitem11->setText(1, QApplication::translate("LoggerConfUFM_USB", "MEHGHT STRING 0 22 RW", 0));
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfUFM_USB", "Height", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfUFM_USB", "Calibration", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem12->child(0);
        ___qtreewidgetitem13->setText(1, QApplication::translate("LoggerConfUFM_USB", "SCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sensor calibration coefficient", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem12->child(1);
        ___qtreewidgetitem14->setText(1, QApplication::translate("LoggerConfUFM_USB", "CCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfUFM_USB", "Charging current calibration ", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem12->child(2);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfUFM_USB", "BAC UINT32 0 100000 RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery capacity", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem16->setText(1, QApplication::translate("LoggerConfUFM_USB", "HIDE", 0));
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfUFM_USB", "Logging", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfUFM_USB", "TF ENUM hh:mm:ss:dd:mm:yyyy hh:mm:ss_dd/MM/yyyy hh:mm:ss_dd/MM/yy RW HIDE", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfUFM_USB", "Time format", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem16->child(1);
        ___qtreewidgetitem18->setText(1, QApplication::translate("LoggerConfUFM_USB", "LF ENUM Format_1 Format_2 Format_3 RW HIDE", 0));
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfUFM_USB", "Log format", 0));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem16->child(2);
        ___qtreewidgetitem19->setText(1, QApplication::translate("LoggerConfUFM_USB", "LSN STRING 0 10 RW HIDE", 0));
        ___qtreewidgetitem19->setText(0, QApplication::translate("LoggerConfUFM_USB", "Free string", 0));
        QTreeWidgetItem *___qtreewidgetitem20 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem20->setText(0, QApplication::translate("LoggerConfUFM_USB", "GSM settings", 0));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem20->child(0);
        ___qtreewidgetitem21->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMIP STRING 0 22 RW", 0));
        ___qtreewidgetitem21->setText(0, QApplication::translate("LoggerConfUFM_USB", "Server IP", 0));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem20->child(1);
        ___qtreewidgetitem22->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMPRT STRING 0 10 RW", 0));
        ___qtreewidgetitem22->setText(0, QApplication::translate("LoggerConfUFM_USB", "Server port", 0));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem20->child(2);
        ___qtreewidgetitem23->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMAPN STRING 0 22 RW", 0));
        ___qtreewidgetitem23->setText(0, QApplication::translate("LoggerConfUFM_USB", "Network APN", 0));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem20->child(3);
        ___qtreewidgetitem24->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMUSR STRING 0 22 RW", 0));
        ___qtreewidgetitem24->setText(0, QApplication::translate("LoggerConfUFM_USB", "Network username", 0));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem20->child(4);
        ___qtreewidgetitem25->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMPWD STRING 0 22 RW", 0));
        ___qtreewidgetitem25->setText(0, QApplication::translate("LoggerConfUFM_USB", "Network password", 0));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem20->child(5);
        ___qtreewidgetitem26->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMPIN STRING 0 10 RW", 0));
        ___qtreewidgetitem26->setText(0, QApplication::translate("LoggerConfUFM_USB", "SIM pin", 0));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem20->child(6);
        ___qtreewidgetitem27->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMRC UINT32 1 10 RW", 0));
        ___qtreewidgetitem27->setText(0, QApplication::translate("LoggerConfUFM_USB", "Reconnect count", 0));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem20->child(7);
        ___qtreewidgetitem28->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMWT UINT32 0 120 RW", 0));
        ___qtreewidgetitem28->setText(0, QApplication::translate("LoggerConfUFM_USB", "Reconnect wait", 0));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem20->child(8);
        ___qtreewidgetitem29->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMNUM STRING 0 22 RW", 0));
        ___qtreewidgetitem29->setText(0, QApplication::translate("LoggerConfUFM_USB", "SMS number", 0));
        QTreeWidgetItem *___qtreewidgetitem30 = InfoTreeWidget->topLevelItem(5);
        ___qtreewidgetitem30->setText(0, QApplication::translate("LoggerConfUFM_USB", "Bluetooth", 0));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem30->child(0);
        ___qtreewidgetitem31->setText(1, QApplication::translate("LoggerConfUFM_USB", "BTACT TF RW", 0));
        ___qtreewidgetitem31->setText(0, QApplication::translate("LoggerConfUFM_USB", "Active", 0));
        QTreeWidgetItem *___qtreewidgetitem32 = ___qtreewidgetitem30->child(1);
        ___qtreewidgetitem32->setText(1, QApplication::translate("LoggerConfUFM_USB", "BTHOST STRING 0 22 RW", 0));
        ___qtreewidgetitem32->setText(0, QApplication::translate("LoggerConfUFM_USB", "Host ID", 0));
        QTreeWidgetItem *___qtreewidgetitem33 = InfoTreeWidget->topLevelItem(6);
        ___qtreewidgetitem33->setText(0, QApplication::translate("LoggerConfUFM_USB", "Mail", 0));
        QTreeWidgetItem *___qtreewidgetitem34 = ___qtreewidgetitem33->child(0);
        ___qtreewidgetitem34->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILSRV STRING 0 22 RW", 0));
        ___qtreewidgetitem34->setText(0, QApplication::translate("LoggerConfUFM_USB", "Mail server", 0));
        QTreeWidgetItem *___qtreewidgetitem35 = ___qtreewidgetitem33->child(1);
        ___qtreewidgetitem35->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILPRT STRING 0 10 RW", 0));
        ___qtreewidgetitem35->setText(0, QApplication::translate("LoggerConfUFM_USB", "Mail port", 0));
        QTreeWidgetItem *___qtreewidgetitem36 = ___qtreewidgetitem33->child(2);
        ___qtreewidgetitem36->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILUSR STRING 0 22 RW", 0));
        ___qtreewidgetitem36->setText(0, QApplication::translate("LoggerConfUFM_USB", "Mail username", 0));
        QTreeWidgetItem *___qtreewidgetitem37 = ___qtreewidgetitem33->child(3);
        ___qtreewidgetitem37->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILPWD STRING 0 22 RW", 0));
        ___qtreewidgetitem37->setText(0, QApplication::translate("LoggerConfUFM_USB", "Mail password", 0));
        QTreeWidgetItem *___qtreewidgetitem38 = ___qtreewidgetitem33->child(4);
        ___qtreewidgetitem38->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILFRM STRING 0 22 RW", 0));
        ___qtreewidgetitem38->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sender mail", 0));
        QTreeWidgetItem *___qtreewidgetitem39 = ___qtreewidgetitem33->child(5);
        ___qtreewidgetitem39->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILTO1 STRING 0 22 RW", 0));
        ___qtreewidgetitem39->setText(0, QApplication::translate("LoggerConfUFM_USB", "Receiver mail", 0));
        QTreeWidgetItem *___qtreewidgetitem40 = ___qtreewidgetitem33->child(6);
        ___qtreewidgetitem40->setText(1, QApplication::translate("LoggerConfUFM_USB", "MAILTO2 STRING 0 22 RW", 0));
        ___qtreewidgetitem40->setText(0, QApplication::translate("LoggerConfUFM_USB", "Receiver mail", 0));
        QTreeWidgetItem *___qtreewidgetitem41 = InfoTreeWidget->topLevelItem(7);
        ___qtreewidgetitem41->setText(0, QApplication::translate("LoggerConfUFM_USB", "Operation", 0));
        QTreeWidgetItem *___qtreewidgetitem42 = ___qtreewidgetitem41->child(0);
        ___qtreewidgetitem42->setText(1, QApplication::translate("LoggerConfUFM_USB", "OPS ENUM Standby Continuous Interval GPRS RW", 0));
        ___qtreewidgetitem42->setText(0, QApplication::translate("LoggerConfUFM_USB", "Operation mode", 0));
        QTreeWidgetItem *___qtreewidgetitem43 = ___qtreewidgetitem41->child(1);
        ___qtreewidgetitem43->setText(1, QApplication::translate("LoggerConfUFM_USB", "LOM TF RW", 0));
        ___qtreewidgetitem43->setText(0, QApplication::translate("LoggerConfUFM_USB", "Lock operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem44 = ___qtreewidgetitem41->child(2);
        ___qtreewidgetitem44->setText(1, QApplication::translate("LoggerConfUFM_USB", "IT UINT32 1 1000000 RW HIDE", 0));
        ___qtreewidgetitem44->setText(0, QApplication::translate("LoggerConfUFM_USB", "LCD indication timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem45 = ___qtreewidgetitem41->child(3);
        ___qtreewidgetitem45->setText(1, QApplication::translate("LoggerConfUFM_USB", "TS TF RW HIDE", 0));
        ___qtreewidgetitem45->setText(0, QApplication::translate("LoggerConfUFM_USB", "Delayed start", 0));
        QTreeWidgetItem *___qtreewidgetitem46 = ___qtreewidgetitem41->child(4);
        ___qtreewidgetitem46->setText(1, QApplication::translate("LoggerConfUFM_USB", "TIME_START DATETIME RW HIDE", 0));
        ___qtreewidgetitem46->setText(0, QApplication::translate("LoggerConfUFM_USB", "Start time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem47 = ___qtreewidgetitem41->child(5);
        ___qtreewidgetitem47->setText(1, QApplication::translate("LoggerConfUFM_USB", "TIME_END DATETIME RW HIDE", 0));
        ___qtreewidgetitem47->setText(0, QApplication::translate("LoggerConfUFM_USB", "End time and date", 0));
        QTreeWidgetItem *___qtreewidgetitem48 = InfoTreeWidget->topLevelItem(8);
        ___qtreewidgetitem48->setText(1, QApplication::translate("LoggerConfUFM_USB", "HIDE", 0));
        ___qtreewidgetitem48->setText(0, QApplication::translate("LoggerConfUFM_USB", "Standby mode", 0));
        QTreeWidgetItem *___qtreewidgetitem49 = InfoTreeWidget->topLevelItem(9);
        ___qtreewidgetitem49->setText(0, QApplication::translate("LoggerConfUFM_USB", "Continuous sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem50 = ___qtreewidgetitem49->child(0);
        ___qtreewidgetitem50->setText(1, QApplication::translate("LoggerConfUFM_USB", "CSI UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem50->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem51 = ___qtreewidgetitem49->child(1);
        ___qtreewidgetitem51->setText(1, QApplication::translate("LoggerConfUFM_USB", "LSNC STRING 0 22 RW", 0));
        ___qtreewidgetitem51->setText(0, QApplication::translate("LoggerConfUFM_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem52 = ___qtreewidgetitem49->child(2);
        ___qtreewidgetitem52->setText(1, QApplication::translate("LoggerConfUFM_USB", "CSS INT32 1 10000000 RW", 0));
        ___qtreewidgetitem52->setText(0, QApplication::translate("LoggerConfUFM_USB", "Samples per file", 0));
        QTreeWidgetItem *___qtreewidgetitem53 = ___qtreewidgetitem49->child(3);
        ___qtreewidgetitem53->setText(1, QApplication::translate("LoggerConfUFM_USB", "CSB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem53->setText(0, QApplication::translate("LoggerConfUFM_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem54 = InfoTreeWidget->topLevelItem(10);
        ___qtreewidgetitem54->setText(0, QApplication::translate("LoggerConfUFM_USB", "Interval sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem55 = ___qtreewidgetitem54->child(0);
        ___qtreewidgetitem55->setText(1, QApplication::translate("LoggerConfUFM_USB", "ISI UINT32 2 1000000 RW", 0));
        ___qtreewidgetitem55->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem56 = ___qtreewidgetitem54->child(1);
        ___qtreewidgetitem56->setText(1, QApplication::translate("LoggerConfUFM_USB", "ISSW UINT32 0 10000 RW", 0));
        ___qtreewidgetitem56->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sensor wait time (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem57 = ___qtreewidgetitem54->child(2);
        ___qtreewidgetitem57->setText(1, QApplication::translate("LoggerConfUFM_USB", "LSNI STRING 0 22 RW", 0));
        ___qtreewidgetitem57->setText(0, QApplication::translate("LoggerConfUFM_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem58 = ___qtreewidgetitem54->child(3);
        ___qtreewidgetitem58->setText(1, QApplication::translate("LoggerConfUFM_USB", "LSF ENUM FILE_PER_MINUTE FILE_PER_HOUR FILE_PER_DAY FILE_PER_MONTH FILE_PER_YEAR RW", 0));
        ___qtreewidgetitem58->setText(0, QApplication::translate("LoggerConfUFM_USB", "Log save option", 0));
        QTreeWidgetItem *___qtreewidgetitem59 = ___qtreewidgetitem54->child(4);
        ___qtreewidgetitem59->setText(1, QApplication::translate("LoggerConfUFM_USB", "ISB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem59->setText(0, QApplication::translate("LoggerConfUFM_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem60 = InfoTreeWidget->topLevelItem(11);
        ___qtreewidgetitem60->setText(0, QApplication::translate("LoggerConfUFM_USB", "GSM operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem61 = ___qtreewidgetitem60->child(0);
        ___qtreewidgetitem61->setText(1, QApplication::translate("LoggerConfUFM_USB", "GSMSWT UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem61->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sensor wait time", 0));
        QTreeWidgetItem *___qtreewidgetitem62 = ___qtreewidgetitem60->child(1);
        ___qtreewidgetitem62->setText(0, QApplication::translate("LoggerConfUFM_USB", "Alarm settings", 0));
        QTreeWidgetItem *___qtreewidgetitem63 = ___qtreewidgetitem62->child(0);
        ___qtreewidgetitem63->setText(1, QApplication::translate("LoggerConfUFM_USB", "ATT ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW", 0));
        ___qtreewidgetitem63->setText(0, QApplication::translate("LoggerConfUFM_USB", "Alarm trigger type", 0));
        QTreeWidgetItem *___qtreewidgetitem64 = ___qtreewidgetitem62->child(1);
        ___qtreewidgetitem64->setText(1, QApplication::translate("LoggerConfUFM_USB", "ARO ENUM TCP SMS EMAIL RW", 0));
        ___qtreewidgetitem64->setText(0, QApplication::translate("LoggerConfUFM_USB", "Alarm report type", 0));
        QTreeWidgetItem *___qtreewidgetitem65 = ___qtreewidgetitem62->child(2);
        ___qtreewidgetitem65->setText(1, QApplication::translate("LoggerConfUFM_USB", "AMAXV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem65->setText(0, QApplication::translate("LoggerConfUFM_USB", "Maximum value", 0));
        QTreeWidgetItem *___qtreewidgetitem66 = ___qtreewidgetitem62->child(3);
        ___qtreewidgetitem66->setText(1, QApplication::translate("LoggerConfUFM_USB", "ADELV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem66->setText(0, QApplication::translate("LoggerConfUFM_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem67 = ___qtreewidgetitem62->child(4);
        ___qtreewidgetitem67->setText(1, QApplication::translate("LoggerConfUFM_USB", "AMINV DOUBLE32 1000 0 20 RW ", 0));
        ___qtreewidgetitem67->setText(0, QApplication::translate("LoggerConfUFM_USB", "Minimum value", 0));
        QTreeWidgetItem *___qtreewidgetitem68 = ___qtreewidgetitem60->child(2);
        ___qtreewidgetitem68->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery full setting", 0));
        QTreeWidgetItem *___qtreewidgetitem69 = ___qtreewidgetitem68->child(0);
        ___qtreewidgetitem69->setText(1, QApplication::translate("LoggerConfUFM_USB", "BFV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem69->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem70 = ___qtreewidgetitem68->child(1);
        ___qtreewidgetitem70->setText(1, QApplication::translate("LoggerConfUFM_USB", "BFRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem70->setText(0, QApplication::translate("LoggerConfUFM_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem71 = ___qtreewidgetitem68->child(2);
        ___qtreewidgetitem71->setText(1, QApplication::translate("LoggerConfUFM_USB", "BFSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem71->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem72 = ___qtreewidgetitem60->child(3);
        ___qtreewidgetitem72->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery half setting", 0));
        QTreeWidgetItem *___qtreewidgetitem73 = ___qtreewidgetitem72->child(0);
        ___qtreewidgetitem73->setText(1, QApplication::translate("LoggerConfUFM_USB", "BHV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem73->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem74 = ___qtreewidgetitem72->child(1);
        ___qtreewidgetitem74->setText(1, QApplication::translate("LoggerConfUFM_USB", "BHRI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem74->setText(0, QApplication::translate("LoggerConfUFM_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem75 = ___qtreewidgetitem72->child(2);
        ___qtreewidgetitem75->setText(1, QApplication::translate("LoggerConfUFM_USB", "BHSI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem75->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem76 = ___qtreewidgetitem60->child(4);
        ___qtreewidgetitem76->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery empty setting", 0));
        QTreeWidgetItem *___qtreewidgetitem77 = ___qtreewidgetitem76->child(0);
        ___qtreewidgetitem77->setText(1, QApplication::translate("LoggerConfUFM_USB", "BEV DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem77->setText(0, QApplication::translate("LoggerConfUFM_USB", "Battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem78 = ___qtreewidgetitem76->child(1);
        ___qtreewidgetitem78->setText(1, QApplication::translate("LoggerConfUFM_USB", "BERI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem78->setText(0, QApplication::translate("LoggerConfUFM_USB", "Report sample number", 0));
        QTreeWidgetItem *___qtreewidgetitem79 = ___qtreewidgetitem76->child(2);
        ___qtreewidgetitem79->setText(1, QApplication::translate("LoggerConfUFM_USB", "BESI UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem79->setText(0, QApplication::translate("LoggerConfUFM_USB", "Sampling interval (seconds)", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        moduleOptions->setTabText(moduleOptions->indexOf(tab_2), QApplication::translate("LoggerConfUFM_USB", "Configuration", 0));
        groupBox_2->setTitle(QApplication::translate("LoggerConfUFM_USB", "TDC1000 Channel 1_2", 0));
        btnTDC1000_12_SaveTo->setText(QApplication::translate("LoggerConfUFM_USB", "Save to", 0));
        btnTDC1000_12_LoadFrom->setText(QApplication::translate("LoggerConfUFM_USB", "Load from", 0));
        btnTDC1000_12_LoadFromRefresh->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem80 = TDC1000_CH1_TreeWidget->headerItem();
        ___qtreewidgetitem80->setText(1, QApplication::translate("LoggerConfUFM_USB", "Value", 0));
        ___qtreewidgetitem80->setText(0, QApplication::translate("LoggerConfUFM_USB", "Property", 0));

        const bool __sortingEnabled1 = TDC1000_CH1_TreeWidget->isSortingEnabled();
        TDC1000_CH1_TreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem81 = TDC1000_CH1_TreeWidget->topLevelItem(0);
        ___qtreewidgetitem81->setText(0, QApplication::translate("LoggerConfUFM_USB", "Time of flight                                             ", 0));
        QTreeWidgetItem *___qtreewidgetitem82 = ___qtreewidgetitem81->child(0);
        ___qtreewidgetitem82->setText(1, QApplication::translate("LoggerConfUFM_USB", "FREQDEV ENUM 2 4 8 16 32 64 128 256  RW", 0));
        ___qtreewidgetitem82->setText(0, QApplication::translate("LoggerConfUFM_USB", "Frequency devision", 0));
        QTreeWidgetItem *___qtreewidgetitem83 = ___qtreewidgetitem81->child(1);
        ___qtreewidgetitem83->setText(1, QApplication::translate("LoggerConfUFM_USB", "PHASESHIFT UINT16 1 30 RW", 0));
        ___qtreewidgetitem83->setText(0, QApplication::translate("LoggerConfUFM_USB", "Phase shift TX", 0));
        QTreeWidgetItem *___qtreewidgetitem84 = ___qtreewidgetitem81->child(2);
        ___qtreewidgetitem84->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMTX UINT16 1 30 RW", 0));
        ___qtreewidgetitem84->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of TX pulses", 0));
        QTreeWidgetItem *___qtreewidgetitem85 = ___qtreewidgetitem81->child(3);
        ___qtreewidgetitem85->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMRX ENUM All 0 1 2 3 4 5 6 7 RW", 0));
        ___qtreewidgetitem85->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of RX pulses", 0));
        QTreeWidgetItem *___qtreewidgetitem86 = ___qtreewidgetitem81->child(4);
        ___qtreewidgetitem86->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMAVG ENUM 1 2 4 8 16 32 64 128 RW", 0));
        ___qtreewidgetitem86->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of AVG", 0));
        QTreeWidgetItem *___qtreewidgetitem87 = ___qtreewidgetitem81->child(5);
        ___qtreewidgetitem87->setText(1, QApplication::translate("LoggerConfUFM_USB", "MEASMODE ENUM MEASURE_TOF MEASURE_TEMP RW", 0));
        ___qtreewidgetitem87->setText(0, QApplication::translate("LoggerConfUFM_USB", "Measurement mode", 0));
        QTreeWidgetItem *___qtreewidgetitem88 = ___qtreewidgetitem81->child(6);
        ___qtreewidgetitem88->setText(1, QApplication::translate("LoggerConfUFM_USB", "TOFMODE ENUM Mode0 Mode1 Mode2 RW", 0));
        ___qtreewidgetitem88->setText(0, QApplication::translate("LoggerConfUFM_USB", "TOF mode", 0));
        QTreeWidgetItem *___qtreewidgetitem89 = ___qtreewidgetitem81->child(7);
        ___qtreewidgetitem89->setText(1, QApplication::translate("LoggerConfUFM_USB", "ECHOTHRESHOLD ENUM 35mV 50mV 75mV 125mV 220mV 410mV 775mV 1500mV RW", 0));
        ___qtreewidgetitem89->setText(0, QApplication::translate("LoggerConfUFM_USB", "Echo threshold voltage", 0));
        QTreeWidgetItem *___qtreewidgetitem90 = ___qtreewidgetitem81->child(8);
        ___qtreewidgetitem90->setText(1, QApplication::translate("LoggerConfUFM_USB", "RECIEVEMODE ENUM Single_echo Multi_echo RW", 0));
        ___qtreewidgetitem90->setText(0, QApplication::translate("LoggerConfUFM_USB", "Recieve mode", 0));
        QTreeWidgetItem *___qtreewidgetitem91 = ___qtreewidgetitem81->child(9);
        ___qtreewidgetitem91->setText(1, QApplication::translate("LoggerConfUFM_USB", "TOFTIMEOUT ENUM 128 256 512 1024 RW", 0));
        ___qtreewidgetitem91->setText(0, QApplication::translate("LoggerConfUFM_USB", "TOF timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem92 = ___qtreewidgetitem81->child(10);
        ___qtreewidgetitem92->setText(1, QApplication::translate("LoggerConfUFM_USB", "SHORTTOFPERIOD ENUM 8 16 32 64 128 256 512 1024 RW", 0));
        ___qtreewidgetitem92->setText(0, QApplication::translate("LoggerConfUFM_USB", "Short TOF period", 0));
        QTreeWidgetItem *___qtreewidgetitem93 = ___qtreewidgetitem81->child(11);
        ___qtreewidgetitem93->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLEDAMPING TF RW", 0));
        ___qtreewidgetitem93->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable damping", 0));
        QTreeWidgetItem *___qtreewidgetitem94 = ___qtreewidgetitem81->child(12);
        ___qtreewidgetitem94->setText(1, QApplication::translate("LoggerConfUFM_USB", "BLANKINGENABLE TF RW", 0));
        ___qtreewidgetitem94->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable blanking", 0));
        QTreeWidgetItem *___qtreewidgetitem95 = ___qtreewidgetitem81->child(13);
        ___qtreewidgetitem95->setText(1, QApplication::translate("LoggerConfUFM_USB", "ECHOTIMEOUTEN TF RW", 0));
        ___qtreewidgetitem95->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable echo timeout ", 0));
        QTreeWidgetItem *___qtreewidgetitem96 = ___qtreewidgetitem81->child(14);
        ___qtreewidgetitem96->setText(1, QApplication::translate("LoggerConfUFM_USB", "SHORTTOF TF RW", 0));
        ___qtreewidgetitem96->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable short TOF measurement", 0));
        QTreeWidgetItem *___qtreewidgetitem97 = ___qtreewidgetitem81->child(15);
        ___qtreewidgetitem97->setText(1, QApplication::translate("LoggerConfUFM_USB", "TIMINGREG UINT16 0 10000 RW", 0));
        ___qtreewidgetitem97->setText(0, QApplication::translate("LoggerConfUFM_USB", "Timing register", 0));
        QTreeWidgetItem *___qtreewidgetitem98 = ___qtreewidgetitem81->child(16);
        ___qtreewidgetitem98->setText(1, QApplication::translate("LoggerConfUFM_USB", "AUTOZERO ENUM 64 128 256 512 RW", 0));
        ___qtreewidgetitem98->setText(0, QApplication::translate("LoggerConfUFM_USB", "Autozero period", 0));
        QTreeWidgetItem *___qtreewidgetitem99 = ___qtreewidgetitem81->child(17);
        ___qtreewidgetitem99->setText(0, QApplication::translate("LoggerConfUFM_USB", "PGA/LNA/VCOM", 0));
        QTreeWidgetItem *___qtreewidgetitem100 = ___qtreewidgetitem99->child(0);
        ___qtreewidgetitem100->setText(1, QApplication::translate("LoggerConfUFM_USB", "VCOMSEL TF RW", 0));
        ___qtreewidgetitem100->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable VCOM", 0));
        QTreeWidgetItem *___qtreewidgetitem101 = ___qtreewidgetitem99->child(1);
        ___qtreewidgetitem101->setText(1, QApplication::translate("LoggerConfUFM_USB", "LNAFEEDBACK ENUM Capacitive Resistive RW", 0));
        ___qtreewidgetitem101->setText(0, QApplication::translate("LoggerConfUFM_USB", "LNA feedback type ", 0));
        QTreeWidgetItem *___qtreewidgetitem102 = ___qtreewidgetitem99->child(2);
        ___qtreewidgetitem102->setText(1, QApplication::translate("LoggerConfUFM_USB", "LNAON TF RW", 0));
        ___qtreewidgetitem102->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable LNA", 0));
        QTreeWidgetItem *___qtreewidgetitem103 = ___qtreewidgetitem99->child(3);
        ___qtreewidgetitem103->setText(1, QApplication::translate("LoggerConfUFM_USB", "PGAON TF RW", 0));
        ___qtreewidgetitem103->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable PGA", 0));
        QTreeWidgetItem *___qtreewidgetitem104 = ___qtreewidgetitem99->child(4);
        ___qtreewidgetitem104->setText(1, QApplication::translate("LoggerConfUFM_USB", "PGAGAIN ENUM 0dB 3dB 6dB 9dB 12dB 15dB 18dB 21dB RW", 0));
        ___qtreewidgetitem104->setText(0, QApplication::translate("LoggerConfUFM_USB", "PGA gain", 0));
        QTreeWidgetItem *___qtreewidgetitem105 = ___qtreewidgetitem81->child(18);
        ___qtreewidgetitem105->setText(0, QApplication::translate("LoggerConfUFM_USB", "Operational", 0));
        QTreeWidgetItem *___qtreewidgetitem106 = ___qtreewidgetitem105->child(0);
        ___qtreewidgetitem106->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLEEXTCHSEL TF RW", 0));
        ___qtreewidgetitem106->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable external channel select", 0));
        QTreeWidgetItem *___qtreewidgetitem107 = ___qtreewidgetitem105->child(1);
        ___qtreewidgetitem107->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLECHSWAP TF RW", 0));
        ___qtreewidgetitem107->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable channel swaping", 0));
        QTreeWidgetItem *___qtreewidgetitem108 = ___qtreewidgetitem105->child(2);
        ___qtreewidgetitem108->setText(1, QApplication::translate("LoggerConfUFM_USB", "CHSEL ENUM CHANNEL1 CHANNEL2 RW", 0));
        ___qtreewidgetitem108->setText(0, QApplication::translate("LoggerConfUFM_USB", "Channel select", 0));
        QTreeWidgetItem *___qtreewidgetitem109 = ___qtreewidgetitem105->child(3);
        ___qtreewidgetitem109->setText(1, QApplication::translate("LoggerConfUFM_USB", "TRIGEDGEPOL ENUM Positive Negative RW", 0));
        ___qtreewidgetitem109->setText(0, QApplication::translate("LoggerConfUFM_USB", "Trigger edge polarity", 0));
        QTreeWidgetItem *___qtreewidgetitem110 = TDC1000_CH1_TreeWidget->topLevelItem(1);
        ___qtreewidgetitem110->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature", 0));
        QTreeWidgetItem *___qtreewidgetitem111 = ___qtreewidgetitem110->child(0);
        ___qtreewidgetitem111->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPMODE ENUM Both Single RW", 0));
        ___qtreewidgetitem111->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature mode", 0));
        QTreeWidgetItem *___qtreewidgetitem112 = ___qtreewidgetitem110->child(1);
        ___qtreewidgetitem112->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPRTDSEL ENUM PT1000 PT500 RW", 0));
        ___qtreewidgetitem112->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature probes", 0));
        QTreeWidgetItem *___qtreewidgetitem113 = ___qtreewidgetitem110->child(2);
        ___qtreewidgetitem113->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPCLKDIV ENUM Use_TX_frequency Divide_by_8 RW", 0));
        ___qtreewidgetitem113->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature clock division", 0));
        TDC1000_CH1_TreeWidget->setSortingEnabled(__sortingEnabled1);

        groupBox_5->setTitle(QApplication::translate("LoggerConfUFM_USB", "TDC7200 Channel 1_2", 0));
        btnTDC7200_12_SaveTo_2->setText(QApplication::translate("LoggerConfUFM_USB", "Save to", 0));
        btnTDC7200_12_LoadFrom_2->setText(QApplication::translate("LoggerConfUFM_USB", "Load from", 0));
        btnTDC7200_12_LoadFromRefresh_2->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem114 = TDC7200_CH1_TreeWidget_2->headerItem();
        ___qtreewidgetitem114->setText(1, QApplication::translate("LoggerConfUFM_USB", "Value", 0));
        ___qtreewidgetitem114->setText(0, QApplication::translate("LoggerConfUFM_USB", "Property", 0));

        const bool __sortingEnabled2 = TDC7200_CH1_TreeWidget_2->isSortingEnabled();
        TDC7200_CH1_TreeWidget_2->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem115 = TDC7200_CH1_TreeWidget_2->topLevelItem(0);
        ___qtreewidgetitem115->setText(0, QApplication::translate("LoggerConfUFM_USB", "Time of flight                                             ", 0));
        QTreeWidgetItem *___qtreewidgetitem116 = ___qtreewidgetitem115->child(0);
        ___qtreewidgetitem116->setText(1, QApplication::translate("LoggerConfUFM_USB", "COARSECLOCKOVERFLOW UINT16 0 160000 RW", 0));
        ___qtreewidgetitem116->setText(0, QApplication::translate("LoggerConfUFM_USB", "Coarse clock overflow", 0));
        QTreeWidgetItem *___qtreewidgetitem117 = ___qtreewidgetitem115->child(1);
        ___qtreewidgetitem117->setText(1, QApplication::translate("LoggerConfUFM_USB", "CLOCKOVERFLOW UINT16 0 160000 RW", 0));
        ___qtreewidgetitem117->setText(0, QApplication::translate("LoggerConfUFM_USB", "Clock overflow", 0));
        QTreeWidgetItem *___qtreewidgetitem118 = ___qtreewidgetitem115->child(2);
        ___qtreewidgetitem118->setText(1, QApplication::translate("LoggerConfUFM_USB", "CLOCKCOUNTERSTOPMASK UINT16 0 160000 RW", 0));
        ___qtreewidgetitem118->setText(0, QApplication::translate("LoggerConfUFM_USB", "Clock counter stop mask", 0));
        QTreeWidgetItem *___qtreewidgetitem119 = ___qtreewidgetitem115->child(3);
        ___qtreewidgetitem119->setText(1, QApplication::translate("LoggerConfUFM_USB", "FORCECALIBRATIONENABLE TF RW", 0));
        ___qtreewidgetitem119->setText(0, QApplication::translate("LoggerConfUFM_USB", "Force calibration enable", 0));
        QTreeWidgetItem *___qtreewidgetitem120 = ___qtreewidgetitem115->child(4);
        ___qtreewidgetitem120->setText(1, QApplication::translate("LoggerConfUFM_USB", "PARITYENABLE TF RW", 0));
        ___qtreewidgetitem120->setText(0, QApplication::translate("LoggerConfUFM_USB", "Parity enable", 0));
        QTreeWidgetItem *___qtreewidgetitem121 = ___qtreewidgetitem115->child(5);
        ___qtreewidgetitem121->setText(1, QApplication::translate("LoggerConfUFM_USB", "TRIGGEREDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem121->setText(0, QApplication::translate("LoggerConfUFM_USB", "Trigger edge", 0));
        QTreeWidgetItem *___qtreewidgetitem122 = ___qtreewidgetitem115->child(6);
        ___qtreewidgetitem122->setText(1, QApplication::translate("LoggerConfUFM_USB", "STOPEDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem122->setText(0, QApplication::translate("LoggerConfUFM_USB", "Stop edge", 0));
        QTreeWidgetItem *___qtreewidgetitem123 = ___qtreewidgetitem115->child(7);
        ___qtreewidgetitem123->setText(1, QApplication::translate("LoggerConfUFM_USB", "STARTEDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem123->setText(0, QApplication::translate("LoggerConfUFM_USB", "Start edge", 0));
        QTreeWidgetItem *___qtreewidgetitem124 = ___qtreewidgetitem115->child(8);
        ___qtreewidgetitem124->setText(1, QApplication::translate("LoggerConfUFM_USB", "MEASMODE ENUM <500ns >500ns RW", 0));
        ___qtreewidgetitem124->setText(0, QApplication::translate("LoggerConfUFM_USB", "Measurement mode", 0));
        QTreeWidgetItem *___qtreewidgetitem125 = ___qtreewidgetitem115->child(9);
        ___qtreewidgetitem125->setText(1, QApplication::translate("LoggerConfUFM_USB", "CALIBBPERIODS ENUM 2 10 20 40 RW", 0));
        ___qtreewidgetitem125->setText(0, QApplication::translate("LoggerConfUFM_USB", "Calibration periods", 0));
        QTreeWidgetItem *___qtreewidgetitem126 = ___qtreewidgetitem115->child(10);
        ___qtreewidgetitem126->setText(1, QApplication::translate("LoggerConfUFM_USB", "AVGCYCLES ENUM 1 2 4 8 16 32 64 128 RW", 0));
        ___qtreewidgetitem126->setText(0, QApplication::translate("LoggerConfUFM_USB", "Average cycles", 0));
        QTreeWidgetItem *___qtreewidgetitem127 = ___qtreewidgetitem115->child(11);
        ___qtreewidgetitem127->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMSTOP ENUM 1 2 3 4 5 RW", 0));
        ___qtreewidgetitem127->setText(0, QApplication::translate("LoggerConfUFM_USB", "Stop number", 0));
        TDC7200_CH1_TreeWidget_2->setSortingEnabled(__sortingEnabled2);

        groupBox_3->setTitle(QApplication::translate("LoggerConfUFM_USB", "TDC1000 Channel 3_4", 0));
        btnTDC1000_34_SaveTo->setText(QApplication::translate("LoggerConfUFM_USB", "Save to", 0));
        btnTDC1000_34_LoadFrom->setText(QApplication::translate("LoggerConfUFM_USB", "Load from", 0));
        btnTDC1000_34_LoadFromRefresh->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem128 = TDC1000_CH2_TreeWidget->headerItem();
        ___qtreewidgetitem128->setText(1, QApplication::translate("LoggerConfUFM_USB", "Value", 0));
        ___qtreewidgetitem128->setText(0, QApplication::translate("LoggerConfUFM_USB", "Property", 0));

        const bool __sortingEnabled3 = TDC1000_CH2_TreeWidget->isSortingEnabled();
        TDC1000_CH2_TreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem129 = TDC1000_CH2_TreeWidget->topLevelItem(0);
        ___qtreewidgetitem129->setText(0, QApplication::translate("LoggerConfUFM_USB", "Time of flight                                                     ", 0));
        QTreeWidgetItem *___qtreewidgetitem130 = ___qtreewidgetitem129->child(0);
        ___qtreewidgetitem130->setText(1, QApplication::translate("LoggerConfUFM_USB", "FREQDEV ENUM 2 4 8 16 32 64 128 256  RW", 0));
        ___qtreewidgetitem130->setText(0, QApplication::translate("LoggerConfUFM_USB", "Frequency devision", 0));
        QTreeWidgetItem *___qtreewidgetitem131 = ___qtreewidgetitem129->child(1);
        ___qtreewidgetitem131->setText(1, QApplication::translate("LoggerConfUFM_USB", "PHASESHIFT UINT16 1 30 RW", 0));
        ___qtreewidgetitem131->setText(0, QApplication::translate("LoggerConfUFM_USB", "Phase shift TX", 0));
        QTreeWidgetItem *___qtreewidgetitem132 = ___qtreewidgetitem129->child(2);
        ___qtreewidgetitem132->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMTX UINT16 1 30 RW", 0));
        ___qtreewidgetitem132->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of TX pulses", 0));
        QTreeWidgetItem *___qtreewidgetitem133 = ___qtreewidgetitem129->child(3);
        ___qtreewidgetitem133->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMRX ENUM All 0 1 2 3 4 5 6 7 RW", 0));
        ___qtreewidgetitem133->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of RX pulses", 0));
        QTreeWidgetItem *___qtreewidgetitem134 = ___qtreewidgetitem129->child(4);
        ___qtreewidgetitem134->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMAVG ENUM 1 2 4 8 16 32 64 128 RW", 0));
        ___qtreewidgetitem134->setText(0, QApplication::translate("LoggerConfUFM_USB", "Number of AVG", 0));
        QTreeWidgetItem *___qtreewidgetitem135 = ___qtreewidgetitem129->child(5);
        ___qtreewidgetitem135->setText(1, QApplication::translate("LoggerConfUFM_USB", "MEASMODE ENUM MEASURE_TOF MEASURE_TEMP RW", 0));
        ___qtreewidgetitem135->setText(0, QApplication::translate("LoggerConfUFM_USB", "Measurement mode", 0));
        QTreeWidgetItem *___qtreewidgetitem136 = ___qtreewidgetitem129->child(6);
        ___qtreewidgetitem136->setText(1, QApplication::translate("LoggerConfUFM_USB", "TOFMODE ENUM Mode0 Mode1 Mode2 RW", 0));
        ___qtreewidgetitem136->setText(0, QApplication::translate("LoggerConfUFM_USB", "TOF mode", 0));
        QTreeWidgetItem *___qtreewidgetitem137 = ___qtreewidgetitem129->child(7);
        ___qtreewidgetitem137->setText(1, QApplication::translate("LoggerConfUFM_USB", "ECHOTHRESHOLD ENUM 35mV 50mV 75mV 125mV 220mV 410mV 775mV 1500mV RW", 0));
        ___qtreewidgetitem137->setText(0, QApplication::translate("LoggerConfUFM_USB", "Echo threshold voltage", 0));
        QTreeWidgetItem *___qtreewidgetitem138 = ___qtreewidgetitem129->child(8);
        ___qtreewidgetitem138->setText(1, QApplication::translate("LoggerConfUFM_USB", "RECIEVEMODE ENUM Single_echo Multi_echo RW", 0));
        ___qtreewidgetitem138->setText(0, QApplication::translate("LoggerConfUFM_USB", "Recieve mode", 0));
        QTreeWidgetItem *___qtreewidgetitem139 = ___qtreewidgetitem129->child(9);
        ___qtreewidgetitem139->setText(1, QApplication::translate("LoggerConfUFM_USB", "TOFTIMEOUT ENUM 128 256 512 1024 RW", 0));
        ___qtreewidgetitem139->setText(0, QApplication::translate("LoggerConfUFM_USB", "TOF timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem140 = ___qtreewidgetitem129->child(10);
        ___qtreewidgetitem140->setText(1, QApplication::translate("LoggerConfUFM_USB", "SHORTTOFPERIOD ENUM 8 16 32 64 128 256 512 1024 RW", 0));
        ___qtreewidgetitem140->setText(0, QApplication::translate("LoggerConfUFM_USB", "Short TOF period", 0));
        QTreeWidgetItem *___qtreewidgetitem141 = ___qtreewidgetitem129->child(11);
        ___qtreewidgetitem141->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLEDAMPING TF RW", 0));
        ___qtreewidgetitem141->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable damping", 0));
        QTreeWidgetItem *___qtreewidgetitem142 = ___qtreewidgetitem129->child(12);
        ___qtreewidgetitem142->setText(1, QApplication::translate("LoggerConfUFM_USB", "BLANKINGENABLE TF RW", 0));
        ___qtreewidgetitem142->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable blanking", 0));
        QTreeWidgetItem *___qtreewidgetitem143 = ___qtreewidgetitem129->child(13);
        ___qtreewidgetitem143->setText(1, QApplication::translate("LoggerConfUFM_USB", "ECHOTIMEOUTEN TF RW", 0));
        ___qtreewidgetitem143->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable echo timeout ", 0));
        QTreeWidgetItem *___qtreewidgetitem144 = ___qtreewidgetitem129->child(14);
        ___qtreewidgetitem144->setText(1, QApplication::translate("LoggerConfUFM_USB", "SHORTTOF TF RW", 0));
        ___qtreewidgetitem144->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable short TOF measurement", 0));
        QTreeWidgetItem *___qtreewidgetitem145 = ___qtreewidgetitem129->child(15);
        ___qtreewidgetitem145->setText(1, QApplication::translate("LoggerConfUFM_USB", "TIMINGREG UINT16 0 1000 RW", 0));
        ___qtreewidgetitem145->setText(0, QApplication::translate("LoggerConfUFM_USB", "Timing reg", 0));
        QTreeWidgetItem *___qtreewidgetitem146 = ___qtreewidgetitem129->child(16);
        ___qtreewidgetitem146->setText(1, QApplication::translate("LoggerConfUFM_USB", "AUTOZERO ENUM 64 128 256 512 RW", 0));
        ___qtreewidgetitem146->setText(0, QApplication::translate("LoggerConfUFM_USB", "Autozero period", 0));
        QTreeWidgetItem *___qtreewidgetitem147 = ___qtreewidgetitem129->child(17);
        ___qtreewidgetitem147->setText(0, QApplication::translate("LoggerConfUFM_USB", "PGA/LNA/VCOM", 0));
        QTreeWidgetItem *___qtreewidgetitem148 = ___qtreewidgetitem147->child(0);
        ___qtreewidgetitem148->setText(1, QApplication::translate("LoggerConfUFM_USB", "VCOMSEL TF RW", 0));
        ___qtreewidgetitem148->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable VCOM", 0));
        QTreeWidgetItem *___qtreewidgetitem149 = ___qtreewidgetitem147->child(1);
        ___qtreewidgetitem149->setText(1, QApplication::translate("LoggerConfUFM_USB", "LNAFEEDBACK ENUM  Capacitive Resistive RW", 0));
        ___qtreewidgetitem149->setText(0, QApplication::translate("LoggerConfUFM_USB", "LNA feedback type ", 0));
        QTreeWidgetItem *___qtreewidgetitem150 = ___qtreewidgetitem147->child(2);
        ___qtreewidgetitem150->setText(1, QApplication::translate("LoggerConfUFM_USB", "LNAON TF RW", 0));
        ___qtreewidgetitem150->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable LNA", 0));
        QTreeWidgetItem *___qtreewidgetitem151 = ___qtreewidgetitem147->child(3);
        ___qtreewidgetitem151->setText(1, QApplication::translate("LoggerConfUFM_USB", "PGAON TF RW", 0));
        ___qtreewidgetitem151->setText(0, QApplication::translate("LoggerConfUFM_USB", "Disable PGA", 0));
        QTreeWidgetItem *___qtreewidgetitem152 = ___qtreewidgetitem147->child(4);
        ___qtreewidgetitem152->setText(1, QApplication::translate("LoggerConfUFM_USB", "PGAGAIN ENUM 0dB 3dB 6dB 9dB 12dB 15dB 18dB 21dB RW", 0));
        ___qtreewidgetitem152->setText(0, QApplication::translate("LoggerConfUFM_USB", "PGA gain", 0));
        QTreeWidgetItem *___qtreewidgetitem153 = ___qtreewidgetitem129->child(18);
        ___qtreewidgetitem153->setText(0, QApplication::translate("LoggerConfUFM_USB", "Operational", 0));
        QTreeWidgetItem *___qtreewidgetitem154 = ___qtreewidgetitem153->child(0);
        ___qtreewidgetitem154->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLEEXTCHSEL TF RW", 0));
        ___qtreewidgetitem154->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable external channel select", 0));
        QTreeWidgetItem *___qtreewidgetitem155 = ___qtreewidgetitem153->child(1);
        ___qtreewidgetitem155->setText(1, QApplication::translate("LoggerConfUFM_USB", "ENABLECHSWAP TF RW", 0));
        ___qtreewidgetitem155->setText(0, QApplication::translate("LoggerConfUFM_USB", "Enable channel swaping", 0));
        QTreeWidgetItem *___qtreewidgetitem156 = ___qtreewidgetitem153->child(2);
        ___qtreewidgetitem156->setText(1, QApplication::translate("LoggerConfUFM_USB", "CHSEL ENUM CHANNEL1 CHANNEL2 RW", 0));
        ___qtreewidgetitem156->setText(0, QApplication::translate("LoggerConfUFM_USB", "Channel select", 0));
        QTreeWidgetItem *___qtreewidgetitem157 = ___qtreewidgetitem153->child(3);
        ___qtreewidgetitem157->setText(1, QApplication::translate("LoggerConfUFM_USB", "TRIGEDGEPOL ENUM Positive Negative RW", 0));
        ___qtreewidgetitem157->setText(0, QApplication::translate("LoggerConfUFM_USB", "Trigger edge polarity", 0));
        QTreeWidgetItem *___qtreewidgetitem158 = TDC1000_CH2_TreeWidget->topLevelItem(1);
        ___qtreewidgetitem158->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature", 0));
        QTreeWidgetItem *___qtreewidgetitem159 = ___qtreewidgetitem158->child(0);
        ___qtreewidgetitem159->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPMODE ENUM Both Single RW", 0));
        ___qtreewidgetitem159->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature mode", 0));
        QTreeWidgetItem *___qtreewidgetitem160 = ___qtreewidgetitem158->child(1);
        ___qtreewidgetitem160->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPRTDSEL ENUM PT1000 PT500 RW", 0));
        ___qtreewidgetitem160->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature probes", 0));
        QTreeWidgetItem *___qtreewidgetitem161 = ___qtreewidgetitem158->child(2);
        ___qtreewidgetitem161->setText(1, QApplication::translate("LoggerConfUFM_USB", "TEMPCLKDIV ENUM Use_TX_frequency Divide_by_8 RW", 0));
        ___qtreewidgetitem161->setText(0, QApplication::translate("LoggerConfUFM_USB", "Temperature clock division", 0));
        TDC1000_CH2_TreeWidget->setSortingEnabled(__sortingEnabled3);

        groupBox_6->setTitle(QApplication::translate("LoggerConfUFM_USB", "TDC7200 Channel 3_4", 0));
        btnTDC7200_34_SaveTo_2->setText(QApplication::translate("LoggerConfUFM_USB", "Save to", 0));
        btnTDC7200_34_LoadFrom_2->setText(QApplication::translate("LoggerConfUFM_USB", "Load from", 0));
        btnTDC7200_34_LoadFromRefresh_2->setText(QString());
        QTreeWidgetItem *___qtreewidgetitem162 = TDC7200_CH2_TreeWidget_2->headerItem();
        ___qtreewidgetitem162->setText(1, QApplication::translate("LoggerConfUFM_USB", "Value", 0));
        ___qtreewidgetitem162->setText(0, QApplication::translate("LoggerConfUFM_USB", "Property", 0));

        const bool __sortingEnabled4 = TDC7200_CH2_TreeWidget_2->isSortingEnabled();
        TDC7200_CH2_TreeWidget_2->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem163 = TDC7200_CH2_TreeWidget_2->topLevelItem(0);
        ___qtreewidgetitem163->setText(0, QApplication::translate("LoggerConfUFM_USB", "Time of flight                                             ", 0));
        QTreeWidgetItem *___qtreewidgetitem164 = ___qtreewidgetitem163->child(0);
        ___qtreewidgetitem164->setText(1, QApplication::translate("LoggerConfUFM_USB", "COARSECLOCKOVERFLOW UINT16 0 160000 RW", 0));
        ___qtreewidgetitem164->setText(0, QApplication::translate("LoggerConfUFM_USB", "Coarse clock overflow", 0));
        QTreeWidgetItem *___qtreewidgetitem165 = ___qtreewidgetitem163->child(1);
        ___qtreewidgetitem165->setText(1, QApplication::translate("LoggerConfUFM_USB", "CLOCKOVERFLOW UINT16 0 160000 RW", 0));
        ___qtreewidgetitem165->setText(0, QApplication::translate("LoggerConfUFM_USB", "Clock overflow", 0));
        QTreeWidgetItem *___qtreewidgetitem166 = ___qtreewidgetitem163->child(2);
        ___qtreewidgetitem166->setText(1, QApplication::translate("LoggerConfUFM_USB", "CLOCKCOUNTERSTOPMASK UINT16 0 160000 RW", 0));
        ___qtreewidgetitem166->setText(0, QApplication::translate("LoggerConfUFM_USB", "Clock counter stop mask", 0));
        QTreeWidgetItem *___qtreewidgetitem167 = ___qtreewidgetitem163->child(3);
        ___qtreewidgetitem167->setText(1, QApplication::translate("LoggerConfUFM_USB", "FORCECALIBRATIONENABLE TF RW", 0));
        ___qtreewidgetitem167->setText(0, QApplication::translate("LoggerConfUFM_USB", "Force calibration enable", 0));
        QTreeWidgetItem *___qtreewidgetitem168 = ___qtreewidgetitem163->child(4);
        ___qtreewidgetitem168->setText(1, QApplication::translate("LoggerConfUFM_USB", "PARITYENABLE TF RW", 0));
        ___qtreewidgetitem168->setText(0, QApplication::translate("LoggerConfUFM_USB", "Parity enable", 0));
        QTreeWidgetItem *___qtreewidgetitem169 = ___qtreewidgetitem163->child(5);
        ___qtreewidgetitem169->setText(1, QApplication::translate("LoggerConfUFM_USB", "TRIGGEREDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem169->setText(0, QApplication::translate("LoggerConfUFM_USB", "Trigger edge", 0));
        QTreeWidgetItem *___qtreewidgetitem170 = ___qtreewidgetitem163->child(6);
        ___qtreewidgetitem170->setText(1, QApplication::translate("LoggerConfUFM_USB", "STOPEDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem170->setText(0, QApplication::translate("LoggerConfUFM_USB", "Stop edge", 0));
        QTreeWidgetItem *___qtreewidgetitem171 = ___qtreewidgetitem163->child(7);
        ___qtreewidgetitem171->setText(1, QApplication::translate("LoggerConfUFM_USB", "STARTEDGE ENUM Rising Falling RW", 0));
        ___qtreewidgetitem171->setText(0, QApplication::translate("LoggerConfUFM_USB", "Start edge", 0));
        QTreeWidgetItem *___qtreewidgetitem172 = ___qtreewidgetitem163->child(8);
        ___qtreewidgetitem172->setText(1, QApplication::translate("LoggerConfUFM_USB", "MEASMODE ENUM <500ns >500ns RW", 0));
        ___qtreewidgetitem172->setText(0, QApplication::translate("LoggerConfUFM_USB", "Measurement mode", 0));
        QTreeWidgetItem *___qtreewidgetitem173 = ___qtreewidgetitem163->child(9);
        ___qtreewidgetitem173->setText(1, QApplication::translate("LoggerConfUFM_USB", "CALIBBPERIODS ENUM 2 10 20 40 RW", 0));
        ___qtreewidgetitem173->setText(0, QApplication::translate("LoggerConfUFM_USB", "Calibration periods", 0));
        QTreeWidgetItem *___qtreewidgetitem174 = ___qtreewidgetitem163->child(10);
        ___qtreewidgetitem174->setText(1, QApplication::translate("LoggerConfUFM_USB", "AVGCYCLES ENUM 1 2 4 8 16 32 64 128 RW", 0));
        ___qtreewidgetitem174->setText(0, QApplication::translate("LoggerConfUFM_USB", "Average cycles", 0));
        QTreeWidgetItem *___qtreewidgetitem175 = ___qtreewidgetitem163->child(11);
        ___qtreewidgetitem175->setText(1, QApplication::translate("LoggerConfUFM_USB", "NUMSTOP ENUM 1 2 3 4 5 RW", 0));
        ___qtreewidgetitem175->setText(0, QApplication::translate("LoggerConfUFM_USB", "Stop number", 0));
        TDC7200_CH2_TreeWidget_2->setSortingEnabled(__sortingEnabled4);

        moduleOptions->setTabText(moduleOptions->indexOf(tab), QApplication::translate("LoggerConfUFM_USB", "Ultrasound", 0));
        groupBox->setTitle(QApplication::translate("LoggerConfUFM_USB", "Flash options", 0));
        btnWriteFlash->setText(QApplication::translate("LoggerConfUFM_USB", "Write flash", 0));
        label->setText(QApplication::translate("LoggerConfUFM_USB", "Write size", 0));
        tbxWriteSize->setText(QApplication::translate("LoggerConfUFM_USB", "1000000", 0));
        btnReadFlash->setText(QApplication::translate("LoggerConfUFM_USB", "Read flash", 0));
        label_2->setText(QApplication::translate("LoggerConfUFM_USB", "Read size ", 0));
        tbxReadSize->setText(QApplication::translate("LoggerConfUFM_USB", "1000000", 0));
        btnLoadHEX->setText(QApplication::translate("LoggerConfUFM_USB", "Load HEX", 0));
        label_3->setText(QApplication::translate("LoggerConfUFM_USB", "HEX path", 0));
        btnEraseFlash->setText(QApplication::translate("LoggerConfUFM_USB", "Erase flash", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_3), QApplication::translate("LoggerConfUFM_USB", "Data browser", 0));
        groupBox_4->setTitle(QApplication::translate("LoggerConfUFM_USB", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfUFM_USB", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfUFM_USB", "STOP", 0));
        Data1_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data1", 0));
        Data2_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data2", 0));
        Data3_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data3", 0));
        Data4_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data4", 0));
        Data5_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data5", 0));
        Data6_Checkbox->setText(QApplication::translate("LoggerConfUFM_USB", "Data6", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_5), QApplication::translate("LoggerConfUFM_USB", "Realtime monitoring", 0));
        btnSendCommand->setText(QApplication::translate("LoggerConfUFM_USB", "SEND COMMAND", 0));
        toolBox->setItemText(toolBox->indexOf(page_4), QApplication::translate("LoggerConfUFM_USB", "Other", 0));
        autoclearCheckBox->setText(QApplication::translate("LoggerConfUFM_USB", "Auto clear", 0));
        clearTerminalBrowser->setText(QApplication::translate("LoggerConfUFM_USB", "Clear screen", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_4), QApplication::translate("LoggerConfUFM_USB", "Testing", 0));
        btnStop->setText(QApplication::translate("LoggerConfUFM_USB", "STOP", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfUFM_USB: public Ui_LoggerConfUFM_USB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFUFM_USB_H
