#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
#include <QTreeWidget>
#include <QListWidget>
#include <QFileDialog>
#include <QMdiArea>
#include <QResizeEvent>
#include <QPainter>

#include "plottingwidget.h"
#include "detector.h"

#include "loggerconfpdlag_USB.h"

#include "loggerinfo.h"
#include "deployedloggers.h"
#include "deployedloggershistory.h"
#include "dialogwait.h"
#include "datatransfer.h"
#include "datavisualizer.h"
#include "treefolderview.h"


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Ui::MainWindow *ui;

    TreeFolderView          *treeDataViewWidget;
    DetectorController      *usbDetector;

    /* Tray funkcije */
    void showTrayMessage(int Type,QString Title, QString Text, int duration);
    void createTrayActions();
    void createTrayIcon();

signals:
    void DriveChangeSignal(QStringList labels,QStringList names);
    void dataTransferFinishedMainSignal();

private slots:
    void slotToQuit();

    void LoggerSelected         (QString ID,LoggerType DeviceType, CommunicationType CommType);
    void LoggerSelectedSimple   (QString ID,LoggerType DeviceType, CommunicationType CommType);
    void LoggerDisconnect       (QString ID,LoggerType DeviceType, CommunicationType CommType);

    void SerialDetected_private         (QString Serial, QString Identifier);
    void SerialNotDetected_private      (QString Serial, QString Identifier);
    void MessageRecieved                (QString Serial, QString Message);
    void DriveChangeDetected            (QStringList labels,QStringList names);

    void iconActivated                  (QSystemTrayIcon::ActivationReason);
    void publishMessageToTerminal       (QString message);
    void publishErrorToTerminal         (QString error);
    void showTime();

    void on_btnSincClocks_clicked               ();
    void on_btnStopReports_clicked              ();
    void on_btnStartReports_clicked             ();

    void on_loggerList_customContextMenuRequested(const QPoint &pos);

    void dataPlotInquiry(QString Path,QString Name);
    void dataTransferFinishedMainSlot();

private:

    void checkForDeployedLogger(QString LoggerID);
    bool eventFilter(QObject *obj, QEvent *event);
    void plotData(QString Path,QString Name);
    void delay( int millisecondsToWait );
    QString getMainWidgetDeviceID(QWidget *Widget);

    QString                 initialPath;

    QLabel                  *StatusBarUserSerial;
    QLabel                  *StatusBarUserBT;
    QLabel                  *StatusBarUserTCP;
    QLabel                  *StatusBarClock;

    QVBoxLayout             *layout;
    QVBoxLayout             *specialWidgetLayout;
    QTabWidget              *specialWidget;
    plottingWidget          *plotting;

    QSystemTrayIcon         *trayIcon;
    QMenu                   *trayIconMenu;
    QAction                 *minimizeAction;
    QAction                 *maximizeAction;
    QAction                 *restoreAction;
    QAction                 *quitAction;

    QTimeLine               *m_pTimeLine;

    QList<LoggerInfo>       ListOfLoggers;

    QWidget                 *MainWidget;
    QString                 sendingSerial;
    QTimer                  *delayedSyncTimer;
    QTimer                  *delayedReportTimer;

    QStringList             _labels;
    QStringList             _names;

//    QImage i;
//    QBrush b;


    unsigned int written;

    QSerialPort *m_port;

//protected:
//    void resizeEvent(QResizeEvent  *resizeEvent);

};

#endif // MAINWINDOW_H

