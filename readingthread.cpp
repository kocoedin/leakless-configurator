#include "readingthread.h"

void ReadingThread::run(){
    QList<quint32> MemoryArray, AddressArray;
    m_currentAddress=0;
    QTimer *Timer=new QTimer();
    Timer->setSingleShot(true);
    connect(Timer, SIGNAL(timeout()), this, SLOT(TimerTimeout()));
    for (int i=0;i<m_memorySize/4;i++){
        progressChanged((i*400)/(m_memorySize));
        m_currentAddress=i*4;
        m_waitingForReply=true;
        m_timeoutFlag=false;
        SendData("flash-read "+QString::number(m_currentAddress,16)+"#");
        qDebug()<<"Sending";
        Timer->start(1000);
        while(m_waitingForReply){
            if(m_timeoutFlag) break;
            QCoreApplication::processEvents();
        }
        Timer->stop();
        if(m_timeoutFlag==true){
            i--;
            continue;
        }
        if(m_stopRequest==true){
            break;
        }
        MemoryArray.append(m_currentMemory);
        AddressArray.append(m_currentAddress);
    }
    progressChanged(100);
    resultReady(AddressArray,MemoryArray);
}
void ReadingThread::RecieveData(QString Data){
    Data=Data.remove("\n").remove("\r");
    if(Data.startsWith("A:")){
        QStringList Split=Data.split(" ");
        if(Split.count()<3) return;
        else{
            quint32 AddressValue, MemoryValue;
            bool ok;
            AddressValue=Split[1].remove("0x").toUInt(&ok,16);
            if(!ok) return;
            Split[3]=Split[3].remove("0x");
            QString FirstByte   =Split[3].mid(6,2);
            QString SecondByte  =Split[3].mid(4,2);
            QString ThirdByte   =Split[3].mid(2,2);
            QString FourthByte  =Split[3].mid(0,2);
            QString FinalValue=FirstByte+SecondByte+ThirdByte+FourthByte;
            MemoryValue=FinalValue.toUInt(&ok,16);
            if(AddressValue==m_currentAddress){
                m_waitingForReply=false;
                m_currentMemory=MemoryValue;
            }
        }
    }
}
void ReadingThread::StopRequest (){
   m_stopRequest=true;

}
void ReadingThread::TimerTimeout (){
    m_timeoutFlag=true;
}
void ReadingThread::SetMemorySize (quint32 MemorySize){
    m_memorySize=MemorySize;
    m_currentAddress=0;
    m_currentMemory=0;
    m_waitingForReply=false;
    m_timeoutFlag=false;
    m_stopRequest=false;
}
int  ReadingThread::GetMemorySize (){
    return m_memorySize;
}
