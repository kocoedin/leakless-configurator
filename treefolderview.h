#ifndef TREEFOLDERVIEW_H
#define TREEFOLDERVIEW_H

#include <QDialog>
#include <QtCore>
#include <QtGui>
#include <QDirModel>
#include <QFileSystemModel>

#include <QListWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QFileDialog>
#include <QList>
#include <QMenu>
#include <QMouseEvent>
#include <QAction>
#include <QTimer>
#include <QIcon>
#include <QMessageBox>
#include <QMimeData>
#include <QUrl>
#include <QDragEnterEvent>
#include <QDragLeaveEvent>
#include <QDragMoveEvent>
#include <QDropEvent>

#include <QDebug>

#include "newplaindialog.h"
#include "dialogwait.h"


namespace Ui {
class TreeFolderView;
}

class TreeFolderView : public QDialog
{
    Q_OBJECT

public:
    explicit TreeFolderView(QWidget *parent = 0);
    ~TreeFolderView();

    QString initialPath;
    void treeInitialSetup();

    void AddProject(QString name, QString pathToItem);
    void AddLocation(QTreeWidgetItem *project, QString name, QString pathToItem);
    void AddDevice(QTreeWidgetItem *location, QString name, QString pathToItem);
    void AddMeasurementFile(QTreeWidgetItem *device, QString name);

signals:
    void dataToPlot(QString Path,QString Name);

public slots:
    void treeRefresh();
    void slotOnDropEvent(QTreeWidgetItem*);
    void slotOnDragMoveEvent(QTreeWidgetItem*);
    void slotOnDragEnterEvent(QTreeWidgetItem*);
    void slotOnDragLeaveEvent(QTreeWidgetItem*);

private slots:

    void dialogClosedRefresh();

    void on_btnChoose_clicked();
    void on_treeWidget_itemDoubleClicked(QTreeWidgetItem *item, int column);
    void on_treeWidget_customContextMenuRequested(const QPoint &pos);
    void showContextMenu(QTreeWidgetItem *item, const QPoint& globalPos);

    void addNewProject();

    void addNewLocation();
    void editProject();
    void deleteProject();

    void addNewDevice();
    void editLocation();
    void deleteLocation();

    void editDevice();
    void deleteDevice();

    void plotData();
    void deleteLog();
    void copyLog();
    void pasteLog();

    void timer_timeout();

    void on_treeWidget_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

private:
    Ui::TreeFolderView *ui;

    QTimer *timer;
    QMenu* contextMenu;
    NewPlainDialog *newPlainDialog;

    QTreeWidgetItem *itemCopy;
    QTreeWidgetItem *itemPaste;
};

#endif // TREEFOLDERVIEW_H
