#ifndef NEWPLAINDIALOG_H
#define NEWPLAINDIALOG_H

#include <QDialog>
#include <QSettings>
#include <QDir>
#include <QTreeWidgetItem>

#include <QDebug>
#include "general.h"

namespace Ui {
class NewPlainDialog;
}

class NewPlainDialog : public QDialog
{
    Q_OBJECT

public:
    explicit NewPlainDialog(QWidget *parent = 0);
    ~NewPlainDialog();

    int folderType;
    int actionType;
    QString fullPath;
    QString _lblName;
    QString _lblComment;

    QString _oldName;
    QString _oldComment;

    QTreeWidgetItem *item;
    QTreeWidget *dialogTreeWidget;

    void refreshDialog();
    bool isInfoIniAvailable();
    void setDialogFromInfoIni();
    void createInfoIni();

signals:
     void dialogClosedOk();


private slots:
    void on_btnOk_clicked();

    void on_btnCancel_clicked();

private:

    Ui::NewPlainDialog *ui;
};

#endif // NEWPLAINDIALOG_H
