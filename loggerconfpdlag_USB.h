#ifndef LoggerConfPDLAG_USB_H
#define LoggerConfPDLAG_USB_H

#include <QWidget>
#include <QTreeWidget>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QCheckBox>
#include <QtGui>
#include <QLineEdit>
#include <QSpinBox>
#include <QDoubleSpinBox>
#include <QFileSystemModel>
#include "plottingwidget.h"
#include "dialogwait.h"
#include "detector.h"

namespace Ui {
class LoggerConfPDLAG_USB;
}

class LoggerConfPDLAG_USB : public QWidget
{
    Q_OBJECT

public:
    explicit LoggerConfPDLAG_USB(QWidget *parent = 0, bool Simple=false);
    ~LoggerConfPDLAG_USB();
    void refereshParameters();

    void SetID(QString ID);
    QString GetID();
    QString rootPath;

    void ConnectToInfoFile(QString Path);

public slots:
    void DriveChangeDetected                (QStringList labels,QStringList names);

private slots:
    void plottingTabWidgetTabCloseRequested (int index);
    void on_dirTree_clicked                 (const QModelIndex &index);
    void on_fileTree_doubleClicked          (const QModelIndex &index);
    void mouseWheel                         (QWheelEvent* event);

    void ComboBox_changed           (int Value);
    void TextBox_changed            (QString Value);
    void CheckBox_changed           (bool Value);
    void SpinBox_changed            (int Value);
    void DoubleSpinBox_changed      (double Value);
    void DateTimeBox_changed        (QDateTime Value);
    void RecieveData                (QString ID,QString Data);
    void sendingProcedure           ();

    void on_Realtime_START_clicked  ();
    void on_Realtime_STOP_clicked   ();
    void drawStreamPlot             ();

    void on_jpegSave_clicked        ();
    void on_pngSave_clicked         ();
    void on_pdfSave_clicked         ();

    void on_btnSaveInfoFile_clicked();

signals:
    void SendData(QString ID, QString Data);

private:
    void FindAllTreeItems   (QTreeWidgetItem *item );
    void plotData           (QString Path,QString Name);
    void FindLoggerDrive    ();

    Ui::LoggerConfPDLAG_USB *ui;

    QList<QString>           Commands;
    QList<QTreeWidgetItem *> Items;
    QVector<QColor>          PlotColors;

    QStringList SendingStrings;
    QTimer sendingTimer;
    QTimer streamingTimer;

    QFileSystemModel *dirModel;
    QFileSystemModel *fileModel;

    QString LoggerID;

    QFile   m_file;
    bool    m_simple;
    int     m_range;
    bool measurementRealtimeStarted;
    double RealtimeCH1Pressure;
    double RealtimeCH2Pressure;
    double RealtimeCH1TotalFlow;
    double RealtimeCH2TotalFlow;
    double RealtimeBattery;
    bool ConfigurationInProgress;
};

#endif // LoggerConfPDLAG_USB_H
