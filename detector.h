#ifndef DETECTOR_H
#define DETECTOR_H

#include <QObject>
#include <QThread>
#include <QSerialPort>
#include <QSerialPortInfo>
#include <QWidget>
#include <QSemaphore>
#include <QtSerialPort/QSerialPort>
#include <QDir>
#include <QTimer>
#include <QDateTime>



class serialPort: public QObject
{

    Q_OBJECT

public:
    serialPort(QObject *parent = 0);
    ~serialPort();

    struct Settings {
        QString name;
        qint32 baudRate;
        QString stringBaudRate;
        QSerialPort::DataBits dataBits;
        QString stringDataBits;
        QSerialPort::Parity parity;
        QString stringParity;
        QSerialPort::StopBits stopBits;
        QString stringStopBits;
        QSerialPort::FlowControl flowControl;
        QString stringFlowControl;
        bool localEchoEnabled;
    };

    void connectToPort(QString PortName);
    void disconnectFromPort();

    bool isConnected(){return m_Connected;}

    QString getSerial();
    QString getIdentifier();

signals:
    void portConnected(QString serial,QString Identifier);
    void portDisconnected(QString serial,QString Identifier);
    void dataRecieved(QString serial,QString data);
    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);

public slots:
    void dataSend(QByteArray data);

private slots:
    void dataReady();
    void aboutToDisconnect();

private:
    bool m_Detected;
    bool m_Connected;
    QByteArray Buffer;

    QString Serial;
    QString Identifier;
    QSerialPort *port;
    QSerialPortInfo *portInfo;


};

class DetectorController : public QObject
{
    Q_OBJECT
public:

    DetectorController();
    void UnsubscribeMessages(QString Serial);
    void SubscribeMessages(QString Serial);
    bool IsConnected(QString Serial);
    void sincClocks();
    void stopReporting();
    void startReporting();

    QStringList driveLabelList;
    QStringList driveNameList;
signals:
    void Responded(QString Serial, QString Response);

    void errorSignal(QString ErrorInfo);
    void messageSignal(QString MessageInfo);
    void Connected(QString Serial,QString Identifier);
    void Disconnected(QString Serial,QString Identifier);
    void DriveChangeDetected(QStringList labels,QStringList names );

public slots:
    void SendDataToModule(QString Serial, QString Data);

private slots:
    void DetectingProcedure();
    void DisconnectedDelete(QString Serial,QString Identifier);
    void ConnectedAdd(QString Serial,QString Identifier);
    void DataRecieved(QString Serial, QString Data);

private:
    QString GetDriveLabel(QString &tdrive);

    QTimer *clockTimer;

    serialPort::Settings Postavke;
    serialPort *newSerialPort;
    QList<serialPort*> SerialPorts;   // Ovaj sluzi za povezivanje porta kada se spoji hidrant
    QStringList listPortsActive;
    QStringList listPortsSerial;

};

#endif // DETECTOR_H
