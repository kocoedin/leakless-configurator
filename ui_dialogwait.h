/********************************************************************************
** Form generated from reading UI file 'dialogwait.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DIALOGWAIT_H
#define UI_DIALOGWAIT_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDialog>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>

QT_BEGIN_NAMESPACE

class Ui_Dialogwait
{
public:
    QProgressBar *progressBar;
    QLabel *label_2;
    QLabel *label;

    void setupUi(QDialog *Dialogwait)
    {
        if (Dialogwait->objectName().isEmpty())
            Dialogwait->setObjectName(QStringLiteral("Dialogwait"));
        Dialogwait->setWindowModality(Qt::WindowModal);
        Dialogwait->resize(293, 70);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Dialogwait->setWindowIcon(icon);
        progressBar = new QProgressBar(Dialogwait);
        progressBar->setObjectName(QStringLiteral("progressBar"));
        progressBar->setGeometry(QRect(40, 10, 241, 23));
        progressBar->setValue(24);
        progressBar->setTextVisible(false);
        label_2 = new QLabel(Dialogwait);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 10, 21, 20));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Measurement_Units/time/time-256.png")));
        label_2->setScaledContents(true);
        label = new QLabel(Dialogwait);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(70, 40, 201, 20));

        retranslateUi(Dialogwait);

        QMetaObject::connectSlotsByName(Dialogwait);
    } // setupUi

    void retranslateUi(QDialog *Dialogwait)
    {
        Dialogwait->setWindowTitle(QApplication::translate("Dialogwait", "Please wait", 0));
        label_2->setText(QString());
        label->setText(QApplication::translate("Dialogwait", "Parsing the saved log. Please wait ....", 0));
    } // retranslateUi

};

namespace Ui {
    class Dialogwait: public Ui_Dialogwait {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DIALOGWAIT_H
