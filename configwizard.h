#ifndef CONFIGWIZARD_H
#define CONFIGWIZARD_H

#include <QDialog>
#include <QSettings>
#include <QFileDialog>
#include <QProgressDialog>
#include <QTcpSocket>
#include <QMessageBox>
#include <QtNetwork>
#include <QCamera>
#include <QCameraControl>
#include <QCameraInfo>
#include <QCameraViewfinder>
#include <QCameraImageCapture>
#include "mainwindow.h"

namespace Ui {
class configWizard;
}

class configWizard : public QDialog
{
    Q_OBJECT

public:
    explicit configWizard(MainWindow *parent = 0);
    ~configWizard();
    void showLastThreeImages();

private slots:


    void on_MeasurementType_currentTextChanged(const QString &arg1);

    void on_Duzina_currentTextChanged(const QString &arg1);

    void on_Sirina_currentTextChanged(const QString &arg1);

    void on_Location_currentTextChanged(const QString &arg1);

    void on_tabs_currentChanged(int index);

    void on_btnNext_clicked();

    void on_rbxIntervalMjerenje_toggled(bool checked);

    void on_rbxBrzoMjerenje_toggled(bool checked);

    void on_btnPrevious_clicked();

    void on_btnRefreshCameras_clicked();

    void on_cbxCameraOption_currentIndexChanged(const QString &arg1);

    void on_configWizard_finished(int result);

    void on_btnTakePicture_clicked();

    void MessageRecieved(QString Serial,QString Message);

    void MeasureTimerTimeout();
    void on_cancelButton_clicked();

    void on_Height_currentTextChanged(const QString &arg1);

private:
    void StartConfigurationProcedure();
    void copyImage(QString source, QString destination, Dialogwait *progressDialog);
    Ui::configWizard *ui;
    QCamera *camera;
    QCameraViewfinder *viewfinder;
    QCameraImageCapture *imageCapture;
    QTimer *MeasureTimer;
    MainWindow *mParent;
    QStringList CameraPicturesPaths;
    int mCompleteCounter;
    double mMinimumPressure;
    bool Stage1Finished;
    bool Stage2Finished;
    bool Stage3Finished;
    bool Stage4Finished;
    bool cameraActive;
};

#endif // CONFIGWIZARD_H
