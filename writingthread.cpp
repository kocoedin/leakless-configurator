#include "writingthread.h"

void WritingThread::run(){
    QList<quint32> MemoryArray, AddressArray;
    m_currentAddress=0;
    QTimer *Timer=new QTimer();
    Timer->setSingleShot(true);
    connect(Timer, SIGNAL(timeout()), this, SLOT(TimerTimeout()));
    for (int i=0;i<m_memorySize/4/8;i++){
        progressChanged((i*400*8)/(m_memorySize));
        m_currentAddress=i*4*8;
        m_waitingForReply=true;
        m_timeoutFlag=false;
        QString DataString;
        for (int g=0;g<8;g++){
            QString FirstByte   =QString("%1").arg((m_memoryArray.at(i*8+g)>>24)&0xFF, 2, 16, QChar('0'));
            QString SecondByte  =QString("%1").arg((m_memoryArray.at(i*8+g)>>16)&0xFF, 2, 16, QChar('0'));
            QString ThirdByte   =QString("%1").arg((m_memoryArray.at(i*8+g)>>8)&0xFF, 2, 16, QChar('0'));
            QString FourthByte  =QString("%1").arg((m_memoryArray.at(i*8+g)>>0)&0xFF, 2, 16, QChar('0'));

            DataString =DataString+" "+ SecondByte+FirstByte+FourthByte+ThirdByte;
        }
        SendData("flash-wd "+QString::number(m_currentAddress,16)+DataString+"#");
        Timer->start(1000);
        while(m_waitingForReply){
            if(m_timeoutFlag) break;
            QCoreApplication::processEvents();
        }
        Timer->stop();
        if(m_timeoutFlag==true){
            i--;
            continue;
        }
        if(m_stopRequest==true){
            break;
        }
        MemoryArray.append(m_currentMemory);
        AddressArray.append(m_currentAddress);
    }
    progressChanged(100);
    resultReady();
}
void WritingThread::RecieveData(QString Data){
    Data=Data.remove("\n").remove("\r");
    if(Data.startsWith("A:")){
        QStringList Split=Data.split(" ");
        if(Split.count()<2) return;
        else{
            quint32 AddressValue, MemoryValue;
            bool ok;
            AddressValue=Split[1].remove("0x").toUInt(&ok,16);
            if(!ok) return;
            if(AddressValue==m_currentAddress){
                m_waitingForReply=false;
                m_currentMemory=MemoryValue;
            }
        }
    }
}
void WritingThread::StopRequest (){
   m_stopRequest=true;

}
void WritingThread::TimerTimeout (){
    m_timeoutFlag=true;
}
void WritingThread::SetMemory (quint32 MemorySize, QList<quint32> MemoryArray){
    m_memoryArray=MemoryArray;
    m_memorySize=MemorySize;
    m_currentAddress=0;
    m_currentMemory=0;
    m_waitingForReply=false;
    m_timeoutFlag=false;
    m_stopRequest=false;
}
