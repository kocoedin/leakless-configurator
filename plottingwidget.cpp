#include "plottingwidget.h"
#include "ui_plottingwidget.h"

plottingWidget::plottingWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::plottingWidget)
{
    srand(QDateTime::currentDateTime().toTime_t());
    ui->setupUi(this);

    ui->customPlot->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectLegend | QCP::iSelectPlottables);
    ui->customPlot->xAxis->rescale();
    ui->customPlot->yAxis->rescale();
    ui->customPlot->axisRect()->setupFullAxesBox();

    //ui->customPlot->plotLayout()->insertRow(0);
    //ui->customPlot->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->customPlot, "Plot"));

    ui->customPlot->xAxis->setLabel("Time");
    ui->customPlot->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    ui->customPlot->xAxis->setDateTimeFormat("MM-dd \n hh:mm:ss");


    ui->customPlot->yAxis->setLabel("Value");
    ui->customPlot->legend->setVisible(true);
    QFont legendFont = font();
    legendFont.setPointSize(10);
    ui->customPlot->legend->setFont(legendFont);
    ui->customPlot->legend->setSelectedFont(legendFont);
    ui->customPlot->legend->setSelectableParts(QCPLegend::spItems); // legend box shall not be selectable, only legend items

    connect(ui->customPlot, SIGNAL(selectionChangedByUser()), this, SLOT(selectionChanged()));
    connect(ui->customPlot, SIGNAL(mousePress(QMouseEvent*)), this, SLOT(mousePress()));
    connect(ui->customPlot->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->customPlot->yAxis2, SLOT(setRange(QCPRange)));
    connect(ui->customPlot, SIGNAL(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)), this, SLOT(titleDoubleClick(QMouseEvent*,QCPPlotTitle*)));
    connect(ui->customPlot, SIGNAL(axisDoubleClick(QCPAxis*,QCPAxis::SelectablePart,QMouseEvent*)), this, SLOT(axisLabelDoubleClick(QCPAxis*,QCPAxis::SelectablePart)));
    connect(ui->customPlot, SIGNAL(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*,QMouseEvent*)), this, SLOT(legendDoubleClick(QCPLegend*,QCPAbstractLegendItem*)));
    connect(ui->customPlot, SIGNAL(plottableClick(QCPAbstractPlottable*,QMouseEvent*)), this, SLOT(graphClicked(QCPAbstractPlottable*)));

    // setup policy and connect slot for context menu popup:
    ui->customPlot->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(ui->customPlot, SIGNAL(customContextMenuRequested(QPoint)), this, SLOT(contextMenuRequest(QPoint)));
    ui->customPlot->yAxis->rescale();

    qDebug()<<"Enter";
}


plottingWidget::~plottingWidget()
{
    delete ui;
}

void plottingWidget::titleDoubleClick(QMouseEvent* event, QCPPlotTitle* title)
{
    Q_UNUSED(event)
    // Set the plot title by double clicking on it
    bool ok;
    QString newTitle = QInputDialog::getText(this, "Change plot title", "New plot title:", QLineEdit::Normal, title->text(), &ok);
    if (ok)
    {
        title->setText(newTitle);
        ui->customPlot->replot();
    }
}
void plottingWidget::axisLabelDoubleClick(QCPAxis *axis, QCPAxis::SelectablePart part)
{
    // Set an axis label by double clicking on it
    if (part == QCPAxis::spAxisLabel) // only react when the actual axis label is clicked, not tick label or axis backbone
    {
        bool ok;
        QString newLabel = QInputDialog::getText(this, "Change axis title", "New axis label:", QLineEdit::Normal, axis->label(), &ok);
        if (ok)
        {
            axis->setLabel(newLabel);
            ui->customPlot->replot();
        }
    }
}
void plottingWidget::legendDoubleClick(QCPLegend *legend, QCPAbstractLegendItem *item)
{
    // Rename a graph by double clicking on its legend item
    Q_UNUSED(legend)
    if (item) // only react if item was clicked (user could have clicked on border padding of legend where there is no item, then item is 0)
    {
        QCPPlottableLegendItem *plItem = qobject_cast<QCPPlottableLegendItem*>(item);
        bool ok;
        QString newName = QInputDialog::getText(this, "QCustomPlot example", "New graph name:", QLineEdit::Normal, plItem->plottable()->name(), &ok);
        if (ok)
        {
            plItem->plottable()->setName(newName);
            ui->customPlot->replot();
        }
    }
}
void plottingWidget::selectionChanged()
{
    /*
     normally, axis base line, axis tick labels and axis labels are selectable separately, but we want
     the user only to be able to select the axis as a whole, so we tie the selected states of the tick labels
     and the axis base line together. However, the axis label shall be selectable individually.

     The selection state of the left and right axes shall be synchronized as well as the state of the
     bottom and top axes.

     Further, we want to synchronize the selection of the graphs with the selection state of the respective
     legend item belonging to that graph. So the user can select a graph by either clicking on the graph itself
     or on its legend item.
    */

    // make top and bottom axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
            ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->xAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
    {
        ui->customPlot->xAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->customPlot->xAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }
    // make left and right axes be selected synchronously, and handle axis and tick labels as one selectable object:
    if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spTickLabels) ||
            ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spAxis) || ui->customPlot->yAxis2->selectedParts().testFlag(QCPAxis::spTickLabels))
    {
        ui->customPlot->yAxis2->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
        ui->customPlot->yAxis->setSelectedParts(QCPAxis::spAxis|QCPAxis::spTickLabels);
    }

    // synchronize selection of graphs with selection of corresponding legend items:
    for (int i=0; i<ui->customPlot->graphCount(); ++i)
    {
        QCPGraph *graph = ui->customPlot->graph(i);
        QCPPlottableLegendItem *item = ui->customPlot->legend->itemWithPlottable(graph);
        if (item->selected() || graph->selected())
        {
            item->setSelected(true);
            graph->setSelected(true);
        }
    }
}
void plottingWidget::mousePress()
{
    // if an axis is selected, only allow the direction of that axis to be dragged
    // if no axis is selected, both directions may be dragged

    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->xAxis->orientation());
    else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeDrag(ui->customPlot->yAxis->orientation());
    else
        ui->customPlot->axisRect()->setRangeDrag(Qt::Horizontal|Qt::Vertical);
}
void plottingWidget::mouseWheel()
{
    // if an axis is selected, only allow the direction of that axis to be zoomed
    // if no axis is selected, both directions may be zoomed

    if (ui->customPlot->xAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->xAxis->orientation());
    else if (ui->customPlot->yAxis->selectedParts().testFlag(QCPAxis::spAxis))
        ui->customPlot->axisRect()->setRangeZoom(ui->customPlot->yAxis->orientation());
    else
        ui->customPlot->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);
}

void plottingWidget::addRandomGraph()
{
    int n = 50; // number of points in graph
    double xScale = (rand()/(double)RAND_MAX + 0.5)*2;
    double yScale = (rand()/(double)RAND_MAX + 0.5)*2;
    double xOffset = (rand()/(double)RAND_MAX - 0.5)*4;
    double yOffset = (rand()/(double)RAND_MAX - 0.5)*5;
    double r1 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r2 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r3 = (rand()/(double)RAND_MAX - 0.5)*2;
    double r4 = (rand()/(double)RAND_MAX - 0.5)*2;
    QVector<double> x(n), y(n);
    for (int i=0; i<n; i++)
    {
        x[i] = (i/(double)n-0.5)*10.0*xScale + xOffset;
        y[i] = (sin(x[i]*r1*5)*sin(cos(x[i]*r2)*r4*3)+r3*cos(sin(x[i])*r4*2))*yScale + yOffset;
    }

    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("New graph %1").arg(ui->customPlot->graphCount()-1));
    ui->customPlot->graph()->setData(x, y);
    ui->customPlot->graph()->setLineStyle((QCPGraph::LineStyle)(rand()%5+1));
    if (rand()%100 > 75)  ui->customPlot->graph()->setScatterStyle(QCPScatterStyle((QCPScatterStyle::ScatterShape)(rand()%9+1)));
    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();

}
void plottingWidget::addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit,QColor Color){

    if (time.count()!=valueDouble.count()) return;
    QVector<double> timeDouble;
    for (int i=0; i<time.count();i++){
        timeDouble.append(time[i].toMSecsSinceEpoch()/1000.0);
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("%1 %2").arg(Name,Unit));
    ui->customPlot->graph()->setData(timeDouble, valueDouble);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssNone);
    QPen graphPen;
    graphPen.setColor(Color);
    //graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->xAxis->grid()->setVisible(false);
    ui->customPlot->replot();
}
void plottingWidget::addGraph(QVector<double> timeDouble, QVector<double> valueDouble, QString Name, QString Unit,QColor Color){

    if (timeDouble.count()!=valueDouble.count()) return;
    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("%1 %2").arg(Name,Unit));
    ui->customPlot->graph()->setData(timeDouble, valueDouble);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssNone);
    QPen graphPen;
    graphPen.setColor(Color);
    //graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->xAxis->grid()->setVisible(false);
    ui->customPlot->replot();
}
void plottingWidget::addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit, QVector<QDateTime> AlarmTimes, QVector<QString> AlarmNames){

    if (time.count()!=valueDouble.count()) return;
    QVector<double> timeDouble;
    QVector<double> alarmTimeDouble;
    QVector<double> alarmValueDouble;
    for (int i=0; i<AlarmTimes.count();i++) {
        alarmTimeDouble.append(AlarmTimes[i].toTime_t());
    }

    for (int i=0; i<time.count();i++) {
        timeDouble.append(time[i].toTime_t());
    }
    ui->customPlot->addGraph();
    ui->customPlot->graph()->setName(QString("%1 [%2]").arg(Name,Unit));
    ui->customPlot->graph()->setData(timeDouble, valueDouble);
    ui->customPlot->graph()->setLineStyle(QCPGraph::lsLine);
    ui->customPlot->graph()->setScatterStyle(QCPScatterStyle::ssNone);

    QPen graphPen;
    graphPen.setColor(QColor(rand()%245+10, rand()%245+10, rand()%245+10));
    graphPen.setWidthF(rand()/(double)RAND_MAX*2+1);
    ui->customPlot->graph()->setPen(graphPen);
    ui->customPlot->replot();

    QPen selectedPen;
    selectedPen.setColor(QColor(255,215,0));
    selectedPen.setWidthF(graphPen.widthF());
    ui->customPlot->graph()->setSelectedPen(selectedPen);
    ui->customPlot->rescaleAxes();
    ui->customPlot->replot();

    // Ovdje crtam alarme
    for (int i=0;i<alarmTimeDouble.size();i++){

       // add the group velocity tracer (green circle):

       QCPItemTracer *groupTracer = new QCPItemTracer(ui->customPlot);
       ui->customPlot->addItem(groupTracer);
       groupTracer->setGraph(ui->customPlot->graph());
       groupTracer->setGraphKey(alarmTimeDouble.at(i));
       groupTracer->setInterpolating(true);
       groupTracer->setStyle(QCPItemTracer::tsCircle);
       groupTracer->setPen(QPen(Qt::red));
       groupTracer->setBrush(Qt::red);
       groupTracer->setSize(7);
       groupTracer->updatePosition();

       QCPItemText *phaseTracerText = new QCPItemText(ui->customPlot);
       ui->customPlot->addItem(phaseTracerText);
       phaseTracerText->position->setCoords(groupTracer->position->coords().rx(),groupTracer->position->coords().ry()); // lower right corner of axis rect
       phaseTracerText->setText(AlarmNames.at(i));
       phaseTracerText->setColor(Qt::red);
       phaseTracerText->setFont(QFont(font().family(), 12));
       phaseTracerText->setTextAlignment(Qt::AlignBottom);

/*
       QCPItemCurve *phaseTracerArrow = new QCPItemCurve(ui->customPlot);
       ui->customPlot->addItem(phaseTracerArrow);
       phaseTracerArrow->start->setParentAnchor(phaseTracerText->left);
       phaseTracerArrow->startDir->setParentAnchor(phaseTracerArrow->start);
       phaseTracerArrow->startDir->setCoords(-40, 0); // direction 30 pixels to the left of parent anchor (tracerArrow->start)
       phaseTracerArrow->end->setParentAnchor(groupTracer->position);
       phaseTracerArrow->end->setCoords(0, -10);
       phaseTracerArrow->endDir->setParentAnchor(phaseTracerArrow->end);
       phaseTracerArrow->endDir->setCoords(0, -90);
       phaseTracerArrow->setHead(QCPLineEnding::esSpikeArrow);
       phaseTracerArrow->setTail(QCPLineEnding(QCPLineEnding::esBar, (phaseTracerText->bottom->pixelPoint().y()-phaseTracerText->top->pixelPoint().y())*0.85));
*/
    }


}
void plottingWidget::removeSelectedGraph()
{
    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        ui->customPlot->removeGraph(ui->customPlot->selectedGraphs().first());
        ui->customPlot->replot();
    }
}


void plottingWidget::changeLineColorSelectedGraph(QAction *ActionClicked)
{
    QPen plotPen;
    if (ui->customPlot->selectedGraphs().size() > 0)
    {

        plotPen.setColor(ui->customPlot->selectedGraphs().first()->pen().color());
        plotPen.setWidthF(ui->customPlot->selectedGraphs().first()->pen().widthF());

        if(ActionClicked->text().startsWith("Red", Qt::CaseInsensitive))    plotPen.setColor(Qt::red);
        if(ActionClicked->text().startsWith("Blue", Qt::CaseInsensitive))   plotPen.setColor(Qt::blue);
        if(ActionClicked->text().startsWith("Green", Qt::CaseInsensitive))  plotPen.setColor(Qt::green);
        if(ActionClicked->text().startsWith("Black", Qt::CaseInsensitive))  plotPen.setColor(Qt::black);
        if(ActionClicked->text().startsWith("Gray", Qt::CaseInsensitive))   plotPen.setColor(Qt::gray);
        if(ActionClicked->text().startsWith("Yellow", Qt::CaseInsensitive)) plotPen.setColor(Qt::yellow);
        if(ActionClicked->text().startsWith("Magenta", Qt::CaseInsensitive))plotPen.setColor(Qt::magenta);
        if(ActionClicked->text().startsWith("Cyan", Qt::CaseInsensitive))   plotPen.setColor(Qt::cyan);

        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                graph->setPen(plotPen);
            }
        }

        ui->customPlot->replot();
    }
}
void plottingWidget::changeLineWidthSelectedGraph(QAction *ActionClicked)
{
    QPen plotPen;
    if (ui->customPlot->selectedGraphs().size() > 0)
    {

        plotPen.setColor(ui->customPlot->selectedGraphs().first()->pen().color());
        plotPen.setWidthF(ui->customPlot->selectedGraphs().first()->pen().widthF());

        if(ActionClicked->text().endsWith("1", Qt::CaseInsensitive)) plotPen.setWidthF(1);
        if(ActionClicked->text().endsWith("2", Qt::CaseInsensitive)) plotPen.setWidthF(2);
        if(ActionClicked->text().endsWith("3", Qt::CaseInsensitive)) plotPen.setWidthF(4);
        if(ActionClicked->text().endsWith("4", Qt::CaseInsensitive)) plotPen.setWidthF(6);
        if(ActionClicked->text().endsWith("5", Qt::CaseInsensitive)) plotPen.setWidthF(8);
        if(ActionClicked->text().endsWith("6", Qt::CaseInsensitive)) plotPen.setWidthF(10);
        if(ActionClicked->text().endsWith("7", Qt::CaseInsensitive)) plotPen.setWidthF(12);
        if(ActionClicked->text().endsWith("8", Qt::CaseInsensitive)) plotPen.setWidthF(14);
        if(ActionClicked->text().endsWith("9", Qt::CaseInsensitive)) plotPen.setWidthF(16);

        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                QPen selectedPen;
                selectedPen.setColor(QColor(255,215,0));
                selectedPen.setWidthF(plotPen.widthF());

                graph->setPen(plotPen);
                graph->setSelectedPen(selectedPen);
            }
        }

        ui->customPlot->replot();
    }
}
void plottingWidget::changeLineStyleSelectedGraph(QAction *ActionClicked)
{
    QPen plotPen;
    if (ui->customPlot->selectedGraphs().size() > 0)
    {

        plotPen.setColor(ui->customPlot->selectedGraphs().first()->pen().color());
        plotPen.setWidthF(ui->customPlot->selectedGraphs().first()->pen().widthF());


        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {

                if(ActionClicked->text().startsWith("Impulse", Qt::CaseInsensitive)) graph->setLineStyle(QCPGraph::lsImpulse);
                if(ActionClicked->text().startsWith("Hold", Qt::CaseInsensitive)) graph->setLineStyle(QCPGraph::lsStepRight);
                if(ActionClicked->text().startsWith("Linear", Qt::CaseInsensitive)) graph->setLineStyle(QCPGraph::lsLine);
                if(ActionClicked->text().startsWith("No", Qt::CaseInsensitive)) graph->setLineStyle(QCPGraph::lsNone);
            }
        }

        ui->customPlot->replot();
    }
}
void plottingWidget::changeScatterColorSelectedGraph(QAction *ActionClicked)
{
    QCPScatterStyle scatterStyle=ui->customPlot->selectedGraphs().first()->scatterStyle();
    QColor Color=QColor(Qt::black);
    if(ActionClicked->text().startsWith("Red", Qt::CaseInsensitive))        Color=QColor(Qt::red);
    if(ActionClicked->text().startsWith("Blue", Qt::CaseInsensitive))       Color=QColor(Qt::blue);
    if(ActionClicked->text().startsWith("Green", Qt::CaseInsensitive))      Color=QColor(Qt::green);
    if(ActionClicked->text().startsWith("Black", Qt::CaseInsensitive))      Color=QColor(Qt::black);
    if(ActionClicked->text().startsWith("Gray", Qt::CaseInsensitive))       Color=QColor(Qt::gray);
    if(ActionClicked->text().startsWith("Yellow", Qt::CaseInsensitive))     Color=QColor(Qt::yellow);
    if(ActionClicked->text().startsWith("Magenta", Qt::CaseInsensitive))    Color=QColor(Qt::magenta);
    if(ActionClicked->text().startsWith("Cyan", Qt::CaseInsensitive))       Color=QColor(Qt::cyan);



    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                ui->customPlot->graph()->setScatterStyle(QCPScatterStyle(scatterStyle.shape(),Color,scatterStyle.size()));
            }
        }

        ui->customPlot->replot();
    }
}
void plottingWidget::changeScatterWidthSelectedGraph(QAction *ActionClicked)
{
    QCPScatterStyle scatterStyle=ui->customPlot->selectedGraphs().first()->scatterStyle();
    float Size=scatterStyle.size();

    if(ActionClicked->text().endsWith("1", Qt::CaseInsensitive))    Size=1;
    if(ActionClicked->text().endsWith("2", Qt::CaseInsensitive))    Size=2;
    if(ActionClicked->text().endsWith("3", Qt::CaseInsensitive))    Size=4;
    if(ActionClicked->text().endsWith("4", Qt::CaseInsensitive))    Size=6;
    if(ActionClicked->text().endsWith("5", Qt::CaseInsensitive))    Size=8;
    if(ActionClicked->text().endsWith("6", Qt::CaseInsensitive))    Size=10;
    if(ActionClicked->text().endsWith("7", Qt::CaseInsensitive))    Size=12;
    if(ActionClicked->text().endsWith("8", Qt::CaseInsensitive))    Size=14;
    if(ActionClicked->text().endsWith("9", Qt::CaseInsensitive))    Size=16;



    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                graph->setScatterStyle(QCPScatterStyle(scatterStyle.shape(),scatterStyle.pen().color(),Size));
            }
        }

        ui->customPlot->replot();
    }
}
void plottingWidget::changeScatterStyleSelectedGraph(QAction *ActionClicked)
{
    QCPScatterStyle scatterStyle=ui->customPlot->selectedGraphs().first()->scatterStyle();





    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph ==graph)
            {
                if(ActionClicked->text().startsWith("Plus", Qt::CaseInsensitive))       graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssPlus,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Dot", Qt::CaseInsensitive))        graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDot,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Circle", Qt::CaseInsensitive))     graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Rectangle", Qt::CaseInsensitive))  graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssSquare,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Cross", Qt::CaseInsensitive))      graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCross,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Star", Qt::CaseInsensitive))       graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssStar,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Triangle", Qt::CaseInsensitive))   graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssTriangle,scatterStyle.pen().color(),scatterStyle.size()));
                if(ActionClicked->text().startsWith("Diamond", Qt::CaseInsensitive))    graph->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDiamond,scatterStyle.pen().color(),scatterStyle.size()));
            }
        }

        ui->customPlot->replot();
    }
}

void plottingWidget::removeAllGraphs()
{
    ui->customPlot->clearGraphs();
    ui->customPlot->replot();
}
void plottingWidget::contextMenuRequest(QPoint pos)
{
    QMenu *menu = new QMenu(this);
    menu->setAttribute(Qt::WA_DeleteOnClose);
    QPixmap pixmap;
    QIcon Icon;
    qDebug()<<"Clicked Graph";
    if (ui->customPlot->legend->selectTest(pos, false) >= 0) // context menu on legend requested
    {
        menu->addAction("Move to top left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignLeft));
        menu->addAction("Move to top center", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignHCenter));
        menu->addAction("Move to top right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignTop|Qt::AlignRight));
        menu->addAction("Move to bottom right", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignRight));
        menu->addAction("Move to bottom left", this, SLOT(moveLegend()))->setData((int)(Qt::AlignBottom|Qt::AlignLeft));
    }
    else  // general context menu on graphs requested
    {
//        menu->addAction("Add random graph", this, SLOT(addRandomGraph()));
        if (ui->customPlot->selectedGraphs().size() > 0){
            menu->addAction("Remove selected graph", this, SLOT(removeSelectedGraph()));

            menu->addSeparator();
                QMenu *editLineMenu = new QMenu(this);
                menu->addMenu(editLineMenu);
                pixmap=QPixmap(":/icons/line-512.png"); Icon=QIcon(pixmap);
                editLineMenu->setTitle("Line options");
                editLineMenu->setIcon(Icon);

                    QMenu *editLineColorMenu = new QMenu(this);
                    editLineMenu->addMenu(editLineColorMenu);
                    pixmap=QPixmap(":/icons/border_color-512.png"); Icon=QIcon(pixmap);

                    editLineColorMenu->setIcon(Icon);
                    editLineColorMenu->setTitle("Line color");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::red);      Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Red");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::blue);     Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Blue");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::green);    Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Green");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::yellow);   Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Yellow");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::cyan);     Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Cyan");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::magenta);  Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Magenta");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::black);    Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Black");
                    pixmap=QPixmap(100,100); pixmap.fill(Qt::gray);     Icon=QIcon(pixmap);
                    editLineColorMenu->addAction(Icon,"Gray");
                    connect(editLineColorMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeLineColorSelectedGraph(QAction *)), Qt::UniqueConnection);

                    QMenu *editLineWidthMenu = new QMenu(this);
                    editLineMenu->addMenu(editLineWidthMenu);
                    pixmap=QPixmap(":/icons/line_width-512.png"); Icon=QIcon(pixmap);

                    editLineWidthMenu->setIcon(Icon);
                    editLineWidthMenu->setTitle("Line width");
                    pixmap=QPixmap(":/icons/1-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 1");
                    pixmap=QPixmap(":/icons/2-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 2");
                    pixmap=QPixmap(":/icons/3-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 3");
                    pixmap=QPixmap(":/icons/4-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 4");
                    pixmap=QPixmap(":/icons/5-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 5");
                    pixmap=QPixmap(":/icons/6-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 6");
                    pixmap=QPixmap(":/icons/7-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 7");
                    pixmap=QPixmap(":/icons/8-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 8");
                    pixmap=QPixmap(":/icons/9-512.png"); Icon=QIcon(pixmap);
                    editLineWidthMenu->addAction(Icon,"Size 9");
                    connect(editLineWidthMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeLineWidthSelectedGraph(QAction *)), Qt::UniqueConnection);

                    QMenu *editLineStyleMenu = new QMenu(this);
                    editLineMenu->addMenu(editLineStyleMenu);
                    pixmap=QPixmap(":/icons/scatter_plot-512_.png"); Icon=QIcon(pixmap);

                    editLineStyleMenu->setIcon(Icon);
                    editLineStyleMenu->setTitle("Line style");
                    pixmap=QPixmap(":/icons/line_chart2-512.png"); Icon=QIcon(pixmap);
                    editLineStyleMenu->addAction(Icon,"No line");
                    pixmap=QPixmap(":/icons/line_chart1-512.png"); Icon=QIcon(pixmap);
                    editLineStyleMenu->addAction(Icon,"Linear");
                    pixmap=QPixmap(":/icons/line_chart3-512.png"); Icon=QIcon(pixmap);
                    editLineStyleMenu->addAction(Icon,"Hold");
                    pixmap=QPixmap(":/icons/line_chart4-512.png"); Icon=QIcon(pixmap);
                    editLineStyleMenu->addAction(Icon,"Impulse");
                    connect(editLineStyleMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeLineStyleSelectedGraph(QAction *)), Qt::UniqueConnection);

            QMenu *editMarkerMenu = new QMenu(this);
            menu->addMenu(editMarkerMenu);
            editMarkerMenu->setTitle("Marker options");
            pixmap=QPixmap(":/icons/marker_pen-512.png"); Icon=QIcon(pixmap);

            editMarkerMenu->setIcon(Icon);

                QMenu *editMarkerColorMenu = new QMenu(this);
                editMarkerMenu->addMenu(editMarkerColorMenu);
                pixmap=QPixmap(":/icons/bg_color-512.png"); Icon=QIcon(pixmap);

                editMarkerColorMenu->setIcon(Icon);
                editMarkerColorMenu->setTitle("Marker color");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::red);      Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Red");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::blue);     Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Blue");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::green);    Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Green");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::yellow);   Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Yellow");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::cyan);     Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Cyan");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::magenta);  Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Magenta");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::black);    Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Black");
                pixmap=QPixmap(100,100); pixmap.fill(Qt::gray);     Icon=QIcon(pixmap);
                editMarkerColorMenu->addAction(Icon,"Gray");
                connect(editMarkerColorMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeScatterColorSelectedGraph(QAction *)), Qt::UniqueConnection);


                QMenu *editMarkerWidthMenu = new QMenu(this);
                editMarkerMenu->addMenu(editMarkerWidthMenu);
                pixmap=QPixmap(":/icons/fit_to_width-512.png"); Icon=QIcon(pixmap);

                editMarkerWidthMenu->setIcon(Icon);
                editMarkerWidthMenu->setTitle("Marker size");
                pixmap=QPixmap(":/icons/1-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 1");
                pixmap=QPixmap(":/icons/2-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 2");
                pixmap=QPixmap(":/icons/3-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 3");
                pixmap=QPixmap(":/icons/4-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 4");
                pixmap=QPixmap(":/icons/5-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 5");
                pixmap=QPixmap(":/icons/6-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 6");
                pixmap=QPixmap(":/icons/7-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 7");
                pixmap=QPixmap(":/icons/8-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 8");
                pixmap=QPixmap(":/icons/9-512.png"); Icon=QIcon(pixmap);
                editMarkerWidthMenu->addAction(Icon,"Size 9");
                connect(editMarkerWidthMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeScatterWidthSelectedGraph(QAction *)), Qt::UniqueConnection);

                QMenu *editMarkerStyleMenu = new QMenu(this);
                editMarkerMenu->addMenu(editMarkerStyleMenu);
                pixmap=QPixmap(":/icons/rounded_rectangle-512.png"); Icon=QIcon(pixmap);

                editMarkerStyleMenu->setIcon(Icon);
                editMarkerStyleMenu->setTitle("Marker shape");
                pixmap=QPixmap(":/icons/rectangle_stroked-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Rectangle");
                pixmap=QPixmap(":/icons/diamond-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Diamond");
                pixmap=QPixmap(":/icons/triangle_stroked-256.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Triangle");
                pixmap=QPixmap(":/icons/right_round-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Circle");
                pixmap=QPixmap(":/icons/X-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Cross");
                pixmap=QPixmap(":/icons/christmas_star-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Star");
                pixmap=QPixmap(":/icons/record-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Dot");
                pixmap=QPixmap(":/icons/plus2-512.png"); Icon=QIcon(pixmap);
                editMarkerStyleMenu->addAction(Icon,"Plus");
                connect(editMarkerStyleMenu, SIGNAL(triggered(QAction *)),this, SLOT(changeScatterStyleSelectedGraph(QAction *)), Qt::UniqueConnection);



        }
        if (ui->customPlot->graphCount() > 0)            menu->addSeparator();


            pixmap=QPixmap(":/icons/remove_image-128.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Remove all graphs", this, SLOT(removeAllGraphs()));

            pixmap=QPixmap(":/icons/resize-512.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Focus plot", this, SLOT(adjustAxes()));

            pixmap=QPixmap(":/icons/pdf-512.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Save to PDF");

            pixmap=QPixmap(":/icons/png-512.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Save to PNG");

            pixmap=QPixmap(":/icons/csv-512.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Save to CSV");

            pixmap=QPixmap(":/icons/txt-512.png"); Icon=QIcon(pixmap);
            menu->addAction(Icon,"Save to TXT");
            connect(menu, SIGNAL(triggered(QAction *)),this, SLOT(saveFigure(QAction *)), Qt::UniqueConnection);
    }

    menu->popup(ui->customPlot->mapToGlobal(pos));
}
void plottingWidget::saveFigure(QAction *Action)
{
   if (Action->text()=="Save to PDF"){

       QString fileName = QFileDialog::getSaveFileName(this,"Save to .pdf","", tr("PDF Files (*.pdf)"));
       ui->customPlot->savePdf(fileName+".pdf",false,1024,768);
   }
   if (Action->text()=="Save to PNG"){
       QString fileName = QFileDialog::getSaveFileName(this,"Save to .png","", tr("PNG Files (*.png)"));
       ui->customPlot->savePng(fileName+".png",1600,900,1);
   }
   if (Action->text()=="Save to TXT"){
       if (ui->customPlot->graphCount()!=2) return;
       const QCPDataMap *dataMapSensor = ui->customPlot->graph(0)->data();
       const QCPDataMap *dataMapBattery = ui->customPlot->graph(1)->data();

       QString fileName = QFileDialog::getSaveFileName(this,"Save data to .txt","", tr("TXT Files (*.txt)"));
       QFile dataSensor(tr("%1").arg(fileName));
       if(dataSensor.open(QFile::WriteOnly |QFile::Truncate))
       {
           QTextStream output(&dataSensor);
           output << "TimeStamp;Pressure;Battery\r\n";
           QMap<double, QCPData>::const_iterator i = dataMapSensor->constBegin();
           QMap<double, QCPData>::const_iterator j = dataMapBattery->constBegin();
           while (i != dataMapSensor->constEnd()) {
               QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key()*1000);
               output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value)<<"; " << tr("%1").arg(j.value().value)  <<"\r\n"<< endl;
               ++i;
               ++j;
           }
       }
       dataSensor.close();

   }
   if (Action->text()=="Save to CSV"){
       if (ui->customPlot->graphCount()!=2) return;
       const QCPDataMap *dataMapSensor = ui->customPlot->graph(0)->data();
       const QCPDataMap *dataMapBattery = ui->customPlot->graph(1)->data();

       QString fileName = QFileDialog::getSaveFileName(this,"Save data to .csv","", tr("CSV Files (*.csv)"));
       QFile dataSensor(tr("%1").arg(fileName));
       if(dataSensor.open(QFile::WriteOnly |QFile::Truncate))
       {
           QTextStream output(&dataSensor);
           output << "TimeStamp;Pressure;Battery\n";
           QMap<double, QCPData>::const_iterator i = dataMapSensor->constBegin();
           QMap<double, QCPData>::const_iterator j = dataMapBattery->constBegin();
           while (i != dataMapSensor->constEnd()) {
               QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key()*1000);
               output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value)<<"; " << tr("%1").arg(j.value().value)  << endl;
               ++i;
               ++j;
           }
       }
       dataSensor.close();

   }
}
void plottingWidget::adjustAxes()
{
    if (ui->customPlot->selectedGraphs().size() > 0)
    {
        for (int i=0; i<ui->customPlot->graphCount(); ++i)
        {
            QCPGraph *graph = ui->customPlot->graph(i);
            QCPGraph *selectedGraph = ui->customPlot->selectedGraphs().first();
            if (selectedGraph == graph)
            {
                ui->customPlot->graph(i)->rescaleAxes();
            }
        }

        ui->customPlot->replot();
    }else{
        ui->customPlot->rescaleAxes();
        ui->customPlot->replot();
    }
}
void plottingWidget::moveLegend()
{
    if (QAction* contextAction = qobject_cast<QAction*>(sender())) // make sure this slot is really called by a context menu action, so it carries the data we need
    {
        bool ok;
        int dataInt = contextAction->data().toInt(&ok);
        if (ok)
        {
            ui->customPlot->axisRect()->insetLayout()->setInsetAlignment(0, (Qt::Alignment)dataInt);
            ui->customPlot->replot();
        }
    }
}
void plottingWidget::graphClicked(QCPAbstractPlottable *plottable)
{

    //ui->statusBar->showMessage(QString("Clicked on graph '%1'.").arg(plottable->name()), 1000);
}





void plottingWidget::on_pdfSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Save to .pdf","", tr("PDF Files (*.pdf)"));
    ui->customPlot->savePdf(fileName,false,1024,768);
}

void plottingWidget::on_pngSave_clicked()
{

    QString fileName = QFileDialog::getSaveFileName(this,"Save to .png","", tr("PNG Files (*.png)"));
    ui->customPlot->savePng(fileName,1024,768);
}

void plottingWidget::on_jpegSave_clicked()
{

    QString fileName = QFileDialog::getSaveFileName(this,"Save to .jpg","", tr("PDF Files (*.jpg)"));
    ui->customPlot->saveJpg(fileName,1024,768);
}

void plottingWidget::on_txtSave_clicked()
{

    const QCPDataMap *dataMapSensor = ui->customPlot->graph(0)->data();
    const QCPDataMap *dataMapBattery = ui->customPlot->graph(1)->data();

        QString fileName1 = QFileDialog::getSaveFileName(this,"Save pressure data to .txt","", tr("TXT Files (*.txt)"));
        QFile dataSensor(tr("%1").arg(fileName1));
        if(dataSensor.open(QFile::WriteOnly |QFile::Truncate))
        {
            QTextStream output(&dataSensor);
            output << "TimeStamp;Pressure\n";
            QMap<double, QCPData>::const_iterator i = dataMapSensor->constBegin();
            while (i != dataMapSensor->constEnd()) {
                QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key());
                output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value) << endl;
                ++i;
            }
        }
        dataSensor.close();
        QString fileName2 = QFileDialog::getSaveFileName(this,"Save battery data to .txt","", tr("TXT Files (*.txt)"));
        QFile dataBattery(tr("%1").arg(fileName2));
        if(dataBattery.open(QFile::WriteOnly |QFile::Truncate))
        {
            QTextStream output(&dataBattery);
            output << "TimeStamp;Battery\n";
            QMap<double, QCPData>::const_iterator i = dataMapBattery->constBegin();
            while (i != dataMapSensor->constEnd()) {
                QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key());
                output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value) << endl;
                ++i;
            }
        }

        dataBattery.close();
}

void plottingWidget::on_csvSave_clicked()
{

    const QCPDataMap *dataMapSensor = ui->customPlot->graph(0)->data();
    const QCPDataMap *dataMapBattery = ui->customPlot->graph(1)->data();

        QString fileName1 = QFileDialog::getSaveFileName(this,"Save pressure data to .csv","", tr("CSV Files (*.csv)"));
        QFile dataSensor(tr("%1").arg(fileName1));
        if(dataSensor.open(QFile::WriteOnly |QFile::Truncate))
        {
            QTextStream output(&dataSensor);
            output << "TimeStamp;Pressure\n";
            QMap<double, QCPData>::const_iterator i = dataMapSensor->constBegin();
            while (i != dataMapSensor->constEnd()) {
                QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key());
                output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value) << endl;
                ++i;
            }
        }
        dataSensor.close();

        QString fileName2 = QFileDialog::getSaveFileName(this,"Save battery data to .csv","", tr("CSV Files (*.csv)"));
        QFile dataBattery(tr("%1").arg(fileName2));
        if(dataBattery.open(QFile::WriteOnly |QFile::Truncate))
        {
            QTextStream output(&dataBattery);
            output << "TimeStamp;Battery\n";
            QMap<double, QCPData>::const_iterator i = dataMapBattery->constBegin();
            while (i != dataMapSensor->constEnd()) {
                QDateTime time=QDateTime::fromMSecsSinceEpoch(i.key());
                output << time.toString("HH:mm:ss.zzz dd/MM/yyyy")<< "; " << tr("%1").arg(i.value().value) << endl;
                ++i;
            }
        }
        dataBattery.close();
}

