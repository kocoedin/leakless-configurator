#ifndef READINGTHREAD_H
#define READINGTHREAD_H

#include <QWidget>
#include <QThread>
#include <QComboBox>
#include <QDateTimeEdit>
#include <QCheckBox>
#include <QtGui>
#include <QLineEdit>
#include <QSpinBox>
#include <QList>
#include <QDoubleSpinBox>
#include <QFileSystemModel>
#include "plottingwidget.h"
#include "dialogwait.h"
#include "detector.h"


class ReadingThread : public QThread
{
    Q_OBJECT
    void run();
public:
    void SetMemorySize (quint32 MemorySize);
    int  GetMemorySize ();

public slots:
    void StopRequest ();
    void RecieveData (QString Data);
    void TimerTimeout ();


signals:
    void SendData(QString Data);
    void resultReady(QList<quint32> AddressArray,QList<quint32> DataArray);
    void progressChanged(int progress);

private:
    quint32 m_memorySize;
    quint32 m_currentAddress;
    quint32 m_currentMemory;
    bool m_timeoutFlag;
    bool m_waitingForReply;
    bool m_stopRequest;
};

#endif // READINGTHREAD_H
