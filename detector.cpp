#include "detector.h"
#include "qt_windows.h"

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>
#include "QDebug"


void CheckSpace(QString DriveName){
    BOOL  fResult;
    wchar_t * pszDrive = &DriveName.toStdWString().at(0); // here you can change the letter of your drive.
    qint64 i64FreeBytesToCaller,i64TotalBytes,i64FreeBytes;
    fResult = GetDiskFreeSpaceEx (pszDrive,(PULARGE_INTEGER)&i64FreeBytesToCaller,(PULARGE_INTEGER)&i64TotalBytes,(PULARGE_INTEGER)&i64FreeBytes);
    if (fResult)
    {
    }
}
/* Kontroler za komunikaciju prema modulu*/
DetectorController::DetectorController()
{
    clockTimer = new QTimer();
    connect(clockTimer, SIGNAL(timeout()), this, SLOT(DetectingProcedure()));
    clockTimer->start(1000);
}

void DetectorController::DetectingProcedure()
{
    QSerialPortInfo portInfo;
    QStringList AvailablePorts;
    foreach (const QSerialPortInfo &infoSerial, portInfo.availablePorts()) {
        if(infoSerial.vendorIdentifier()==65282 && (infoSerial.productIdentifier()==2||infoSerial.productIdentifier()==3||infoSerial.productIdentifier()==5)){ // Predpostavimo da samo ova kombinacija garantira da je nas uredaj spojen na komp
            if(!listPortsActive.contains(infoSerial.portName())){
                newSerialPort=new serialPort(this);
                newSerialPort->connectToPort(infoSerial.portName());
                if(newSerialPort->isConnected()){

                    connect(newSerialPort,SIGNAL(messageSignal(QString)) , this, SIGNAL(messageSignal(QString)));
                    connect(newSerialPort,SIGNAL(errorSignal(QString))   , this, SIGNAL(errorSignal(QString)));

                    connect(newSerialPort,SIGNAL(portConnected(QString,QString))    , this, SIGNAL(Connected(QString,QString)));
                    connect(newSerialPort,SIGNAL(portConnected(QString,QString))    , this, SLOT(ConnectedAdd(QString,QString)));

                    connect(newSerialPort,SIGNAL(portDisconnected(QString,QString)) , this, SIGNAL(Disconnected(QString,QString)));
                    connect(newSerialPort,SIGNAL(portDisconnected(QString,QString)) , this, SLOT(DisconnectedDelete(QString,QString)));

                    connect(newSerialPort,SIGNAL(dataRecieved(QString,QString)), this, SLOT(DataRecieved(QString,QString)));
                    connect(newSerialPort,SIGNAL(dataRecieved(QString,QString)), this, SIGNAL(Responded(QString,QString)));

                    if(infoSerial.productIdentifier()==2)newSerialPort->dataSend("RETURN ID#");
                    else if (infoSerial.productIdentifier()==3)newSerialPort->dataSend("RETURN ID#");
                    else if (infoSerial.productIdentifier()==5)newSerialPort->dataSend("param-get ID#");

                    SerialPorts.append(newSerialPort);
                    listPortsActive.append(infoSerial.portName());
                }
            }
            AvailablePorts.append(infoSerial.portName());
        }

            // Provjeri da li svi sa connected portsa zapravo postoje
    }


    // Trazim da li ima neki u Ports active a da nije u Available ports
    for(int k=0;k<listPortsActive.count();k++){
        if(!AvailablePorts.contains(listPortsActive.at(k))){
            listPortsActive.removeAt(k);
            emit errorSignal("Logger: "+SerialPorts.at(k)->getSerial()+" removed");
            SerialPorts.at(k)->deleteLater();
            SerialPorts.removeAt(k);
        }
    }


    QStringList driveLabelList_1;
    QStringList driveNameList_1;

    // Tu trazim Drivove
    QFileInfoList DriveList=QDir::drives();
    for(int i =0;i<DriveList.count();i++){//QFileInfo s, DriveList){
        QString DriveLabel;
        QString tdrv=DriveList.at(i).absolutePath();
        //qDebug() << tdrv;
        DriveLabel=GetDriveLabel(tdrv);
        if(DriveLabel=="") continue;
        driveLabelList_1.append(DriveLabel);
        driveNameList_1.append(tdrv);
    }

    if(driveLabelList_1!=driveLabelList || driveNameList_1!=driveNameList){
        driveLabelList=driveLabelList_1;
        driveNameList=driveNameList_1;
        //qDebug()<<"Drive detected";
        emit DriveChangeDetected(driveLabelList,driveNameList);
    }

}

void DetectorController::DisconnectedDelete(QString Serial,QString Identifier){
    for(int i=0;i<SerialPorts.count();i++){

        if(SerialPorts[i]->getSerial()==Serial){
           SerialPorts[i]->deleteLater();
           SerialPorts.removeAt(i);
           break;
        }
    }

}

void DetectorController::ConnectedAdd(QString Serial,QString Identifier){
    //listPortsActive.append(SerialPorts.last()->get);

}

QString DetectorController::GetDriveLabel(QString & tdrive)
{
     WCHAR szVolumeName[256] ;
     WCHAR szFileSystemName[256];
     DWORD dwSerialNumber = 0;
     DWORD dwMaxFileNameLength=256;
     DWORD dwFileSystemFlags=0;
     bool ret = GetVolumeInformation( (WCHAR *) tdrive.utf16(),szVolumeName,256,&dwSerialNumber,&dwMaxFileNameLength,&dwFileSystemFlags,szFileSystemName,256);
     if(!ret)return QString("");
     QString vName=QString::fromUtf16 ( (const ushort *) szVolumeName) ;
     vName.trimmed();
     return vName;
}


void DetectorController::DataRecieved(QString Serial, QString Data){
    emit messageSignal("Logger: "+Serial+" responded "+ Data);

}




void DetectorController::SendDataToModule(QString Serial,QString Data){
    foreach(serialPort *s, SerialPorts){
        if(s->getSerial()==Serial)
        s->dataSend(Data.toLatin1());
    }
}

void DetectorController::UnsubscribeMessages(QString Serial){
}

void DetectorController::SubscribeMessages(QString Serial){
}


bool DetectorController::IsConnected(QString Serial){
    return true;
}

void DetectorController::sincClocks(){
    QDateTime timeNow= QDateTime::currentDateTime();
    QString timeNowString = timeNow.toString("hh:mm:ss:dd:MM:yy");
    foreach(serialPort *s, SerialPorts){
        s->dataSend(QString("CONFIG TIME SET "+timeNowString+"#").toLatin1());
    }

}

void DetectorController::stopReporting(){
    foreach(serialPort *s, SerialPorts){
        s->dataSend(QString("STATUS STOP#").toLatin1());
        s->dataSend(QString("status-get STOP#").toLatin1());
    }

}

void DetectorController::startReporting(){
    foreach(serialPort *s, SerialPorts){
        s->dataSend(QString("STATUS START#").toLatin1());
        s->dataSend(QString("status-get START#").toLatin1());
    }

}
/* Serijski port */
serialPort::serialPort(QObject *parent)
{
    port=new QSerialPort();
    m_Connected=false;
    m_Detected=false;
  }

serialPort::~serialPort()
{
    port->close();
    emit portDisconnected(Serial,Identifier);
}

void serialPort::aboutToDisconnect()
{
    port->close();
    emit errorSignal("Logger disconnecting");
    emit portDisconnected(Serial,Identifier);
}

void serialPort::connectToPort(QString PortName)
{
    if (port->isOpen()) port->close();

    m_Connected = false;
    port->setPortName(PortName);
    port->close();
    if (!port->open(QSerialPort::ReadWrite))
    {
        emit errorSignal(Serial+" Problem:"+port->errorString());
        emit portDisconnected(Serial,Identifier);
        port->close();
        return;
    }
    if (port->isOpen()){
        m_Connected=true;
    }

    connect(port, SIGNAL(readyRead()), this, SLOT(dataReady()));
}

void serialPort::disconnectFromPort()
{
    if (port->isOpen()){
        port->close();
        emit messageSignal(Serial+tr(" Disconnected from port:%1").arg(port->portName()));
        emit portDisconnected(Serial,Identifier);
    }
}



void serialPort::dataReady()
{
    QByteArray data;
    data = port->readAll();
    for (int i=0; i<data.size();i++){
        if (data.at(i)=='\n'){

            if(!m_Detected && (QString(Buffer).remove("\n").remove("\r").startsWith("[ID]"))){
                //qDebug()<<QString(Buffer);
                QStringList Outputs=QString(Buffer).split(" ");
                if (Outputs.size()<2) return;
                Serial=Outputs.at(1);
                if (Outputs.size()<3)Identifier = "Unknown";
                else Identifier=Outputs.at(2);

                emit messageSignal("Logger: "+Serial+" connected");
                emit portConnected(Serial, Identifier);
                m_Detected=true;
            }
            if (Serial!=""){
                emit dataRecieved(Serial,QString(Buffer));
                Buffer.clear();
            }
        }else{
            Buffer.append(data.at(i));
        }
    }
}

void serialPort::dataSend(QByteArray data)
{
    if(port->write(data)<0){
        emit errorSignal(Serial+" Port is not connected!");
        emit portDisconnected(Serial,Identifier);
        port->close();
    }
}

QString serialPort::getSerial(){
    return Serial;
}

QString serialPort::getIdentifier(){
    return Identifier;
}
