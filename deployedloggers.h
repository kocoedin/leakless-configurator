#ifndef DEPLOYEDLOGGERS_H
#define DEPLOYEDLOGGERS_H

#include <QWidget>
#include <QWizard>
#include <QMessageBox>
#include <QFile>
#include <QCamera>
#include <QDateTime>
#include <QCameraControl>
#include <QCameraInfo>
#include <QCameraViewfinder>
#include <QListWidget>
#include <QCameraImageCapture>
#include <QMenu>
#include <QSettings>
#include <QStandardPaths>
#include <QTimer>
#include <QDir>
#include <QTreeWidget>

namespace Ui {
class DeployedLoggers;
}

class DeployedLoggers : public QWidget
{
    Q_OBJECT

public:
    explicit DeployedLoggers(QWidget *parent = 0);
    ~DeployedLoggers();

    void refreshLoggerList();

private slots:
    void on_loggerTreeWidget_itemClicked(QTreeWidgetItem *item, int column);
    void timer_timeout();

    void on_loggerTreeWidget_customContextMenuRequested(const QPoint &pos);

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::DeployedLoggers *ui;

    QTimer *timer;
};

#endif // DEPLOYEDLOGGERS_H
