/********************************************************************************
** Form generated from reading UI file 'loggerinfo.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERINFO_H
#define UI_LOGGERINFO_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QProgressBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerInfo
{
public:
    QHBoxLayout *horizontalLayout_8;
    QGroupBox *groupBoxInfo;
    QVBoxLayout *verticalLayout_5;
    QHBoxLayout *horizontalLayout;
    QLabel *label_8;
    QLabel *lblIdentifier;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_4;
    QLabel *lblType;
    QHBoxLayout *horizontalLayout_12;
    QLabel *lblConnectionTypeIcon;
    QLabel *lblConnectionType;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_5;
    QLabel *lblTime;
    QHBoxLayout *horizontalLayout_11;
    QLabel *label_11;
    QLabel *lblOperationStatus;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBoxBattery;
    QHBoxLayout *horizontalLayout_6;
    QProgressBar *BatteryVoltageProggress;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_3;
    QLabel *lblVoltage;
    QHBoxLayout *horizontalLayout_7;
    QLabel *lblChargingCurrent;
    QHBoxLayout *horizontalLayout_5;
    QLabel *lblChargingStatus;
    QHBoxLayout *horizontalLayout_4;
    QLabel *lblCapacity;
    QHBoxLayout *horizontalLayout_2;
    QPushButton *btnSelect;
    QPushButton *btnTransferData;

    void setupUi(QWidget *LoggerInfo)
    {
        if (LoggerInfo->objectName().isEmpty())
            LoggerInfo->setObjectName(QStringLiteral("LoggerInfo"));
        LoggerInfo->resize(590, 202);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LoggerInfo->sizePolicy().hasHeightForWidth());
        LoggerInfo->setSizePolicy(sizePolicy);
        horizontalLayout_8 = new QHBoxLayout(LoggerInfo);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        groupBoxInfo = new QGroupBox(LoggerInfo);
        groupBoxInfo->setObjectName(QStringLiteral("groupBoxInfo"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBoxInfo->sizePolicy().hasHeightForWidth());
        groupBoxInfo->setSizePolicy(sizePolicy1);
        QFont font;
        font.setPointSize(9);
        groupBoxInfo->setFont(font);
        verticalLayout_5 = new QVBoxLayout(groupBoxInfo);
        verticalLayout_5->setSpacing(2);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(2);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        label_8 = new QLabel(groupBoxInfo);
        label_8->setObjectName(QStringLiteral("label_8"));
        sizePolicy1.setHeightForWidth(label_8->sizePolicy().hasHeightForWidth());
        label_8->setSizePolicy(sizePolicy1);
        label_8->setMaximumSize(QSize(20, 20));
        label_8->setFont(font);
        label_8->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png")));
        label_8->setScaledContents(true);

        horizontalLayout->addWidget(label_8);

        lblIdentifier = new QLabel(groupBoxInfo);
        lblIdentifier->setObjectName(QStringLiteral("lblIdentifier"));
        sizePolicy1.setHeightForWidth(lblIdentifier->sizePolicy().hasHeightForWidth());
        lblIdentifier->setSizePolicy(sizePolicy1);
        lblIdentifier->setFont(font);

        horizontalLayout->addWidget(lblIdentifier);


        verticalLayout_5->addLayout(horizontalLayout);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setSpacing(2);
        horizontalLayout_9->setObjectName(QStringLiteral("horizontalLayout_9"));
        label_4 = new QLabel(groupBoxInfo);
        label_4->setObjectName(QStringLiteral("label_4"));
        sizePolicy1.setHeightForWidth(label_4->sizePolicy().hasHeightForWidth());
        label_4->setSizePolicy(sizePolicy1);
        label_4->setMaximumSize(QSize(20, 20));
        label_4->setFont(font);
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Memory_Cards/sd/sd-256.png")));
        label_4->setScaledContents(true);

        horizontalLayout_9->addWidget(label_4);

        lblType = new QLabel(groupBoxInfo);
        lblType->setObjectName(QStringLiteral("lblType"));
        sizePolicy1.setHeightForWidth(lblType->sizePolicy().hasHeightForWidth());
        lblType->setSizePolicy(sizePolicy1);
        lblType->setFont(font);

        horizontalLayout_9->addWidget(lblType);


        verticalLayout_5->addLayout(horizontalLayout_9);

        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setSpacing(2);
        horizontalLayout_12->setObjectName(QStringLiteral("horizontalLayout_12"));
        lblConnectionTypeIcon = new QLabel(groupBoxInfo);
        lblConnectionTypeIcon->setObjectName(QStringLiteral("lblConnectionTypeIcon"));
        sizePolicy1.setHeightForWidth(lblConnectionTypeIcon->sizePolicy().hasHeightForWidth());
        lblConnectionTypeIcon->setSizePolicy(sizePolicy1);
        lblConnectionTypeIcon->setMaximumSize(QSize(20, 20));
        lblConnectionTypeIcon->setFont(font);
        lblConnectionTypeIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/USB/usb_logo/usb_logo-256.png")));
        lblConnectionTypeIcon->setScaledContents(true);

        horizontalLayout_12->addWidget(lblConnectionTypeIcon);

        lblConnectionType = new QLabel(groupBoxInfo);
        lblConnectionType->setObjectName(QStringLiteral("lblConnectionType"));
        sizePolicy1.setHeightForWidth(lblConnectionType->sizePolicy().hasHeightForWidth());
        lblConnectionType->setSizePolicy(sizePolicy1);
        lblConnectionType->setFont(font);

        horizontalLayout_12->addWidget(lblConnectionType);


        verticalLayout_5->addLayout(horizontalLayout_12);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setSpacing(2);
        horizontalLayout_10->setObjectName(QStringLiteral("horizontalLayout_10"));
        label_5 = new QLabel(groupBoxInfo);
        label_5->setObjectName(QStringLiteral("label_5"));
        sizePolicy1.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy1);
        label_5->setMaximumSize(QSize(20, 20));
        label_5->setFont(font);
        label_5->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/clock/clock-256.png")));
        label_5->setScaledContents(true);

        horizontalLayout_10->addWidget(label_5);

        lblTime = new QLabel(groupBoxInfo);
        lblTime->setObjectName(QStringLiteral("lblTime"));
        sizePolicy1.setHeightForWidth(lblTime->sizePolicy().hasHeightForWidth());
        lblTime->setSizePolicy(sizePolicy1);
        lblTime->setFont(font);

        horizontalLayout_10->addWidget(lblTime);


        verticalLayout_5->addLayout(horizontalLayout_10);

        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(2);
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        label_11 = new QLabel(groupBoxInfo);
        label_11->setObjectName(QStringLiteral("label_11"));
        sizePolicy1.setHeightForWidth(label_11->sizePolicy().hasHeightForWidth());
        label_11->setSizePolicy(sizePolicy1);
        label_11->setMaximumSize(QSize(20, 20));
        label_11->setFont(font);
        label_11->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Data_Grid/View_Details/View_Details-256.png")));
        label_11->setScaledContents(true);

        horizontalLayout_11->addWidget(label_11);

        lblOperationStatus = new QLabel(groupBoxInfo);
        lblOperationStatus->setObjectName(QStringLiteral("lblOperationStatus"));
        sizePolicy1.setHeightForWidth(lblOperationStatus->sizePolicy().hasHeightForWidth());
        lblOperationStatus->setSizePolicy(sizePolicy1);
        lblOperationStatus->setFont(font);

        horizontalLayout_11->addWidget(lblOperationStatus);


        verticalLayout_5->addLayout(horizontalLayout_11);


        horizontalLayout_8->addWidget(groupBoxInfo);

        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        groupBoxBattery = new QGroupBox(LoggerInfo);
        groupBoxBattery->setObjectName(QStringLiteral("groupBoxBattery"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Maximum);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(groupBoxBattery->sizePolicy().hasHeightForWidth());
        groupBoxBattery->setSizePolicy(sizePolicy2);
        groupBoxBattery->setMinimumSize(QSize(200, 0));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        font1.setPointSize(9);
        groupBoxBattery->setFont(font1);
        horizontalLayout_6 = new QHBoxLayout(groupBoxBattery);
        horizontalLayout_6->setSpacing(6);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(-1, 9, 9, 9);
        BatteryVoltageProggress = new QProgressBar(groupBoxBattery);
        BatteryVoltageProggress->setObjectName(QStringLiteral("BatteryVoltageProggress"));
        sizePolicy1.setHeightForWidth(BatteryVoltageProggress->sizePolicy().hasHeightForWidth());
        BatteryVoltageProggress->setSizePolicy(sizePolicy1);
        BatteryVoltageProggress->setMinimumSize(QSize(50, 0));
        BatteryVoltageProggress->setFont(font);
        BatteryVoltageProggress->setMaximum(4200);
        BatteryVoltageProggress->setValue(3700);
        BatteryVoltageProggress->setTextVisible(false);
        BatteryVoltageProggress->setOrientation(Qt::Vertical);
        BatteryVoltageProggress->setInvertedAppearance(false);

        horizontalLayout_6->addWidget(BatteryVoltageProggress);

        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setSpacing(2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(2);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        lblVoltage = new QLabel(groupBoxBattery);
        lblVoltage->setObjectName(QStringLiteral("lblVoltage"));
        sizePolicy1.setHeightForWidth(lblVoltage->sizePolicy().hasHeightForWidth());
        lblVoltage->setSizePolicy(sizePolicy1);
        lblVoltage->setFont(font);

        horizontalLayout_3->addWidget(lblVoltage);


        verticalLayout_2->addLayout(horizontalLayout_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(2);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        lblChargingCurrent = new QLabel(groupBoxBattery);
        lblChargingCurrent->setObjectName(QStringLiteral("lblChargingCurrent"));
        sizePolicy1.setHeightForWidth(lblChargingCurrent->sizePolicy().hasHeightForWidth());
        lblChargingCurrent->setSizePolicy(sizePolicy1);
        lblChargingCurrent->setFont(font);

        horizontalLayout_7->addWidget(lblChargingCurrent);


        verticalLayout_2->addLayout(horizontalLayout_7);

        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(2);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        lblChargingStatus = new QLabel(groupBoxBattery);
        lblChargingStatus->setObjectName(QStringLiteral("lblChargingStatus"));
        sizePolicy1.setHeightForWidth(lblChargingStatus->sizePolicy().hasHeightForWidth());
        lblChargingStatus->setSizePolicy(sizePolicy1);
        lblChargingStatus->setFont(font);

        horizontalLayout_5->addWidget(lblChargingStatus);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(2);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        lblCapacity = new QLabel(groupBoxBattery);
        lblCapacity->setObjectName(QStringLiteral("lblCapacity"));
        sizePolicy1.setHeightForWidth(lblCapacity->sizePolicy().hasHeightForWidth());
        lblCapacity->setSizePolicy(sizePolicy1);
        lblCapacity->setFont(font);

        horizontalLayout_4->addWidget(lblCapacity);


        verticalLayout_2->addLayout(horizontalLayout_4);


        horizontalLayout_6->addLayout(verticalLayout_2);


        verticalLayout_3->addWidget(groupBoxBattery);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        btnSelect = new QPushButton(LoggerInfo);
        btnSelect->setObjectName(QStringLiteral("btnSelect"));
        sizePolicy1.setHeightForWidth(btnSelect->sizePolicy().hasHeightForWidth());
        btnSelect->setSizePolicy(sizePolicy1);
        btnSelect->setFont(font1);

        horizontalLayout_2->addWidget(btnSelect);

        btnTransferData = new QPushButton(LoggerInfo);
        btnTransferData->setObjectName(QStringLiteral("btnTransferData"));
        sizePolicy1.setHeightForWidth(btnTransferData->sizePolicy().hasHeightForWidth());
        btnTransferData->setSizePolicy(sizePolicy1);
        btnTransferData->setFont(font1);

        horizontalLayout_2->addWidget(btnTransferData);


        verticalLayout_3->addLayout(horizontalLayout_2);


        horizontalLayout_8->addLayout(verticalLayout_3);


        retranslateUi(LoggerInfo);

        QMetaObject::connectSlotsByName(LoggerInfo);
    } // setupUi

    void retranslateUi(QWidget *LoggerInfo)
    {
        LoggerInfo->setWindowTitle(QApplication::translate("LoggerInfo", "Form", 0));
        groupBoxInfo->setTitle(QApplication::translate("LoggerInfo", "Logger", 0));
        label_8->setText(QString());
        lblIdentifier->setText(QApplication::translate("LoggerInfo", "Identifier", 0));
        label_4->setText(QString());
        lblType->setText(QApplication::translate("LoggerInfo", "Logger type:", 0));
        lblConnectionTypeIcon->setText(QString());
        lblConnectionType->setText(QApplication::translate("LoggerInfo", "Connection type:", 0));
        label_5->setText(QString());
        lblTime->setText(QApplication::translate("LoggerInfo", "Time: ", 0));
        label_11->setText(QString());
        lblOperationStatus->setText(QApplication::translate("LoggerInfo", "Operation status:", 0));
        groupBoxBattery->setTitle(QApplication::translate("LoggerInfo", "Battery status", 0));
        lblVoltage->setText(QApplication::translate("LoggerInfo", "Voltage:", 0));
        lblChargingCurrent->setText(QApplication::translate("LoggerInfo", "Current:", 0));
        lblChargingStatus->setText(QApplication::translate("LoggerInfo", "Status:", 0));
        lblCapacity->setText(QApplication::translate("LoggerInfo", "Capacity:", 0));
        btnSelect->setText(QApplication::translate("LoggerInfo", "Select", 0));
        btnTransferData->setText(QApplication::translate("LoggerInfo", "Transfer Data", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerInfo: public Ui_LoggerInfo {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERINFO_H
