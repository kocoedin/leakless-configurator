/********************************************************************************
** Form generated from reading UI file 'loggerconfpdlag_USB.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFPDLAG_USB_H
#define UI_LOGGERCONFPDLAG_USB_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDockWidget>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfPDLAG_USB
{
public:
    QHBoxLayout *horizontalLayout_6;
    QTabWidget *moduleOptions;
    QWidget *tab_2;
    QHBoxLayout *horizontalLayout_2;
    QVBoxLayout *verticalLayout;
    QLabel *label_5;
    QSpacerItem *verticalSpacer;
    QLabel *label_6;
    QTextEdit *tedInfoFileEdit;
    QPushButton *btnSaveInfoFile;
    QTreeWidget *InfoTreeWidget;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QTreeView *dirTree;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_3;
    QListView *fileTree;
    QDockWidget *dockWidget;
    QWidget *dockWidgetContents;
    QHBoxLayout *horizontalLayout_8;
    QTabWidget *plottingTab;
    QWidget *tab;
    QVBoxLayout *verticalLayout_15;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *CH1Pressure_Checkbox;
    QCheckBox *CH2Pressure_Checkbox;
    QCheckBox *CH1TotalFlow_Checkbox;
    QCheckBox *CH2TotalFlow_Checkbox;
    QCheckBox *Battery_Checkbox;
    QSpacerItem *horizontalSpacer_4;
    QPushButton *jpegSave;
    QPushButton *pngSave;
    QPushButton *pdfSave;
    QHBoxLayout *horizontalLayout_7;
    QVBoxLayout *verticalLayout_8;
    QCustomPlot *realtimeWidget;

    void setupUi(QWidget *LoggerConfPDLAG_USB)
    {
        if (LoggerConfPDLAG_USB->objectName().isEmpty())
            LoggerConfPDLAG_USB->setObjectName(QStringLiteral("LoggerConfPDLAG_USB"));
<<<<<<< HEAD
        LoggerConfPDLAG_USB->resize(986, 721);
=======
        LoggerConfPDLAG_USB->resize(986, 727);
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
        horizontalLayout_6 = new QHBoxLayout(LoggerConfPDLAG_USB);
        horizontalLayout_6->setSpacing(0);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        horizontalLayout_6->setContentsMargins(0, 0, 0, 0);
        moduleOptions = new QTabWidget(LoggerConfPDLAG_USB);
        moduleOptions->setObjectName(QStringLiteral("moduleOptions"));
        moduleOptions->setEnabled(true);
        moduleOptions->setMinimumSize(QSize(986, 0));
        QFont font;
        font.setPointSize(9);
        moduleOptions->setFont(font);
        moduleOptions->setAutoFillBackground(false);
        moduleOptions->setTabShape(QTabWidget::Rounded);
        moduleOptions->setIconSize(QSize(16, 16));
        moduleOptions->setElideMode(Qt::ElideNone);
        moduleOptions->setDocumentMode(false);
        moduleOptions->setTabsClosable(false);
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        horizontalLayout_2 = new QHBoxLayout(tab_2);
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label_5 = new QLabel(tab_2);
        label_5->setObjectName(QStringLiteral("label_5"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_5->sizePolicy().hasHeightForWidth());
        label_5->setSizePolicy(sizePolicy);
        label_5->setMaximumSize(QSize(465, 418));
        label_5->setTextFormat(Qt::AutoText);
        label_5->setPixmap(QPixmap(QString::fromUtf8("PDLAG.png")));
        label_5->setScaledContents(true);
        label_5->setAlignment(Qt::AlignCenter);
        label_5->setWordWrap(false);

        verticalLayout->addWidget(label_5);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Preferred);

        verticalLayout->addItem(verticalSpacer);

        label_6 = new QLabel(tab_2);
        label_6->setObjectName(QStringLiteral("label_6"));
        QFont font1;
        font1.setPointSize(12);
        font1.setBold(true);
        font1.setWeight(75);
        label_6->setFont(font1);

        verticalLayout->addWidget(label_6);

        tedInfoFileEdit = new QTextEdit(tab_2);
        tedInfoFileEdit->setObjectName(QStringLiteral("tedInfoFileEdit"));
        QSizePolicy sizePolicy1(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(tedInfoFileEdit->sizePolicy().hasHeightForWidth());
        tedInfoFileEdit->setSizePolicy(sizePolicy1);

        verticalLayout->addWidget(tedInfoFileEdit);

        btnSaveInfoFile = new QPushButton(tab_2);
        btnSaveInfoFile->setObjectName(QStringLiteral("btnSaveInfoFile"));
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/System/save/save-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSaveInfoFile->setIcon(icon);
        btnSaveInfoFile->setFlat(false);

        verticalLayout->addWidget(btnSaveInfoFile);


        horizontalLayout_2->addLayout(verticalLayout);

        InfoTreeWidget = new QTreeWidget(tab_2);
        QFont font2;
        font2.setBold(true);
        font2.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font2);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(false);
        font3.setWeight(50);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Microsoft/system_report/system_report-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Industry/display/display-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Printed_Matter/book/book-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/settings2/settings2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/pressure/pressure-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Sewing/tape_measure/tape_measure-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/Industry/water/water-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Industry/hydroelectric/hydroelectric-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/win8/PNG/Transport/car_battery/car_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electronics/electronics-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Battery/empty_battery/empty_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/win8/PNG/Buzz/low_importance/low_importance-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/win8/PNG/Industry/radio_tower/radio_tower-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/win8/PNG/Folders/internet/internet-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/win8/PNG/Users/user/user-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/win8/PNG/Registration/password2/password2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/card_inserting/card_inserting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/win8/PNG/Objects/timer/timer-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/clock/clock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/server/server-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon25;
        icon25.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/ip_adress/ip_adress-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon26;
        icon26.addFile(QStringLiteral(":/icons/win8/PNG/It_Infrastructure/voip_gateway/voip_gateway-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon27;
        icon27.addFile(QStringLiteral(":/icons/win8/PNG/Buzz/gmail_login/gmail_login-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font4;
        font4.setBold(false);
        font4.setWeight(50);
        QIcon icon28;
        icon28.addFile(QStringLiteral(":/icons/win8/PNG/Registration/email/email-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon29;
        icon29.addFile(QStringLiteral(":/icons/win8/PNG/Cell_Phones/sms/sms-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon30;
        icon30.addFile(QStringLiteral(":/icons/win8/PNG/Cell_Phones/cell_phone/cell_phone-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font5;
        font5.setPointSize(12);
        QIcon icon31;
        icon31.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon32;
        icon32.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon33;
        icon33.addFile(QStringLiteral(":/icons/win8/PNG/Timeline_List_Grid/list/list-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon34;
        icon34.addFile(QStringLiteral(":/icons/win8/PNG/Ecommerce/alarm_clock/alarm_clock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon35;
        icon35.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-64 - Min.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon36;
        icon36.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-64 - Max.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon37;
        icon37.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_threshold/electrical_threshold-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font3);
        __qtreewidgetitem1->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem5->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem6->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem7->setFont(0, font3);
        __qtreewidgetitem7->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem8->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem9->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem10->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem11->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem12->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem13->setIcon(0, icon11);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem14->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem15->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem16->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem7);
        __qtreewidgetitem17->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem18->setFont(0, font3);
        __qtreewidgetitem18->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem19 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem19->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem20 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem20->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem21 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem21->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem22 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem22->setIcon(0, icon20);
        QTreeWidgetItem *__qtreewidgetitem23 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem23->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem24 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem24->setIcon(0, icon22);
        QTreeWidgetItem *__qtreewidgetitem25 = new QTreeWidgetItem(__qtreewidgetitem18);
        __qtreewidgetitem25->setIcon(0, icon23);
        QTreeWidgetItem *__qtreewidgetitem26 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem26->setFont(0, font3);
        __qtreewidgetitem26->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem27 = new QTreeWidgetItem(__qtreewidgetitem26);
        __qtreewidgetitem27->setIcon(0, icon25);
        QTreeWidgetItem *__qtreewidgetitem28 = new QTreeWidgetItem(__qtreewidgetitem26);
        __qtreewidgetitem28->setIcon(0, icon26);
        QTreeWidgetItem *__qtreewidgetitem29 = new QTreeWidgetItem(__qtreewidgetitem26);
        __qtreewidgetitem29->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem30 = new QTreeWidgetItem(__qtreewidgetitem26);
        __qtreewidgetitem30->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem31 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem31->setFont(1, font4);
        __qtreewidgetitem31->setFont(0, font3);
        __qtreewidgetitem31->setIcon(0, icon27);
        QTreeWidgetItem *__qtreewidgetitem32 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem32->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem33 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem33->setIcon(0, icon24);
        QTreeWidgetItem *__qtreewidgetitem34 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem34->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem35 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem35->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem36 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem36->setIcon(0, icon28);
        QTreeWidgetItem *__qtreewidgetitem37 = new QTreeWidgetItem(__qtreewidgetitem31);
        __qtreewidgetitem37->setIcon(0, icon28);
        QTreeWidgetItem *__qtreewidgetitem38 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem38->setFont(0, font3);
        __qtreewidgetitem38->setIcon(0, icon29);
        QTreeWidgetItem *__qtreewidgetitem39 = new QTreeWidgetItem(__qtreewidgetitem38);
        __qtreewidgetitem39->setIcon(0, icon30);
        QTreeWidgetItem *__qtreewidgetitem40 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem40->setFont(0, font5);
        QTreeWidgetItem *__qtreewidgetitem41 = new QTreeWidgetItem(__qtreewidgetitem40);
        __qtreewidgetitem41->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem42 = new QTreeWidgetItem(__qtreewidgetitem40);
        __qtreewidgetitem42->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem43 = new QTreeWidgetItem(__qtreewidgetitem40);
        __qtreewidgetitem43->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem44 = new QTreeWidgetItem(__qtreewidgetitem40);
        __qtreewidgetitem44->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem45 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem45->setFont(0, font3);
        QTreeWidgetItem *__qtreewidgetitem46 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem46->setIcon(0, icon31);
        QTreeWidgetItem *__qtreewidgetitem47 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem47->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem48 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem48->setIcon(0, icon32);
        QTreeWidgetItem *__qtreewidgetitem49 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem49->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem50 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem50->setIcon(0, icon33);
        QTreeWidgetItem *__qtreewidgetitem51 = new QTreeWidgetItem(__qtreewidgetitem45);
        __qtreewidgetitem51->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem52 = new QTreeWidgetItem(__qtreewidgetitem45);
        QTreeWidgetItem *__qtreewidgetitem53 = new QTreeWidgetItem(__qtreewidgetitem52);
        __qtreewidgetitem53->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem54 = new QTreeWidgetItem(__qtreewidgetitem52);
        __qtreewidgetitem54->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem55 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem55->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem56 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem56->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem57 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem57->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem58 = new QTreeWidgetItem(__qtreewidgetitem54);
        __qtreewidgetitem58->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem59 = new QTreeWidgetItem(__qtreewidgetitem52);
        __qtreewidgetitem59->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem60 = new QTreeWidgetItem(__qtreewidgetitem59);
        __qtreewidgetitem60->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem61 = new QTreeWidgetItem(__qtreewidgetitem59);
        __qtreewidgetitem61->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem62 = new QTreeWidgetItem(__qtreewidgetitem59);
        __qtreewidgetitem62->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem63 = new QTreeWidgetItem(__qtreewidgetitem59);
        __qtreewidgetitem63->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem64 = new QTreeWidgetItem(__qtreewidgetitem52);
        __qtreewidgetitem64->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem65 = new QTreeWidgetItem(__qtreewidgetitem64);
        __qtreewidgetitem65->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem66 = new QTreeWidgetItem(__qtreewidgetitem64);
        __qtreewidgetitem66->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem67 = new QTreeWidgetItem(__qtreewidgetitem64);
        __qtreewidgetitem67->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem68 = new QTreeWidgetItem(__qtreewidgetitem64);
        __qtreewidgetitem68->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem69 = new QTreeWidgetItem(__qtreewidgetitem52);
        __qtreewidgetitem69->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem70 = new QTreeWidgetItem(__qtreewidgetitem69);
        __qtreewidgetitem70->setIcon(0, icon34);
        QTreeWidgetItem *__qtreewidgetitem71 = new QTreeWidgetItem(__qtreewidgetitem69);
        __qtreewidgetitem71->setIcon(0, icon35);
        QTreeWidgetItem *__qtreewidgetitem72 = new QTreeWidgetItem(__qtreewidgetitem69);
        __qtreewidgetitem72->setIcon(0, icon36);
        QTreeWidgetItem *__qtreewidgetitem73 = new QTreeWidgetItem(__qtreewidgetitem69);
        __qtreewidgetitem73->setIcon(0, icon37);
        QTreeWidgetItem *__qtreewidgetitem74 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem74->setFont(0, font3);
        QTreeWidgetItem *__qtreewidgetitem75 = new QTreeWidgetItem(__qtreewidgetitem74);
        __qtreewidgetitem75->setIcon(0, icon31);
        QTreeWidgetItem *__qtreewidgetitem76 = new QTreeWidgetItem(__qtreewidgetitem74);
        __qtreewidgetitem76->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem77 = new QTreeWidgetItem(__qtreewidgetitem74);
        __qtreewidgetitem77->setIcon(0, icon21);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy2(QSizePolicy::MinimumExpanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy2);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        horizontalLayout_2->addWidget(InfoTreeWidget);

        QIcon icon38;
        icon38.addFile(QStringLiteral(":/icons/win8/PNG/System/settings/settings-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        moduleOptions->addTab(tab_2, icon38, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_7 = new QVBoxLayout(tab_3);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(tab_3);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(30, 30));
        label->setFrameShape(QFrame::NoFrame);
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/folder/folder-256.png")));
        label->setScaledContents(true);

        horizontalLayout_3->addWidget(label);

        label_2 = new QLabel(tab_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_3);

        dirTree = new QTreeView(tab_3);
        dirTree->setObjectName(QStringLiteral("dirTree"));
        QSizePolicy sizePolicy3(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(dirTree->sizePolicy().hasHeightForWidth());
        dirTree->setSizePolicy(sizePolicy3);

        verticalLayout_3->addWidget(dirTree);


        horizontalLayout_5->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(30, 30));
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/document/document-256.png")));
        label_3->setScaledContents(true);

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(tab_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout_4);

        fileTree = new QListView(tab_3);
        fileTree->setObjectName(QStringLiteral("fileTree"));
        sizePolicy3.setHeightForWidth(fileTree->sizePolicy().hasHeightForWidth());
        fileTree->setSizePolicy(sizePolicy3);
        fileTree->setMinimumSize(QSize(0, 0));
        fileTree->setAlternatingRowColors(true);
        fileTree->setSelectionRectVisible(true);

        verticalLayout_4->addWidget(fileTree);


        horizontalLayout_5->addLayout(verticalLayout_4);


        verticalLayout_7->addLayout(horizontalLayout_5);

        dockWidget = new QDockWidget(tab_3);
        dockWidget->setObjectName(QStringLiteral("dockWidget"));
        dockWidget->setFeatures(QDockWidget::DockWidgetFloatable|QDockWidget::DockWidgetMovable);
        dockWidgetContents = new QWidget();
        dockWidgetContents->setObjectName(QStringLiteral("dockWidgetContents"));
        horizontalLayout_8 = new QHBoxLayout(dockWidgetContents);
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        plottingTab = new QTabWidget(dockWidgetContents);
        plottingTab->setObjectName(QStringLiteral("plottingTab"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(plottingTab->sizePolicy().hasHeightForWidth());
        plottingTab->setSizePolicy(sizePolicy4);
        plottingTab->setMinimumSize(QSize(0, 400));
        plottingTab->setTabPosition(QTabWidget::South);
        plottingTab->setTabShape(QTabWidget::Rounded);
        plottingTab->setElideMode(Qt::ElideNone);
        plottingTab->setDocumentMode(false);
        plottingTab->setTabsClosable(true);
        plottingTab->setMovable(true);

        horizontalLayout_8->addWidget(plottingTab);

        dockWidget->setWidget(dockWidgetContents);

        verticalLayout_7->addWidget(dockWidget);

        QIcon icon39;
        icon39.addFile(QStringLiteral(":/icons/win8/PNG/Charts/scatter_plot/scatter_plot-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        moduleOptions->addTab(tab_3, icon39, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_15 = new QVBoxLayout(tab);
        verticalLayout_15->setObjectName(QStringLiteral("verticalLayout_15"));
        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout = new QHBoxLayout(groupBox_3);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        Realtime_START = new QPushButton(groupBox_3);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
<<<<<<< HEAD
        QIcon icon40;
        icon40.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_START->setIcon(icon40);
=======
        QFont font6;
        font6.setFamily(QStringLiteral("Open Sans"));
        font6.setPointSize(10);
        font6.setBold(false);
        font6.setWeight(50);
        Realtime_START->setFont(font6);
        QIcon icon39;
        icon39.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_START->setIcon(icon39);
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_3);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
<<<<<<< HEAD
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(Realtime_STOP->sizePolicy().hasHeightForWidth());
        Realtime_STOP->setSizePolicy(sizePolicy4);
        QIcon icon41;
        icon41.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon41);
=======
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(Realtime_STOP->sizePolicy().hasHeightForWidth());
        Realtime_STOP->setSizePolicy(sizePolicy5);
        Realtime_STOP->setFont(font6);
        QIcon icon40;
        icon40.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon40);
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(Realtime_STOP);

        CH1Pressure_Checkbox = new QCheckBox(groupBox_3);
        CH1Pressure_Checkbox->setObjectName(QStringLiteral("CH1Pressure_Checkbox"));
        CH1Pressure_Checkbox->setFont(font6);

        horizontalLayout->addWidget(CH1Pressure_Checkbox);

        CH2Pressure_Checkbox = new QCheckBox(groupBox_3);
        CH2Pressure_Checkbox->setObjectName(QStringLiteral("CH2Pressure_Checkbox"));
        CH2Pressure_Checkbox->setFont(font6);

        horizontalLayout->addWidget(CH2Pressure_Checkbox);

        CH1TotalFlow_Checkbox = new QCheckBox(groupBox_3);
        CH1TotalFlow_Checkbox->setObjectName(QStringLiteral("CH1TotalFlow_Checkbox"));
        CH1TotalFlow_Checkbox->setFont(font6);

        horizontalLayout->addWidget(CH1TotalFlow_Checkbox);

        CH2TotalFlow_Checkbox = new QCheckBox(groupBox_3);
        CH2TotalFlow_Checkbox->setObjectName(QStringLiteral("CH2TotalFlow_Checkbox"));
        CH2TotalFlow_Checkbox->setFont(font6);

        horizontalLayout->addWidget(CH2TotalFlow_Checkbox);

        Battery_Checkbox = new QCheckBox(groupBox_3);
        Battery_Checkbox->setObjectName(QStringLiteral("Battery_Checkbox"));
        Battery_Checkbox->setFont(font6);

        horizontalLayout->addWidget(Battery_Checkbox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        jpegSave = new QPushButton(groupBox_3);
        jpegSave->setObjectName(QStringLiteral("jpegSave"));
        QIcon icon42;
        icon42.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/jpg/jpg-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        jpegSave->setIcon(icon42);
        jpegSave->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(jpegSave);

        pngSave = new QPushButton(groupBox_3);
        pngSave->setObjectName(QStringLiteral("pngSave"));
        QIcon icon43;
        icon43.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/png/png-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        pngSave->setIcon(icon43);
        pngSave->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(pngSave);

        pdfSave = new QPushButton(groupBox_3);
        pdfSave->setObjectName(QStringLiteral("pdfSave"));
        QIcon icon44;
        icon44.addFile(QStringLiteral(":/icons/win8/PNG/File_Types/pdf/pdf-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        pdfSave->setIcon(icon44);
        pdfSave->setIconSize(QSize(32, 32));

        horizontalLayout->addWidget(pdfSave);


        verticalLayout_15->addWidget(groupBox_3);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        verticalLayout_8 = new QVBoxLayout();
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        realtimeWidget = new QCustomPlot(tab);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        QSizePolicy sizePolicy6(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy6);

        verticalLayout_8->addWidget(realtimeWidget);


        horizontalLayout_7->addLayout(verticalLayout_8);


        verticalLayout_15->addLayout(horizontalLayout_7);

        QIcon icon45;
        icon45.addFile(QStringLiteral(":/icons/win8/PNG/Charts/area_chart/area_chart-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        moduleOptions->addTab(tab, icon45, QString());

        horizontalLayout_6->addWidget(moduleOptions);


        retranslateUi(LoggerConfPDLAG_USB);

        moduleOptions->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LoggerConfPDLAG_USB);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfPDLAG_USB)
    {
        LoggerConfPDLAG_USB->setWindowTitle(QApplication::translate("LoggerConfPDLAG_USB", "Form", 0));
        label_5->setText(QString());
        label_6->setText(QApplication::translate("LoggerConfPDLAG_USB", "Info:", 0));
        tedInfoFileEdit->setPlaceholderText(QString());
        btnSaveInfoFile->setText(QApplication::translate("LoggerConfPDLAG_USB", "Save info file", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "IDN STRING 0 15 RW_S", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "FIRM STRING 0 10 R_S", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "SRI UINT16 100 10000 RW", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Status report interval", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LWT UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "LCD indication timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem1->child(4);
        ___qtreewidgetitem6->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "TR ENUM ENG CRO RW_S", 0));
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Language", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Calibration", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem7->child(0);
        ___qtreewidgetitem8->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "S1PS DOUBLE32 1000 0 500 RW_S", 0));
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Pressure factor", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem7->child(1);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "S2PS DOUBLE32 1000 0 500 RW_S", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Pressure factor", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem7->child(2);
        ___qtreewidgetitem10->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "PMU ENUM Bar Pa Psi RW_S", 0));
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Pressure unit", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem7->child(3);
        ___qtreewidgetitem11->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "S1FS DOUBLE32 1000 0 500 RW_S", 0));
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Flow factor", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem7->child(4);
        ___qtreewidgetitem12->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "S2FS DOUBLE32 1000 0 500 RW_S", 0));
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Flow factor", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem7->child(5);
        ___qtreewidgetitem13->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "FMU ENUM Liter m3 Gallon RW_S", 0));
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Flow unit", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem7->child(6);
        ___qtreewidgetitem14->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "BAC UINT32 0 100000 RW", 0));
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Battery capacity", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem7->child(7);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "SFG DOUBLE32 1000 0 10 RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Filter gain", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = ___qtreewidgetitem7->child(8);
        ___qtreewidgetitem16->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MBV DOUBLE32 1000 3 6 RW", 0));
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem7->child(9);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "FMMT UINT32 1 1000 RW", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Flow measure minimum time", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Network settings", 0));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem18->child(0);
        ___qtreewidgetitem19->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "APN STRING 0 15 RW_S", 0));
        ___qtreewidgetitem19->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Network APN", 0));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem18->child(1);
        ___qtreewidgetitem20->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "USR STRING 0 15 RW_S", 0));
        ___qtreewidgetitem20->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Network username", 0));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem18->child(2);
        ___qtreewidgetitem21->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "PASS STRING 0 15 RW_S", 0));
        ___qtreewidgetitem21->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Network password", 0));
        QTreeWidgetItem *___qtreewidgetitem22 = ___qtreewidgetitem18->child(3);
        ___qtreewidgetitem22->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "PIN STRING 0 6 RW_S", 0));
        ___qtreewidgetitem22->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "SIM pin", 0));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem18->child(4);
        ___qtreewidgetitem23->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "GRC UINT32 0 99 RW", 0));
        ___qtreewidgetitem23->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Reconnect count", 0));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem18->child(5);
        ___qtreewidgetitem24->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "GWT UINT32 0 99 RW", 0));
        ___qtreewidgetitem24->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Reconnect wait", 0));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem18->child(6);
        ___qtreewidgetitem25->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "ITSI UINT32 0 100000 RW", 0));
        ___qtreewidgetitem25->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Time sync interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem26 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem26->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Server settings", 0));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem26->child(0);
        ___qtreewidgetitem27->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "IP STRING 0 15 RW", 0));
        ___qtreewidgetitem27->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Server IP", 0));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem26->child(1);
        ___qtreewidgetitem28->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "PORT STRING 0 7 RW", 0));
        ___qtreewidgetitem28->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Server PORT", 0));
        QTreeWidgetItem *___qtreewidgetitem29 = ___qtreewidgetitem26->child(2);
        ___qtreewidgetitem29->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "SUSR STRING 0 15 RW", 0));
        ___qtreewidgetitem29->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Server username", 0));
        QTreeWidgetItem *___qtreewidgetitem30 = ___qtreewidgetitem26->child(3);
        ___qtreewidgetitem30->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "SPAS STRING 0 15 RW", 0));
        ___qtreewidgetitem30->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Server password", 0));
        QTreeWidgetItem *___qtreewidgetitem31 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem31->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail settings", 0));
        QTreeWidgetItem *___qtreewidgetitem32 = ___qtreewidgetitem31->child(0);
        ___qtreewidgetitem32->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MSRV STRING 0 31 RW", 0));
        ___qtreewidgetitem32->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail server", 0));
        QTreeWidgetItem *___qtreewidgetitem33 = ___qtreewidgetitem31->child(1);
        ___qtreewidgetitem33->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MPRT STRING 0 13 RW", 0));
        ___qtreewidgetitem33->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail port", 0));
        QTreeWidgetItem *___qtreewidgetitem34 = ___qtreewidgetitem31->child(2);
        ___qtreewidgetitem34->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MUSR STRING 0 15 RW", 0));
        ___qtreewidgetitem34->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail username", 0));
        QTreeWidgetItem *___qtreewidgetitem35 = ___qtreewidgetitem31->child(3);
        ___qtreewidgetitem35->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MPAS STRING 0 15 RW", 0));
        ___qtreewidgetitem35->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail password", 0));
        QTreeWidgetItem *___qtreewidgetitem36 = ___qtreewidgetitem31->child(4);
        ___qtreewidgetitem36->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MFR STRING 0 31 RW", 0));
        ___qtreewidgetitem36->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail FROM", 0));
        QTreeWidgetItem *___qtreewidgetitem37 = ___qtreewidgetitem31->child(5);
        ___qtreewidgetitem37->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "MTO STRING 0 31 RW_S", 0));
        ___qtreewidgetitem37->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Mail TO", 0));
        QTreeWidgetItem *___qtreewidgetitem38 = InfoTreeWidget->topLevelItem(5);
        ___qtreewidgetitem38->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "SMS settings", 0));
        QTreeWidgetItem *___qtreewidgetitem39 = ___qtreewidgetitem38->child(0);
        ___qtreewidgetitem39->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "NUM STRING 0 15 RW_S", 0));
        ___qtreewidgetitem39->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "SMS number", 0));
        QTreeWidgetItem *___qtreewidgetitem40 = InfoTreeWidget->topLevelItem(6);
        ___qtreewidgetitem40->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Channels", 0));
        QTreeWidgetItem *___qtreewidgetitem41 = ___qtreewidgetitem40->child(0);
        ___qtreewidgetitem41->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LP1 TF RW_S", 0));
        ___qtreewidgetitem41->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Pressure enable", 0));
        QTreeWidgetItem *___qtreewidgetitem42 = ___qtreewidgetitem40->child(1);
        ___qtreewidgetitem42->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LP2 TF RW_S", 0));
        ___qtreewidgetitem42->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Pressure enable", 0));
        QTreeWidgetItem *___qtreewidgetitem43 = ___qtreewidgetitem40->child(2);
        ___qtreewidgetitem43->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LF1 TF RW_S", 0));
        ___qtreewidgetitem43->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Flow enable", 0));
        QTreeWidgetItem *___qtreewidgetitem44 = ___qtreewidgetitem40->child(3);
        ___qtreewidgetitem44->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LF2 TF RW_S", 0));
        ___qtreewidgetitem44->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Flow enable", 0));
        QTreeWidgetItem *___qtreewidgetitem45 = InfoTreeWidget->topLevelItem(7);
        ___qtreewidgetitem45->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Interval sampling", 0));
        QTreeWidgetItem *___qtreewidgetitem46 = ___qtreewidgetitem45->child(0);
        ___qtreewidgetitem46->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "ILT UINT32 1 1000000 RW_S", 0));
        ___qtreewidgetitem46->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem47 = ___qtreewidgetitem45->child(1);
        ___qtreewidgetitem47->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "GSP UINT32 1 1000000 RW_S", 0));
        ___qtreewidgetitem47->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "GPRS report after (samples)", 0));
        QTreeWidgetItem *___qtreewidgetitem48 = ___qtreewidgetitem45->child(2);
        ___qtreewidgetitem48->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "SWT UINT32 0 10000 RW", 0));
        ___qtreewidgetitem48->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Sensor wait time (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem49 = ___qtreewidgetitem45->child(3);
        ___qtreewidgetitem49->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "ILFN STRING 0 15 RW_S", 0));
        ___qtreewidgetitem49->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem50 = ___qtreewidgetitem45->child(4);
        ___qtreewidgetitem50->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "LEN TF RW_S", 0));
        ___qtreewidgetitem50->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "LOG Enable", 0));
        QTreeWidgetItem *___qtreewidgetitem51 = ___qtreewidgetitem45->child(5);
        ___qtreewidgetitem51->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "GEN TF RW_S", 0));
        ___qtreewidgetitem51->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "GPRS Enable", 0));
        QTreeWidgetItem *___qtreewidgetitem52 = ___qtreewidgetitem45->child(6);
        ___qtreewidgetitem52->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Alarms", 0));
        QTreeWidgetItem *___qtreewidgetitem53 = ___qtreewidgetitem52->child(0);
        ___qtreewidgetitem53->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "ARO ENUM OFF GRPS SMS MAIL RW_S", 0));
        ___qtreewidgetitem53->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Alarm status", 0));
        QTreeWidgetItem *___qtreewidgetitem54 = ___qtreewidgetitem52->child(1);
        ___qtreewidgetitem54->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Pressure alarm", 0));
        QTreeWidgetItem *___qtreewidgetitem55 = ___qtreewidgetitem54->child(0);
        ___qtreewidgetitem55->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP1 ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW_S", 0));
        ___qtreewidgetitem55->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "State", 0));
        QTreeWidgetItem *___qtreewidgetitem56 = ___qtreewidgetitem54->child(1);
        ___qtreewidgetitem56->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP1B DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem56->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Min value", 0));
        QTreeWidgetItem *___qtreewidgetitem57 = ___qtreewidgetitem54->child(2);
        ___qtreewidgetitem57->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP1T DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem57->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Max value", 0));
        QTreeWidgetItem *___qtreewidgetitem58 = ___qtreewidgetitem54->child(3);
        ___qtreewidgetitem58->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP1D DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem58->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem59 = ___qtreewidgetitem52->child(2);
        ___qtreewidgetitem59->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Pressure alarm", 0));
        QTreeWidgetItem *___qtreewidgetitem60 = ___qtreewidgetitem59->child(0);
        ___qtreewidgetitem60->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP2 ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW_S", 0));
        ___qtreewidgetitem60->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "State", 0));
        QTreeWidgetItem *___qtreewidgetitem61 = ___qtreewidgetitem59->child(1);
        ___qtreewidgetitem61->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP2B DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem61->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Min value", 0));
        QTreeWidgetItem *___qtreewidgetitem62 = ___qtreewidgetitem59->child(2);
        ___qtreewidgetitem62->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP2T DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem62->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Max value", 0));
        QTreeWidgetItem *___qtreewidgetitem63 = ___qtreewidgetitem59->child(3);
        ___qtreewidgetitem63->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AP2D DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem63->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem64 = ___qtreewidgetitem52->child(3);
        ___qtreewidgetitem64->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH1 Flow alarm", 0));
        QTreeWidgetItem *___qtreewidgetitem65 = ___qtreewidgetitem64->child(0);
        ___qtreewidgetitem65->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF1 ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW_S", 0));
        ___qtreewidgetitem65->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "State", 0));
        QTreeWidgetItem *___qtreewidgetitem66 = ___qtreewidgetitem64->child(1);
        ___qtreewidgetitem66->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF1B DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem66->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Min value", 0));
        QTreeWidgetItem *___qtreewidgetitem67 = ___qtreewidgetitem64->child(2);
        ___qtreewidgetitem67->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF1T DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem67->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Max value", 0));
        QTreeWidgetItem *___qtreewidgetitem68 = ___qtreewidgetitem64->child(3);
        ___qtreewidgetitem68->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF1D DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem68->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem69 = ___qtreewidgetitem52->child(4);
        ___qtreewidgetitem69->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "CH2 Flow alarm", 0));
        QTreeWidgetItem *___qtreewidgetitem70 = ___qtreewidgetitem69->child(0);
        ___qtreewidgetitem70->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF2 ENUM OFF MAX DELTA MAX-DELTA MIN MAX-MIN DELTA-MIN MAX-DELTA-MIN RW_S", 0));
        ___qtreewidgetitem70->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "State", 0));
        QTreeWidgetItem *___qtreewidgetitem71 = ___qtreewidgetitem69->child(1);
        ___qtreewidgetitem71->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF2B DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem71->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Min value", 0));
        QTreeWidgetItem *___qtreewidgetitem72 = ___qtreewidgetitem69->child(2);
        ___qtreewidgetitem72->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF2T DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem72->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Max value", 0));
        QTreeWidgetItem *___qtreewidgetitem73 = ___qtreewidgetitem69->child(3);
        ___qtreewidgetitem73->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "AF1D DOUBLE32 1000 0 99 RW_S", 0));
        ___qtreewidgetitem73->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Delta value", 0));
        QTreeWidgetItem *___qtreewidgetitem74 = InfoTreeWidget->topLevelItem(8);
        ___qtreewidgetitem74->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Continuous sampling", 0));
        QTreeWidgetItem *___qtreewidgetitem75 = ___qtreewidgetitem74->child(0);
        ___qtreewidgetitem75->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "CLT UINT32 50 999 RW_S", 0));
        ___qtreewidgetitem75->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem76 = ___qtreewidgetitem74->child(1);
        ___qtreewidgetitem76->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "CLFN STRING 0 15 RW_S", 0));
        ___qtreewidgetitem76->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem77 = ___qtreewidgetitem74->child(2);
        ___qtreewidgetitem77->setText(1, QApplication::translate("LoggerConfPDLAG_USB", "CLPF INT32 100000 10000000 RW_S", 0));
        ___qtreewidgetitem77->setText(0, QApplication::translate("LoggerConfPDLAG_USB", "Samples per file", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        moduleOptions->setTabText(moduleOptions->indexOf(tab_2), QApplication::translate("LoggerConfPDLAG_USB", "Configuration", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("LoggerConfPDLAG_USB", "Folder browser", 0));
        label_3->setText(QString());
        label_4->setText(QApplication::translate("LoggerConfPDLAG_USB", "Log browser", 0));
        dockWidget->setWindowTitle(QApplication::translate("LoggerConfPDLAG_USB", "Plot window", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_3), QApplication::translate("LoggerConfPDLAG_USB", "Data browser", 0));
        groupBox_3->setTitle(QApplication::translate("LoggerConfPDLAG_USB", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfPDLAG_USB", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfPDLAG_USB", "STOP", 0));
        CH1Pressure_Checkbox->setText(QApplication::translate("LoggerConfPDLAG_USB", "CH1 Pressure", 0));
        CH2Pressure_Checkbox->setText(QApplication::translate("LoggerConfPDLAG_USB", "CH2 Pressure", 0));
        CH1TotalFlow_Checkbox->setText(QApplication::translate("LoggerConfPDLAG_USB", "CH1 Total flow", 0));
        CH2TotalFlow_Checkbox->setText(QApplication::translate("LoggerConfPDLAG_USB", "CH2 Total flow", 0));
        Battery_Checkbox->setText(QApplication::translate("LoggerConfPDLAG_USB", "Battery", 0));
        jpegSave->setText(QString());
        pngSave->setText(QString());
        pdfSave->setText(QString());
        moduleOptions->setTabText(moduleOptions->indexOf(tab), QApplication::translate("LoggerConfPDLAG_USB", "Realtime monitoring", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfPDLAG_USB: public Ui_LoggerConfPDLAG_USB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFPDLAG_USB_H
