#ifndef GENERAL
#define GENERAL

typedef enum {
    USBChannel  =0,
    BTChannel   =1,
    GSMChannel  =2,
    TCPChannel  =3,
} CommunicationType;

typedef enum {
    LoggerPDL   =0,
    RS232Logger =1,
    LoggerPDLH2 =2,  // GSM/BT logger
    LoggerPDLAG =3,
    FlowMeter   =5,
} LoggerType;

typedef enum {
    ItemProject     =0,
    ItemLocation    =1,
    ItemDevice      =2,
    ItemMeasurement =3,
} ItemType;

typedef enum {
    ItemNew   =0,
    ItemEdit =1,
} ActionType;

#endif // GENERAL

