/********************************************************************************
** Form generated from reading UI file 'loggerconfRS232_USB_simple.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFRS232_USB_SIMPLE_H
#define UI_LOGGERCONFRS232_USB_SIMPLE_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfRS232_USB_simple
{
public:
    QVBoxLayout *verticalLayout_3;
    QToolBox *toolBox;
    QWidget *page;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *InfoTreeWidget;
    QWidget *page_2;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *Sensor_Checkbox;
    QCheckBox *Battery_Checkbox;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_5;
    QHBoxLayout *horizontalLayout_8;
    QVBoxLayout *verticalLayout_6;
    QCustomPlot *realtimeWidget;
    QSlider *horizontalSlider;
    QSlider *verticalSlider;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_11;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_9;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QTreeView *dirTree;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_3;
    QListView *fileTree;
    QTabWidget *plottingTab;
    QWidget *page_4;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *LoggerConfRS232_USB_simple)
    {
        if (LoggerConfRS232_USB_simple->objectName().isEmpty())
            LoggerConfRS232_USB_simple->setObjectName(QStringLiteral("LoggerConfRS232_USB_simple"));
        LoggerConfRS232_USB_simple->resize(714, 664);
        verticalLayout_3 = new QVBoxLayout(LoggerConfRS232_USB_simple);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        toolBox = new QToolBox(LoggerConfRS232_USB_simple);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        QFont font;
        font.setPointSize(12);
        toolBox->setFont(font);
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 696, 514));
        verticalLayout_2 = new QVBoxLayout(page);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        InfoTreeWidget = new QTreeWidget(page);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font1);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/barcode/barcode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/lock/lock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/calendar/calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font);
        __qtreewidgetitem1->setIcon(0, icon);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem5->setFont(0, font);
        __qtreewidgetitem5->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem6->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem5);
        __qtreewidgetitem7->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem8->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem8);
        __qtreewidgetitem9->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(__qtreewidgetitem8);
        __qtreewidgetitem10->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem8);
        __qtreewidgetitem11->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem12->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem13->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem14->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem12);
        __qtreewidgetitem15->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem16->setFont(0, font);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem17->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem18->setIcon(0, icon8);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        verticalLayout_2->addWidget(InfoTreeWidget);

        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/System/settings/settings-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page, icon10, QStringLiteral("Configuration"));
        page_2 = new QWidget();
        page_2->setObjectName(QStringLiteral("page_2"));
        page_2->setGeometry(QRect(0, 0, 469, 132));
        verticalLayout = new QVBoxLayout(page_2);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        groupBox_3 = new QGroupBox(page_2);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_6 = new QHBoxLayout(groupBox_3);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        Realtime_START = new QPushButton(groupBox_3);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
        Realtime_START->setIcon(icon5);
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_3);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        Realtime_STOP->setIcon(icon11);
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_STOP);

        Sensor_Checkbox = new QCheckBox(groupBox_3);
        Sensor_Checkbox->setObjectName(QStringLiteral("Sensor_Checkbox"));
        QFont font2;
        font2.setPointSize(12);
        font2.setBold(true);
        font2.setWeight(75);
        Sensor_Checkbox->setFont(font2);

        horizontalLayout_6->addWidget(Sensor_Checkbox);

        Battery_Checkbox = new QCheckBox(groupBox_3);
        Battery_Checkbox->setObjectName(QStringLiteral("Battery_Checkbox"));
        Battery_Checkbox->setFont(font2);

        horizontalLayout_6->addWidget(Battery_Checkbox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font3;
        font3.setPointSize(14);
        label_5->setFont(font3);

        horizontalLayout_6->addWidget(label_5);


        verticalLayout->addWidget(groupBox_3);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QStringLiteral("horizontalLayout_8"));
        verticalLayout_6 = new QVBoxLayout();
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        realtimeWidget = new QCustomPlot(page_2);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy1);

        verticalLayout_6->addWidget(realtimeWidget);

        horizontalSlider = new QSlider(page_2);
        horizontalSlider->setObjectName(QStringLiteral("horizontalSlider"));
        horizontalSlider->setMinimum(2);
        horizontalSlider->setMaximum(100);
        horizontalSlider->setPageStep(2);
        horizontalSlider->setValue(10);
        horizontalSlider->setOrientation(Qt::Horizontal);

        verticalLayout_6->addWidget(horizontalSlider);


        horizontalLayout_8->addLayout(verticalLayout_6);

        verticalSlider = new QSlider(page_2);
        verticalSlider->setObjectName(QStringLiteral("verticalSlider"));
        verticalSlider->setMinimum(1);
        verticalSlider->setMaximum(30);
        verticalSlider->setPageStep(2);
        verticalSlider->setValue(5);
        verticalSlider->setOrientation(Qt::Vertical);

        horizontalLayout_8->addWidget(verticalSlider);


        verticalLayout->addLayout(horizontalLayout_8);

        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/win8/PNG/Charts/scatter_plot/scatter_plot-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_2, icon12, QStringLiteral("Realtime monitor"));
        page_3 = new QWidget();
        page_3->setObjectName(QStringLiteral("page_3"));
        page_3->setGeometry(QRect(0, 0, 392, 539));
        verticalLayout_11 = new QVBoxLayout(page_3);
        verticalLayout_11->setObjectName(QStringLiteral("verticalLayout_11"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_9 = new QVBoxLayout();
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(page_3);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(30, 30));
        label->setFrameShape(QFrame::NoFrame);
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/folder/folder-256.png")));
        label->setScaledContents(true);

        horizontalLayout_3->addWidget(label);

        label_2 = new QLabel(page_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_9->addLayout(horizontalLayout_3);

        dirTree = new QTreeView(page_3);
        dirTree->setObjectName(QStringLiteral("dirTree"));

        verticalLayout_9->addWidget(dirTree);


        horizontalLayout_5->addLayout(verticalLayout_9);

        verticalLayout_10 = new QVBoxLayout();
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(page_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(30, 30));
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/document/document-256.png")));
        label_3->setScaledContents(true);

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(page_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_10->addLayout(horizontalLayout_4);

        fileTree = new QListView(page_3);
        fileTree->setObjectName(QStringLiteral("fileTree"));
        fileTree->setMinimumSize(QSize(0, 0));
        fileTree->setAlternatingRowColors(true);
        fileTree->setSelectionRectVisible(true);

        verticalLayout_10->addWidget(fileTree);


        horizontalLayout_5->addLayout(verticalLayout_10);


        verticalLayout_11->addLayout(horizontalLayout_5);

        plottingTab = new QTabWidget(page_3);
        plottingTab->setObjectName(QStringLiteral("plottingTab"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(plottingTab->sizePolicy().hasHeightForWidth());
        plottingTab->setSizePolicy(sizePolicy2);
        plottingTab->setMinimumSize(QSize(0, 400));
        plottingTab->setTabPosition(QTabWidget::South);
        plottingTab->setTabShape(QTabWidget::Rounded);
        plottingTab->setElideMode(Qt::ElideNone);
        plottingTab->setDocumentMode(false);
        plottingTab->setTabsClosable(true);
        plottingTab->setMovable(true);

        verticalLayout_11->addWidget(plottingTab);

        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Charts/area_chart/area_chart-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_3, icon13, QStringLiteral("Data plotting"));
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        page_4->setGeometry(QRect(0, 0, 162, 124));
        verticalLayout_4 = new QVBoxLayout(page_4);
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lineEdit = new QLineEdit(page_4);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        pushButton = new QPushButton(page_4);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout_4->addLayout(horizontalLayout_2);

        textBrowser = new QTextBrowser(page_4);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_4->addWidget(textBrowser);

        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Ethernet/ethernet_off/ethernet_off-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_4, icon14, QStringLiteral("RS232 data"));

        verticalLayout_3->addWidget(toolBox);


        retranslateUi(LoggerConfRS232_USB_simple);

        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LoggerConfRS232_USB_simple);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfRS232_USB_simple)
    {
        LoggerConfRS232_USB_simple->setWindowTitle(QApplication::translate("LoggerConfRS232_USB_simple", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "IDN STRING 0 10 RW", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "FIRM STRING 0 10 R", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "HIDE", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "ID", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Operation", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = ___qtreewidgetitem5->child(0);
        ___qtreewidgetitem6->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "OPS ENUM Standby Continuous Interval RS232 GPRS RW", 0));
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Operation mode", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem5->child(1);
        ___qtreewidgetitem7->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "LOM TF RW", 0));
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Lock operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Continuous sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem8->child(0);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "CSI UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = ___qtreewidgetitem8->child(1);
        ___qtreewidgetitem10->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "LSNC STRING 0 22 RW", 0));
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem8->child(2);
        ___qtreewidgetitem11->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "CSS INT32 1 10000000 RW", 0));
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Samples per file", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Interval sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem12->child(0);
        ___qtreewidgetitem13->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "ISI UINT32 2 1000000 RW", 0));
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = ___qtreewidgetitem12->child(1);
        ___qtreewidgetitem14->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "LSNI STRING 0 22 RW", 0));
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem12->child(2);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "LSF ENUM FILE_PER_MINUTE FILE_PER_HOUR FILE_PER_DAY FILE_PER_MONTH FILE_PER_YEAR RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Log save option", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "RS232 sampling", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "LSNR STRING 0 22 RW", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem16->child(1);
        ___qtreewidgetitem18->setText(1, QApplication::translate("LoggerConfRS232_USB_simple", "RLPF INT32 1 10000000 RW", 0));
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfRS232_USB_simple", "Lines per file", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("LoggerConfRS232_USB_simple", "Configuration", 0));
        groupBox_3->setTitle(QApplication::translate("LoggerConfRS232_USB_simple", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfRS232_USB_simple", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfRS232_USB_simple", "STOP", 0));
        Sensor_Checkbox->setText(QApplication::translate("LoggerConfRS232_USB_simple", "Sensor", 0));
        Battery_Checkbox->setText(QApplication::translate("LoggerConfRS232_USB_simple", "Battery", 0));
        label_5->setText(QApplication::translate("LoggerConfRS232_USB_simple", "Export:", 0));
        toolBox->setItemText(toolBox->indexOf(page_2), QApplication::translate("LoggerConfRS232_USB_simple", "Realtime monitor", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("LoggerConfRS232_USB_simple", "Folder browser", 0));
        label_3->setText(QString());
        label_4->setText(QApplication::translate("LoggerConfRS232_USB_simple", "Log browser", 0));
        toolBox->setItemText(toolBox->indexOf(page_3), QApplication::translate("LoggerConfRS232_USB_simple", "Data plotting", 0));
        pushButton->setText(QApplication::translate("LoggerConfRS232_USB_simple", "SEND", 0));
        toolBox->setItemText(toolBox->indexOf(page_4), QApplication::translate("LoggerConfRS232_USB_simple", "RS232 data", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfRS232_USB_simple: public Ui_LoggerConfRS232_USB_simple {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFRS232_USB_SIMPLE_H
