#include "loggerinfo.h"
#include "ui_loggerinfo.h"
#include <QDateTime>
#include <QMenu>
#include <QDebug>

LoggerInfo::LoggerInfo(QWidget *parent,QString serial, CommunicationType type) :
    QWidget(parent),
    ui(new Ui::LoggerInfo)
{
    ui->setupUi(this);
    setSerial(serial);

    // Ovdje treba ikonicu i stil promjeniti
    switch (type){
        case USBChannel:
            ui->lblConnectionType->setText("Connection type: USB");
            ui->lblConnectionTypeIcon->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/USB/usb_logo/usb_logo-256.png")));
            CommType=USBChannel;
            //ui->btnDisconnect->hide();
        break;
    }

    if(serial.startsWith("1x")){
        ui->lblType->setText("Device type: RS232 Logger");
        DeviceType=RS232Logger;
    }

    if(serial.startsWith("0x")){
        ui->lblType->setText("Device type: Pressure logger");
        DeviceType=LoggerPDL;
    }

    if(serial.startsWith("2x")){
        ui->lblType->setText("Device type: GPRS/BT P logger");
        DeviceType=LoggerPDLH2;
    }

    if(serial.startsWith("3x")){
        ui->lblType->setText("Device type: LeakLess PDLAG");
        DeviceType=LoggerPDLAG;
    }

    if(serial.startsWith("5x")){
        ui->lblType->setText("Device type: Flow meter");
        DeviceType=FlowMeter;
    }
}

LoggerInfo::~LoggerInfo()
{
    delete ui;
}

void LoggerInfo::DriveChangeDetected(QStringList labels,QStringList names)
{
    QString ID = getSerial();
    for (int i=0;i<names.count();i++){
        if(ID.contains(labels.at(i),Qt::CaseInsensitive)){
            // Tu pozicioniram kontrole da gledaj taj disk
            QString sPath=names.at(i);
            pathToConnectedLogger = sPath;
        }
    }
}

void LoggerInfo::setStatus(QString StatusString, double freeSpace){
    QStringList StatusList=StatusString.split(',');
    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTimeString = dateTime.toString();
    if (StatusList.count()<7) return;

    QDateTime loggerTime=QDateTime::fromString(StatusList.at(1),"HH:mm:ss dd/MM/yyyy");
    QDateTime loggerTimeMore = dateTime.addSecs(60);
    QDateTime loggerTimeLess = dateTime.addSecs(-60);

    if(!loggerTime.isValid()){
        ui->lblTime->setText("Time: <span style='color:red'>"+StatusList.at(1)+"</span>");
    }else if (loggerTime.toMSecsSinceEpoch()>loggerTimeMore.toMSecsSinceEpoch() || loggerTime.toMSecsSinceEpoch()<loggerTimeLess.toMSecsSinceEpoch()){
        // Crveno
        ui->lblTime->setText("Time: <span style='color:red'>"+StatusList.at(1)+"</span>");
    }else{
        // Crno
        ui->lblTime->setText("Time: <span style='color:black'>"+StatusList.at(1)+"</span>");
    }
    ui->lblCapacity->setText("Capacity: "+StatusList.at(5)+" [mAh]");
    ui->lblChargingCurrent->setText("Current: "+StatusList.at(4)+" [mA]");
    ui->lblChargingStatus->setText("Status: "+StatusList.at(3));
    ui->lblOperationStatus->setText("Mode: "+StatusList.at(6));
    ui->lblVoltage->setText("Voltage: "+StatusList.at(2)+" [V]");

    double BatteryVolatage= StatusList.at(2).toDouble();
    if ((int)(BatteryVolatage*1000)>=ui->BatteryVoltageProggress->maximum()){
        ui->BatteryVoltageProggress->setValue(ui->BatteryVoltageProggress->maximum());
    }else{
        ui->BatteryVoltageProggress->setValue(BatteryVolatage*1000);
    }
}

QString LoggerInfo::getSerial(){
    return SerialNumber;
}

QString LoggerInfo::getIdentifier(){
    return Identifier;
}

CommunicationType LoggerInfo::getCommunicationType(){
    return CommType;
}

LoggerType LoggerInfo::getLoggerType(){
    return DeviceType;
}

void LoggerInfo::setSerial(QString serial){
    ui->groupBoxInfo->setTitle("Logger: "+serial);
    SerialNumber=serial;
}

void LoggerInfo::setIdentifier(QString identifier){
    if(SerialNumber.startsWith("1x")){
        ui->lblIdentifier->setText("Identifier: "+identifier);
    }else{
        ui->lblIdentifier->setText("Identifier: "+identifier);
    }
    Identifier=identifier;
}

void LoggerInfo::on_btnSelect_clicked()
{
    if(QApplication::queryKeyboardModifiers()==Qt::ControlModifier){
        emit LoggerSelected(SerialNumber,DeviceType, CommType);
    }else{
        emit LoggerSelectedSimple(SerialNumber,DeviceType, CommType);
    }
}

void LoggerInfo::on_btnDisconnect_clicked()
{
    emit LoggerDisconnect(SerialNumber,DeviceType, CommType);
}

void LoggerInfo::on_btnTransferData_clicked()
{
    DataTransfer *dataTransferWindow = new DataTransfer(this);
    dataTransferWindow->setWindowTitle("Data transfer window");
    dataTransferWindow->initialPathDevice = pathToConnectedLogger;
    dataTransferWindow->SerialNumber = getSerial();
    dataTransferWindow->Identifier = getIdentifier();
    dataTransferWindow->DeviceType = getLoggerType();
    dataTransferWindow->initialSetup();
    dataTransferWindow->show();
    this->setDisabled(true);
    dataTransferWindow->setEnabled(true);
    connect(dataTransferWindow,SIGNAL(finished(int)),this,SLOT(dataTransferFinished(int)));
}

void LoggerInfo::dataTransferFinished(int code){
    this->setEnabled(true);

    emit dataTransferFinishedLoggerInfoSignal();
}
