#ifndef DATATRANSFER_H
#define DATATRANSFER_H

#include <QDialog>
#include <QListWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QFileDialog>
#include <QList>
#include <QMenu>
#include <QMouseEvent>
#include <QAction>
#include <QTimer>
#include <QIcon>
#include <QMessageBox>
#include <QSettings>
#include <QDateTime>

#include <QDebug>

#include "general.h"
#include "dialogwait.h"

namespace Ui {
class DataTransfer;
}

class DataTransfer : public QDialog
{
    Q_OBJECT

public:
    explicit DataTransfer(QWidget *parent = 0);
    ~DataTransfer();

    QString SerialNumber;
    QString Identifier;
    LoggerType DeviceType;

    QString initialPathPC;
    QString initialPathDevice;
    void initialSetup();
    void treeInitialSetupPC();
    void treeInitialSetupDevice();

    void AddProject(QString name, QString pathToItem);
    void AddLocation(QTreeWidgetItem *project, QString name, QString pathToItem);
    void AddDevice(QTreeWidgetItem *location, QString name, QString pathToItem);
    void AddMeasurementFile(QTreeWidgetItem *device, QString name);

signals:

private slots:
    void dataTransferSelectionChanged();
    void on_btnRight_clicked();

private:
    qint16 _col = 200;
    qint16 _alpha = 255;
    Ui::DataTransfer *ui;

};

#endif // DATATRANSFER_H
