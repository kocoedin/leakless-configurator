#ifndef LOGGERINFO_H
#define LOGGERINFO_H

#include <QWidget>
#include "general.h"
#include "datatransfer.h"

namespace Ui {
class LoggerInfo;
}

class LoggerInfo : public QWidget
{
    Q_OBJECT

public:
    explicit LoggerInfo(QWidget *parent = 0, QString serial="Unknown", CommunicationType type=USBChannel);
    ~LoggerInfo();
    void setStatus(QString StatusString, double freeSpace);
    QString getSerial();
    QString getIdentifier();
    CommunicationType getCommunicationType();
    LoggerType getLoggerType();
    void setSerial(QString serial);
    void setIdentifier(QString identifier);

    QString pathToConnectedLogger;

signals:
    void LoggerSelected(QString ID,LoggerType DeviceType, CommunicationType CommType);
    void LoggerSelectedSimple(QString ID,LoggerType DeviceType, CommunicationType CommType);
    void LoggerDisconnect(QString ID,LoggerType DeviceType, CommunicationType CommType);
    void dataTransferFinishedLoggerInfoSignal();

public slots:
    void DriveChangeDetected(QStringList labels,QStringList names);

private slots:
    void on_btnSelect_clicked();
    void on_btnDisconnect_clicked();

    //void on_lblType_objectNameChanged(const QString &objectName);

    void on_btnTransferData_clicked();
    void dataTransferFinished(int code);

private:
    Ui::LoggerInfo *ui;
    QString SerialNumber;
    QString Identifier;
    CommunicationType CommType;
    LoggerType DeviceType;
    QPalette pal;
};

#endif // LOGGERINFO_H
