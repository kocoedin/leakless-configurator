#include "loggerconfpdlag_USB.h"
#include "ui_loggerconfpdlag_USB.h"

LoggerConfPDLAG_USB::LoggerConfPDLAG_USB(QWidget *parent,bool Simple) :
    QWidget(parent),
    ui(new Ui::LoggerConfPDLAG_USB)
{
    ui->setupUi(this);
    ConfigurationInProgress=false;
    sendingTimer.setInterval(10);
    connect(&sendingTimer, SIGNAL(timeout()),this,SLOT(sendingProcedure()));

    m_simple=Simple;
    m_range=10;

    FindAllTreeItems(ui->InfoTreeWidget->invisibleRootItem());

    ui->InfoTreeWidget->resizeColumnToContents(0);
    ui->InfoTreeWidget->expandAll();

    connect(ui->plottingTab, SIGNAL(tabCloseRequested(int)), this,SLOT(plottingTabWidgetTabCloseRequested(int)));
    PlotColors<<Qt::red<<Qt::blue<<Qt::cyan<<Qt::black<<Qt::green;

    /* Realtime plot */
    for(int i=0;i<5 ; i++){
        ui->realtimeWidget->addGraph();
        ui->realtimeWidget->graph()->setLineStyle(QCPGraph::lsLine);
        ui->realtimeWidget->graph()->setScatterStyle(QCPScatterStyle::ssNone);

        QPen graphPen;
        QPen selectedPen;
        graphPen.setColor(PlotColors.at(i));
        switch (i){
        case 0:
            ui->realtimeWidget->graph()->setName(QString("CH1 Pressure"));
            break;
        case 1:
            ui->realtimeWidget->graph()->setName(QString("CH2 Pressure"));
            break;
        case 2:
            ui->realtimeWidget->graph()->setName(QString("CH1 Total flow"));
            break;
        case 3:
            ui->realtimeWidget->graph()->setName(QString("CH2 Total flow"));
            break;
        case 4:
            ui->realtimeWidget->graph()->setName(QString("Battery [V]"));
            break;
        }
        graphPen.setWidthF(3);
        ui->realtimeWidget->graph()->setPen(graphPen);
        ui->realtimeWidget->replot();

        selectedPen.setColor(QColor(255,215,0));
        selectedPen.setWidthF(graphPen.widthF());
        ui->realtimeWidget->graph()->setSelectedPen(selectedPen);
        ui->realtimeWidget->rescaleAxes();
        ui->realtimeWidget->replot();

    }

    ui->realtimeWidget->setInteractions(QCP::iRangeDrag | QCP::iRangeZoom | QCP::iSelectAxes | QCP::iSelectPlottables);
    ui->realtimeWidget->xAxis->rescale();
    ui->realtimeWidget->yAxis->rescale();
    ui->realtimeWidget->axisRect()->setupFullAxesBox();
    ui->realtimeWidget->plotLayout()->insertRow(0);
    ui->realtimeWidget->plotLayout()->addElement(0, 0, new QCPPlotTitle(ui->realtimeWidget, "Realtime data"));
    ui->realtimeWidget->xAxis->setLabel("Time (10 sec)");
    ui->realtimeWidget->xAxis->setTickLabelType(QCPAxis::ltDateTime);
    ui->realtimeWidget->xAxis->setDateTimeFormat("mm:ss");
    ui->realtimeWidget->yAxis->setLabel("Value");
    ui->realtimeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
    ui->realtimeWidget->yAxis->rescale();

    streamingTimer.setInterval(10);
    streamingTimer.stop();

    connect(ui->realtimeWidget, SIGNAL(mouseWheel(QWheelEvent*)), this, SLOT(mouseWheel(QWheelEvent*)));
    connect(ui->realtimeWidget->xAxis, SIGNAL(rangeChanged(QCPRange)), ui->realtimeWidget->xAxis2, SLOT(setRange(QCPRange)));
    connect(ui->realtimeWidget->yAxis, SIGNAL(rangeChanged(QCPRange)), ui->realtimeWidget->yAxis2, SLOT(setRange(QCPRange)));
    connect(&streamingTimer,SIGNAL(timeout()), this, SLOT(drawStreamPlot()));

    /* File Browse*/
    ui->dirTree->setModel(NULL);
    ui->fileTree->setModel(NULL);

}

LoggerConfPDLAG_USB::~LoggerConfPDLAG_USB()
{
    delete ui;
}

void LoggerConfPDLAG_USB::SetID(QString ID){
    LoggerID=ID;
}

QString LoggerConfPDLAG_USB::GetID(){
    return LoggerID;
}

void LoggerConfPDLAG_USB::refereshParameters(){

    SendingStrings.clear();
    foreach(QString s, Commands){
        SendingStrings.append("CONFIG "+s+" GET 0#");
    }
    ConfigurationInProgress=true;
    sendingTimer.start();
}

void LoggerConfPDLAG_USB::FindAllTreeItems( QTreeWidgetItem *item )
{
    if (item->text(1)!=""){
        QStringList parameters=item->text(1).split(" ");

        if(m_simple && !parameters.last().contains("_S")){
            item->setHidden(true);
        }else{
            Commands.append(parameters.at(0));
            Items.append(item);
        }
        item->setText(1,"");


        if(parameters.last()=="HIDE"){
            item->setHidden(true);
        }else if(parameters.at(1)=="ENUM"){
            QComboBox *comboBox=new QComboBox();
            comboBox->setProperty("Command",parameters.at(0));
            for (int i=2;i<parameters.count()-1;i++) comboBox->addItem(parameters.at(i));
            ui->InfoTreeWidget->setItemWidget(item,1,comboBox);
            connect(comboBox,SIGNAL(currentIndexChanged(int)),this,SLOT(ComboBox_changed(int)));
        }else if(parameters.at(1)=="TF"){
            QCheckBox *checkBox=new QCheckBox();
            checkBox->setProperty("Command",parameters.at(0));
            ui->InfoTreeWidget->setItemWidget(item, 1, checkBox);
            connect(checkBox,SIGNAL(clicked(bool)),this,SLOT(CheckBox_changed(bool)));
        }else if(parameters.at(1)=="DT"){
            QDateTimeEdit *dateTimeBox=new QDateTimeEdit();
            dateTimeBox->setProperty("Command",parameters.at(0));
            ui->InfoTreeWidget->setItemWidget(item, 1, dateTimeBox);
            connect(dateTimeBox,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(DateTimeBox_changed(QDateTime)));
        }else if(parameters.at(1)=="STRING"){
            QLineEdit *textBox=new QLineEdit();
            textBox->setProperty("Command",parameters.at(0));
            ui->InfoTreeWidget->setItemWidget(item, 1, textBox);
            textBox->setMaxLength(parameters.at(3).toInt());
            if (!parameters.last().contains("RW")) textBox->setEnabled(false);
            connect(textBox,SIGNAL(textChanged(QString)),this,SLOT(TextBox_changed(QString)));
        }else if(parameters.at(1)=="UINT32"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",4);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="UINT16"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",2);
            spinBox->setProperty("Signed",false);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="UINT8"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",1);
            spinBox->setProperty("Signed",false);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="INT32"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",4);
            spinBox->setProperty("Signed",false);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="INT16"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",2);
            spinBox->setProperty("Signed",false);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="INT8"){
            QSpinBox *spinBox= new QSpinBox();
            spinBox->setProperty("Command",parameters.at(0));
            spinBox->setProperty("Size",1);
            spinBox->setProperty("Signed",false);
            ui->InfoTreeWidget->setItemWidget(item, 1, spinBox);
            spinBox->setMinimum(parameters.at(2).toInt());
            spinBox->setMaximum(parameters.at(3).toInt());
            connect(spinBox,SIGNAL(valueChanged(int)),this, SLOT(SpinBox_changed(int)));
        }else if(parameters.at(1)=="DOUBLE32"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",4);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->InfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            doubleSpinBox->setDecimals(3);
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
        }else if(parameters.at(1)=="DOUBLE16"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",2);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->InfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
        }else if(parameters.at(1)=="DOUBLE8"){
            QDoubleSpinBox *doubleSpinBox= new QDoubleSpinBox();
            doubleSpinBox->setProperty("Command",parameters.at(0));
            doubleSpinBox->setProperty("Size",1);
            doubleSpinBox->setProperty("Multiplier",parameters.at(2));
            ui->InfoTreeWidget->setItemWidget(item, 1, doubleSpinBox);
            doubleSpinBox->setMinimum(parameters.at(3).toDouble());
            doubleSpinBox->setMaximum(parameters.at(4).toDouble());
            connect(doubleSpinBox,SIGNAL(valueChanged(double)),this, SLOT(DoubleSpinBox_changed(double)));
        }else if(parameters.at(1)=="DATETIME"){
            QDateTimeEdit *dateTimeEdit= new QDateTimeEdit();
            dateTimeEdit->setProperty("Command",parameters.at(0));
            dateTimeEdit->setCalendarPopup(true);
            ui->InfoTreeWidget->setItemWidget(item, 1, dateTimeEdit);
            connect(dateTimeEdit,SIGNAL(dateTimeChanged(QDateTime)),this, SLOT(DateTimeBox_changed(QDateTime)));
        }else{


        }

    }
    for( int i = 0; i < item->childCount(); ++i )FindAllTreeItems( item->child(i) );
}

void LoggerConfPDLAG_USB::plottingTabWidgetTabCloseRequested(int index){
    ui->plottingTab->removeTab(index);
}

void LoggerConfPDLAG_USB::ComboBox_changed(int Value){
    if (ConfigurationInProgress) return;
    QComboBox *obj  = dynamic_cast<QComboBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        SendData(LoggerID,QString("CONFIG "+Command+" SET %1#").arg(Value));
    }
}

void LoggerConfPDLAG_USB::TextBox_changed(QString Value){
    if (ConfigurationInProgress) return;
    QLineEdit *obj  = dynamic_cast<QLineEdit*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        SendData(LoggerID,QString("CONFIG "+Command+" SET %1#").arg(Value));
    }
}

void LoggerConfPDLAG_USB::CheckBox_changed(bool Value){
    if (ConfigurationInProgress) return;
    QCheckBox *obj  = dynamic_cast<QCheckBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        if (Value) SendData(LoggerID,"CONFIG "+Command+" SET 1#");
        else SendData(LoggerID,"CONFIG "+Command+" SET 0#");
    }
}

void LoggerConfPDLAG_USB::DateTimeBox_changed(QDateTime Value){
    if (ConfigurationInProgress) return;
    QDateTimeEdit *obj  = dynamic_cast<QDateTimeEdit*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        SendData(LoggerID,"CONFIG "+Command+" SET "+obj->dateTime().toString("hh:mm:ss:dd:MM:yy")+"#");
    }

}

void LoggerConfPDLAG_USB::SpinBox_changed(int Value){
    if (ConfigurationInProgress) return;
    unsigned int Number;
    Number=(unsigned int)(Value);
    QString HexValue;
    HexValue=QString::number((unsigned int)Number,16);
    QSpinBox *obj  = dynamic_cast<QSpinBox*>(sender());
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        SendData(LoggerID,QString("CONFIG "+Command+" SET %1#").arg(HexValue));
    }
}

void LoggerConfPDLAG_USB::DoubleSpinBox_changed(double Value){
    if (ConfigurationInProgress) return;
    QDoubleSpinBox *obj  = dynamic_cast<QDoubleSpinBox*>(sender());
    int Multiplier=obj->property("Multiplier").toInt();
    unsigned long Number=Value*Multiplier;
    QString HexValue;
    if( obj != NULL )
    {
        QString Command=obj->property("Command").toString();
        HexValue=QString::number((unsigned long)(Number),16);
        SendData(LoggerID,QString("CONFIG "+Command+" SET %1#").arg(HexValue));
    }
}

void LoggerConfPDLAG_USB::RecieveData(QString ID,QString Data){

    if (ID!=LoggerID) return;
    if (Data.startsWith("STATUS")) return;

    if (measurementRealtimeStarted==true){
        if (Data.startsWith("[Stream]", Qt::CaseInsensitive)){
            QStringList Splitted=Data.split(" ");
            if (Splitted.count()<6) return;
            RealtimeCH1Pressure     =Splitted.at(1).toDouble();
            RealtimeCH2Pressure     =Splitted.at(2).toDouble();
            RealtimeCH1TotalFlow    =Splitted.at(3).toDouble();
            RealtimeCH2TotalFlow    =Splitted.at(4).toDouble();
            RealtimeBattery         =Splitted.at(5).toDouble();
            return;
        }
    }


    QStringList DataSplit=Data.split(" ");
    if (DataSplit.count()<4 || DataSplit.at(0)!="[Config]") return;
    QString Command =DataSplit.at(1);
    QString Value=DataSplit.at(3);

    QTreeWidgetItem *item=Items.at(Commands.indexOf(Command));

    bool ok;
    QWidget *widget=ui->InfoTreeWidget->itemWidget(item,1);
    if(QComboBox *pb = qobject_cast<QComboBox *>(widget)) {
        if (DataSplit.at(2)=="GET")pb->setCurrentIndex(Value.toInt());
        else{
            disconnect(pb,SIGNAL(currentIndexChanged(int)),this,SLOT(ComboBox_changed(int)));
            pb->setCurrentIndex(Value.toInt());
            connect(pb,SIGNAL(currentIndexChanged(int)),this,SLOT(ComboBox_changed(int)));
        }
    }else if(QSpinBox *pb = qobject_cast<QSpinBox *>(widget)){
        if (DataSplit.at(2)=="GET")pb->setValue(Value.toInt(&ok,16));
        else{
            disconnect(pb,SIGNAL(valueChanged(int)),this,SLOT(SpinBox_changed(int)));
            pb->setValue(Value.toInt(&ok,16));
            connect(pb,SIGNAL(valueChanged(int)),this,SLOT(SpinBox_changed(int)));
        }
    }else if(QDoubleSpinBox *pb = qobject_cast<QDoubleSpinBox *>(widget)){
        if (DataSplit.at(2)=="GET")pb->setValue(Value.toInt(&ok,16)*1.0/pb->property("Multiplier").toInt(&ok));
        else{
            disconnect(pb,SIGNAL(valueChanged(double)),this,SLOT(DoubleSpinBox_changed(double)));
            pb->setValue(Value.toInt(&ok,16)*1.0/pb->property("Multiplier").toInt(&ok));
            connect(pb,SIGNAL(valueChanged(double)),this,SLOT(DoubleSpinBox_changed(double)));
        }
    }else if(QLineEdit *pb = qobject_cast<QLineEdit *>(widget)){
        if (DataSplit.at(2)=="GET")pb->setText(Value);
        else{
            disconnect(pb,SIGNAL(textChanged(QString)),this,SLOT(TextBox_changed(QString)));
            pb->setText(Value);
            connect(pb,SIGNAL(textChanged(QString)),this,SLOT(TextBox_changed(QString)));
        }
    }else if(QCheckBox *pb = qobject_cast<QCheckBox *>(widget)){
        if (DataSplit.at(2)=="GET"){
            if(Value.contains("1")) pb->setChecked(true);
            else pb->setChecked(false);
        }else{
            disconnect(pb,SIGNAL(clicked(bool)),this,SLOT(CheckBox_changed(bool)));
            if(Value.contains("1")) pb->setChecked(true);
            else pb->setChecked(false);
            connect(pb,SIGNAL(clicked(bool)),this,SLOT(CheckBox_changed(bool)));
        }
    }else if(QDateTimeEdit *pb = qobject_cast<QDateTimeEdit *>(widget)){
        if (DataSplit.at(2)=="GET"){
            pb->setDateTime(QDateTime::fromString(Value,"hh:mm:ss:dd:MM:yyyy"));
        }else{
            disconnect(pb,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(DateTimeBox_changed(QDateTime)));
            pb->setDateTime(QDateTime::fromString(Value,"hh:mm:ss:dd:MM:yyyy"));
            connect(pb,SIGNAL(dateTimeChanged(QDateTime)),this,SLOT(DateTimeBox_changed(QDateTime)));
        }
    }
}

void LoggerConfPDLAG_USB::sendingProcedure()
{
    if(SendingStrings.count()==0){
        SendingStrings.clear();
        sendingTimer.stop();
        ConfigurationInProgress=false;
        // Ovdje opet connectati
    }else{
        SendData(LoggerID,SendingStrings.at(0));
        SendingStrings.removeAt(0);
    }

}


void LoggerConfPDLAG_USB::on_fileTree_doubleClicked(const QModelIndex &index)
{
    QString sPath=fileModel->fileInfo(index).absoluteFilePath();
    QFile fromFile(sPath);
    QByteArray temp=qgetenv("TEMP");
    QString s(temp);
    QString TempDir=s;
    QString TempFile=TempDir.append("\\temp.log");
    QFile toFile(TempFile);
    qint64 nCopySize = fromFile.size();
    if (nCopySize>1000){
        if (toFile.exists()) toFile.remove();
        Dialogwait *progressDialog;
        progressDialog = new Dialogwait(this);
        progressDialog->show();

        fromFile.open(QIODevice::ReadOnly);
        toFile.open(QIODevice::WriteOnly|QIODevice::Truncate);
        progressDialog->setMin(0);
        progressDialog->setMax(nCopySize);
        for (qint64 i = 0; i < nCopySize; i+=512) {
            toFile.write(fromFile.read(i)); // write a byte
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
            fromFile.seek(i);  // move to next byte to read
            toFile.seek(i); // move to next byte to write
            progressDialog->setValue((int)(i));
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        }
        fromFile.close();
        toFile.close();
        progressDialog->setText("Plotting data...");
        progressDialog->startWait();
        QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        plotData(TempFile,sPath);//plotData("temp.log",sPath);
        progressDialog->stopWait();
    }else{
        plotData(sPath,sPath);
    }


}

void LoggerConfPDLAG_USB::on_dirTree_clicked(const QModelIndex &index)
{
    QString sPath=dirModel->fileInfo(index).absoluteFilePath();
    ui->fileTree->setRootIndex(fileModel->setRootPath(sPath));
}

void LoggerConfPDLAG_USB::plotData(QString Path,QString Name){
    int i=0;
    QVector<QString>                Names;
    QVector<QString>                Units;
    QDate                           Date;
    QTime                           Time;
    QDateTime                       TimeStamp;
    QVector<double>                 TimeStamps;
    QVector<QVector<double> >       Values;

    int dataToPlotCount=0;
    Names.clear();
    Units.clear();
    QFile file(Path);
    if (!file.open(QIODevice::ReadOnly | QIODevice::Text))return;

    QTextStream in(&file);

    /* Citam header file */
    QString line = in.readLine();
    QStringList Data=line.split(";");

    if(Data.length()<2){
        QMessageBox::warning(this, tr("Error"),tr("Error parsing the log file"),QMessageBox::Ok);
        return;
    }
    for(i=2;i<Data.count();i++){
        QStringList Split;
        Split=Data.at(i).split(" ");
        if(Split.count()==2){
            Names.append(Split.at(0));
            Units.append(Split.at(1));
            dataToPlotCount++;
        }

    }
    Values.resize(dataToPlotCount);

    plottingWidget *plotting=new plottingWidget(this);
    ui->plottingTab->addTab(plotting,Name);

    /* Ovdje odredujem sta uopce trebam plottati */
    while (!in.atEnd()) {
        line = in.readLine();
        Data=line.split(";");
        Data.removeAll("");
        if (Data.count()<2+dataToPlotCount) continue;

        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
<<<<<<< HEAD
        if(Data.at(0).contains(".")){
            Time=QTime::fromString(Data.at(0),"HH:mm:ss.zzz");
        }else{
            Time=QTime::fromString(Data.at(0),"HH:mm:ss");
        }
=======
        if (Data.at(0).contains('.'))
            Time=QTime::fromString(Data.at(0),"HH:mm:ss.zzz");
        else
            Time=QTime::fromString(Data.at(0),"HH:mm:ss");
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
        Date=QDate::fromString(Data.at(1),"dd/MM/yy");
        Date=Date.addYears(100);
        TimeStamp=QDateTime(Date,Time);
        if (TimeStamp.isValid()) TimeStamps.append(TimeStamp.toMSecsSinceEpoch()/1000.0);
        else continue;
        for (int j=0; j<dataToPlotCount;j++){
            Values[j].append(Data.at(2+j).toDouble());
        }
    }
    file.close();

    for (int i=0;i<dataToPlotCount;i++){
        plotting->addGraph(TimeStamps,Values[i],Names[i],Units[i],PlotColors.at(i));
    }

}


void LoggerConfPDLAG_USB::on_Realtime_START_clicked()
{

    ui->realtimeWidget->graph(0)->clearData();
    ui->realtimeWidget->graph(1)->clearData();
    ui->realtimeWidget->graph(2)->clearData();
    ui->realtimeWidget->graph(3)->clearData();
    ui->realtimeWidget->graph(4)->clearData();
    SendData(LoggerID,"RETURN STREAM ON#");
    ui->Realtime_START->setDisabled(true);
    ui->Realtime_STOP->setEnabled(true);

    measurementRealtimeStarted = true;
    streamingTimer.start();
}

void LoggerConfPDLAG_USB::on_Realtime_STOP_clicked()
{

    measurementRealtimeStarted=false;
    ui->Realtime_START->setEnabled(true);
    ui->Realtime_STOP->setDisabled(true);
    measurementRealtimeStarted = false;
    streamingTimer.stop();
    SendData(LoggerID,"RETURN STREAM OFF#");
}

void LoggerConfPDLAG_USB::FindLoggerDrive()
{
}

void LoggerConfPDLAG_USB::DriveChangeDetected(QStringList labels,QStringList names){
    for (int i=0;i<names.count();i++){
        if(LoggerID.contains(labels.at(i),Qt::CaseInsensitive)){
            // Tu pozicioniram kontrole da gledaj taj disk
            QString sPath=names.at(i);
            qDebug() << sPath;
            rootPath = sPath;
            ConnectToInfoFile(sPath);
            // Tu pozicioniram kontrole da gledaj taj disk
            dirModel=new QFileSystemModel(this);
            dirModel->setFilter(QDir::NoDotAndDotDot| QDir::AllDirs);
            dirModel->setRootPath(sPath);
            QStringList filter;
            filter.append(labels.at(i));
            dirModel->setNameFilters(filter);

            ui->dirTree->setModel(dirModel);
            ui->dirTree->setRootIndex(dirModel->index(sPath));
            fileModel=new QFileSystemModel(this);
            fileModel->setFilter(QDir::NoDotAndDotDot| QDir::Files);
            fileModel->setRootPath(sPath);

            QStringList fileFilter;
            fileFilter.append("*.log");
            fileModel->setNameFilters(fileFilter);
            ui->fileTree->setModel(fileModel);
            ui->fileTree->setRootIndex(fileModel->setRootPath(sPath));
        }
    }
}

void LoggerConfPDLAG_USB::ConnectToInfoFile(QString Path){
    m_file.setFileName(Path+"\\info.info");
    if (m_file.open(QFile::ReadWrite | QFile::Text)){
        ui->tedInfoFileEdit->setPlainText(m_file.readAll());
        m_file.close();
    }
}

void LoggerConfPDLAG_USB::on_btnSaveInfoFile_clicked()
{
    if (m_file.open(QFile::WriteOnly | QFile::Text)){
        QTextStream outStream(&m_file);
        outStream<<ui->tedInfoFileEdit->toPlainText();
        m_file.close();
    }
}

void LoggerConfPDLAG_USB::drawStreamPlot()
{

    double key = QDateTime::currentDateTime().toMSecsSinceEpoch();///1000.0;

    // Provjeriti koji se podaci plotaju
    if (ui->CH1Pressure_Checkbox->isChecked()){
        ui->realtimeWidget->graph(0)->addData(key,RealtimeCH1Pressure);
    }
    if (ui->CH2Pressure_Checkbox->isChecked()){
        ui->realtimeWidget->graph(1)->addData(key,RealtimeCH2Pressure);
    }
    if (ui->CH1TotalFlow_Checkbox->isChecked()){
        ui->realtimeWidget->graph(2)->addData(key,RealtimeCH1TotalFlow);
    }
    if (ui->CH2TotalFlow_Checkbox->isChecked()){
        ui->realtimeWidget->graph(3)->addData(key,RealtimeCH2TotalFlow);
    }
    if (ui->Battery_Checkbox->isChecked()){
        ui->realtimeWidget->graph(4)->addData(key,RealtimeBattery);
    }


    ui->realtimeWidget->xAxis->setRange(key+0.25, m_range*1000, Qt::AlignRight);
    ui->realtimeWidget->replot();

}
void LoggerConfPDLAG_USB::mouseWheel(QWheelEvent* event)
{
    if (ui->realtimeWidget->xAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->realtimeWidget->axisRect()->setRangeZoom(ui->realtimeWidget->xAxis->orientation());
        if (event->delta()<0){
            if(m_range++>360) m_range=360;
        }
        if (event->delta()>0){
            if(m_range--<2) m_range=2;

        }
    }
    else if (ui->realtimeWidget->yAxis->selectedParts().testFlag(QCPAxis::spAxis)){
        ui->realtimeWidget->axisRect()->setRangeZoom(ui->realtimeWidget->yAxis->orientation());

    }
    else
        ui->realtimeWidget->axisRect()->setRangeZoom(Qt::Horizontal|Qt::Vertical);


    ui->realtimeWidget->xAxis->setLabel(tr("Time (%1 sec)").arg(m_range));
}

void LoggerConfPDLAG_USB::on_jpegSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Save to .png","", tr("PNG Files (*.png)"));
    if(fileName=="") return;
    ui->realtimeWidget->saveJpg(fileName+".jpg",1600,900,1,-1);
}

void LoggerConfPDLAG_USB::on_pngSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Save to .png","", tr("PNG Files (*.png)"));
    if(fileName=="") return;
    ui->realtimeWidget->savePng(fileName+".png",1600,900,1);
}

void LoggerConfPDLAG_USB::on_pdfSave_clicked()
{
    QString fileName = QFileDialog::getSaveFileName(this,"Save to .pdf","", tr("PDF Files (*.pdf)"));
    if(fileName=="") return;
    ui->realtimeWidget->savePdf(fileName+".pdf",false,1024,768);
}
