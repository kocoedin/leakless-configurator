#include "deployedloggershistory.h"
#include "ui_deployedloggershistory.h"
using namespace std;

DeployedLoggersHistory::DeployedLoggersHistory(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DeployedLoggersHistory)
{
    ui->setupUi(this);

    ui->rbxNoFilter->setChecked(true);

    ui->cbxDate->setEnabled(false);
    ui->dteDeployTime->setEnabled(false);
    ui->dteRemoveTime->setEnabled(false);

    ui->cbxLocation->setEnabled(false);
    ui->cmbxLocation->setEnabled(false);

    ui->cbxType->setEnabled(false);
    ui->cmbxType->setEnabled(false);

    connect(ui->rbxFilter,SIGNAL(toggled(bool)),this,SLOT(refreshLoggersHistoryList()));

    connect(ui->rbxFilter,SIGNAL(toggled(bool)),ui->cbxDate,SLOT(setEnabled(bool)));
    connect(ui->cbxDate,SIGNAL(toggled(bool)),ui->dteDeployTime,SLOT(setEnabled(bool)));
    connect(ui->cbxDate,SIGNAL(toggled(bool)),ui->dteRemoveTime,SLOT(setEnabled(bool)));
    connect(ui->cbxDate,SIGNAL(toggled(bool)),this,SLOT(refreshLoggersHistoryList()));
    connect(ui->dteDeployTime,SIGNAL(dateChanged(QDate)),this,SLOT(refreshLoggersHistoryList()));
    connect(ui->dteRemoveTime,SIGNAL(dateChanged(QDate)),this,SLOT(refreshLoggersHistoryList()));

    connect(ui->rbxFilter,SIGNAL(toggled(bool)),ui->cbxLocation,SLOT(setEnabled(bool)));
    connect(ui->cbxLocation,SIGNAL(toggled(bool)),ui->cmbxLocation,SLOT(setEnabled(bool)));
    connect(ui->cbxLocation,SIGNAL(toggled(bool)),this,SLOT(refreshLoggersHistoryList()));
    connect(ui->cmbxLocation,SIGNAL(currentTextChanged(QString)),this,SLOT(refreshLoggersHistoryList()));

    connect(ui->rbxFilter,SIGNAL(toggled(bool)),ui->cbxType,SLOT(setEnabled(bool)));
    connect(ui->cbxType,SIGNAL(toggled(bool)),ui->cmbxType,SLOT(setEnabled(bool)));
    connect(ui->cbxType,SIGNAL(toggled(bool)),this,SLOT(refreshLoggersHistoryList()));
    connect(ui->cmbxType,SIGNAL(currentTextChanged(QString)),this,SLOT(refreshLoggersHistoryList()));

    fillFilterComboBoxes();
    refreshLoggersHistoryList();
}

DeployedLoggersHistory::~DeployedLoggersHistory()
{
    delete ui;
}

void DeployedLoggersHistory::refreshLoggersHistoryList(){
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QDir ImageDir(FolderPath+"images");
    QDir FileDir(FolderPath);

    ui->trwLoggersHistory->clear();

    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);

        preferences.beginGroup("GeneralInformation");
        QString DeviceID=preferences.value("DeviceID").toString();
        QString DeviceTimeString=preferences.value("Time").toString();
        QString DeviceDateString=preferences.value("Date").toString();
        QTime DeviceTime=QTime::fromString(DeviceTimeString,"hh:mm:ss");
        QDate DeviceDate=QDate::fromString(DeviceDateString,"dd/MM/yyyy");
        QDateTime deployedTime=QDateTime(DeviceDate,DeviceTime);
        QDateTime removedTime=QDateTime(DeviceDate,DeviceTime);
        bool DeployFinished=preferences.value("Finished",false).toBool();
        preferences.endGroup();

        preferences.beginGroup("LocationInformation");
        QString type=preferences.value("Type").toString();
        QString location=preferences.value("Location").toString();
        QString DeviceLatitude=preferences.value("Latitude").toString();
        QString DeviceLongitude=preferences.value("Longitude").toString();
        preferences.endGroup();


        bool boolFiltered=false;
        bool boolFilterDate=false;
        bool boolFilterLocation=false;
        bool boolFilterType=false;

        if (DeployFinished)
        {
            if (ui->rbxFilter->isChecked())
            {
                if (ui->cbxDate->isChecked())
                {
                    QDateTime deployedTimeFilter=ui->dteDeployTime->dateTime();
                    QDateTime removedTimeFitler=ui->dteRemoveTime->dateTime();
                    qint64 msecDeployedTimeFitler=deployedTimeFilter.toMSecsSinceEpoch();
                    qint64 msecRemovedTimeFitler=removedTimeFitler.toMSecsSinceEpoch();

                    qint64 msecDeployedTime=deployedTime.toMSecsSinceEpoch();
                    qint64 msecRemovedTime=removedTime.toMSecsSinceEpoch();

                    if ((msecDeployedTime<msecRemovedTimeFitler) &&
                            (msecRemovedTime>msecDeployedTimeFitler) &&
                            (msecDeployedTimeFitler<msecRemovedTimeFitler))
                    {
                        boolFilterDate = true;
                    }
                }
                else
                    boolFilterDate = true;
                if (ui->cbxLocation->isChecked())
                {
                    QString filterLocation=ui->cmbxLocation->currentText();
                    if ((location==filterLocation) || (filterLocation==""))
                    {
                        boolFilterLocation = true;
                    }
                }
                else
                    boolFilterLocation = true;
                if (ui->cbxType->isChecked())
                {
                    QString filterType=ui->cmbxType->currentText();
                    if ((type==filterType) || (filterType==""))
                        boolFilterType = true;
                }
                else
                    boolFilterType = true;

                if (boolFilterDate && boolFilterLocation && boolFilterType)
                    boolFiltered = true;
            }
            else
            {
                boolFiltered = true;
            }
        }
        else
            continue;

        if (boolFiltered)
        {
            QTreeWidgetItem *newDevice=new QTreeWidgetItem();
            newDevice->setText(0, DeviceLatitude);
            newDevice->setText(1, DeviceLongitude);
            newDevice->setText(2, DeviceID);
            newDevice->setText(3, QDateTime(DeviceDate,DeviceTime).toString("dd/MM/yyyy hh:mm:ss"));
            QDateTime TimeDateNow=QDateTime::currentDateTime();
            QDateTime TimeDateStart=QDateTime(DeviceDate,DeviceTime);
            qint64 msec=TimeDateStart.msecsTo(TimeDateNow);
            qint64 secs=msec/1000;
            qint64 minutes=secs/60;
            qint64 hours=minutes/60;
            newDevice->setText(4, QString::number(hours) + " hours");
            newDevice->setText(5, fileInfo.filePath());
            ui->trwLoggersHistory->addTopLevelItem(newDevice);
        }
    }

    ui->trwLoggersHistory->resizeColumnToContents(0);
    ui->trwLoggersHistory->resizeColumnToContents(1);
    ui->trwLoggersHistory->resizeColumnToContents(2);
    ui->trwLoggersHistory->resizeColumnToContents(3);
    ui->trwLoggersHistory->resizeColumnToContents(4);
    ui->trwLoggersHistory->expandAll();
}

void DeployedLoggersHistory::fillFilterComboBoxes()
{
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QDir FileDir(FolderPath);

    ui->cmbxLocation->clear();
    ui->cmbxType->clear();

    QStringList locations;
    locations.append("");
    QStringList types;
    types.append("");

    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);

        preferences.beginGroup("GeneralInformation");
        bool DeployFinished=preferences.value("Finished",false).toBool();
        preferences.endGroup();

        preferences.beginGroup("LocationInformation");
        QString type=preferences.value("Type").toString();
        QString location=preferences.value("Location").toString();
        preferences.endGroup();

        if (DeployFinished)
        {
            types.append(type);
            locations.append(location);
        }
    }

    ui->cmbxLocation->addItems(locations);
    ui->cmbxType->addItems(types);
}

void DeployedLoggersHistory::on_trwLoggersHistory_itemClicked(QTreeWidgetItem *item, int column)
{
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QString ImageFolderPath=FolderPath+"images";
    QFile   inputFile(item->text(5));

    QSettings preferences(item->text(5),QSettings::IniFormat);
    preferences.beginGroup("OtherInformation");
    QStringList ImageList=preferences.value("ImagePaths").toStringList();
    preferences.endGroup();

    ui->lswLoggersHistory->clear();

    for (int k=0;k<ImageList.count();k++){

        QFile imageFile(ImageFolderPath+"//"+ImageList.at(k).split("//").last());
        if (!imageFile.exists()) continue;
        QImage image;
        image.load(imageFile.fileName());
        QListWidgetItem *imageItem=new QListWidgetItem();
        imageItem->setIcon(QIcon(QPixmap::fromImage(image)));
        imageItem->setToolTip(imageFile.fileName());
        ui->lswLoggersHistory->setIconSize(QSize(150,150));
        ui->lswLoggersHistory->addItem(imageItem);

    }

    ui->txbLoggersHistory->clear();
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
           QString line = in.readLine();
           ui->txbLoggersHistory->append(line);
        }
        inputFile.close();
    }

}

void DeployedLoggersHistory::on_trwLoggersHistory_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidgetItem* item = ui->trwLoggersHistory->itemAt(pos);

    if (item) {
        QMenu myMenu;
        myMenu.addAction("Delete entry");

        QAction* selectedItem = myMenu.exec(QCursor::pos());
        if (selectedItem==NULL) return;
        if (selectedItem->text().startsWith("Delete"))
        {
            int ret = QMessageBox::warning(this, tr("Warning"), tr("You are about to delete this record.\nAre you sure?"),QMessageBox::Yes | QMessageBox::Cancel);
            if (ret!= QMessageBox::Yes) return;
            // Pitat are you sure
            QFile   inputFile(item->text(5));
            if (inputFile.exists()) inputFile.remove();
            refreshLoggersHistoryList();
        }
        else
        {
            // ffu
        }
    }
}



void DeployedLoggersHistory::on_rbxFilter_toggled(bool checked)
{

}
