#include "configwizard.h"
#include "ui_configwizard.h"

configWizard::configWizard(MainWindow *parent) :
    QDialog(parent),
    ui(new Ui::configWizard)
{

    ui->setupUi(this);
    QSettings preferences("Settings.ini",QSettings::IniFormat);
    mMinimumPressure=preferences.value("MinimumPressure",0).toDouble();

    mParent=parent;
    connect(mParent->usbDetector,SIGNAL(Responded(QString,QString)), this, SLOT(MessageRecieved(QString,QString)));
    mParent->writeToLog("User started configuration wizard");

    Stage1Finished=false;
    Stage2Finished=false;
    Stage3Finished=false;
    Stage4Finished=false;
    cameraActive=false;

    QStringList TipoviMjerenja;
    QStringList GeografskaSirina;
    QStringList GeografskaDuzina;
    QStringList Visina;
    QStringList Lokacije;

    MeasureTimer=new QTimer();
    MeasureTimer->setInterval(1000);
    connect(MeasureTimer,SIGNAL(timeout()), this, SLOT(MeasureTimerTimeout()));

    preferences.beginGroup("DefaultOptions");
    TipoviMjerenja=preferences.value("TipoviMjerenja").toStringList();
    GeografskaSirina=preferences.value("GeografskeSirine").toStringList();
    GeografskaDuzina=preferences.value("GeografskeDuzine").toStringList();
    Visina=preferences.value("Visine").toStringList();
    Lokacije=preferences.value("Lokacije").toStringList();
    preferences.endGroup();

    TipoviMjerenja.insert(0,"");
    GeografskaDuzina.insert(0,"");
    GeografskaSirina.insert(0,"");
    Visina.insert(0,"");
    Lokacije.insert(0,"");
    ui->MeasurementType->addItems(TipoviMjerenja);
    ui->Sirina->addItems(GeografskaSirina);
    ui->Duzina->addItems(GeografskaDuzina);
    ui->Height->addItems(Visina);
    ui->Location->addItems(Lokacije);

    mCompleteCounter=0;
    ui->btnNext->setEnabled(false);

}

configWizard::~configWizard()
{
    delete ui;
}

void configWizard::on_MeasurementType_currentTextChanged(const QString &arg1)
{
    if (ui->MeasurementType->currentText().simplified().replace(" ","").size()>0){
        ui->lblMeasurementType->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        mCompleteCounter++;
        if(mCompleteCounter>=5){
            ui->btnNext->setEnabled(true);
            Stage1Finished=true;
        }
    }else{
        ui->lblMeasurementType->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
        mCompleteCounter--;
        if(mCompleteCounter<5){
            ui->btnNext->setEnabled(false);
            Stage1Finished=false;
        }
    }
}

void configWizard::on_Duzina_currentTextChanged(const QString &arg1)
{

    if (ui->Duzina->currentText().simplified().replace(" ","").size()>0){
        ui->lblDuzina->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        mCompleteCounter++;
        if(mCompleteCounter>=5){
            ui->btnNext->setEnabled(true);
            Stage1Finished=true;
        }
    }else{
        ui->lblDuzina->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
        mCompleteCounter--;
        if(mCompleteCounter<5){
            ui->btnNext->setEnabled(false);
            Stage1Finished=false;
        }
    }
}

void configWizard::on_Sirina_currentTextChanged(const QString &arg1)
{
    if (ui->Sirina->currentText().simplified().replace(" ","").size()>0){
        ui->lblSirina->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        mCompleteCounter++;
        if(mCompleteCounter>=5){
            ui->btnNext->setEnabled(true);
            Stage1Finished=true;
        }
    }else{
        ui->lblSirina->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
        mCompleteCounter--;
        if(mCompleteCounter<5){
            ui->btnNext->setEnabled(false);
            Stage1Finished=false;
        }
    }
}

void configWizard::on_Location_currentTextChanged(const QString &arg1)
{

    if (ui->Location->currentText().simplified().replace(" ","").size()>0){
        ui->lblLocation->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        mCompleteCounter++;
        if(mCompleteCounter>=5){
            ui->btnNext->setEnabled(true);
            Stage1Finished=true;
        }
    }else{
        ui->lblLocation->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
        mCompleteCounter--;
        if(mCompleteCounter<5){
            ui->btnNext->setEnabled(false);
            Stage1Finished=false;
        }
    }
}

void configWizard::on_Height_currentTextChanged(const QString &arg1)
{

    if (ui->Height->currentText().simplified().replace(" ","").size()>0){
        ui->lblHeight->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        mCompleteCounter++;
        if(mCompleteCounter>=5){
            ui->btnNext->setEnabled(true);
            Stage1Finished=true;
        }
    }else{
        ui->lblHeight->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
        mCompleteCounter--;
        if(mCompleteCounter<5){
            ui->btnNext->setEnabled(false);
            Stage1Finished=false;
        }
    }
}
void configWizard::on_tabs_currentChanged(int index)
{
    if (index>0) ui->btnPrevious->setEnabled(true);
    else ui->btnPrevious->setEnabled(false);

    if (index==3){
        ui->btnNext->setText("Započni");
        if (!Stage4Finished) ui->btnNext->setDisabled(true);
        else ui->btnNext->setEnabled(true);
    }
    else
    {
        ui->btnNext->setText("Dalje");
        ui->btnNext->setEnabled(true);
    }
    MeasureTimer->stop();
    mParent->SendDataToCurrentLogger("SET OPA_POW OFF#");
    MainWindow::delay(100);
    mParent->SendDataToCurrentLogger("SET SENS_POW OFF#");
    MainWindow::delay(100);
    switch (index){
    case (0):
        mParent->writeToLog("User started 'Location configuration'");
        break;
    case (1):

        if (!Stage1Finished){
            QMessageBox::warning(this,"Pogreška","Molimo unesite sve parametre lokacije",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(0);
        }
        mParent->writeToLog("User started 'Location photographing'");
        break;

    case (2):

        if (!Stage1Finished){
            QMessageBox::warning(this,"Pogreška","Molimo unesite sve parametre lokacije",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(0);
            return;
        }
        if (!Stage2Finished){
            QMessageBox::warning(this,"Pogreška","Potrebno je obaviti snimanje lokacije mjerenja",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(1);
            return;
        }
        mParent->writeToLog("User started 'Logger configuration'");
        break;
    case (3):
        if (!Stage1Finished){
            QMessageBox::warning(this,"Pogreška","Molimo unesite sve parametre lokacije",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(0);
            return;
        }
        if (!Stage2Finished){
            QMessageBox::warning(this,"Pogreška","Potrebno je obaviti snimanje lokacije mjerenja",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(1);
            return;
        }
        if (!Stage3Finished){
            QMessageBox::warning(this,"Pogreška","Potrebno je obaviti snimanje lokacije mjerenja",QMessageBox::Ok);
            ui->tabs->setCurrentIndex(2);
            return;
        }

        mParent->writeToLog("User started 'Pressure testing'");
        MainWindow::delay(100);
        mParent->SendDataToCurrentLogger("SET OPA_POW ON#");
        MainWindow::delay(100);
        mParent->SendDataToCurrentLogger("SET SENS_POW ON#");
        MainWindow::delay(100);
        MeasureTimer->start();
        break;
    }
}

void configWizard::on_btnNext_clicked()
{

    MeasureTimer->stop();
    if (ui->btnNext->text()=="Započni"){
        StartConfigurationProcedure();
    }
    if((ui->tabs->currentIndex()+1)>ui->tabs->count()) return;
    ui->tabs->setCurrentIndex(ui->tabs->currentIndex()+1);

}

void configWizard::on_btnPrevious_clicked()
{

    if((ui->tabs->currentIndex()-1)<0) return;
    ui->tabs->setCurrentIndex(ui->tabs->currentIndex()-1);
}

void configWizard::on_rbxIntervalMjerenje_toggled(bool checked)
{

    Stage3Finished=true;
    if (checked){
        ui->lblIntervalDescr->setEnabled(true);
        ui->lblIntervalTime->setEnabled(true);
    }else{
        ui->lblIntervalDescr->setEnabled(false);
        ui->lblIntervalTime->setEnabled(false);
    }

    if(ui->rbxBrzoMjerenje->isChecked() || ui->rbxIntervalMjerenje->isChecked()){
        ui->lblMjerenje->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
    }else{
        ui->lblMjerenje->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
    }
}

void configWizard::on_rbxBrzoMjerenje_toggled(bool checked)
{


    Stage3Finished=true;
    if (checked){
        ui->LblKontinuiranoDescr->setEnabled(true);
        ui->lblKontinuiranoTime->setEnabled(true);
    }else{
        ui->LblKontinuiranoDescr->setEnabled(false);
        ui->lblKontinuiranoTime->setEnabled(false);
    }

    if(ui->rbxBrzoMjerenje->isChecked() || ui->rbxIntervalMjerenje->isChecked()){
        ui->lblMjerenje->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
    }else{
        ui->lblMjerenje->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
    }
}

void configWizard::on_btnRefreshCameras_clicked()
{

    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    ui->cbxCameraOption->clear();
    if (cameras.count()<=0) {
        ui->cbxCameraOption->addItem("Nije pronađena kamera");
        return;
    }
    foreach (const QCameraInfo &cameraInfo, cameras) {
        ui->cbxCameraOption->addItem(cameraInfo.description());
    }
}

void configWizard::on_cbxCameraOption_currentIndexChanged(const QString &arg1)
{
    QList<QCameraInfo> cameras = QCameraInfo::availableCameras();
    if(arg1.contains("Nije pronađena kamera")) return;

    bool CameraFound=false;
    foreach (const QCameraInfo &cameraInfo, cameras) {
        if(cameraInfo.description()==arg1){
            camera = new QCamera(cameraInfo);
            CameraFound=true;
        }
    }

    if (!CameraFound) return;

    viewfinder = new QCameraViewfinder;
    camera->setViewfinder(viewfinder);
    viewfinder->setGeometry(200,200,200,200);
    ui->videoLayout->addWidget(viewfinder);
    viewfinder->show();
    camera->start(); // to start the viewfinder
    cameraActive=true;
}

void configWizard::on_configWizard_finished(int result)
{
    if (camera==NULL) return;
    MeasureTimer->stop();
    mParent->SendDataToCurrentLogger("SET OPA_POW OFF#");
    mParent->SendDataToCurrentLogger("SET SENS_POW OFF#");
    camera->stop(); // to start the viewfinder
}

void configWizard::on_btnTakePicture_clicked()
{
    if (camera==NULL) return;
    if (!cameraActive) return;
    imageCapture = new QCameraImageCapture(camera);

    camera->setCaptureMode(QCamera::CaptureStillImage);
    camera->start(); // Viewfinder frames start flowing

    //on half pressed shutter button
    camera->searchAndLock();

    QByteArray temp=qgetenv("TEMP");
    QString s(temp);
    QString TempDir=s;
    QDateTime time=QDateTime::currentDateTime();
    QString TempFile=TempDir.append(time.toString("\\HH_mm_ss_zzz dd_MM_yyyy"));
    imageCapture->capture(TempFile);
    CameraPicturesPaths.append(TempFile);
    camera->unlock();

    QFile imageFile(TempFile+".jpg");

    int counter=0;
    while (!imageFile.exists() && counter<50){
         MainWindow::delay(100);
         counter++;
    }
    MainWindow::delay(500);
    showLastThreeImages();
}


void configWizard::showLastThreeImages(){
    if (CameraPicturesPaths.count()==0) return;
    QImage image1,image2,image3;

    if (CameraPicturesPaths.count()==1){
        image1.load(CameraPicturesPaths.at(0)+".jpg");

        qDebug() << CameraPicturesPaths.at(0);
        ui->lblSlika_1->setPixmap(QPixmap::fromImage(image1));
        ui->lblSlika1->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
    }else if (CameraPicturesPaths.count()==2){
        image1.load(CameraPicturesPaths.at(1)+".jpg");
        image2.load(CameraPicturesPaths.at(0)+".jpg");
        ui->lblSlika_1->setPixmap(QPixmap::fromImage(image1));
        ui->lblSlika_2->setPixmap(QPixmap::fromImage(image2));
        ui->lblSlika1->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        ui->lblSlika2->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        qDebug() << CameraPicturesPaths.at(0);
        qDebug() << CameraPicturesPaths.at(1);
    }else if (CameraPicturesPaths.count()==3){
        image1.load(CameraPicturesPaths.at(2)+".jpg");
        image2.load(CameraPicturesPaths.at(1)+".jpg");
        image3.load(CameraPicturesPaths.at(0)+".jpg");
        ui->lblSlika_1->setPixmap(QPixmap::fromImage(image1));
        ui->lblSlika_2->setPixmap(QPixmap::fromImage(image2));
        ui->lblSlika_3->setPixmap(QPixmap::fromImage(image3));
        ui->lblSlika1->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        ui->lblSlika2->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        ui->lblSlika3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        qDebug() << CameraPicturesPaths.at(0);
        qDebug() << CameraPicturesPaths.at(1);
        qDebug() << CameraPicturesPaths.at(2);
        Stage2Finished=true;
    }else{
        image1.load(CameraPicturesPaths.at(CameraPicturesPaths.count()-1)+".jpg");
        image2.load(CameraPicturesPaths.at(CameraPicturesPaths.count()-2)+".jpg");
        image3.load(CameraPicturesPaths.at(CameraPicturesPaths.count()-3)+".jpg");
        ui->lblSlika_1->setPixmap(QPixmap::fromImage(image1));
        ui->lblSlika_2->setPixmap(QPixmap::fromImage(image2));
        ui->lblSlika_3->setPixmap(QPixmap::fromImage(image3));
        ui->lblSlika1->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        ui->lblSlika2->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        ui->lblSlika3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
        qDebug() << CameraPicturesPaths.at(0);
        qDebug() << CameraPicturesPaths.at(1);
        qDebug() << CameraPicturesPaths.at(2);
    }
}

void configWizard::MessageRecieved(QString Serial,QString Message){
    static int count=0;
    if (Message.startsWith("SENSOR VALUE:")){
        QStringList splitted= Message.split(" ");
        if (splitted.count()<3) return;
        bool ok;
        double result= splitted[2].remove("\r").remove("\n").remove(",").toDouble(&ok);
        if (result>=mMinimumPressure){
            count++;

            if (count>5){
                ui->lblTlak->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/checked_checkbox/checked_checkbox-256.png")));
                Stage4Finished=true;
                ui->btnNext->setEnabled(true);
            }
        }else{
            count=0;
            ui->lblTlak->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/unchecked_checkbox/unchecked_checkbox-256.png")));
            Stage4Finished=false;
            ui->btnNext->setEnabled(false);
        }
        ui->lcdTlak->display(result);
    }
}

void configWizard::MeasureTimerTimeout(){
    mParent->SendDataToCurrentLogger("GET SENSOR#");

}

void configWizard::StartConfigurationProcedure()
{
    mParent->writeToLog("User started 'Final configuration'");
    // Otvori folder na uredaju
    mParent->usbDetector->stopReporting();
    QString infoString="";
    QString smallInfoString="Configured,";
    infoString.append("Vrijeme:             "+QDateTime::currentDateTime().toString("HH:mm:ss")+"\r\n");
    infoString.append("Datum:               "+QDateTime::currentDateTime().toString("dd/MM/yyyy")+"\r\n");
    infoString.append("Korisnik:            "+mParent->user()+"\r\n");
    smallInfoString.append("User:"+mParent->user()+",");
    Dialogwait *progressDialog;
    progressDialog = new Dialogwait(this);
    progressDialog->show();

    progressDialog->startWait();

    // Pronalazak direktorija
    QString sPath=mParent->findCurrentDiskPath();

    if (sPath==""){
        QMessageBox::critical(this,"Greška", "Nije moguće pronaći disk od uređaja\r\n Provjerite serijski broj diska!",QMessageBox::Ok);
        progressDialog->stopWait();
        return;
    }

    progressDialog->setText("Traženje diska");
    QString DirectoryName=sPath;
    QDateTime currentDT= QDateTime::currentDateTime();
    QString HexValue;
    QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );

    progressDialog->setText("Konfiguriranje uređaja");
    if (ui->rbxBrzoMjerenje->isChecked()){
        infoString.append("Tip:                 Brzo mjerenje\r\n");
        smallInfoString.append("FastMeasure,");
        DirectoryName.append("//B_"+currentDT.toString("HH_mm_ss_dd_MM"));
        // Podesi Brzo mjerenje i interval
        unsigned short Number;
        Number=(unsigned short)(ui->lblKontinuiranoTime->value());
        infoString.append("Vrijeme uzorkovanja: "+QString::number(ui->lblKontinuiranoTime->value())+" milisekundi\r\n");
        smallInfoString.append(QString::number(ui->lblKontinuiranoTime->value())+",");
        HexValue=QString::number((unsigned short)Number,16);
        mParent->SendDataToCurrentLogger("CONFIG CSI SET "+ HexValue+"#");
        MainWindow::delay(100);
        mParent->SendDataToCurrentLogger("CONFIG OPS SET 1#");
        MainWindow::delay(100);
    }else{
        infoString.append("Tip:                 Sporo mjerenje\r\n");
        smallInfoString.append("SlowMeasure,");
        DirectoryName.append("//S_"+currentDT.toString("HH_mm_ss_dd_MM"));
        // Podesi Sporo mjerenje i interval
        unsigned short Number;
        Number=(unsigned short)(ui->lblIntervalTime->value());
        infoString.append("Vrijeme uzorkovanja: "+QString::number(ui->lblIntervalTime->value())+" sekundi\r\n");
        smallInfoString.append(QString::number(ui->lblIntervalTime->value())+",");
        QString HexValue;
        HexValue=QString::number((unsigned short)(Number-2),16);
        mParent->SendDataToCurrentLogger("CONFIG ISI SET "+ HexValue+"#");
        MainWindow::delay(100);
        mParent->SendDataToCurrentLogger("CONFIG OPS SET 2#");
        MainWindow::delay(100);
    }
    if (ui->cbxLock->isChecked()){
        mParent->SendDataToCurrentLogger("CONFIG LOM SET 1#");
        infoString.append("Zaključavanje:       Zaključano\r\n");
        smallInfoString.append("Lock,");
        MainWindow::delay(200);
    }else{
        mParent->SendDataToCurrentLogger("CONFIG LOM SET 0#");
        infoString.append("Zaključavanje:       Otključano\r\n");
        smallInfoString.append("Unlock,");
        MainWindow::delay(200);
    }

    QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
    if (ui->rbxBrzoMjerenje->isChecked()){
        QString FolderName="B_"+currentDT.toString("HH_mm_ss_dd_MM");
        smallInfoString.append("Folder:"+FolderName+",");
        mParent->SendDataToCurrentLogger("CONFIG LSNC SET "+FolderName+"#");
        MainWindow::delay(200);
    }else{
        QString FolderName="S_"+currentDT.toString("HH_mm_ss_dd_MM");
        mParent->SendDataToCurrentLogger("CONFIG LSNI SET "+FolderName+"#");
        smallInfoString.append("Folder:"+FolderName+",");
        MainWindow::delay(200);
    }

    progressDialog->setText("Stvaranje datoteke");
    QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
    MainWindow::delay(200);
    // Podesavanje folder namea
    QDir directory(DirectoryName);
    if (!directory.exists()) directory.mkpath(DirectoryName);

    // Kopiranje slika
    QDir figDirectory(DirectoryName+"\\Slike\\");
    if (!figDirectory.exists()) figDirectory.mkpath(DirectoryName+"\\Slike\\");

    if (CameraPicturesPaths.count()<3){
        QMessageBox::critical(this,"Greška", "Nađeno je nedovoljno slika\r\n Molimo ponovite postupak!",QMessageBox::Ok);
        progressDialog->stopWait();
        return;
    }

    QString source="", destination="";
    source=CameraPicturesPaths.at(0)+".jpg";
    destination=DirectoryName+"//Slike//Slika1.jpg";
    progressDialog->setText("Kopiranje slike 1");
    copyImage(source,destination,progressDialog);
    source=CameraPicturesPaths.at(1)+".jpg";
    destination=DirectoryName+"//Slike//Slika2.jpg";
    progressDialog->setText("Kopiranje slike 2");
    copyImage(source,destination,progressDialog);
    source=CameraPicturesPaths.at(2)+".jpg";
    destination=DirectoryName+"//Slike//Slika3.jpg";
    progressDialog->setText("Kopiranje slike 3");
    copyImage(source,destination,progressDialog);

    // Upisivanje informacija o lokaciji
    infoString.append("\r\nInformacije o lokaciji:\r\n");
    infoString.append("Mjerenje tlaka na:   "+ui->MeasurementType->currentText()+"\r\n");
    smallInfoString.append("Type:"+ui->MeasurementType->currentText()+",");
    infoString.append("Geografska širina:   "+ui->Sirina->currentText()+"\r\n");
    smallInfoString.append("Lat:"+ui->Sirina->currentText()+",");
    infoString.append("Geografska dužina:   "+ui->Duzina->currentText()+"\r\n");
    smallInfoString.append("Long:"+ui->Duzina->currentText()+",");
    infoString.append("Visina:              "+ui->Duzina->currentText()+"\r\n");
    smallInfoString.append("Height:"+ui->Duzina->currentText()+",");
    infoString.append("Lokacija:            "+ui->Location->currentText()+"\r\n");
    smallInfoString.append("Loc:"+ui->Location->currentText()+",");
    infoString.append("Komentar:            "+ui->Comment->toPlainText()+"\r\n");
    smallInfoString.append("Com:"+ui->Comment->toPlainText()+",");


    // Upisivanje informacijske datoteke
    progressDialog->setText("Stvaranje informacijske datoteke");
    QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );

    QDir infDirectory(DirectoryName+"\\Informacije\\");
    if (!infDirectory.exists()) infDirectory.mkpath(DirectoryName+"\\Informacije\\");
    QFile infFile(DirectoryName+"\\Informacije\\info.inf");
    if (!infFile.open(QIODevice::Append| QIODevice::Text)){

        QMessageBox::critical(this,"Greška", "Nije moguće zapisati informacijsku datoteku\r\n Molimo ponovite postupak!",QMessageBox::Ok);
        progressDialog->stopWait();
        return;
    }

    QTextStream out(&infFile);
    out << infoString;
    infFile.close();
    mParent->writeToLog(smallInfoString); // Posalji na server ili upisi u log
    progressDialog->stopWait();
    progressDialog->hide();

    QMessageBox::information(this,"Informacija","Molimo iskopčajte uređaj iz računala\r\nUređaj će započeti provođenje mjerenja");
    mParent->usbDetector->startReporting();
    this->close();
}


void configWizard::copyImage(QString source, QString destination, Dialogwait *progressDialog)
{
        qDebug()<<source;
        qDebug()<<destination;
        QFile fromFile(source);
        QFile toFile(destination);
        qint64 nCopySize = fromFile.size();
        qDebug()<<"Size: "<<nCopySize;
        if (nCopySize>1000){
            if (toFile.exists()) toFile.remove();

            fromFile.open(QIODevice::ReadOnly);
            toFile.open(QIODevice::WriteOnly|QIODevice::Truncate);
            progressDialog->setMin(0);
            progressDialog->setMax(nCopySize);
            for (qint64 i = 0; i < nCopySize; i+=512) {
                toFile.write(fromFile.read(i)); // write a byte
                QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
                fromFile.seek(i);  // move to next byte to read
                toFile.seek(i); // move to next byte to write
                progressDialog->setValue((int)(i));
                QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
            }
            fromFile.close();
            toFile.close();

            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        }else{
             QFile::copy(source,destination);
        }




}

void configWizard::on_cancelButton_clicked()
{
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Upit", "Jeste li sigurni?",QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
        MeasureTimer->stop();

        mParent->SendDataToCurrentLogger("SET OPA_POW OFF#");
        mParent->SendDataToCurrentLogger("SET SENS_POW OFF#");
        if (camera!=NULL)camera->stop();
        this->close();
    } else {
    }
}

