/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QFrame>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMdiArea>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actShowConzole;
    QAction *actShowSidebar;
    QAction *actExit;
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout_2;
    QSplitter *splitter;
    QWidget *layoutWidget;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QSpacerItem *horizontalSpacer_3;
    QLabel *lblImage;
    QSpacerItem *horizontalSpacer_2;
    QFrame *frame;
    QVBoxLayout *verticalLayout;
    QLabel *lblConnectedDevices;
    QListWidget *loggerList;
<<<<<<< HEAD
    QLabel *lblCommonOptions;
    QHBoxLayout *horizontalLayout_7;
    QPushButton *btnStartReports;
    QPushButton *btnSincClocks;
    QPushButton *btnStopReports;
    QLabel *lblTools;
    QFrame *toolFrame;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_5;
    QLabel *label_4;
    QPushButton *btnNewMeasurementWizardOnd;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QPushButton *btnDeployedLoggers;
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer;
    QPushButton *btnSincClocks;
    QFrame *frame_2;
    QVBoxLayout *verticalLayout_5;
    QFrame *frame_4;
    QVBoxLayout *verticalLayout_6;
    QMdiArea *mainLayout;
    QTextEdit *messageWindow;
    QToolBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuOptions;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QStringLiteral("MainWindow"));
        MainWindow->resize(1087, 743);
        QSizePolicy sizePolicy(QSizePolicy::Maximum, QSizePolicy::Maximum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(MainWindow->sizePolicy().hasHeightForWidth());
        MainWindow->setSizePolicy(sizePolicy);
        QIcon icon;
        icon.addFile(QStringLiteral("settings-512.png"), QSize(), QIcon::Normal, QIcon::Off);
        MainWindow->setWindowIcon(icon);
        MainWindow->setStyleSheet(QStringLiteral(""));
        actShowConzole = new QAction(MainWindow);
        actShowConzole->setObjectName(QStringLiteral("actShowConzole"));
        actShowConzole->setCheckable(true);
        actShowConzole->setChecked(false);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Debug/console/console-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        actShowConzole->setIcon(icon1);
        actShowSidebar = new QAction(MainWindow);
        actShowSidebar->setObjectName(QStringLiteral("actShowSidebar"));
        actShowSidebar->setCheckable(true);
        actShowSidebar->setChecked(true);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Timeline_List_Grid/list/list-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        actShowSidebar->setIcon(icon2);
        actExit = new QAction(MainWindow);
        actExit->setObjectName(QStringLiteral("actExit"));
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Registration/exit/exit-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        actExit->setIcon(icon3);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout_2 = new QVBoxLayout(centralWidget);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        splitter = new QSplitter(centralWidget);
        splitter->setObjectName(QStringLiteral("splitter"));
        splitter->setOrientation(Qt::Horizontal);
        layoutWidget = new QWidget(splitter);
        layoutWidget->setObjectName(QStringLiteral("layoutWidget"));
        verticalLayout_3 = new QVBoxLayout(layoutWidget);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetDefaultConstraint);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_3);

        lblImage = new QLabel(layoutWidget);
        lblImage->setObjectName(QStringLiteral("lblImage"));

        horizontalLayout_3->addWidget(lblImage);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_3);

        frame = new QFrame(layoutWidget);
        frame->setObjectName(QStringLiteral("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setMinimumSize(QSize(0, 300));
        frame->setBaseSize(QSize(100, 0));
        frame->setStyleSheet(QLatin1String("#frame{\n"
"border: 3px solid gray;\n"
"border-radius:5px;\n"
"}"));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame->setLineWidth(1);
        frame->setMidLineWidth(0);
        verticalLayout = new QVBoxLayout(frame);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        verticalLayout->setContentsMargins(9, -1, -1, -1);
        lblConnectedDevices = new QLabel(frame);
        lblConnectedDevices->setObjectName(QStringLiteral("lblConnectedDevices"));
        QFont font;
        font.setFamily(QStringLiteral("Open Sans"));
        font.setPointSize(10);
        font.setBold(false);
        font.setWeight(50);
        font.setKerning(true);
        lblConnectedDevices->setFont(font);

        verticalLayout->addWidget(lblConnectedDevices);

        loggerList = new QListWidget(frame);
        loggerList->setObjectName(QStringLiteral("loggerList"));
        QSizePolicy sizePolicy2(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(loggerList->sizePolicy().hasHeightForWidth());
        loggerList->setSizePolicy(sizePolicy2);
        loggerList->setMinimumSize(QSize(460, 0));
        QFont font1;
        font1.setFamily(QStringLiteral("Open Sans"));
        font1.setPointSize(18);
        font1.setBold(true);
        font1.setWeight(75);
        loggerList->setFont(font1);
        loggerList->setContextMenuPolicy(Qt::CustomContextMenu);
        loggerList->setAcceptDrops(false);
        loggerList->setFrameShape(QFrame::StyledPanel);
        loggerList->setFrameShadow(QFrame::Plain);
        loggerList->setLineWidth(1);
        loggerList->setSizeAdjustPolicy(QAbstractScrollArea::AdjustToContentsOnFirstShow);
        loggerList->setProperty("showDropIndicator", QVariant(true));
        loggerList->setDragDropOverwriteMode(true);
        loggerList->setDragDropMode(QAbstractItemView::DragOnly);
        loggerList->setAlternatingRowColors(true);
        loggerList->setMovement(QListView::Snap);
        loggerList->setViewMode(QListView::ListMode);
        loggerList->setSelectionRectVisible(true);

<<<<<<< HEAD
        horizontalLayout_8->addWidget(loggerList);


        verticalLayout->addWidget(frame_5);

        lblCommonOptions = new QLabel(frame);
        lblCommonOptions->setObjectName(QStringLiteral("lblCommonOptions"));

        verticalLayout->addWidget(lblCommonOptions);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setObjectName(QStringLiteral("horizontalLayout_7"));
        horizontalLayout_7->setContentsMargins(20, -1, -1, -1);
        btnStartReports = new QPushButton(frame);
        btnStartReports->setObjectName(QStringLiteral("btnStartReports"));
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStartReports->setIcon(icon4);
        btnStartReports->setIconSize(QSize(32, 32));

        horizontalLayout_7->addWidget(btnStartReports);

        btnSincClocks = new QPushButton(frame);
        btnSincClocks->setObjectName(QStringLiteral("btnSincClocks"));
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/sinchronize/sinchronize-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnSincClocks->setIcon(icon5);
        btnSincClocks->setIconSize(QSize(32, 32));

        horizontalLayout_7->addWidget(btnSincClocks);

        btnStopReports = new QPushButton(frame);
        btnStopReports->setObjectName(QStringLiteral("btnStopReports"));
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        btnStopReports->setIcon(icon6);
        btnStopReports->setIconSize(QSize(32, 32));

        horizontalLayout_7->addWidget(btnStopReports);


        verticalLayout->addLayout(horizontalLayout_7);

        lblTools = new QLabel(frame);
        lblTools->setObjectName(QStringLiteral("lblTools"));

        verticalLayout->addWidget(lblTools);

        toolFrame = new QFrame(frame);
        toolFrame->setObjectName(QStringLiteral("toolFrame"));
        toolFrame->setFrameShape(QFrame::StyledPanel);
        toolFrame->setFrameShadow(QFrame::Raised);
        verticalLayout_2 = new QVBoxLayout(toolFrame);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setSpacing(6);
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        horizontalLayout_5->setContentsMargins(20, -1, -1, -1);
        label_4 = new QLabel(toolFrame);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setMinimumSize(QSize(32, 32));
        label_4->setMaximumSize(QSize(32, 32));
        label_4->setAutoFillBackground(false);
        label_4->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Halloween/wizard/wizard-256.png")));
        label_4->setScaledContents(true);

        horizontalLayout_5->addWidget(label_4);

        btnNewMeasurementWizardOnd = new QPushButton(toolFrame);
        btnNewMeasurementWizardOnd->setObjectName(QStringLiteral("btnNewMeasurementWizardOnd"));
        btnNewMeasurementWizardOnd->setMaximumSize(QSize(10000, 1000));
        QFont font;
        font.setPointSize(12);
        btnNewMeasurementWizardOnd->setFont(font);
        btnNewMeasurementWizardOnd->setLayoutDirection(Qt::LeftToRight);
        btnNewMeasurementWizardOnd->setIconSize(QSize(32, 32));
        btnNewMeasurementWizardOnd->setCheckable(false);

        horizontalLayout_5->addWidget(btnNewMeasurementWizardOnd);


        verticalLayout_2->addLayout(horizontalLayout_5);

        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        horizontalLayout_3->setContentsMargins(20, -1, -1, -1);
        label = new QLabel(toolFrame);
        label->setObjectName(QStringLiteral("label"));
        label->setMinimumSize(QSize(32, 32));
        label->setMaximumSize(QSize(32, 32));
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Industry/deployment/deployment-256.png")));
        label->setScaledContents(true);

        horizontalLayout_3->addWidget(label);

        btnDeployedLoggers = new QPushButton(toolFrame);
        btnDeployedLoggers->setObjectName(QStringLiteral("btnDeployedLoggers"));
        btnDeployedLoggers->setMaximumSize(QSize(10000, 1000));
        btnDeployedLoggers->setFont(font);
        btnDeployedLoggers->setLayoutDirection(Qt::LeftToRight);
        btnDeployedLoggers->setIconSize(QSize(32, 32));
        btnDeployedLoggers->setCheckable(false);

        horizontalLayout_3->addWidget(btnDeployedLoggers);


        verticalLayout_2->addLayout(horizontalLayout_3);
=======
        verticalLayout->addWidget(loggerList);
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(6);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
<<<<<<< HEAD
        horizontalLayout->setContentsMargins(20, -1, -1, -1);
        label_2 = new QLabel(toolFrame);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setMinimumSize(QSize(32, 32));
        label_2->setMaximumSize(QSize(32, 32));
        label_2->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Movie_Genres/historical/historical-256.png")));
        label_2->setScaledContents(true);
=======
        horizontalLayout->setContentsMargins(0, -1, -1, -1);
        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

        horizontalLayout->addItem(horizontalSpacer);

<<<<<<< HEAD
        btnDeployedLoggersHistory = new QPushButton(toolFrame);
        btnDeployedLoggersHistory->setObjectName(QStringLiteral("btnDeployedLoggersHistory"));
        btnDeployedLoggersHistory->setMaximumSize(QSize(10000, 1000));
        btnDeployedLoggersHistory->setFont(font);
        btnDeployedLoggersHistory->setLayoutDirection(Qt::LeftToRight);
        btnDeployedLoggersHistory->setIconSize(QSize(32, 32));
        btnDeployedLoggersHistory->setCheckable(false);
=======
        btnSincClocks = new QPushButton(frame);
        btnSincClocks->setObjectName(QStringLiteral("btnSincClocks"));
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

        horizontalLayout->addWidget(btnSincClocks);


        verticalLayout_2->addLayout(horizontalLayout);

<<<<<<< HEAD
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        horizontalLayout_4->setContentsMargins(20, -1, -1, -1);
        label_3 = new QLabel(toolFrame);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMinimumSize(QSize(32, 32));
        label_3->setMaximumSize(QSize(32, 32));
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Halloween/wizard/wizard-256.png")));
        label_3->setScaledContents(true);

        horizontalLayout_4->addWidget(label_3);

        btnNewMeasurementWizard = new QPushButton(toolFrame);
        btnNewMeasurementWizard->setObjectName(QStringLiteral("btnNewMeasurementWizard"));
        btnNewMeasurementWizard->setMaximumSize(QSize(10000, 1000));
        btnNewMeasurementWizard->setFont(font);
        btnNewMeasurementWizard->setLayoutDirection(Qt::LeftToRight);
        btnNewMeasurementWizard->setIconSize(QSize(32, 32));
        btnNewMeasurementWizard->setCheckable(false);

        horizontalLayout_4->addWidget(btnNewMeasurementWizard);


        verticalLayout_2->addLayout(horizontalLayout_4);


        verticalLayout->addWidget(toolFrame);
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

        verticalLayout_3->addWidget(frame);

        splitter->addWidget(layoutWidget);
        frame_2 = new QFrame(splitter);
        frame_2->setObjectName(QStringLiteral("frame_2"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(frame_2->sizePolicy().hasHeightForWidth());
        frame_2->setSizePolicy(sizePolicy3);
        frame_2->setMinimumSize(QSize(500, 0));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        verticalLayout_5 = new QVBoxLayout(frame_2);
        verticalLayout_5->setSpacing(0);
        verticalLayout_5->setContentsMargins(11, 11, 11, 11);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        verticalLayout_5->setContentsMargins(0, 0, 0, 0);
        frame_4 = new QFrame(frame_2);
        frame_4->setObjectName(QStringLiteral("frame_4"));
        sizePolicy3.setHeightForWidth(frame_4->sizePolicy().hasHeightForWidth());
        frame_4->setSizePolicy(sizePolicy3);
        frame_4->setStyleSheet(QStringLiteral(""));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        verticalLayout_6 = new QVBoxLayout(frame_4);
        verticalLayout_6->setSpacing(0);
        verticalLayout_6->setContentsMargins(11, 11, 11, 11);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        verticalLayout_6->setContentsMargins(0, 0, 0, 0);
        mainLayout = new QMdiArea(frame_4);
        mainLayout->setObjectName(QStringLiteral("mainLayout"));
        mainLayout->setAutoFillBackground(false);
        mainLayout->setStyleSheet(QLatin1String("#mainLayout{\n"
"border: 2px solid gray;\n"
"border-radius:5px;\n"
"}"));
        mainLayout->setFrameShape(QFrame::StyledPanel);
        mainLayout->setFrameShadow(QFrame::Plain);
        mainLayout->setLineWidth(0);
        mainLayout->setMidLineWidth(0);

        verticalLayout_6->addWidget(mainLayout);


        verticalLayout_5->addWidget(frame_4);

        messageWindow = new QTextEdit(frame_2);
        messageWindow->setObjectName(QStringLiteral("messageWindow"));
        QSizePolicy sizePolicy4(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(messageWindow->sizePolicy().hasHeightForWidth());
        messageWindow->setSizePolicy(sizePolicy4);
        messageWindow->setMaximumSize(QSize(16777215, 80));
        QFont font2;
        font2.setPointSize(8);
        messageWindow->setFont(font2);
        messageWindow->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
        messageWindow->setReadOnly(true);

        verticalLayout_5->addWidget(messageWindow);

        splitter->addWidget(frame_2);

        verticalLayout_2->addWidget(splitter);

        MainWindow->setCentralWidget(centralWidget);
        statusBar = new QToolBar(MainWindow);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        statusBar->setMovable(true);
        statusBar->setFloatable(false);
        MainWindow->addToolBar(Qt::LeftToolBarArea, statusBar);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 1087, 21));
        menuOptions = new QMenu(menuBar);
        menuOptions->setObjectName(QStringLiteral("menuOptions"));
        MainWindow->setMenuBar(menuBar);

        menuBar->addAction(menuOptions->menuAction());
        menuOptions->addAction(actShowSidebar);
        menuOptions->addAction(actShowConzole);
        menuOptions->addAction(actExit);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "LeakLess Configurator by SMTech", 0));
        actShowConzole->setText(QApplication::translate("MainWindow", "Show console", 0));
#ifndef QT_NO_TOOLTIP
        actShowConzole->setToolTip(QApplication::translate("MainWindow", "Show debug console", 0));
#endif // QT_NO_TOOLTIP
        actShowSidebar->setText(QApplication::translate("MainWindow", "Show sidebar", 0));
#ifndef QT_NO_TOOLTIP
        actShowSidebar->setToolTip(QApplication::translate("MainWindow", "Toggle sidebar", 0));
#endif // QT_NO_TOOLTIP
        actExit->setText(QApplication::translate("MainWindow", "Exit", 0));
        lblImage->setText(QString());
        lblConnectedDevices->setText(QApplication::translate("MainWindow", "Connected devices", 0));
        btnSincClocks->setText(QApplication::translate("MainWindow", "Sync devices", 0));
        statusBar->setWindowTitle(QApplication::translate("MainWindow", "toolBar", 0));
        menuOptions->setTitle(QApplication::translate("MainWindow", "Options", 0));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
