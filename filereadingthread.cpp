#include "filereadingthread.h"




void FileReadingThread::run(){
    QFile inputFile(m_filePath);
    QString HighAddrString,LowAddrString;
    QList<quint32> MemoryArray, AddressArray;
    quint32 currentAddressHigh,currentAddressLow, currentAddress;
    bool ok;
    if (inputFile.open(QIODevice::ReadOnly))
    {
       QTextStream in(&inputFile);
       while (!in.atEnd())
       {
          QString line = in.readLine();
          if(line.startsWith(":02")){
              HighAddrString=line.mid(9,4);

              currentAddressHigh=HighAddrString.toUInt(&ok,16);
          }else if (line.startsWith(":10")){
              LowAddrString=line.mid(3,4);
              currentAddressLow=LowAddrString.toUInt(&ok,16);

              currentAddress=currentAddressHigh<<16 | currentAddressLow;
              if(currentAddress>=m_memoryStart){
                  QString DataString=line.mid(9,32);
                  QList<quint32> Data;
                  for(int i=0;i<4;i++){
                      quint32 DataEntry=DataString.mid(8*i,8).toUInt(&ok,16);
                      Data.append(DataEntry);
                      MemoryArray.append(DataEntry);
                      AddressArray.append(currentAddress+i*4-m_memoryStart);
                  }
              }
          }
       }
       inputFile.close();
    }
    progressChanged(100);
    resultReady(AddressArray,MemoryArray);
}
void FileReadingThread::StopRequest (){
   m_stopRequest=true;
}
void FileReadingThread::SetData (QString filePath,quint32 MemoryStart,quint32 MemorySize){
    m_filePath=filePath;
    m_memorySize=MemorySize;
    m_memoryStart=MemoryStart;
    m_currentAddress=0;
    m_stopRequest=false;
}



