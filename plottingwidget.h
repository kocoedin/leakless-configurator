#ifndef PLOTTINGWIDGET_H
#define PLOTTINGWIDGET_H

#include <QWidget>
#include "qcustomplot.h"

namespace Ui {
class plottingWidget;
}

class plottingWidget : public QWidget
{
    Q_OBJECT

public:
    explicit plottingWidget(QWidget *parent = 0);
    ~plottingWidget();
    void addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit, QColor Color);
    void addGraph(QVector<double> timeDouble, QVector<double> valueDouble, QString Name, QString Unit,QColor Color);

    void addGraph(QVector<QDateTime> time, QVector<double> valueDouble, QString Name, QString Unit, QVector<QDateTime> AlarmTimes, QVector<QString> AlarmNames);

public slots:
    void addRandomGraph();


private slots:
    void titleDoubleClick(QMouseEvent *event, QCPPlotTitle *title);
    void axisLabelDoubleClick(QCPAxis* axis, QCPAxis::SelectablePart part);
    void legendDoubleClick(QCPLegend* legend, QCPAbstractLegendItem* item);
    void selectionChanged();
    void mousePress();
    void mouseWheel();

    void removeSelectedGraph();
    void removeAllGraphs();

    void contextMenuRequest(QPoint pos);
    void moveLegend();
    void graphClicked(QCPAbstractPlottable *plottable);
    void changeLineColorSelectedGraph(QAction* ActionClicked);
    void changeLineWidthSelectedGraph(QAction *ActionClicked);
    void changeLineStyleSelectedGraph(QAction *ActionClicked);
    void changeScatterColorSelectedGraph(QAction *ActionClicked);
    void changeScatterWidthSelectedGraph(QAction *ActionClicked);
    void changeScatterStyleSelectedGraph(QAction *ActionClicked);
    void saveFigure(QAction *Action);
    void adjustAxes();

    void on_pdfSave_clicked();
    void on_pngSave_clicked();
    void on_jpegSave_clicked();
    void on_csvSave_clicked();
    void on_txtSave_clicked();


private:
    unsigned int xRange;

    Ui::plottingWidget *ui;
};

#endif // PLOTTINGWIDGET_H
