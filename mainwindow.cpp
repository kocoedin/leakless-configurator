#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "loggerinfo.h"
#include "dialogwait.h"
#include "loggerconfpdlag_USB.h"
#include "datatransfer.h"
#include <QDebug>

// Check why strange "<<<<<< HEAD" terms have showed up

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    createTrayActions();
    createTrayIcon();
    trayIcon->show();
    trayIcon->setToolTip("Pressure logger configurator by SMTech");
    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

<<<<<<< HEAD
    ui->toolFrame->setVisible(false);
    ui->lblTools->setVisible(false);

=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    /* Status bars */
    StatusBarUserSerial =  new QLabel();
    StatusBarUserSerial->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/It_Infrastructure/disconnected/disconnected-256.png")));;
    StatusBarUserSerial->setScaledContents(true);
    StatusBarUserSerial->setMaximumHeight(25);
    StatusBarUserSerial->setMaximumWidth(25);

<<<<<<< HEAD
//    StatusBarUserBT     =  new QLabel();
//    StatusBarUserBT->setPixmap(QPixmap(QString::fromUtf8("://win8/PNG/It_Infrastructure/bluetooth/bluetooth-256.png")));;
//    StatusBarUserBT->setScaledContents(true);
//    StatusBarUserBT->setMaximumHeight(25);
//    StatusBarUserBT->setMaximumWidth(25);

    StatusBarUserTCP    =  new QLabel();;

=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    StatusBarClock      =  new QLabel();
    StatusBarClock->setStyleSheet ("font: 12pt \"Open Sans\";");

    /* Toolbarovi */
    this->addToolBar(Qt::BottomToolBarArea  ,ui->statusBar);
    ui->statusBar->addWidget(StatusBarClock);
    ui->statusBar->addWidget(StatusBarUserSerial);
<<<<<<< HEAD
//    ui->statusBar->addWidget(StatusBarUserBT);
//    ui->statusBar->addWidget(StatusBarUserTCP);
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    ui->statusBar->setLayoutDirection(Qt::RightToLeft);
    ui->loggerList->setIconSize(QSize(80,80));

    /* Connect to toolbar actions */
    connect(ui->actShowConzole, SIGNAL(toggled(bool)),ui->messageWindow , SLOT(setVisible(bool)));
    connect(ui->actShowSidebar, SIGNAL(toggled(bool)),ui->frame         , SLOT(setVisible(bool)));
    connect(ui->actExit,        SIGNAL(triggered()),  this              , SLOT(slotToQuit()));

    ui->actShowSidebar->setChecked(true);

    /* Hide message window */
    ui->messageWindow->hide();

    /* Gradim FolderTreeView */

    /* Tree Data View */
    treeDataViewWidget = new TreeFolderView(this);
    //treeDataViewWidget->initialPath = initialPath;
    ui->verticalLayout_3->addWidget(treeDataViewWidget);
    ui->verticalLayout_3->setSizeConstraint(QLayout::SetMinimumSize);
    ui->splitter->setStretchFactor(1,1);
    treeDataViewWidget->show();
    connect(treeDataViewWidget, SIGNAL(dataToPlot(QString,QString)), this, SLOT(dataPlotInquiry(QString,QString)));
    connect(this, SIGNAL(dataTransferFinishedMainSignal()), treeDataViewWidget, SLOT(treeRefresh()));

    QImage image(":/icons/Logo.png");
    int factor = 4;
    QImage image_scaled = image.scaled(image.width()/factor,image.height()/factor,
                                       Qt::IgnoreAspectRatio,Qt::SmoothTransformation);
    ui->lblImage->setScaledContents(true);
    ui->lblImage->setPixmap(QPixmap::fromImage(image_scaled));
    ui->lblImage->show();

    //
    QPixmap input(":/icons/LeakLessLogoBW_15.png");
    QImage image2(input.size(), QImage::Format_ARGB32_Premultiplied);
    image2.fill(Qt::transparent); //
    QPainter p(&image2);
    p.setOpacity(0.03);
    p.drawPixmap(0, 0, input);
    p.end();
    ui->mainLayout->setBackground(image2);


    /* Plotting widget dio */
    usbDetector=new DetectorController();
    publishMessageToTerminal("USB device detector started");
    connect(usbDetector,SIGNAL(Connected(QString,QString))                  ,this,SLOT(SerialDetected_private(QString,QString)));
    connect(usbDetector,SIGNAL(Disconnected(QString,QString))               ,this,SLOT(SerialNotDetected_private(QString,QString)));
    connect(usbDetector,SIGNAL(Responded(QString,QString))                  ,this,SLOT(MessageRecieved(QString,QString)));
    connect(usbDetector,SIGNAL(messageSignal(QString))                      ,this,SLOT(publishMessageToTerminal(QString)));
    connect(usbDetector,SIGNAL(errorSignal(QString))                        ,this,SLOT(publishErrorToTerminal(QString)));
    connect(usbDetector,SIGNAL(DriveChangeDetected(QStringList,QStringList)),this,SLOT(DriveChangeDetected(QStringList,QStringList)));

<<<<<<< HEAD
//    BTDetector = new BTDetectorController();
//    publishMessageToTerminal("BT device detector started");
//    BTDetector->BTSubscribeListeners();
//    connect(BTDetector,SIGNAL(Connected(QString,QString))       ,this,SLOT(BTSerialDetected_private(QString,QString)));
//    connect(BTDetector,SIGNAL(Disconnected(QString,QString))    ,this,SLOT(BTSerialNotDetected_private(QString,QString)));
//    connect(BTDetector,SIGNAL(Responded(QString,QString))       ,this,SLOT(BTMessageRecieved(QString,QString)));
//    connect(BTDetector,SIGNAL(messageSignal(QString))           ,this,SLOT(publishMessageToTerminal(QString)));
//    connect(BTDetector,SIGNAL(errorSignal(QString))             ,this,SLOT(publishErrorToTerminal(QString)));

//    TCPDetector=new TCPDetectorController();
//    publishMessageToTerminal("TCP device detector started");
//    connect(TCPDetector,SIGNAL(Connected(QString,QString))      ,this,SLOT(BTSerialDetected_private(QString,QString)));
//    connect(TCPDetector,SIGNAL(Disconnected(QString,QString))   ,this,SLOT(BTSerialNotDetected_private(QString,QString)));
//    connect(TCPDetector,SIGNAL(Responded(QString,QString))      ,this,SLOT(BTMessageRecieved(QString,QString)));
//    connect(TCPDetector,SIGNAL(messageSignal(QString))          ,this,SLOT(publishMessageToTerminal(QString)));
//    connect(TCPDetector,SIGNAL(errorSignal(QString))            ,this,SLOT(publishErrorToTerminal(QString)));

=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    /* Sat */
    QTimer *clockTimer = new QTimer(this);
    connect(clockTimer, SIGNAL(timeout()), this, SLOT(showTime()));
    clockTimer->start(1000);

    /* Delayed sync timer */
    delayedSyncTimer= new QTimer(this);
    delayedSyncTimer->setInterval(1000);
    connect(delayedSyncTimer, SIGNAL(timeout()), this, SLOT(on_btnSincClocks_clicked()));

    /* Delayed report timer */
    delayedReportTimer= new QTimer(this);
    delayedReportTimer->setInterval(1500);
    connect(delayedReportTimer, SIGNAL(timeout()), this, SLOT(on_btnStartReports_clicked()));

    /* Dodajem oznaku da nema loggera */
    QListWidgetItem *item= new QListWidgetItem("No loggers detected",ui->loggerList);
    QIcon icon(":/icons/win8/PNG/Very_Basic/search/search-256.png");
    QFont font = item->font();
    font.setWeight(25);
    font.setPointSize(25);
    item->setFont(font);
    item->setIcon(icon);
    item->setSizeHint(QSize(100,100));
<<<<<<< HEAD

//    /* Deployed loggers je prvi */
//    DeployedLoggers *deployedLoggersWindow = new DeployedLoggers(this);
//    ui->subjectText->setText("Deployed loggers list");
//    ui->mainLayout->addWidget(deployedLoggersWindow);
//    deployedLoggersWindow->show();

=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

    this->showMaximized();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::slotToQuit(){
    QApplication::exit();
}


void MainWindow::dataPlotInquiry(QString Path,QString Name)
{
    DataVisualizer *dataViz= new DataVisualizer();
    QMdiSubWindow *subWindow = new QMdiSubWindow;
    subWindow->setWidget(dataViz);
    subWindow->setAttribute(Qt::WA_DeleteOnClose);
    ui->mainLayout->addSubWindow(subWindow);
    subWindow->setWindowIcon(QIcon(":/icons/Logo2.png"));
    subWindow->resize(QSize(640,480));
    subWindow->setWindowTitle("Data visualization");

    subWindow->show();
    dataViz->plotData(Path,Name);

}

void MainWindow::MessageRecieved(QString Serial,QString Message){

    if(Message.startsWith("STATUS",Qt::CaseInsensitive)){
        QStringList Status=Message.split(",");
        for (int i=0;i<ui->loggerList->count();i++){
            if(((LoggerInfo *)(ui->loggerList->itemWidget(ui->loggerList->item(i))))->getSerial()==Serial){
                LoggerInfo* StatusStrip =  (LoggerInfo *)(ui->loggerList->itemWidget(ui->loggerList->item(i)));
                StatusStrip->setStatus(Message,11);
            }
        }
    }else{ // Ako imam odabran taj modul onda se njegove poruke ispisuju osim ovih status

    }
}

bool MainWindow::eventFilter(QObject *obj, QEvent *event)
{
    if(event->type() == QEvent::Wheel)
    {
        return true;
    }
    return false;
}

void MainWindow::SerialDetected_private(QString Serial, QString Identifier){
    //checkForDeployedLogger(Serial);
    StatusBarUserSerial->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/It_Infrastructure/connected/connected-256.png")));;
    trayIcon->showMessage("USB Logger connected", "Serial number: "+ Serial, QSystemTrayIcon::Information,10000);

    if (ui->loggerList->item(0)->text().startsWith("No")){
        ui->loggerList->clear();
    }

    QListWidgetItem *item= new QListWidgetItem(ui->loggerList);
    item->setSizeHint(QSize(450,160));
    LoggerInfo *itemWidget=new LoggerInfo(this,Serial,USBChannel); //USBChannelName
    itemWidget->DriveChangeDetected(_labels,_names); // samo prvi put
    connect(itemWidget,SIGNAL(LoggerSelected(QString,LoggerType,CommunicationType)),this,SLOT(LoggerSelected(QString,LoggerType,CommunicationType)));
    connect(itemWidget,SIGNAL(LoggerSelectedSimple(QString,LoggerType,CommunicationType)),this,SLOT(LoggerSelectedSimple(QString,LoggerType,CommunicationType)));
    connect(itemWidget,SIGNAL(dataTransferFinishedLoggerInfoSignal()), this, SLOT(dataTransferFinishedMainSlot()));
    connect(this,SIGNAL(DriveChangeSignal(QStringList,QStringList)), itemWidget, SLOT(DriveChangeDetected(QStringList,QStringList)));

    qDebug() << Serial;
    qDebug() << Identifier;

    itemWidget->setSerial(Serial);
    itemWidget->setIdentifier(Identifier);
    ui->loggerList->setItemWidget(item,itemWidget);
    item->setFlags(Qt::ItemIsSelectable);
    itemWidget->setStyleSheet("font: 9pt \"Open Sans\";");

    // OVO TREBA ODKOMENTIRATI
    usbDetector->sincClocks();
<<<<<<< HEAD
    usbDetector->startReporting();
    //delayedSyncTimer->start();
    //delayedReportTimer->start();
=======
    delayedSyncTimer->start();
    delayedReportTimer->start();
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52

}

void MainWindow::SerialNotDetected_private(QString Serial, QString Identifier){

    StatusBarUserSerial->setToolTip("Terminal device not detected");
    trayIcon->showMessage("Logger disconnected", "Serial number: "+ Serial, QSystemTrayIcon::Information,10000);

    //ui->moduleOptions->setDisabled(true);

    for (int i=0;i<ui->loggerList->count();i++){
        if(Serial!=""){
            if(((LoggerInfo *)(ui->loggerList->itemWidget(ui->loggerList->item(i))))->getSerial()==Serial){
                ui->loggerList->removeItemWidget(ui->loggerList->item(i));
                delete ui->loggerList->takeItem(i);
            }
        }
    }
    // Treba maknut main widget
//    if (ui->mainLayout->count()>0){
//        if (getMainWidgetDeviceID(ui->mainLayout->itemAt(0)->widget())==Serial){
//            QLayoutItem *child;
//            while ((child = ui->mainLayout->takeAt(0)) != NULL) {
//                delete child->widget();
//                delete child;
//            }
//        }
//    }

    if(ui->loggerList->count()==0){
        QListWidgetItem *item= new QListWidgetItem("No loggers detected",ui->loggerList);
        QIcon icon(":/icons/win8/PNG/Very_Basic/search/search-256.png");
        QFont font = item->font();
        font.setWeight(20);
        font.setPointSize(20);
        item->setFont(font);
        item->setIcon(icon);
        item->setSizeHint(QSize(80,80));
    }

}

void MainWindow::showTrayMessage(int Type,QString Title, QString Text, int duration)
{
    QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(Type);
    trayIcon->showMessage(Title, Text, icon, duration*1000);
}

void MainWindow::DriveChangeDetected(QStringList labels,QStringList names){
    emit DriveChangeSignal(labels,names);
    // koristim globalne zato da kod inicijalizacije LoggerInfo mogu path poslozit
    _labels=labels;
    _names=names;

}

void MainWindow::createTrayActions()
{
    minimizeAction = new QAction(tr("Mi&nimize"), this);
    connect(minimizeAction, SIGNAL(triggered()), this, SLOT(hide()));

    maximizeAction = new QAction(tr("Ma&ximize"), this);
    connect(maximizeAction, SIGNAL(triggered()), this, SLOT(showMaximized()));

    restoreAction = new QAction(tr("&Restore"), this);
    connect(restoreAction, SIGNAL(triggered()), this, SLOT(showNormal()));

    quitAction = new QAction(tr("&Quit"), this);
    connect(quitAction, SIGNAL(triggered()), qApp, SLOT(quit()));
}

void MainWindow::createTrayIcon()
{
    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(minimizeAction);
    trayIconMenu->addAction(maximizeAction);
    trayIconMenu->addAction(restoreAction);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(quitAction);

    trayIcon = new QSystemTrayIcon(QIcon(":/icons/THIcon.jpg"));
    trayIcon->setContextMenu(trayIconMenu);
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
        case QSystemTrayIcon::DoubleClick:
            this->showMaximized();
            break;
        case QSystemTrayIcon::MiddleClick:
            showTrayMessage(1,"Logger monitor is running!", "Please close the program on system tray", 1000);
            break;
        default:
            break;
    }
}

void MainWindow::showTime()
{
    QDateTime time(QDateTime::currentDateTime());
    QString text = time.toString("   dd-MM-yyyy, hh:mm");
    StatusBarClock->setText(text);
}

void MainWindow::publishMessageToTerminal(QString message){
    if (message.contains("[RS232]")) return;
    if (message==" ") return;
    if (message.isEmpty()) return;
    if (message.contains("A:")) return;
    QDateTime time(QDateTime::currentDateTime());


    int fw = ui->messageWindow->fontWeight();
    QColor tc = ui->messageWindow->textColor();
    // append
    ui->messageWindow->setFontWeight( QFont::DemiBold );
    ui->messageWindow->setTextColor( QColor( "blue" ) );

    ui->messageWindow->append( time.toString("(hh:mm:ss) ")+message.remove("\n").remove("\r") );
    // restore
    ui->messageWindow->setFontWeight( fw );
    ui->messageWindow->setTextColor( tc );

    QScrollBar *sb = ui->messageWindow->verticalScrollBar();
    sb->setValue(sb->maximum());
}

void MainWindow::publishErrorToTerminal(QString error){
    if (error==" ") return;
    if(error.isEmpty()) return;
    QDateTime time(QDateTime::currentDateTime());
    int fw = ui->messageWindow->fontWeight();
    QColor tc = ui->messageWindow->textColor();
    // append
    ui->messageWindow->setFontWeight( QFont::DemiBold );
    ui->messageWindow->setTextColor( QColor( "red" ) );
    ui->messageWindow->append( time.toString("(hh:mm:ss) ") + error.remove('\n').remove('\r') );
    // restore
    ui->messageWindow->setFontWeight( fw );
    ui->messageWindow->setTextColor( tc );

    QScrollBar *sb = ui->messageWindow->verticalScrollBar();
    sb->setValue(sb->maximum());

}


void MainWindow::on_btnSincClocks_clicked()
{
    usbDetector->sincClocks();
<<<<<<< HEAD
//    BTDetector->sincClocks();
//    TCPDetector->sincClocks();
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    delayedSyncTimer->stop();
}

void MainWindow::on_btnStopReports_clicked()
{
    usbDetector->stopReporting();
<<<<<<< HEAD
//    BTDetector->stopReporting();
//    TCPDetector->stopReporting();
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
}

void MainWindow::on_btnStartReports_clicked()
{
    usbDetector->startReporting();
<<<<<<< HEAD
//    BTDetector->startReporting();
//    TCPDetector->startReporting();
=======
>>>>>>> 8e7383cf773efce06f055155e01193bd2543ef52
    delayedReportTimer->stop();
}


QString MainWindow::getMainWidgetDeviceID(QWidget *Widget)
{
    if (Widget==NULL) return "";
    return Widget->property("DeviceID").toString();
}

void MainWindow::LoggerSelected(QString ID, LoggerType DeviceType, CommunicationType CommType)
{
    // Povezivanje sa driveom
    sendingSerial=ID;

    publishMessageToTerminal("Selected");
//    if (ui->mainLayout->count()==0){
//    }else{
//        QLayoutItem *child;
//            while ((child = ui->mainLayout->takeAt(0)) != NULL)  {
//                delete child->widget();
//                delete child;
//        }
//    }


    switch (DeviceType){
        case LoggerPDLAG:
        {
            if (CommType==USBChannel){
                LoggerConfPDLAG_USB *loggerPDLAG_USB=new LoggerConfPDLAG_USB(this);
                ui->mainLayout->addSubWindow(loggerPDLAG_USB);
                loggerPDLAG_USB->setAttribute(Qt::WA_DeleteOnClose);
                loggerPDLAG_USB->setWindowIcon(QIcon(":/icons/Logo2.png"));
                loggerPDLAG_USB->setWindowTitle("Logger Manager - ID:"+ID);
                loggerPDLAG_USB->setProperty("DeviceID",ID);
                MainWidget=loggerPDLAG_USB;
                loggerPDLAG_USB->SetID(sendingSerial);
                loggerPDLAG_USB->show();
                loggerPDLAG_USB->refereshParameters();
                loggerPDLAG_USB->DriveChangeDetected(usbDetector->driveLabelList,usbDetector->driveNameList);
                connect(loggerPDLAG_USB,SIGNAL(SendData(QString,QString))               ,usbDetector,SLOT(SendDataToModule(QString,QString)));
                connect(usbDetector,SIGNAL(Responded(QString,QString))                  ,loggerPDLAG_USB,SLOT(RecieveData(QString,QString)));
                connect(this, SIGNAL(DriveChangeSignal(QStringList,QStringList))        ,loggerPDLAG_USB,SLOT(DriveChangeDetected(QStringList,QStringList)));
           }
        }
        break;
    }
}

void MainWindow::LoggerSelectedSimple(QString ID, LoggerType DeviceType, CommunicationType CommType)
{
    // Povezivanje sa driveom
    sendingSerial=ID;

//    if (ui->mainLayout->count()==0){
//    }else{
//        QLayoutItem *child;
//            while ((child = ui->mainLayout->takeAt(0)) != NULL)  {
//                delete child->widget();
//                delete child;
//        }
//    }

    switch (DeviceType){
        case LoggerPDLAG:
        {
            if (CommType==USBChannel){
                LoggerConfPDLAG_USB *loggerPDLAG_USB=new LoggerConfPDLAG_USB(this,true);
                ui->mainLayout->addSubWindow(loggerPDLAG_USB);
                loggerPDLAG_USB->setWindowTitle("Logger Manager - ID:"+ID);
                loggerPDLAG_USB->setWindowIcon(QIcon(":/icons/Logo2.png"));
                loggerPDLAG_USB->setProperty("DeviceID",ID);
                MainWidget=loggerPDLAG_USB;
                loggerPDLAG_USB->SetID(sendingSerial);
                loggerPDLAG_USB->show();
                loggerPDLAG_USB->refereshParameters();
                loggerPDLAG_USB->DriveChangeDetected(usbDetector->driveLabelList,usbDetector->driveNameList);
                connect(loggerPDLAG_USB,SIGNAL(SendData(QString,QString))               ,usbDetector,SLOT(SendDataToModule(QString,QString)));
                connect(usbDetector,SIGNAL(Responded(QString,QString))                  ,loggerPDLAG_USB,SLOT(RecieveData(QString,QString)));
                connect(this, SIGNAL(DriveChangeSignal(QStringList,QStringList))        ,loggerPDLAG_USB,SLOT(DriveChangeDetected(QStringList,QStringList)));
            }
        }
        break;
    }
}

void MainWindow::LoggerDisconnect(QString ID,LoggerType DeviceType, CommunicationType CommType)
{

}

void MainWindow::delay( int millisecondsToWait )
{
    QTime dieTime = QTime::currentTime().addMSecs( millisecondsToWait );
    while( QTime::currentTime() < dieTime )
    {
        QCoreApplication::processEvents( QEventLoop::AllEvents, 100 );
    }
}

void MainWindow::checkForDeployedLogger(QString LoggerID)
{
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QDir FileDir(FolderPath);

    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);

        preferences.beginGroup("GeneralInformation");
        bool DeployFinished=preferences.value("Finished",false).toBool();
        QString DeviceID=preferences.value("DeviceID").toString();

        if (!DeployFinished && DeviceID==LoggerID){
            preferences.setValue("Finished",true);
            preferences.setValue("RemoveTime",QTime::currentTime().toString("hh:mm:ss"));
            preferences.setValue("RemoveDate",QDate::currentDate().toString("dd/MM/yyyy"));

        }
        preferences.endGroup();

    }
}

void MainWindow::on_loggerList_customContextMenuRequested(const QPoint &pos)
{
    // for most widgets
    QPoint globalPos = ui->loggerList->mapToGlobal(pos);
    QMenu myMenu;
    myMenu.addAction("Add network logger");

    QAction* selectedItem = myMenu.exec(globalPos);
    if (selectedItem)
    {

    }
    else
    {

    }
}

void MainWindow::dataTransferFinishedMainSlot()
{
    emit dataTransferFinishedMainSignal();
}

//void MainWindow::resizeEvent(QResizeEvent  *resizeEvent)
//{
//    QImage newBackground(resizeEvent->size(), QImage::Format_ARGB32_Premultiplied);
//    QPainter p(&newBackground);
//    p.fillRect(newBackground.rect(), b);

//    QImage scaled = i.scaled(resizeEvent->size(),Qt::KeepAspectRatio);
//    QRect scaledRect = scaled.rect();
//    scaledRect.moveCenter(newBackground.rect().center());
//    p.drawImage(scaledRect, scaled);
//    ui->mainLayout->setBackground(newBackground);
//    ui->mainLayout->resizeEvent(resizeEvent);
//}
