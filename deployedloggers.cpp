#include "deployedloggers.h"
#include "ui_deployedloggers.h"

DeployedLoggers::DeployedLoggers(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DeployedLoggers)
{
    ui->setupUi(this);
    timer=new QTimer();
    timer->setInterval(1000);
    timer->connect(timer, SIGNAL(timeout()), this, SLOT(timer_timeout()));

    refreshLoggerList();
}

DeployedLoggers::~DeployedLoggers()
{
    delete timer;
    delete ui;
}

void DeployedLoggers::refreshLoggerList(){
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QDir ImageDir(FolderPath+"images");
    QDir FileDir(FolderPath);
    timer->stop();

    ui->loggerTreeWidget->clear();

    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);

        preferences.beginGroup("GeneralInformation");
        QString DeviceID=preferences.value("DeviceID").toString();
        QString DeviceTimeString=preferences.value("Time").toString();
        QString DeviceDateString=preferences.value("Date").toString();
        QTime DeviceTime=QTime::fromString(DeviceTimeString,"hh:mm:ss");
        QDate DeviceDate=QDate::fromString(DeviceDateString,"dd/MM/yyyy");
        bool DeployFinished=preferences.value("Finished",false).toBool();
        preferences.endGroup();

        preferences.beginGroup("LocationInformation");
        QString DeviceLatitude=preferences.value("Latitude").toString();
        QString DeviceLongitude=preferences.value("Longitude").toString();
        preferences.endGroup();

        if (DeployFinished) continue;

        QTreeWidgetItem *newDevice=new QTreeWidgetItem();
        newDevice->setText(0, DeviceLatitude);
        newDevice->setText(1, DeviceLongitude);
        newDevice->setText(2, DeviceID);
        newDevice->setText(3, QDateTime(DeviceDate,DeviceTime).toString("dd/MM/yyyy hh:mm:ss"));
        QDateTime TimeDateNow=QDateTime::currentDateTime();
        QDateTime TimeDateStart=QDateTime(DeviceDate,DeviceTime);
        qint64 msec=TimeDateStart.msecsTo(TimeDateNow);
        qint64 secs=msec/1000;
        qint64 minutes=secs/60;
        qint64 hours=minutes/60;
        newDevice->setText(4, QString::number(hours) + " hours");
        newDevice->setText(5, fileInfo.filePath());
        ui->loggerTreeWidget->addTopLevelItem(newDevice);


    }
    timer->start();

    ui->loggerTreeWidget->resizeColumnToContents(0);
    ui->loggerTreeWidget->resizeColumnToContents(1);
    ui->loggerTreeWidget->resizeColumnToContents(2);
    ui->loggerTreeWidget->resizeColumnToContents(3);
    ui->loggerTreeWidget->resizeColumnToContents(4);
    ui->loggerTreeWidget->expandAll();
}

void DeployedLoggers::on_loggerTreeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    QSettings settings("ConfiguratorConfig.ini", QSettings::IniFormat);
    QString ApplicationDataLocation=settings.value("Settings/DataPath").toString();;
    QString FolderPath=ApplicationDataLocation+"//LoggerConfigurator//";
    QString ImageFolderPath=FolderPath+"images";
    QFile   inputFile(item->text(5));

    QSettings preferences(item->text(5),QSettings::IniFormat);
    preferences.beginGroup("OtherInformation");
    QStringList ImageList=preferences.value("ImagePaths").toStringList();
    preferences.endGroup();

    ui->listWidget->clear();

    for (int k=0;k<ImageList.count();k++){

        QFile imageFile(ImageFolderPath+"//"+ImageList.at(k).split("//").last());
        if (!imageFile.exists()) continue;
        QImage image;
        image.load(imageFile.fileName());
        QListWidgetItem *imageItem=new QListWidgetItem();
        imageItem->setIcon(QIcon(QPixmap::fromImage(image)));
        imageItem->setToolTip(imageFile.fileName());
        ui->listWidget->setIconSize(QSize(150,150));
        ui->listWidget->addItem(imageItem);


    }

    ui->textBrowser->clear();
    if (inputFile.open(QIODevice::ReadOnly))
    {
        QTextStream in(&inputFile);
        while (!in.atEnd())
        {
           QString line = in.readLine();
           ui->textBrowser->append(line);
        }
        inputFile.close();
    }

}

void DeployedLoggers::timer_timeout()
{
    QDateTime TimeDateNow=QDateTime::currentDateTime();
    for (int i=0;i<ui->loggerTreeWidget->topLevelItemCount();i++){
        QString DateTimeString=ui->loggerTreeWidget->topLevelItem(i)->text(3);
        QDateTime DeployTime= QDateTime::fromString(DateTimeString,"dd/MM/yyyy hh:mm:ss");
        qint64 msec=DeployTime.msecsTo(TimeDateNow);
        qint64 secs=msec/1000;
        qint64 minutes=secs/60;
        qint64 hours=minutes/60;
        ui->loggerTreeWidget->topLevelItem(i)->setText(4, QString::number(hours)+ " hours");
    }

}

void DeployedLoggers::on_loggerTreeWidget_customContextMenuRequested(const QPoint &pos)
{
    QTreeWidgetItem* item = ui->loggerTreeWidget->itemAt(pos);

    if (item) {
        QMenu myMenu;
        myMenu.addAction("Move to history");
        myMenu.addAction("Delete entry");

        QAction* selectedItem = myMenu.exec(QCursor::pos());
        if (selectedItem==NULL) return;
        if (selectedItem->text().startsWith("Delete"))
        {

            int ret = QMessageBox::warning(this, tr("Warning"), tr("You are about to delete this record.\nAre you sure?"),QMessageBox::Yes | QMessageBox::Cancel);
            if (ret!= QMessageBox::Yes) return;
            // Pitat are you sure
            QFile   inputFile(item->text(5));
            if (inputFile.exists()) inputFile.remove();
            refreshLoggerList();

        }
        else
        {
            QSettings preferences(item->text(5),QSettings::IniFormat);
            preferences.beginGroup("GeneralInformation");
            preferences.setValue("Finished",true);
            preferences.endGroup();
            refreshLoggerList();
        }
    }
}

void DeployedLoggers::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{

}
