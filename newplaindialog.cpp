#include "newplaindialog.h"
#include "ui_newplaindialog.h"

NewPlainDialog::NewPlainDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewPlainDialog)
{
    ui->setupUi(this);
}

NewPlainDialog::~NewPlainDialog()
{
    delete ui;
}

void NewPlainDialog::refreshDialog(){
    bool iniAvailable = isInfoIniAvailable();

    switch (actionType) {
    case ItemNew:
    {
        qDebug() << "refreshDialog & ItemNew";
        ui->lblName->setText(_lblName);
        ui->lblComment->setText(_lblComment);
        break;
    }
    case ItemEdit:
    {
        if (iniAvailable)
        {
            ui->lblName->setText(_lblName);
            ui->lblComment->setText(_lblComment);
            setDialogFromInfoIni();
        }
        else
        {
            ui->lblName->setText(_lblName);
            ui->lblComment->setText(_lblComment);
        }

        break;
    }
    }

}

bool NewPlainDialog::isInfoIniAvailable(){
    QDir FileDir(fullPath);
    QStringList filter;
    filter << "info.ini";
    FileDir.setNameFilters(filter);
    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);

    return !list.empty();
}

void NewPlainDialog::setDialogFromInfoIni(){
    QDir FileDir(fullPath);
    QStringList filter;
    filter << "info.ini";
    FileDir.setNameFilters(filter);
    QFileInfoList list = FileDir.entryInfoList(QDir::NoDotAndDotDot|QDir::Files);

    // for loop not needed!
    for (int i = 0; i < list.size(); ++i) {
        QFileInfo fileInfo = list.at(i);
        QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);

        QString Name=preferences.value("Name").toString();
        QString Comment=preferences.value("Comment").toString();

        ui->lineEditName->setText(Name);
        ui->textEditComment->setText(Comment);

        _oldName = Name;
        _oldComment = Comment;
    }
}

void NewPlainDialog::createInfoIni(){
    QSettings* settings = new QSettings(fullPath + "/info.ini", QSettings::IniFormat);
    settings->setValue("Name", ui->lineEditName->text());
    settings->setValue("Comment", ui->textEditComment->toPlainText());
}


void NewPlainDialog::on_btnOk_clicked()
{
    // ok clicked

    switch (folderType) {
    case ItemProject:
    {
        switch (actionType) {
        case ItemNew:
        {
            qDebug() << "ItemProject & ItemNew";
            fullPath = fullPath + "/Proj_" + ui->lineEditName->text();
            QDir FileDir(fullPath);
            createInfoIni();
            QTreeWidgetItem *itm = new QTreeWidgetItem(dialogTreeWidget,ItemProject);
            QIcon icon(":/icons/win8/PNG/System/edit_property/edit_property-256.png");
            itm->setIcon(0,icon);
            itm->setText(0,ui->lineEditName->text());
            dialogTreeWidget->addTopLevelItem(itm);
            break;
        }
        case ItemEdit:
        {
            qDebug() << "ItemProject & ItemEdit";
            QDir FileDir(fullPath);
            FileDir.cdUp();
            QString newFileName;
            newFileName = "Proj_" + ui->lineEditName->text();
            QString oldFileName = "Proj_" + _oldName;
            FileDir.rename(oldFileName,newFileName); // maybe cdUp()
            item->setText(0,ui->lineEditName->text());
            fullPath = FileDir.path() + "/Proj_" + ui->lineEditName->text();
            createInfoIni();
            break;
        }
        }
        break;
    }
    case ItemLocation:
    {
        switch (actionType) {
        case ItemNew:
        {
            qDebug() << "ItemLocation & ItemNew";
            fullPath = fullPath + "/Loc_" + ui->lineEditName->text();
            QDir FileDir(fullPath);
            createInfoIni();
            QTreeWidgetItem *itm = new QTreeWidgetItem(ItemLocation);
            QIcon icon(":/icons/win8/PNG/Maps_and_Geolocation/point_objects/point_objects-256.png");
            itm->setIcon(0,icon);
            itm->setText(0,ui->lineEditName->text());
            item->addChild(itm);
            break;
        }
        case ItemEdit:
        {
            qDebug() << "ItemLocation & ItemEdit";
            QDir FileDir(fullPath);
            FileDir.cdUp();
            QString newFileName;
            newFileName = "Loc_" + ui->lineEditName->text();
            QString oldFileName = "Loc_" + _oldName;
            FileDir.rename(oldFileName,newFileName); // maybe cdUp()
            item->setText(0,ui->lineEditName->text());
            fullPath = FileDir.path() + "/Loc_" + ui->lineEditName->text();
            createInfoIni();
            break;
        }
        }
        break;
    }
    case ItemDevice:
    {
        switch (actionType) {
        case ItemNew:
        {
            qDebug() << "ItemDevice & ItemNew";
            fullPath = fullPath + "/Dev_" + ui->lineEditName->text();
            QDir FileDir(fullPath);
            createInfoIni();
            QTreeWidgetItem *itm = new QTreeWidgetItem(ItemDevice);
            QIcon icon(":/icons/win8/PNG/Industry/electronics/electronics-256.png");
            itm->setIcon(0,icon);
            itm->setText(0,ui->lineEditName->text());
            item->addChild(itm);
            break;
        }
        case ItemEdit:
        {
            qDebug() << "ItemDevice & ItemEdit";
            QDir FileDir(fullPath);
            FileDir.cdUp();
            QString newFileName;
            newFileName = "Dev_" + ui->lineEditName->text();
            QString oldFileName = "Dev_" + _oldName;
            FileDir.rename(oldFileName,newFileName); // maybe cdUp()
            item->setText(0,ui->lineEditName->text());
            fullPath = FileDir.path() + "/Dev_" + ui->lineEditName->text();
            createInfoIni();
            break;
        }
        }
        break;
    }
    }

    //emit dialogClosedOk();
    this->close();
}

void NewPlainDialog::on_btnCancel_clicked()
{
    // cancel clicked

    this->close();
}
