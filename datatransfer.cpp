#include "datatransfer.h"
#include "ui_datatransfer.h"

DataTransfer::DataTransfer(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::DataTransfer)
{
    ui->setupUi(this);

    ui->btnRight->setDisabled(true);

    initialPathPC = "C:/QtLeakLess";
    initialPathDevice = "G:/";

    ui->treeWidgetPC->setHeaderLabel("Stored data");
    ui->treeWidgetDevice->setHeaderLabel("Device data");
}

DataTransfer::~DataTransfer()
{
    delete ui;
}

void DataTransfer::initialSetup()
{
    treeInitialSetupPC();
    treeInitialSetupDevice();

    connect(ui->treeWidgetPC, SIGNAL(itemSelectionChanged()), this, SLOT(dataTransferSelectionChanged()));
    connect(ui->treeWidgetDevice, SIGNAL(itemSelectionChanged()), this, SLOT(dataTransferSelectionChanged()));
}

void DataTransfer::treeInitialSetupPC()
{
    QDir dirProj(initialPathPC);
    qDebug() << initialPathPC;
    dirProj.setFilter(QDir::Dirs);
    dirProj.setSorting(QDir::Size);
    QStringList filtersProj;
    filtersProj << "Proj_*";
    dirProj.setNameFilters(filtersProj);
    QFileInfoList listProj = dirProj.entryInfoList();
    for (int i = 0; i < listProj.size(); ++i) {
        QFileInfo fileInfoProj = listProj.at(i);
        QString nameProj = fileInfoProj.fileName();
        nameProj.replace(0, 5, "");

        QString fullPath = initialPathPC + "/" + fileInfoProj.fileName();
        AddProject(nameProj,fullPath);
    }
}

void DataTransfer::treeInitialSetupDevice()
{
    QDir dirProj(initialPathDevice);
    dirProj.setFilter(QDir::Dirs);
    dirProj.setSorting(QDir::Size);
    QFileInfoList listProj = dirProj.entryInfoList();
    for (int i = 0; i < listProj.size(); ++i) {
        QFileInfo fileInfoProj = listProj.at(i);
        QString name = fileInfoProj.fileName();
         qDebug() << name;
        QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidgetDevice,ItemProject);
        QIcon icon(":/icons/win8/PNG/System/edit_property/edit_property-256.png");
        itm->setText(0,name);
        itm->setIcon(0,icon);
        itm->setSizeHint(0,QSize(20,20));
        itm->setFlags(Qt::ItemIsEnabled);
        itm->setExpanded(true);
        itm->setTextColor(0,QColor(_col,_col,_col,_alpha));
        ui->treeWidgetDevice->addTopLevelItem(itm);

        QString filesPath = initialPathDevice + "/" + name;
        QDir dirFiles(filesPath);
        dirFiles.setFilter(QDir::Files);
        dirFiles.setSorting(QDir::Size);
        QStringList filters;
        filters << "*Time*" << "*Date*";
        dirFiles.setNameFilters(filters);
        QFileInfoList listFiles = dirFiles.entryInfoList();
        for (int j = 0; j < listFiles.size(); ++j) {
            QFileInfo fileInfoFiles = listFiles.at(j);

            QTreeWidgetItem *itmFile = new QTreeWidgetItem(ItemMeasurement);
            itmFile->setText(0,fileInfoFiles.fileName());
            itmFile->setSizeHint(0,QSize(20,20));
            itm->addChild(itmFile);
        }
    }
}

void DataTransfer::AddProject(QString name, QString pathToItem)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem(ui->treeWidgetPC,ItemProject);
    QIcon icon(":/icons/win8/PNG/System/edit_property/edit_property-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    ui->treeWidgetPC->addTopLevelItem(itm);
    itm->setExpanded(true);
    itm->setFlags(Qt::ItemIsEnabled);
    itm->setTextColor(0,QColor(_col,_col,_col,_alpha));

    pathToItem.append("/");
    QDir dir(pathToItem);
    dir.setFilter(QDir::Dirs);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "Loc_*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        QString fileName = fileInfo.fileName();
        fileName.replace(0, 4, "");

        QString fullPath = pathToItem + "/" + fileInfo.fileName();
        AddLocation(itm,fileName,fullPath);
    }

}

void DataTransfer::AddLocation(QTreeWidgetItem *project, QString name, QString pathToItem)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemLocation);
    //itm->ItemType(ItemLocation);
    QIcon icon(":/icons/win8/PNG/Maps_and_Geolocation/point_objects/point_objects-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    project->addChild(itm);
    itm->setExpanded(true);
    itm->setFlags(Qt::ItemIsEnabled);
    itm->setTextColor(0,QColor(_col,_col,_col,_alpha));

    pathToItem.append("/");
    QDir dir(pathToItem);
    qDebug() << pathToItem;
    dir.setFilter(QDir::Dirs);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "Dev_*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        QString fileName = fileInfo.fileName();
        fileName.replace(0, 4, "");

        QString fullPath = pathToItem + "/" + fileInfo.fileName();
        AddDevice(itm,fileName,fullPath);
    }
}

void DataTransfer::AddDevice(QTreeWidgetItem *location, QString name, QString pathToItem){
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemDevice);
    QIcon icon(":/icons/win8/PNG/Industry/electronics/electronics-256.png");
    itm->setText(0,name);
    itm->setIcon(0,icon);
    itm->setSizeHint(0,QSize(20,20));
    location->addChild(itm);
    itm->setExpanded(true);

    pathToItem.append("/");
    QDir dir(pathToItem);
    qDebug() << pathToItem;
    dir.setFilter(QDir::Files);
    dir.setSorting(QDir::Size);
    QStringList filters;
    filters << "*.log*";
    dir.setNameFilters(filters);
    QFileInfoList list = dir.entryInfoList();
    for (int j = 0; j < list.size(); ++j) {
        QFileInfo fileInfo = list.at(j);
        AddMeasurementFile(itm,fileInfo.fileName());
    }
}

void DataTransfer::AddMeasurementFile(QTreeWidgetItem *device, QString name)
{
    QTreeWidgetItem *itm = new QTreeWidgetItem(ItemMeasurement);
    itm->setText(0,name);
    itm->setSizeHint(0,QSize(20,20));
    device->addChild(itm);
    itm->setExpanded(true);
    itm->setFlags(Qt::ItemIsDragEnabled);
    itm->setTextColor(0,QColor(_col,_col,_col,_alpha));
}

void DataTransfer::dataTransferSelectionChanged()
{
    //ui->treeWidgetPC->currentItem()->is
    QList<QTreeWidgetItem*> selectedItemsPC = ui->treeWidgetPC->selectedItems();
    QList<QTreeWidgetItem*> selectedItemsDevice = ui->treeWidgetDevice->selectedItems();

    if (selectedItemsPC.isEmpty() | selectedItemsDevice.isEmpty()){
        ui->btnRight->setDisabled(true);
    }else{
        ui->btnRight->setDisabled(false);
    }
}

void DataTransfer::on_btnRight_clicked()
{
    QList<QTreeWidgetItem*> selectedItemsPC = ui->treeWidgetPC->selectedItems();
    QList<QTreeWidgetItem*> selectedItemsDevice = ui->treeWidgetDevice->selectedItems();

    AddMeasurementFile(selectedItemsPC.first(),selectedItemsDevice.first()->text(0));

    QString pathPCLocation;
    pathPCLocation = initialPathPC +
            "/Proj_" + selectedItemsPC.first()->parent()->parent()->text(0) +
            "/Loc_" + selectedItemsPC.first()->parent()->text(0) +
            "/Dev_" + selectedItemsPC.first()->text(0) + "/";
    QString pathDeviceFile;
    pathDeviceFile = initialPathDevice +
            "/" + selectedItemsDevice.first()->parent()->text(0) +
            "/" + selectedItemsDevice.first()->text(0);

    QFile fromFile(pathDeviceFile);
    QString pathPCFileLocation = pathPCLocation+selectedItemsDevice.first()->text(0);
    QFile toFile(pathPCFileLocation);
    qint64 nCopySize = fromFile.size();
    if (nCopySize>1000){
        if (toFile.exists()) toFile.remove();
        Dialogwait *progressDialog;
        progressDialog = new Dialogwait(this);
        progressDialog->show();

        fromFile.open(QIODevice::ReadOnly);
        toFile.open(QIODevice::WriteOnly|QIODevice::Truncate);
        progressDialog->setMin(0);
        progressDialog->setMax(nCopySize);
        for (qint64 i = 0; i < nCopySize; i+=512) {
            toFile.write(fromFile.read(i)); // write a byte
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
            fromFile.seek(i);  // move to next byte to read
            toFile.seek(i); // move to next byte to write
            progressDialog->setValue((int)(i));
            QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        }
        fromFile.close();
        toFile.close();
        progressDialog->setText("Copying data...");
        progressDialog->startWait();
        QCoreApplication::processEvents( QEventLoop::AllEvents, 1000 );
        progressDialog->stopWait();
    }else{
        toFile.copy(pathDeviceFile,pathPCFileLocation);
    }


    // Making ini file alongside each copyied log file

    // fetch first and last timestep from file
    QString startTime; QString startDate;
    QString endTime; QString endDate;
    if(!toFile.open(QIODevice::ReadOnly)) {
        QMessageBox::information(0, "error", toFile.errorString());
    }
    QTextStream in(&toFile);
    long int numLine = 0;
    while(!in.atEnd()) {
        QString line = in.readLine();
        QStringList fields = line.split(";");
        numLine++;
        if (numLine == 2){
            startTime=fields.at(0);
            startDate=fields.at(1);
        }
        endTime=fields.at(0);
        endDate=fields.at(1);
    }
    toFile.close();

    // store saving time
    QDateTime dateTime = QDateTime::currentDateTime();
    QString dateTimeString = dateTime.toString("hh:mm:ss:zzz; dd/mm/yy"); // DORADITI ZAPIS
    QString pathPCFileLocationIni = pathPCFileLocation.replace(pathPCFileLocation.length()-4,4,".ini");
    QSettings* settings = new QSettings(pathPCFileLocationIni, QSettings::IniFormat);

    settings->setValue("SerialNumber", SerialNumber);
    settings->setValue("Identifier", Identifier);
    settings->setValue("SavingTimeDate", dateTimeString);

    if (numLine > 2){
        settings->setValue("StartTimeDate",startTime + "; " + startDate);
        settings->setValue("EndTimeDate",endTime + "; " + endDate);
    }

//    QSettings preferences(fileInfo.filePath(),QSettings::IniFormat);
//    QString Name=preferences.value("Name").toString();
//    QString Comment=preferences.value("Comment").toString();

}
