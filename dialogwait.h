#ifndef DIALOGWAIT_H
#define DIALOGWAIT_H

#include <QDialog>
#include <QTimer>

namespace Ui {
class Dialogwait;
}

class Dialogwait : public QDialog
{
    Q_OBJECT

public:
    explicit Dialogwait(QWidget *parent = 0);
    ~Dialogwait();
    void startWait();
    void stopWait();
    void setValue(int value);
    void setMax(int value);
    void setMin(int value);

    void setText(QString text);
private slots:
    void timeout();

private:
    Ui::Dialogwait *ui;

    QTimer *waitTimer;
    int counter;
};

#endif // DIALOGWAIT_H
