/********************************************************************************
** Form generated from reading UI file 'loggerconfRS232_USB.ui'
**
** Created by: Qt User Interface Compiler version 5.4.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGGERCONFRS232_USB_H
#define UI_LOGGERCONFRS232_USB_H

#include "qcustomplot.h"
#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QCheckBox>
#include <QtWidgets/QDateTimeEdit>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QGroupBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QListView>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QSplitter>
#include <QtWidgets/QTabWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTextEdit>
#include <QtWidgets/QToolBox>
#include <QtWidgets/QTreeView>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoggerConfRS232_USB
{
public:
    QVBoxLayout *verticalLayout_5;
    QTabWidget *moduleOptions;
    QWidget *tab_2;
    QVBoxLayout *verticalLayout_2;
    QTreeWidget *InfoTreeWidget;
    QWidget *tab_3;
    QVBoxLayout *verticalLayout_7;
    QHBoxLayout *horizontalLayout_5;
    QVBoxLayout *verticalLayout_3;
    QHBoxLayout *horizontalLayout_3;
    QLabel *label;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_2;
    QTreeView *dirTree;
    QVBoxLayout *verticalLayout_4;
    QHBoxLayout *horizontalLayout_4;
    QLabel *label_3;
    QLabel *label_4;
    QSpacerItem *horizontalSpacer_3;
    QListView *fileTree;
    QTabWidget *plottingTab;
    QWidget *tab;
    QVBoxLayout *verticalLayout_8;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_6;
    QPushButton *Realtime_START;
    QPushButton *Realtime_STOP;
    QCheckBox *Sensor_Checkbox;
    QCheckBox *Battery_Checkbox;
    QSpacerItem *horizontalSpacer_4;
    QLabel *label_5;
    QCustomPlot *realtimeWidget;
    QWidget *tab_4;
    QHBoxLayout *horizontalLayout;
    QToolBox *toolBox;
    QWidget *page;
    QGridLayout *gridLayout;
    QPushButton *btnReadSens;
    QPushButton *btnReadBatt;
    QPushButton *btnSDDetect;
    QPushButton *btnExtLedOn;
    QPushButton *btnChargerStat;
    QPushButton *btnOpaPowOn;
    QPushButton *btnIntLedOff;
    QPushButton *btnExtLedOff;
    QPushButton *BtnMemPowOff;
    QPushButton *btnSensPowOff;
    QPushButton *btnOpaPowOff;
    QPushButton *btnIntLedOn;
    QPushButton *btnChargerOn;
    QPushButton *btnSensPowOn;
    QPushButton *btnMemPowOn;
    QPushButton *btnChargerOff;
    QWidget *page_5;
    QVBoxLayout *verticalLayout_9;
    QSplitter *splitter_3;
    QPushButton *btnModemPowOn;
    QPushButton *BtnModemPowOff;
    QPushButton *btnSimStatus;
    QSpacerItem *verticalSpacer_8;
    QSplitter *splitter_4;
    QLineEdit *tbxModemSimPin;
    QPushButton *btnSimPIN;
    QSpacerItem *verticalSpacer_2;
    QSplitter *splitter_6;
    QPushButton *btnModemLogin;
    QPushButton *btnModemLogout;
    QSpacerItem *verticalSpacer_9;
    QSplitter *splitter_5;
    QLineEdit *tbxModemCommand;
    QPushButton *btnModemSend;
    QSpacerItem *verticalSpacer_3;
    QWidget *page_4;
    QVBoxLayout *verticalLayout_6;
    QGridLayout *gridLayout_3;
    QPushButton *btnSendCommand;
    QPushButton *btnCustomTimeSet;
    QDateTimeEdit *dateTimeEdit;
    QLineEdit *customCommand;
    QSpacerItem *verticalSpacer;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout_11;
    QCheckBox *autoclearCheckBox;
    QSpacerItem *horizontalSpacer;
    QPushButton *clearTerminalBrowser;
    QTextEdit *terminalBrowser;
    QWidget *tab_5;
    QVBoxLayout *verticalLayout_10;
    QHBoxLayout *horizontalLayout_2;
    QLineEdit *lineEdit;
    QPushButton *pushButton;
    QTextBrowser *textBrowser;

    void setupUi(QWidget *LoggerConfRS232_USB)
    {
        if (LoggerConfRS232_USB->objectName().isEmpty())
            LoggerConfRS232_USB->setObjectName(QStringLiteral("LoggerConfRS232_USB"));
        LoggerConfRS232_USB->resize(656, 706);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(LoggerConfRS232_USB->sizePolicy().hasHeightForWidth());
        LoggerConfRS232_USB->setSizePolicy(sizePolicy);
        verticalLayout_5 = new QVBoxLayout(LoggerConfRS232_USB);
        verticalLayout_5->setObjectName(QStringLiteral("verticalLayout_5"));
        moduleOptions = new QTabWidget(LoggerConfRS232_USB);
        moduleOptions->setObjectName(QStringLiteral("moduleOptions"));
        moduleOptions->setEnabled(true);
        QFont font;
        font.setPointSize(9);
        moduleOptions->setFont(font);
        moduleOptions->setAutoFillBackground(false);
        moduleOptions->setIconSize(QSize(16, 16));
        moduleOptions->setElideMode(Qt::ElideNone);
        moduleOptions->setDocumentMode(false);
        moduleOptions->setTabsClosable(false);
        tab_2 = new QWidget();
        tab_2->setObjectName(QStringLiteral("tab_2"));
        verticalLayout_2 = new QVBoxLayout(tab_2);
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        InfoTreeWidget = new QTreeWidget(tab_2);
        QFont font1;
        font1.setBold(true);
        font1.setWeight(75);
        QTreeWidgetItem *__qtreewidgetitem = new QTreeWidgetItem();
        __qtreewidgetitem->setFont(1, font1);
        InfoTreeWidget->setHeaderItem(__qtreewidgetitem);
        QIcon icon;
        icon.addFile(QStringLiteral(":/icons/win8/PNG/Sections_of_Website/about/about-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QFont font2;
        font2.setPointSize(12);
        QIcon icon1;
        icon1.addFile(QStringLiteral(":/icons/win8/PNG/Text_Formatting/rename/rename-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon2;
        icon2.addFile(QStringLiteral(":/icons/win8/PNG/Google_Services/google_code/google_code-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon3;
        icon3.addFile(QStringLiteral(":/icons/win8/PNG/Payment_Methods/barcode/barcode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon4;
        icon4.addFile(QStringLiteral(":/icons/win8/PNG/Microsoft/system_report/system_report-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon5;
        icon5.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/settings2/settings2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon6;
        icon6.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electrical_sensor/electrical_sensor-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon7;
        icon7.addFile(QStringLiteral(":/icons/win8/PNG/Industry/electricity/electricity-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon8;
        icon8.addFile(QStringLiteral(":/icons/win8/PNG/Transport/car_battery/car_battery-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon9;
        icon9.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/play/play-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon10;
        icon10.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/lock/lock-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon11;
        icon11.addFile(QStringLiteral(":/icons/win8/PNG/Industry/display/display-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon12;
        icon12.addFile(QStringLiteral(":/icons/win8/PNG/Industry/led_diode/led_diode-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon13;
        icon13.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/time/time-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon14;
        icon14.addFile(QStringLiteral(":/icons/win8/PNG/Data_Grid/Numerical_Sorting/numerical_sorting-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon15;
        icon15.addFile(QStringLiteral(":/icons/win8/PNG/Battery/almost_empty/almost_empty-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon16;
        icon16.addFile(QStringLiteral(":/icons/win8/PNG/Very_Basic/calendar/calendar-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon17;
        icon17.addFile(QStringLiteral(":/icons/win8/PNG/Measurement_Units/speed/speed-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon18;
        icon18.addFile(QStringLiteral(":/icons/win8/PNG/Mathematic/plus_minus/plus_minus-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon19;
        icon19.addFile(QStringLiteral(":/icons/win8/PNG/Network_Status/connected_no_data/connected_no_data-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon20;
        icon20.addFile(QStringLiteral(":/icons/win8/PNG/Media_Controls/stop/stop-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QIcon icon21;
        icon21.addFile(QStringLiteral(":/icons/win8/PNG/Registration/enter2/enter2-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        QTreeWidgetItem *__qtreewidgetitem1 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem1->setFont(0, font2);
        __qtreewidgetitem1->setIcon(0, icon);
        QTreeWidgetItem *__qtreewidgetitem2 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem2->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem3 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem3->setIcon(0, icon2);
        QTreeWidgetItem *__qtreewidgetitem4 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem4->setIcon(0, icon3);
        QTreeWidgetItem *__qtreewidgetitem5 = new QTreeWidgetItem(__qtreewidgetitem1);
        __qtreewidgetitem5->setIcon(0, icon4);
        QTreeWidgetItem *__qtreewidgetitem6 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem6->setFont(0, font2);
        __qtreewidgetitem6->setIcon(0, icon5);
        QTreeWidgetItem *__qtreewidgetitem7 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem7->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem8 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem8->setIcon(0, icon7);
        QTreeWidgetItem *__qtreewidgetitem9 = new QTreeWidgetItem(__qtreewidgetitem6);
        __qtreewidgetitem9->setIcon(0, icon8);
        QTreeWidgetItem *__qtreewidgetitem10 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem10->setFont(0, font2);
        __qtreewidgetitem10->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem11 = new QTreeWidgetItem(__qtreewidgetitem10);
        __qtreewidgetitem11->setIcon(0, icon9);
        QTreeWidgetItem *__qtreewidgetitem12 = new QTreeWidgetItem(__qtreewidgetitem10);
        __qtreewidgetitem12->setIcon(0, icon10);
        QTreeWidgetItem *__qtreewidgetitem13 = new QTreeWidgetItem(__qtreewidgetitem10);
        __qtreewidgetitem13->setIcon(0, icon11);
        QTreeWidgetItem *__qtreewidgetitem14 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem14->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem15 = new QTreeWidgetItem(__qtreewidgetitem14);
        __qtreewidgetitem15->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem16 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem16->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem17 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem17->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem18 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem18->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem19 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem19->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem20 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem20->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem21 = new QTreeWidgetItem(__qtreewidgetitem16);
        __qtreewidgetitem21->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem22 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem22->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem23 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem23->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem24 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem24->setIcon(0, icon13);
        QTreeWidgetItem *__qtreewidgetitem25 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem25->setIcon(0, icon6);
        QTreeWidgetItem *__qtreewidgetitem26 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem26->setIcon(0, icon1);
        QTreeWidgetItem *__qtreewidgetitem27 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem27->setIcon(0, icon16);
        QTreeWidgetItem *__qtreewidgetitem28 = new QTreeWidgetItem(__qtreewidgetitem22);
        __qtreewidgetitem28->setIcon(0, icon15);
        QTreeWidgetItem *__qtreewidgetitem29 = new QTreeWidgetItem(InfoTreeWidget);
        __qtreewidgetitem29->setFont(0, font2);
        QTreeWidgetItem *__qtreewidgetitem30 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem30->setIcon(0, icon12);
        QTreeWidgetItem *__qtreewidgetitem31 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem31->setIcon(0, icon17);
        QTreeWidgetItem *__qtreewidgetitem32 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem32->setIcon(0, icon18);
        QTreeWidgetItem *__qtreewidgetitem33 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem33->setIcon(0, icon19);
        QTreeWidgetItem *__qtreewidgetitem34 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem34->setIcon(0, icon20);
        QTreeWidgetItem *__qtreewidgetitem35 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem35->setIcon(0, icon21);
        QTreeWidgetItem *__qtreewidgetitem36 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem36->setIcon(0, icon14);
        QTreeWidgetItem *__qtreewidgetitem37 = new QTreeWidgetItem(__qtreewidgetitem29);
        __qtreewidgetitem37->setIcon(0, icon15);
        InfoTreeWidget->setObjectName(QStringLiteral("InfoTreeWidget"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(InfoTreeWidget->sizePolicy().hasHeightForWidth());
        InfoTreeWidget->setSizePolicy(sizePolicy1);
        InfoTreeWidget->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        InfoTreeWidget->setEditTriggers(QAbstractItemView::NoEditTriggers);
        InfoTreeWidget->setAlternatingRowColors(true);
        InfoTreeWidget->setAutoExpandDelay(-1);
        InfoTreeWidget->setUniformRowHeights(false);
        InfoTreeWidget->setAnimated(true);
        InfoTreeWidget->setWordWrap(true);
        InfoTreeWidget->setHeaderHidden(true);
        InfoTreeWidget->header()->setVisible(false);
        InfoTreeWidget->header()->setCascadingSectionResizes(false);
        InfoTreeWidget->header()->setHighlightSections(false);
        InfoTreeWidget->header()->setProperty("showSortIndicator", QVariant(false));
        InfoTreeWidget->header()->setStretchLastSection(true);

        verticalLayout_2->addWidget(InfoTreeWidget);

        moduleOptions->addTab(tab_2, QString());
        tab_3 = new QWidget();
        tab_3->setObjectName(QStringLiteral("tab_3"));
        verticalLayout_7 = new QVBoxLayout(tab_3);
        verticalLayout_7->setObjectName(QStringLiteral("verticalLayout_7"));
        horizontalLayout_5 = new QHBoxLayout();
        horizontalLayout_5->setObjectName(QStringLiteral("horizontalLayout_5"));
        verticalLayout_3 = new QVBoxLayout();
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QStringLiteral("horizontalLayout_3"));
        label = new QLabel(tab_3);
        label->setObjectName(QStringLiteral("label"));
        label->setMaximumSize(QSize(30, 30));
        label->setFrameShape(QFrame::NoFrame);
        label->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/folder/folder-256.png")));
        label->setScaledContents(true);

        horizontalLayout_3->addWidget(label);

        label_2 = new QLabel(tab_3);
        label_2->setObjectName(QStringLiteral("label_2"));

        horizontalLayout_3->addWidget(label_2);

        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_2);


        verticalLayout_3->addLayout(horizontalLayout_3);

        dirTree = new QTreeView(tab_3);
        dirTree->setObjectName(QStringLiteral("dirTree"));

        verticalLayout_3->addWidget(dirTree);


        horizontalLayout_5->addLayout(verticalLayout_3);

        verticalLayout_4 = new QVBoxLayout();
        verticalLayout_4->setObjectName(QStringLiteral("verticalLayout_4"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QStringLiteral("horizontalLayout_4"));
        label_3 = new QLabel(tab_3);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setMaximumSize(QSize(30, 30));
        label_3->setFrameShape(QFrame::NoFrame);
        label_3->setPixmap(QPixmap(QString::fromUtf8(":/icons/win8/PNG/Very_Basic/document/document-256.png")));
        label_3->setScaledContents(true);

        horizontalLayout_4->addWidget(label_3);

        label_4 = new QLabel(tab_3);
        label_4->setObjectName(QStringLiteral("label_4"));

        horizontalLayout_4->addWidget(label_4);

        horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Minimum, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer_3);


        verticalLayout_4->addLayout(horizontalLayout_4);

        fileTree = new QListView(tab_3);
        fileTree->setObjectName(QStringLiteral("fileTree"));
        fileTree->setMinimumSize(QSize(0, 0));
        fileTree->setAlternatingRowColors(true);
        fileTree->setSelectionRectVisible(true);

        verticalLayout_4->addWidget(fileTree);


        horizontalLayout_5->addLayout(verticalLayout_4);


        verticalLayout_7->addLayout(horizontalLayout_5);

        plottingTab = new QTabWidget(tab_3);
        plottingTab->setObjectName(QStringLiteral("plottingTab"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::MinimumExpanding);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(plottingTab->sizePolicy().hasHeightForWidth());
        plottingTab->setSizePolicy(sizePolicy2);
        plottingTab->setMinimumSize(QSize(0, 400));
        plottingTab->setTabPosition(QTabWidget::South);
        plottingTab->setTabShape(QTabWidget::Rounded);
        plottingTab->setElideMode(Qt::ElideNone);
        plottingTab->setDocumentMode(false);
        plottingTab->setTabsClosable(true);
        plottingTab->setMovable(true);

        verticalLayout_7->addWidget(plottingTab);

        moduleOptions->addTab(tab_3, QString());
        tab = new QWidget();
        tab->setObjectName(QStringLiteral("tab"));
        verticalLayout_8 = new QVBoxLayout(tab);
        verticalLayout_8->setObjectName(QStringLiteral("verticalLayout_8"));
        groupBox_3 = new QGroupBox(tab);
        groupBox_3->setObjectName(QStringLiteral("groupBox_3"));
        horizontalLayout_6 = new QHBoxLayout(groupBox_3);
        horizontalLayout_6->setObjectName(QStringLiteral("horizontalLayout_6"));
        Realtime_START = new QPushButton(groupBox_3);
        Realtime_START->setObjectName(QStringLiteral("Realtime_START"));
        Realtime_START->setIcon(icon9);
        Realtime_START->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_START);

        Realtime_STOP = new QPushButton(groupBox_3);
        Realtime_STOP->setObjectName(QStringLiteral("Realtime_STOP"));
        Realtime_STOP->setIcon(icon20);
        Realtime_STOP->setIconSize(QSize(32, 32));

        horizontalLayout_6->addWidget(Realtime_STOP);

        Sensor_Checkbox = new QCheckBox(groupBox_3);
        Sensor_Checkbox->setObjectName(QStringLiteral("Sensor_Checkbox"));
        QFont font3;
        font3.setPointSize(12);
        font3.setBold(true);
        font3.setWeight(75);
        Sensor_Checkbox->setFont(font3);

        horizontalLayout_6->addWidget(Sensor_Checkbox);

        Battery_Checkbox = new QCheckBox(groupBox_3);
        Battery_Checkbox->setObjectName(QStringLiteral("Battery_Checkbox"));
        Battery_Checkbox->setFont(font3);

        horizontalLayout_6->addWidget(Battery_Checkbox);

        horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_6->addItem(horizontalSpacer_4);

        label_5 = new QLabel(groupBox_3);
        label_5->setObjectName(QStringLiteral("label_5"));
        QFont font4;
        font4.setPointSize(14);
        label_5->setFont(font4);

        horizontalLayout_6->addWidget(label_5);


        verticalLayout_8->addWidget(groupBox_3);

        realtimeWidget = new QCustomPlot(tab);
        realtimeWidget->setObjectName(QStringLiteral("realtimeWidget"));
        QSizePolicy sizePolicy3(QSizePolicy::Preferred, QSizePolicy::MinimumExpanding);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(realtimeWidget->sizePolicy().hasHeightForWidth());
        realtimeWidget->setSizePolicy(sizePolicy3);

        verticalLayout_8->addWidget(realtimeWidget);

        moduleOptions->addTab(tab, QString());
        tab_4 = new QWidget();
        tab_4->setObjectName(QStringLiteral("tab_4"));
        horizontalLayout = new QHBoxLayout(tab_4);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        toolBox = new QToolBox(tab_4);
        toolBox->setObjectName(QStringLiteral("toolBox"));
        page = new QWidget();
        page->setObjectName(QStringLiteral("page"));
        page->setGeometry(QRect(0, 0, 304, 553));
        gridLayout = new QGridLayout(page);
        gridLayout->setObjectName(QStringLiteral("gridLayout"));
        btnReadSens = new QPushButton(page);
        btnReadSens->setObjectName(QStringLiteral("btnReadSens"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Expanding);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(btnReadSens->sizePolicy().hasHeightForWidth());
        btnReadSens->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnReadSens, 7, 0, 1, 1);

        btnReadBatt = new QPushButton(page);
        btnReadBatt->setObjectName(QStringLiteral("btnReadBatt"));
        sizePolicy4.setHeightForWidth(btnReadBatt->sizePolicy().hasHeightForWidth());
        btnReadBatt->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnReadBatt, 6, 0, 1, 1);

        btnSDDetect = new QPushButton(page);
        btnSDDetect->setObjectName(QStringLiteral("btnSDDetect"));
        sizePolicy4.setHeightForWidth(btnSDDetect->sizePolicy().hasHeightForWidth());
        btnSDDetect->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnSDDetect, 7, 1, 1, 1);

        btnExtLedOn = new QPushButton(page);
        btnExtLedOn->setObjectName(QStringLiteral("btnExtLedOn"));
        sizePolicy4.setHeightForWidth(btnExtLedOn->sizePolicy().hasHeightForWidth());
        btnExtLedOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnExtLedOn, 4, 0, 1, 1);

        btnChargerStat = new QPushButton(page);
        btnChargerStat->setObjectName(QStringLiteral("btnChargerStat"));
        sizePolicy4.setHeightForWidth(btnChargerStat->sizePolicy().hasHeightForWidth());
        btnChargerStat->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnChargerStat, 6, 1, 1, 1);

        btnOpaPowOn = new QPushButton(page);
        btnOpaPowOn->setObjectName(QStringLiteral("btnOpaPowOn"));
        sizePolicy4.setHeightForWidth(btnOpaPowOn->sizePolicy().hasHeightForWidth());
        btnOpaPowOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnOpaPowOn, 2, 0, 1, 1);

        btnIntLedOff = new QPushButton(page);
        btnIntLedOff->setObjectName(QStringLiteral("btnIntLedOff"));
        sizePolicy4.setHeightForWidth(btnIntLedOff->sizePolicy().hasHeightForWidth());
        btnIntLedOff->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnIntLedOff, 3, 1, 1, 1);

        btnExtLedOff = new QPushButton(page);
        btnExtLedOff->setObjectName(QStringLiteral("btnExtLedOff"));
        sizePolicy4.setHeightForWidth(btnExtLedOff->sizePolicy().hasHeightForWidth());
        btnExtLedOff->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnExtLedOff, 4, 1, 1, 1);

        BtnMemPowOff = new QPushButton(page);
        BtnMemPowOff->setObjectName(QStringLiteral("BtnMemPowOff"));
        sizePolicy4.setHeightForWidth(BtnMemPowOff->sizePolicy().hasHeightForWidth());
        BtnMemPowOff->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(BtnMemPowOff, 0, 1, 1, 1);

        btnSensPowOff = new QPushButton(page);
        btnSensPowOff->setObjectName(QStringLiteral("btnSensPowOff"));
        sizePolicy4.setHeightForWidth(btnSensPowOff->sizePolicy().hasHeightForWidth());
        btnSensPowOff->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnSensPowOff, 1, 1, 1, 1);

        btnOpaPowOff = new QPushButton(page);
        btnOpaPowOff->setObjectName(QStringLiteral("btnOpaPowOff"));
        sizePolicy4.setHeightForWidth(btnOpaPowOff->sizePolicy().hasHeightForWidth());
        btnOpaPowOff->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnOpaPowOff, 2, 1, 1, 1);

        btnIntLedOn = new QPushButton(page);
        btnIntLedOn->setObjectName(QStringLiteral("btnIntLedOn"));
        sizePolicy4.setHeightForWidth(btnIntLedOn->sizePolicy().hasHeightForWidth());
        btnIntLedOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnIntLedOn, 3, 0, 1, 1);

        btnChargerOn = new QPushButton(page);
        btnChargerOn->setObjectName(QStringLiteral("btnChargerOn"));
        sizePolicy4.setHeightForWidth(btnChargerOn->sizePolicy().hasHeightForWidth());
        btnChargerOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnChargerOn, 5, 1, 1, 1);

        btnSensPowOn = new QPushButton(page);
        btnSensPowOn->setObjectName(QStringLiteral("btnSensPowOn"));
        sizePolicy4.setHeightForWidth(btnSensPowOn->sizePolicy().hasHeightForWidth());
        btnSensPowOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnSensPowOn, 1, 0, 1, 1);

        btnMemPowOn = new QPushButton(page);
        btnMemPowOn->setObjectName(QStringLiteral("btnMemPowOn"));
        sizePolicy4.setHeightForWidth(btnMemPowOn->sizePolicy().hasHeightForWidth());
        btnMemPowOn->setSizePolicy(sizePolicy4);

        gridLayout->addWidget(btnMemPowOn, 0, 0, 1, 1);

        btnChargerOff = new QPushButton(page);
        btnChargerOff->setObjectName(QStringLiteral("btnChargerOff"));
        sizePolicy4.setHeightForWidth(btnChargerOff->sizePolicy().hasHeightForWidth());
        btnChargerOff->setSizePolicy(sizePolicy4);
        btnChargerOff->setAutoFillBackground(false);

        gridLayout->addWidget(btnChargerOff, 5, 0, 1, 1);

        QIcon icon22;
        icon22.addFile(QStringLiteral(":/icons/win8/PNG/System/settings/settings-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page, icon22, QStringLiteral("General"));
        page_5 = new QWidget();
        page_5->setObjectName(QStringLiteral("page_5"));
        page_5->setGeometry(QRect(0, 0, 256, 272));
        verticalLayout_9 = new QVBoxLayout(page_5);
        verticalLayout_9->setObjectName(QStringLiteral("verticalLayout_9"));
        splitter_3 = new QSplitter(page_5);
        splitter_3->setObjectName(QStringLiteral("splitter_3"));
        splitter_3->setOrientation(Qt::Horizontal);
        btnModemPowOn = new QPushButton(splitter_3);
        btnModemPowOn->setObjectName(QStringLiteral("btnModemPowOn"));
        sizePolicy4.setHeightForWidth(btnModemPowOn->sizePolicy().hasHeightForWidth());
        btnModemPowOn->setSizePolicy(sizePolicy4);
        splitter_3->addWidget(btnModemPowOn);
        BtnModemPowOff = new QPushButton(splitter_3);
        BtnModemPowOff->setObjectName(QStringLiteral("BtnModemPowOff"));
        splitter_3->addWidget(BtnModemPowOff);
        btnSimStatus = new QPushButton(splitter_3);
        btnSimStatus->setObjectName(QStringLiteral("btnSimStatus"));
        splitter_3->addWidget(btnSimStatus);

        verticalLayout_9->addWidget(splitter_3);

        verticalSpacer_8 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_8);

        splitter_4 = new QSplitter(page_5);
        splitter_4->setObjectName(QStringLiteral("splitter_4"));
        splitter_4->setOrientation(Qt::Horizontal);
        tbxModemSimPin = new QLineEdit(splitter_4);
        tbxModemSimPin->setObjectName(QStringLiteral("tbxModemSimPin"));
        splitter_4->addWidget(tbxModemSimPin);
        btnSimPIN = new QPushButton(splitter_4);
        btnSimPIN->setObjectName(QStringLiteral("btnSimPIN"));
        QSizePolicy sizePolicy5(QSizePolicy::Minimum, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(btnSimPIN->sizePolicy().hasHeightForWidth());
        btnSimPIN->setSizePolicy(sizePolicy5);
        splitter_4->addWidget(btnSimPIN);

        verticalLayout_9->addWidget(splitter_4);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_2);

        splitter_6 = new QSplitter(page_5);
        splitter_6->setObjectName(QStringLiteral("splitter_6"));
        splitter_6->setOrientation(Qt::Horizontal);
        btnModemLogin = new QPushButton(splitter_6);
        btnModemLogin->setObjectName(QStringLiteral("btnModemLogin"));
        QSizePolicy sizePolicy6(QSizePolicy::Minimum, QSizePolicy::MinimumExpanding);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(btnModemLogin->sizePolicy().hasHeightForWidth());
        btnModemLogin->setSizePolicy(sizePolicy6);
        btnModemLogin->setCheckable(false);
        splitter_6->addWidget(btnModemLogin);
        btnModemLogout = new QPushButton(splitter_6);
        btnModemLogout->setObjectName(QStringLiteral("btnModemLogout"));
        splitter_6->addWidget(btnModemLogout);

        verticalLayout_9->addWidget(splitter_6);

        verticalSpacer_9 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Fixed);

        verticalLayout_9->addItem(verticalSpacer_9);

        splitter_5 = new QSplitter(page_5);
        splitter_5->setObjectName(QStringLiteral("splitter_5"));
        splitter_5->setOrientation(Qt::Horizontal);
        tbxModemCommand = new QLineEdit(splitter_5);
        tbxModemCommand->setObjectName(QStringLiteral("tbxModemCommand"));
        splitter_5->addWidget(tbxModemCommand);
        btnModemSend = new QPushButton(splitter_5);
        btnModemSend->setObjectName(QStringLiteral("btnModemSend"));
        splitter_5->addWidget(btnModemSend);

        verticalLayout_9->addWidget(splitter_5);

        verticalSpacer_3 = new QSpacerItem(20, 63, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_3);

        QIcon icon23;
        icon23.addFile(QStringLiteral(":/icons/win8/PNG/Ethernet/rj45/rj45-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_5, icon23, QStringLiteral("Modem"));
        page_4 = new QWidget();
        page_4->setObjectName(QStringLiteral("page_4"));
        page_4->setGeometry(QRect(0, 0, 270, 78));
        verticalLayout_6 = new QVBoxLayout(page_4);
        verticalLayout_6->setObjectName(QStringLiteral("verticalLayout_6"));
        gridLayout_3 = new QGridLayout();
        gridLayout_3->setObjectName(QStringLiteral("gridLayout_3"));
        btnSendCommand = new QPushButton(page_4);
        btnSendCommand->setObjectName(QStringLiteral("btnSendCommand"));
        sizePolicy4.setHeightForWidth(btnSendCommand->sizePolicy().hasHeightForWidth());
        btnSendCommand->setSizePolicy(sizePolicy4);

        gridLayout_3->addWidget(btnSendCommand, 0, 1, 1, 1);

        btnCustomTimeSet = new QPushButton(page_4);
        btnCustomTimeSet->setObjectName(QStringLiteral("btnCustomTimeSet"));
        sizePolicy4.setHeightForWidth(btnCustomTimeSet->sizePolicy().hasHeightForWidth());
        btnCustomTimeSet->setSizePolicy(sizePolicy4);

        gridLayout_3->addWidget(btnCustomTimeSet, 1, 1, 1, 1);

        dateTimeEdit = new QDateTimeEdit(page_4);
        dateTimeEdit->setObjectName(QStringLiteral("dateTimeEdit"));
        sizePolicy4.setHeightForWidth(dateTimeEdit->sizePolicy().hasHeightForWidth());
        dateTimeEdit->setSizePolicy(sizePolicy4);
        dateTimeEdit->setButtonSymbols(QAbstractSpinBox::UpDownArrows);
        dateTimeEdit->setProperty("showGroupSeparator", QVariant(false));
        dateTimeEdit->setCalendarPopup(true);

        gridLayout_3->addWidget(dateTimeEdit, 1, 0, 1, 1);

        customCommand = new QLineEdit(page_4);
        customCommand->setObjectName(QStringLiteral("customCommand"));
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(customCommand->sizePolicy().hasHeightForWidth());
        customCommand->setSizePolicy(sizePolicy7);

        gridLayout_3->addWidget(customCommand, 0, 0, 1, 1);


        verticalLayout_6->addLayout(gridLayout_3);

        verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer);

        QIcon icon24;
        icon24.addFile(QStringLiteral(":/icons/win8/PNG/System/help/help-256.png"), QSize(), QIcon::Normal, QIcon::Off);
        toolBox->addItem(page_4, icon24, QStringLiteral("Other"));

        horizontalLayout->addWidget(toolBox);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setObjectName(QStringLiteral("horizontalLayout_11"));
        autoclearCheckBox = new QCheckBox(tab_4);
        autoclearCheckBox->setObjectName(QStringLiteral("autoclearCheckBox"));
        autoclearCheckBox->setChecked(true);

        horizontalLayout_11->addWidget(autoclearCheckBox);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_11->addItem(horizontalSpacer);

        clearTerminalBrowser = new QPushButton(tab_4);
        clearTerminalBrowser->setObjectName(QStringLiteral("clearTerminalBrowser"));

        horizontalLayout_11->addWidget(clearTerminalBrowser);


        verticalLayout->addLayout(horizontalLayout_11);

        terminalBrowser = new QTextEdit(tab_4);
        terminalBrowser->setObjectName(QStringLiteral("terminalBrowser"));

        verticalLayout->addWidget(terminalBrowser);


        horizontalLayout->addLayout(verticalLayout);

        moduleOptions->addTab(tab_4, QString());
        tab_5 = new QWidget();
        tab_5->setObjectName(QStringLiteral("tab_5"));
        verticalLayout_10 = new QVBoxLayout(tab_5);
        verticalLayout_10->setObjectName(QStringLiteral("verticalLayout_10"));
        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
        lineEdit = new QLineEdit(tab_5);
        lineEdit->setObjectName(QStringLiteral("lineEdit"));

        horizontalLayout_2->addWidget(lineEdit);

        pushButton = new QPushButton(tab_5);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout_2->addWidget(pushButton);


        verticalLayout_10->addLayout(horizontalLayout_2);

        textBrowser = new QTextBrowser(tab_5);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_10->addWidget(textBrowser);

        moduleOptions->addTab(tab_5, QString());

        verticalLayout_5->addWidget(moduleOptions);


        retranslateUi(LoggerConfRS232_USB);

        moduleOptions->setCurrentIndex(0);
        toolBox->setCurrentIndex(0);


        QMetaObject::connectSlotsByName(LoggerConfRS232_USB);
    } // setupUi

    void retranslateUi(QWidget *LoggerConfRS232_USB)
    {
        LoggerConfRS232_USB->setWindowTitle(QApplication::translate("LoggerConfRS232_USB", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = InfoTreeWidget->headerItem();
        ___qtreewidgetitem->setText(1, QApplication::translate("LoggerConfRS232_USB", "Value", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("LoggerConfRS232_USB", "Property", 0));

        const bool __sortingEnabled = InfoTreeWidget->isSortingEnabled();
        InfoTreeWidget->setSortingEnabled(false);
        QTreeWidgetItem *___qtreewidgetitem1 = InfoTreeWidget->topLevelItem(0);
        ___qtreewidgetitem1->setText(0, QApplication::translate("LoggerConfRS232_USB", "General", 0));
        QTreeWidgetItem *___qtreewidgetitem2 = ___qtreewidgetitem1->child(0);
        ___qtreewidgetitem2->setText(1, QApplication::translate("LoggerConfRS232_USB", "IDN STRING 0 10 RW", 0));
        ___qtreewidgetitem2->setText(0, QApplication::translate("LoggerConfRS232_USB", "Identifier", 0));
        QTreeWidgetItem *___qtreewidgetitem3 = ___qtreewidgetitem1->child(1);
        ___qtreewidgetitem3->setText(1, QApplication::translate("LoggerConfRS232_USB", "FIRM STRING 0 10 R", 0));
        ___qtreewidgetitem3->setText(0, QApplication::translate("LoggerConfRS232_USB", "Firmware", 0));
        QTreeWidgetItem *___qtreewidgetitem4 = ___qtreewidgetitem1->child(2);
        ___qtreewidgetitem4->setText(1, QApplication::translate("LoggerConfRS232_USB", "HIDE", 0));
        ___qtreewidgetitem4->setText(0, QApplication::translate("LoggerConfRS232_USB", "ID", 0));
        QTreeWidgetItem *___qtreewidgetitem5 = ___qtreewidgetitem1->child(3);
        ___qtreewidgetitem5->setText(1, QApplication::translate("LoggerConfRS232_USB", "SRI UINT16 100 10000 RW", 0));
        ___qtreewidgetitem5->setText(0, QApplication::translate("LoggerConfRS232_USB", "Status report interval", 0));
        QTreeWidgetItem *___qtreewidgetitem6 = InfoTreeWidget->topLevelItem(1);
        ___qtreewidgetitem6->setText(0, QApplication::translate("LoggerConfRS232_USB", "Calibration", 0));
        QTreeWidgetItem *___qtreewidgetitem7 = ___qtreewidgetitem6->child(0);
        ___qtreewidgetitem7->setText(1, QApplication::translate("LoggerConfRS232_USB", "SCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem7->setText(0, QApplication::translate("LoggerConfRS232_USB", "Sensor calibration coefficient", 0));
        QTreeWidgetItem *___qtreewidgetitem8 = ___qtreewidgetitem6->child(1);
        ___qtreewidgetitem8->setText(1, QApplication::translate("LoggerConfRS232_USB", "CCM DOUBLE32 1000 0 500 RW", 0));
        ___qtreewidgetitem8->setText(0, QApplication::translate("LoggerConfRS232_USB", "Charging current calibration ", 0));
        QTreeWidgetItem *___qtreewidgetitem9 = ___qtreewidgetitem6->child(2);
        ___qtreewidgetitem9->setText(1, QApplication::translate("LoggerConfRS232_USB", "BAC UINT32 0 100000 RW", 0));
        ___qtreewidgetitem9->setText(0, QApplication::translate("LoggerConfRS232_USB", "Battery capacity", 0));
        QTreeWidgetItem *___qtreewidgetitem10 = InfoTreeWidget->topLevelItem(2);
        ___qtreewidgetitem10->setText(0, QApplication::translate("LoggerConfRS232_USB", "Operation", 0));
        QTreeWidgetItem *___qtreewidgetitem11 = ___qtreewidgetitem10->child(0);
        ___qtreewidgetitem11->setText(1, QApplication::translate("LoggerConfRS232_USB", "OPS ENUM Standby Continuous Interval RS232 RW", 0));
        ___qtreewidgetitem11->setText(0, QApplication::translate("LoggerConfRS232_USB", "Operation mode", 0));
        QTreeWidgetItem *___qtreewidgetitem12 = ___qtreewidgetitem10->child(1);
        ___qtreewidgetitem12->setText(1, QApplication::translate("LoggerConfRS232_USB", "LOM TF RW", 0));
        ___qtreewidgetitem12->setText(0, QApplication::translate("LoggerConfRS232_USB", "Lock operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem13 = ___qtreewidgetitem10->child(2);
        ___qtreewidgetitem13->setText(1, QApplication::translate("LoggerConfRS232_USB", "IT UINT32 1 1000000 RW", 0));
        ___qtreewidgetitem13->setText(0, QApplication::translate("LoggerConfRS232_USB", "LCD indication timeout", 0));
        QTreeWidgetItem *___qtreewidgetitem14 = InfoTreeWidget->topLevelItem(3);
        ___qtreewidgetitem14->setText(0, QApplication::translate("LoggerConfRS232_USB", "Standby mode", 0));
        QTreeWidgetItem *___qtreewidgetitem15 = ___qtreewidgetitem14->child(0);
        ___qtreewidgetitem15->setText(1, QApplication::translate("LoggerConfRS232_USB", "SBII UINT16 100 10000 RW", 0));
        ___qtreewidgetitem15->setText(0, QApplication::translate("LoggerConfRS232_USB", "Indication blinking interval", 0));
        QTreeWidgetItem *___qtreewidgetitem16 = InfoTreeWidget->topLevelItem(4);
        ___qtreewidgetitem16->setText(0, QApplication::translate("LoggerConfRS232_USB", "Continuous sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem17 = ___qtreewidgetitem16->child(0);
        ___qtreewidgetitem17->setText(1, QApplication::translate("LoggerConfRS232_USB", "CSII UINT16 100 10000 RW", 0));
        ___qtreewidgetitem17->setText(0, QApplication::translate("LoggerConfRS232_USB", "Indication blinking interval", 0));
        QTreeWidgetItem *___qtreewidgetitem18 = ___qtreewidgetitem16->child(1);
        ___qtreewidgetitem18->setText(1, QApplication::translate("LoggerConfRS232_USB", "CSI UINT32 0 1000000 RW", 0));
        ___qtreewidgetitem18->setText(0, QApplication::translate("LoggerConfRS232_USB", "Sampling interval (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem19 = ___qtreewidgetitem16->child(2);
        ___qtreewidgetitem19->setText(1, QApplication::translate("LoggerConfRS232_USB", "LSNC STRING 0 22 RW", 0));
        ___qtreewidgetitem19->setText(0, QApplication::translate("LoggerConfRS232_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem20 = ___qtreewidgetitem16->child(3);
        ___qtreewidgetitem20->setText(1, QApplication::translate("LoggerConfRS232_USB", "CSS INT32 1 10000000 RW", 0));
        ___qtreewidgetitem20->setText(0, QApplication::translate("LoggerConfRS232_USB", "Samples per file", 0));
        QTreeWidgetItem *___qtreewidgetitem21 = ___qtreewidgetitem16->child(4);
        ___qtreewidgetitem21->setText(1, QApplication::translate("LoggerConfRS232_USB", "CSB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem21->setText(0, QApplication::translate("LoggerConfRS232_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem22 = InfoTreeWidget->topLevelItem(5);
        ___qtreewidgetitem22->setText(0, QApplication::translate("LoggerConfRS232_USB", "Interval sampling operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem23 = ___qtreewidgetitem22->child(0);
        ___qtreewidgetitem23->setText(1, QApplication::translate("LoggerConfRS232_USB", "ISII UINT32 10 10000 RW", 0));
        ___qtreewidgetitem23->setText(0, QApplication::translate("LoggerConfRS232_USB", "Indication blinking interval", 0));
        QTreeWidgetItem *___qtreewidgetitem24 = ___qtreewidgetitem22->child(1);
        ___qtreewidgetitem24->setText(1, QApplication::translate("LoggerConfRS232_USB", "ISI UINT32 2 1000000 RW", 0));
        ___qtreewidgetitem24->setText(0, QApplication::translate("LoggerConfRS232_USB", "Sampling interval (seconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem25 = ___qtreewidgetitem22->child(2);
        ___qtreewidgetitem25->setText(1, QApplication::translate("LoggerConfRS232_USB", "ISSW UINT32 0 10000 RW", 0));
        ___qtreewidgetitem25->setText(0, QApplication::translate("LoggerConfRS232_USB", "Sensor wait time (miliseconds)", 0));
        QTreeWidgetItem *___qtreewidgetitem26 = ___qtreewidgetitem22->child(3);
        ___qtreewidgetitem26->setText(1, QApplication::translate("LoggerConfRS232_USB", "LSNI STRING 0 22 RW", 0));
        ___qtreewidgetitem26->setText(0, QApplication::translate("LoggerConfRS232_USB", "Folder name", 0));
        QTreeWidgetItem *___qtreewidgetitem27 = ___qtreewidgetitem22->child(4);
        ___qtreewidgetitem27->setText(1, QApplication::translate("LoggerConfRS232_USB", "LSF ENUM FILE_PER_MINUTE FILE_PER_HOUR FILE_PER_DAY FILE_PER_MONTH FILE_PER_YEAR RW", 0));
        ___qtreewidgetitem27->setText(0, QApplication::translate("LoggerConfRS232_USB", "Log save option", 0));
        QTreeWidgetItem *___qtreewidgetitem28 = ___qtreewidgetitem22->child(5);
        ___qtreewidgetitem28->setText(1, QApplication::translate("LoggerConfRS232_USB", "ISB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem28->setText(0, QApplication::translate("LoggerConfRS232_USB", "Minimum battery value", 0));
        QTreeWidgetItem *___qtreewidgetitem29 = InfoTreeWidget->topLevelItem(6);
        ___qtreewidgetitem29->setText(0, QApplication::translate("LoggerConfRS232_USB", "RS232 operating mode", 0));
        QTreeWidgetItem *___qtreewidgetitem30 = ___qtreewidgetitem29->child(0);
        ___qtreewidgetitem30->setText(1, QApplication::translate("LoggerConfRS232_USB", "RII UINT32 10 10000 RW", 0));
        ___qtreewidgetitem30->setText(0, QApplication::translate("LoggerConfRS232_USB", "Indication blinking interval", 0));
        QTreeWidgetItem *___qtreewidgetitem31 = ___qtreewidgetitem29->child(1);
        ___qtreewidgetitem31->setText(1, QApplication::translate("LoggerConfRS232_USB", "RBR ENUM 9600 14400\n"
"19200 28800 38400 56000 57600 115200 256000 RW", 0));
        ___qtreewidgetitem31->setText(0, QApplication::translate("LoggerConfRS232_USB", "Baudrate", 0));
        QTreeWidgetItem *___qtreewidgetitem32 = ___qtreewidgetitem29->child(2);
        ___qtreewidgetitem32->setText(1, QApplication::translate("LoggerConfRS232_USB", "RPA ENUM NONE EVEN OFF RW", 0));
        ___qtreewidgetitem32->setText(0, QApplication::translate("LoggerConfRS232_USB", "Parity", 0));
        QTreeWidgetItem *___qtreewidgetitem33 = ___qtreewidgetitem29->child(3);
        ___qtreewidgetitem33->setText(1, QApplication::translate("LoggerConfRS232_USB", "RDB ENUM 8 9 RW", 0));
        ___qtreewidgetitem33->setText(0, QApplication::translate("LoggerConfRS232_USB", "Data bits", 0));
        QTreeWidgetItem *___qtreewidgetitem34 = ___qtreewidgetitem29->child(4);
        ___qtreewidgetitem34->setText(1, QApplication::translate("LoggerConfRS232_USB", "RSB ENUM 1 2 RW", 0));
        ___qtreewidgetitem34->setText(0, QApplication::translate("LoggerConfRS232_USB", "Stop bits", 0));
        QTreeWidgetItem *___qtreewidgetitem35 = ___qtreewidgetitem29->child(5);
        ___qtreewidgetitem35->setText(1, QApplication::translate("LoggerConfRS232_USB", "RTE INT32 1 10000000 RW", 0));
        ___qtreewidgetitem35->setText(0, QApplication::translate("LoggerConfRS232_USB", "Terminator", 0));
        QTreeWidgetItem *___qtreewidgetitem36 = ___qtreewidgetitem29->child(6);
        ___qtreewidgetitem36->setText(1, QApplication::translate("LoggerConfRS232_USB", "RLPF INT32 1 10000000 RW", 0));
        ___qtreewidgetitem36->setText(0, QApplication::translate("LoggerConfRS232_USB", "Lines per file", 0));
        QTreeWidgetItem *___qtreewidgetitem37 = ___qtreewidgetitem29->child(7);
        ___qtreewidgetitem37->setText(1, QApplication::translate("LoggerConfRS232_USB", "RLB DOUBLE32 1000 3 6 RW ", 0));
        ___qtreewidgetitem37->setText(0, QApplication::translate("LoggerConfRS232_USB", "Minimum battery value", 0));
        InfoTreeWidget->setSortingEnabled(__sortingEnabled);

        moduleOptions->setTabText(moduleOptions->indexOf(tab_2), QApplication::translate("LoggerConfRS232_USB", "Configuration", 0));
        label->setText(QString());
        label_2->setText(QApplication::translate("LoggerConfRS232_USB", "Folder browser", 0));
        label_3->setText(QString());
        label_4->setText(QApplication::translate("LoggerConfRS232_USB", "Log browser", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_3), QApplication::translate("LoggerConfRS232_USB", "Data browser", 0));
        groupBox_3->setTitle(QApplication::translate("LoggerConfRS232_USB", "Select", 0));
        Realtime_START->setText(QApplication::translate("LoggerConfRS232_USB", "START", 0));
        Realtime_STOP->setText(QApplication::translate("LoggerConfRS232_USB", "STOP", 0));
        Sensor_Checkbox->setText(QApplication::translate("LoggerConfRS232_USB", "Sensor", 0));
        Battery_Checkbox->setText(QApplication::translate("LoggerConfRS232_USB", "Battery", 0));
        label_5->setText(QApplication::translate("LoggerConfRS232_USB", "Export:", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab), QApplication::translate("LoggerConfRS232_USB", "Realtime monitoring", 0));
        btnReadSens->setText(QApplication::translate("LoggerConfRS232_USB", "READ SENS", 0));
        btnReadBatt->setText(QApplication::translate("LoggerConfRS232_USB", "READ BATT", 0));
        btnSDDetect->setText(QApplication::translate("LoggerConfRS232_USB", "SD DETECT", 0));
        btnExtLedOn->setText(QApplication::translate("LoggerConfRS232_USB", "LCD LED ON", 0));
        btnChargerStat->setText(QApplication::translate("LoggerConfRS232_USB", "CHARGER STAT", 0));
        btnOpaPowOn->setText(QApplication::translate("LoggerConfRS232_USB", "OPA POW ON", 0));
        btnIntLedOff->setText(QApplication::translate("LoggerConfRS232_USB", "INT LED OFF", 0));
        btnExtLedOff->setText(QApplication::translate("LoggerConfRS232_USB", "LCD LED OFF", 0));
        BtnMemPowOff->setText(QApplication::translate("LoggerConfRS232_USB", "MEM POW OFF", 0));
        btnSensPowOff->setText(QApplication::translate("LoggerConfRS232_USB", "SENS POW OFF", 0));
        btnOpaPowOff->setText(QApplication::translate("LoggerConfRS232_USB", "OPA POW OFF", 0));
        btnIntLedOn->setText(QApplication::translate("LoggerConfRS232_USB", "INT LED ON", 0));
        btnChargerOn->setText(QApplication::translate("LoggerConfRS232_USB", "CHARGER OFF", 0));
        btnSensPowOn->setText(QApplication::translate("LoggerConfRS232_USB", "SENS POW ON", 0));
        btnMemPowOn->setText(QApplication::translate("LoggerConfRS232_USB", "MEM POW ON", 0));
        btnChargerOff->setText(QApplication::translate("LoggerConfRS232_USB", "CHARGER ON", 0));
        toolBox->setItemText(toolBox->indexOf(page), QApplication::translate("LoggerConfRS232_USB", "General", 0));
        btnModemPowOn->setText(QApplication::translate("LoggerConfRS232_USB", "POWER ON", 0));
        BtnModemPowOff->setText(QApplication::translate("LoggerConfRS232_USB", "POWER OFF", 0));
        btnSimStatus->setText(QApplication::translate("LoggerConfRS232_USB", "SIM STATUS", 0));
        btnSimPIN->setText(QApplication::translate("LoggerConfRS232_USB", "SET SIM PIN", 0));
        btnModemLogin->setText(QApplication::translate("LoggerConfRS232_USB", "LOGIN", 0));
        btnModemLogout->setText(QApplication::translate("LoggerConfRS232_USB", "LOGOUT", 0));
        btnModemSend->setText(QApplication::translate("LoggerConfRS232_USB", "SEND COMMAND", 0));
        toolBox->setItemText(toolBox->indexOf(page_5), QApplication::translate("LoggerConfRS232_USB", "Modem", 0));
        btnSendCommand->setText(QApplication::translate("LoggerConfRS232_USB", "SEND COMMAND", 0));
        btnCustomTimeSet->setText(QApplication::translate("LoggerConfRS232_USB", "SET CUSTOM TIME", 0));
        toolBox->setItemText(toolBox->indexOf(page_4), QApplication::translate("LoggerConfRS232_USB", "Other", 0));
        autoclearCheckBox->setText(QApplication::translate("LoggerConfRS232_USB", "Auto clear", 0));
        clearTerminalBrowser->setText(QApplication::translate("LoggerConfRS232_USB", "Clear screen", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_4), QApplication::translate("LoggerConfRS232_USB", "Testing", 0));
        pushButton->setText(QApplication::translate("LoggerConfRS232_USB", "SEND", 0));
        moduleOptions->setTabText(moduleOptions->indexOf(tab_5), QApplication::translate("LoggerConfRS232_USB", "RS232 data", 0));
    } // retranslateUi

};

namespace Ui {
    class LoggerConfRS232_USB: public Ui_LoggerConfRS232_USB {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGGERCONFRS232_USB_H
