#ifndef DATAVISUALIZER_H
#define DATAVISUALIZER_H

#include <QDialog>
#include <QMessageBox>
#include "plottingwidget.h"
#include "dialogwait.h"

namespace Ui {
class DataVisualizer;
}

class DataVisualizer : public QDialog
{
    Q_OBJECT

public:
    explicit DataVisualizer(QWidget *parent = 0);
    ~DataVisualizer();


    void plotData(QString Path,QString Name);

private:

    Ui::DataVisualizer *ui;

    QVector<QColor>          PlotColors;

};

#endif // DATAVISUALIZER_H
