/********************************************************************************
** Form generated from reading UI file 'deployedloggers.ui'
**
** Created by: Qt User Interface Compiler version 5.6.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DEPLOYEDLOGGERS_H
#define UI_DEPLOYEDLOGGERS_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QListWidget>
#include <QtWidgets/QTextBrowser>
#include <QtWidgets/QTreeWidget>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_DeployedLoggers
{
public:
    QVBoxLayout *verticalLayout_3;
    QTreeWidget *loggerTreeWidget;
    QHBoxLayout *horizontalLayout;
    QVBoxLayout *verticalLayout_2;
    QLabel *label_2;
    QTextBrowser *textBrowser;
    QVBoxLayout *verticalLayout;
    QLabel *label;
    QListWidget *listWidget;

    void setupUi(QWidget *DeployedLoggers)
    {
        if (DeployedLoggers->objectName().isEmpty())
            DeployedLoggers->setObjectName(QStringLiteral("DeployedLoggers"));
        DeployedLoggers->resize(710, 566);
        verticalLayout_3 = new QVBoxLayout(DeployedLoggers);
        verticalLayout_3->setObjectName(QStringLiteral("verticalLayout_3"));
        loggerTreeWidget = new QTreeWidget(DeployedLoggers);
        loggerTreeWidget->setObjectName(QStringLiteral("loggerTreeWidget"));
        QFont font;
        font.setPointSize(12);
        loggerTreeWidget->setFont(font);
        loggerTreeWidget->setContextMenuPolicy(Qt::CustomContextMenu);
        loggerTreeWidget->setAlternatingRowColors(true);

        verticalLayout_3->addWidget(loggerTreeWidget);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        verticalLayout_2 = new QVBoxLayout();
        verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
        label_2 = new QLabel(DeployedLoggers);
        label_2->setObjectName(QStringLiteral("label_2"));

        verticalLayout_2->addWidget(label_2);

        textBrowser = new QTextBrowser(DeployedLoggers);
        textBrowser->setObjectName(QStringLiteral("textBrowser"));

        verticalLayout_2->addWidget(textBrowser);


        horizontalLayout->addLayout(verticalLayout_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        label = new QLabel(DeployedLoggers);
        label->setObjectName(QStringLiteral("label"));

        verticalLayout->addWidget(label);

        listWidget = new QListWidget(DeployedLoggers);
        listWidget->setObjectName(QStringLiteral("listWidget"));
        listWidget->setViewMode(QListView::IconMode);

        verticalLayout->addWidget(listWidget);


        horizontalLayout->addLayout(verticalLayout);


        verticalLayout_3->addLayout(horizontalLayout);


        retranslateUi(DeployedLoggers);

        QMetaObject::connectSlotsByName(DeployedLoggers);
    } // setupUi

    void retranslateUi(QWidget *DeployedLoggers)
    {
        DeployedLoggers->setWindowTitle(QApplication::translate("DeployedLoggers", "Form", 0));
        QTreeWidgetItem *___qtreewidgetitem = loggerTreeWidget->headerItem();
        ___qtreewidgetitem->setText(5, QApplication::translate("DeployedLoggers", "File path", 0));
        ___qtreewidgetitem->setText(4, QApplication::translate("DeployedLoggers", "Running time", 0));
        ___qtreewidgetitem->setText(3, QApplication::translate("DeployedLoggers", "Deploy time", 0));
        ___qtreewidgetitem->setText(2, QApplication::translate("DeployedLoggers", "Logger ID", 0));
        ___qtreewidgetitem->setText(1, QApplication::translate("DeployedLoggers", "Longitude", 0));
        ___qtreewidgetitem->setText(0, QApplication::translate("DeployedLoggers", "Latitude", 0));
        label_2->setText(QApplication::translate("DeployedLoggers", "Information", 0));
        label->setText(QApplication::translate("DeployedLoggers", "Images", 0));
    } // retranslateUi

};

namespace Ui {
    class DeployedLoggers: public Ui_DeployedLoggers {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DEPLOYEDLOGGERS_H
